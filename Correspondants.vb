﻿
Public Class Correspondants

    '=============================================================================================================================================
    ' Procédure lors de la fermeture de la feuille par la croix
    Private Sub Correspondants_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        '==============================================
        ' Blocage de la procédure de fermeture automatique
        e.Cancel = True
        '==============================================
        ' Demande de fermeture par Fondu
        Fermeture()
    End Sub

    ' ==============================================================================================================================================
    ' Procédure au chargement de la page
    Private Sub Correspondants_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        '==============================================
        ' Demande d'ouverture par Fondu
        Me.Opacity = 0
        Timer1.Enabled = True
    End Sub


    '=============================================================================================================================================
    ' PROCÉDURES MESSAGERIE
    '=============================================================================================================================================

    Private Sub TextBox20_MouseHover(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox20.MouseHover
        TextBox20.Font = New Font(TextBox20.Font, FontStyle.Underline)
    End Sub

    Private Sub TextBox20_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox20.MouseLeave
        TextBox20.Font = New Font(TextBox20.Font, FontStyle.Regular)
    End Sub

    Private Sub TextBox20_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox20.MouseClick
        EnvoiMessage()
    End Sub


    Private Sub TextBox2_MouseHover(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox2.MouseHover
        TextBox2.Font = New Font(TextBox2.Font, FontStyle.Underline)
    End Sub

    Private Sub TextBox2_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox2.MouseLeave
        TextBox2.Font = New Font(TextBox2.Font, FontStyle.Regular)
    End Sub

    Private Sub TextBox2_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox2.MouseClick
        EnvoiMessage()
    End Sub

    Private Sub EnvoiMessage()

        Dim Destinataire As String
        Dim Objet As String
        Dim Corps As String
        Dim Copy As String
        Dim Message As String

        ' ============================================
        On Error GoTo TraiteErreurOrganigramme
        ' ============================================
        ' Récupération des adresses de messagerie pour l'Action
        Destinataire = ""

        If TextBox20.Text = "" And TextBox2.Text = "" Then Exit Sub

        If TextBox20.Text <> "" Then
            Destinataire = TextBox20.Text
            If TextBox2.Text <> "" Then
                Destinataire = Destinataire & ","
            End If
        End If

        If TextBox2.Text <> "" Then
            Destinataire = Destinataire & TextBox2.Text
        End If

        ' ============================================
        ' Création de l'objet
        Objet = "QUADRISPEL."
        ' ============================================
        ' Corps du message
        Corps = "Bonjour%2C%0A%0A%0A%0A"

        ' ============================================
        Message = "C:\Program Files\Courrielleur Mélanie2\thunderbird"
        Message = Message & " -compose " & "to='" & Destinataire & "'"
        Message = Message & "," & "subject=" & Objet & ","
        Message = Message & "body=" & Corps
        '''''''''''''''''''''''''''''''''''''''''''''''       '      Message = Message & "," & "attachment=file:///" & fichierjoint
        
        Call Shell(Message, vbNormalFocus)

        Exit Sub

        ' ============================================
TraiteErreurOrganigramme:
        If Err.Number = 0 Then
            '          MsgBox("Transaction Messagerie correcte", MsgBoxStyle.Information, " QUADRISPEL - Information")
            Exit Sub
        End If
        '        MsgBox(Err.Number & "    " & Err.Description, MsgBoxStyle.Exclamation, " QUADRISPEL - Erreur Message")
        ' ============================================================= 
        ' Remise à zéro du presse papier
        Clipboard.Clear()
        Clipboard.SetText(Destinataire)
        MsgBox("Difficultés d'activation de la Messagerie !" & vbCrLf & vbCrLf & "Adresses Messagerie dans le Presse Papier !", MsgBoxStyle.Information, " QUADRISPEL - Information Messagerie")

    End Sub

    '=============================================================================================================================================
    ' PROCÉDURES DIVERSES
    '=============================================================================================================================================
    ' Traitement du repositionnement automatique de la feuille quand on l'a déplacée
    Private Sub Correspondants_MouseEnter(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.MouseEnter
        Me.CenterToScreen()
    End Sub

    '=============================================================================================================================================
    ' Traitement de réduction des fenêtres
    Private Sub Correspondants_Move(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Move
        ' ============================================
        ' Passage de la feuille principale transparente quand on réduit cette feuille
        If Me.WindowState = 1 Then
            Form1.Opacity = 0
        End If
        ' ============================================
        ' Passage de la feuille principale opaque quand on rétablit cette feuille
        If Me.WindowState = 0 Then
            Form1.Opacity = 1
        End If
    End Sub

    ' ==============================================================================================================================================
    ' Bouton de fermeture de l'application
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button2.Select()
        ' ==================================
        ' Demande de fermeture par Fondu
        Fermeture()
    End Sub

    '=============================================================================================================================================
    ' PROCÉDURE D'OUVERTURE ET DE FERMETURE
    '=============================================================================================================================================
    ' Procédure d'ouverture par Fondu
    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        Me.Opacity = Me.Opacity + 0.06
        If Me.Opacity >= 0.95 Then
            Timer1.Enabled = False
            Me.Opacity = 1
        End If
    End Sub

    Private Sub Fermeture()
        ' ============================================
        ' Essai pour éviter qu'une application ne passe devant la feuille ayant demandé celle-ci
        Form1.Activate()
        ' ============================================
        Me.Visible = False
    End Sub

    '=============================================================================================================================================
    ' PROCÉDURE POUR ÉVITER LE CURSEUR CLIGNOTANT DANS LES CHAMPS
    '=============================================================================================================================================

    Private Sub TextBox1_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox1.GotFocus
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button2.Select()
    End Sub

    Private Sub TextBox2_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox2.GotFocus
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button2.Select()
    End Sub

    Private Sub TextBox3_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox3.GotFocus
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button2.Select()
    End Sub

    Private Sub TextBox43_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox4.GotFocus
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button2.Select()
    End Sub

    Private Sub TextBox5_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox5.GotFocus
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button2.Select()
    End Sub

    Private Sub TextBox6_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox6.GotFocus
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button2.Select()
    End Sub

    Private Sub TextBox7_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox7.GotFocus
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button2.Select()
    End Sub

    Private Sub TextBox15_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox15.GotFocus
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button2.Select()
    End Sub

    Private Sub TextBox16_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox16.GotFocus
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button2.Select()
    End Sub

    Private Sub TextBox17_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox17.GotFocus
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button2.Select()
    End Sub

    Private Sub TextBox18_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox18.GotFocus
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button2.Select()
    End Sub

    Private Sub TextBox19_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox19.GotFocus
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button2.Select()
    End Sub

    Private Sub TextBox20_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox20.GotFocus
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button2.Select()
    End Sub



End Class