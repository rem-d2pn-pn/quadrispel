﻿Public Class Familles

    '=============================================================================================================================================
    ' Procédure lors de la fermeture de la feuille par la croix
    Private Sub Familles_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        '==============================================
        ' Blocage de la procédure de fermeture automatique
        e.Cancel = True
        '==============================================
        ' Demande de fermeture par Fondu
        Fermeture()
    End Sub

    ' ==============================================================================================================================================
    ' Procédure au chargement de la page
    Private Sub Familles_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        '==============================================
        ' Blocage des boutons à l'ouverture
        Button4.Enabled = False
        Button5.Enabled = False
        Button6.Enabled = False

        ' ==================================
        ' Désélection de toute la liste 2
        For i = 0 To ListBox2.Items.Count - 1
            ListBox2.SetSelected(i, False)
        Next


        '==============================================
        ' Demande d'ouverture par Fondu
        Me.Opacity = 0
        Timer1.Enabled = True
    End Sub


    '=============================================================================================================================================
    ' PROCÉDURES BOUTONS
    '=============================================================================================================================================
    ' Bouton Transférer
    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Dim i As Integer
        Dim j As Integer
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button2.Select()
        ' ==================================
        ' Vérification que des sélections n'ont pas déjà été transférées
        For i = 0 To ListBox1.Items.Count - 1
            If ListBox1.GetSelected(i) = True Then
                ' ==================================
                ' Vérification si le nom pointé a déjà été transféré
                For j = 0 To ListBox2.Items.Count - 1
                    If ListBox1.Items.Item(i) = ListBox2.Items(j) Then
                        ' ==================================
                        ' Message indiquant une erreur de transfert
                        MsgBox("La famille " & ListBox2.Items(j) & " a déjà été transférée ! ", MsgBoxStyle.Exclamation, " QUADRISPEL - Erreur de transfert")
                        Exit Sub
                    End If
                Next
            End If
        Next
        ' ==================================
        ' Transfert dans la liste 2 des noms de Familles sélectionnés dans la liste 1
        For i = 0 To ListBox1.Items.Count - 1
            ' ==================================
            ' Vérification si le nom pointé a été sélectionné
            If ListBox1.GetSelected(i) = True Then
                ListBox2.Items.Add(ListBox1.Items.Item(i))
            End If
        Next
        ' ==================================
        ' Désélection de toute la liste 1
        For i = 0 To ListBox1.Items.Count - 1
            ListBox1.SetSelected(i, False)
        Next
        ' ==================================
        ' Désélection de toute la liste 2
        For i = 0 To ListBox2.Items.Count - 1
            ListBox2.SetSelected(i, False)
        Next
        ' ==================================
        ' Desactivation des boutons Monter, Descendre et Supprimer
        Button4.Enabled = False
        Button5.Enabled = False
        Button6.Enabled = False
        ' ==================================
        ' Initialisation du bouton Toutes
        Button7.Text = "Toutes"
        ' ==================================
        ' Validation sauvegarde à la fermeture
        Variables.SauveFichierMasque = True
    End Sub

    '=============================================================================================================================================
    ' Bouton Supprimer
    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        ' ==================================
        ' Sortie si l'index = -1
        If ListBox2.SelectedIndex = -1 Then Exit Sub

        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button2.Select()
        ' ==================================
        ' Invalidation du bouton supprimer
        Button6.Enabled = False

        ' ==================================
        ' Suppression du nom de Familles sélectionné
        ListBox2.Items.RemoveAt(ListBox2.SelectedIndex)
        ' ==================================
        ' Desactivation des boutons Monter, Descendre et Supprimer
        Button4.Enabled = False
        Button5.Enabled = False
        Button6.Enabled = False

        ' ==================================
        ' Validation sauvegarde à la fermeture
        Variables.SauveFichierMasque = True

    End Sub

    '=============================================================================================================================================
    ' Bouton Monter
    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Dim i As Integer
        ' ==================================
        ' Sortie si l'index = -1
        If ListBox2.SelectedIndex = -1 Then Exit Sub

        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button2.Select()
        ' ==================================
        ' Récupération de l'index sélectionné
        i = ListBox2.SelectedIndex
        ' ==================================
        ' Sortie si pas de sélection ou sélection du premier nom
        If i = -1 Or i = 0 Then Exit Sub
        ' ==================================
        ' Insertion du nom sélectionné
        ListBox2.Items.Insert(i - 1, ListBox2.SelectedItem)
        ' ==================================
        ' Effacement de l'index sélectionné
        ListBox2.Items.RemoveAt(i + 1)
        ' ==================================
        ' Sélection du nouvel index sélectionné
        i = i - 1
        ListBox2.SetSelected(i, True)
        ' ==================================
        ' Validation des boutons de sélections des Familles
        Button5.Enabled = True
        If i < 1 Then
            Button4.Enabled = False
        End If
        ' ==================================
        ' Validation sauvegarde à la fermeture
        Variables.SauveFichierMasque = True
    End Sub

    '=============================================================================================================================================
    ' Bouton Descendre
    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Dim i As Integer
        ' ==================================
        ' Sortie si l'index = -1
        If ListBox2.SelectedIndex = -1 Then Exit Sub

        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button2.Select()
        ' ==================================
        ' Récupération de l'index sélectionné
        i = ListBox2.SelectedIndex
        ' ==================================
        ' Sortie si pas de sélection ou sélection du dernier nom
        If i = -1 Or i = ListBox2.Items.Count - 1 Then Exit Sub
        ' ==================================
        ' Insertion du nom sélectionné
        ListBox2.Items.Insert(i + 2, ListBox2.SelectedItem)
        ' ==================================
        ' Effacement de l'index sélectionné
        ListBox2.Items.RemoveAt(i)
        ' ==================================
        ' Sélection du nouvel index sélectionné
        i = i + 1
        ListBox2.SetSelected(i, True)
        ' ==================================
        ' Validation des boutons de sélections des Familles
        Button4.Enabled = True
        If i >= ListBox2.Items.Count - 1 Then
            Button5.Enabled = False
        End If
        ' ==================================
        ' Validation sauvegarde à la fermeture
        Variables.SauveFichierMasque = True
    End Sub

    '=============================================================================================================================================
    ' Bouton Toutes / Aucune de la Liste 1
    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button2.Select()
        ' ==================================
        ' Vérification du texte du bouton
        If Button7.Text = "Toutes" Then
            ' ==================================
            ' MAJ du texte du bouton
            Button7.Text = "Aucune"
            ' ==================================
            ' Sélection de toute la liste 1
            For i = 0 To ListBox1.Items.Count - 1
                ListBox1.SetSelected(i, True)
            Next
            Exit Sub
        End If
        ' ==================================
        ' MAJ du texte du bouton
        Button7.Text = "Toutes"
        ' ==================================
        ' Désélection de toute la liste 1
        For i = 0 To ListBox1.Items.Count - 1
            ListBox1.SetSelected(i, False)
        Next
    End Sub

    '=============================================================================================================================================
    ' Activation des boutons Monter, Descendre et Supprimer en fonction de la sélection dans la Liste
    Private Sub ListBox2_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListBox2.SelectedIndexChanged
        If ListBox2.SelectedItem = "" Then Exit Sub
        ' ============================================
        ' Activation du bouton Supprimer
        Button6.Enabled = True
        ' ============================================
        ' Activation des boutons Monter et Descendre
        If ListBox2.Items.Count > 1 Then
            Button4.Enabled = True
            Button5.Enabled = True
        End If
        ' ============================================
        ' Desactivation du bouton Monter
        If ListBox2.SelectedIndex = 0 Then
            Button4.Enabled = False
        End If
        ' ============================================
        ' Desactivation du bouton Descendre
        If ListBox2.SelectedIndex >= ListBox2.Items.Count - 1 Then
            Button5.Enabled = False
        End If
    End Sub


    '=============================================================================================================================================
    ' PROCÉDURES DIVERSES
    '=============================================================================================================================================
    ' Traitement du repositionnement automatique de la feuille quand on l'a déplacée
    Private Sub Familles_MouseEnter(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.MouseEnter
        Me.CenterToScreen()
    End Sub

    '=============================================================================================================================================
    ' Traitement de réduction des fenêtres
    Private Sub Familles_Move(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Move
        ' ============================================
        ' Passage de la feuille principale transparente quand on réduit cette feuille
        If Me.WindowState = 1 Then
            Form1.Opacity = 0
        End If
        ' ============================================
        ' Passage de la feuille principale opaque quand on rétablit cette feuille
        If Me.WindowState = 0 Then
            Form1.Opacity = 1
        End If
    End Sub

    ' ==============================================================================================================================================
    ' Bouton de fermeture de l'application
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button2.Select()
        ' ==================================
        ' Demande de fermeture par Fondu
        Fermeture()
    End Sub


    '=============================================================================================================================================
    ' PROCÉDURE D'OUVERTURE ET DE FERMETURE
    '=============================================================================================================================================
    ' Procédure d'ouverture par Fondu
    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        Me.Opacity = Me.Opacity + 0.06
        If Me.Opacity >= 0.95 Then
            Timer1.Enabled = False
            Me.Opacity = 1
        End If
    End Sub

    Private Sub Fermeture()
        ' ============================================
        ' Essai pour éviter qu'une application ne passe devant la feuille ayant demandé celle-ci
        Form1.Activate()
        ' ============================================
        Me.Visible = False
    End Sub


End Class