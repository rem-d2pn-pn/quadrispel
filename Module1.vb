﻿Module ListeVariables

    Public Class Variables

        ' ==================================
        ' Variables Ouverture Fermeture
        ' ==================================

        Public Shared Largeur As Double
        Public Shared Hauteur As Double
        Public Shared PasLargeur As Double
        Public Shared PasHauteur As Double
        Public Shared PasRouge As Integer
        Public Shared PasVert As Integer
        Public Shared PasBleu As Integer
        Public Shared CoulRouge As Integer
        Public Shared CoulVert As Integer
        Public Shared CoulBleu As Integer
        Public Shared Largeur2 As Double
        Public Shared Hauteur2 As Double
        Public Shared PasLargeur2 As Double
        Public Shared PasHauteur2 As Double
        Public Shared PasRouge2 As Integer
        Public Shared PasVert2 As Integer
        Public Shared PasBleu2 As Integer
        Public Shared CoulRouge2 As Integer
        Public Shared CoulVert2 As Integer
        Public Shared CoulBleu2 As Integer
        Public Shared LargeurTri As Integer
        Public Shared HauteurTri As Integer
        Public Shared PasLargeurTri As Integer
        Public Shared PasHauteurTri As Integer
        Public Shared FormHauteur As Integer
        Public Shared FormLargeur As Integer

        ' ==================================
        ' Variables Diverses
        ' ==================================

        ' Variable pour le chemin des fichiers à charger
        Public Shared Repertoire As String

        ' Variables pour les mouvements des ascenseurs
        Public Shared NumeroList As Integer
        Public Shared NumeroListSuivant As Integer

        ' Variable pour sauvegarde fichier à la fermeture application
        Public Shared SauveFichierRésultat As Boolean
        Public Shared SauveFichierMasque As Boolean
        Public Shared TypeFichierChargé As String

        ' Variable ascenseur des boutons.
        Public Shared AffichageBoutons As Boolean

        ' Variable ancienne couleur ligne DataGridView
        Public Shared AncienneLigne As Integer

        ' Variable pour la validation de la sauvegarde du fichier Quadrige²
        Public Shared ArretSauve As Boolean
        Public Shared PrésenceParamètre As Boolean

        ' Variable de déblocage de la feuille Propriétés et Paramètres
        Public Shared FeuiProResLibre As Boolean

        ' Variable Sauvegarde Fichier SPEL
        Public Shared FichierSPEL As Boolean

        Public Shared AdresseCourrielleur As String

        ' Variable pour éviter deux fois l'affichage du message d'information de sauvegarde du fichier Quadrige²
        Public Shared UneFois As Boolean

        ' ==================================
        ' Variables Tableaux
        ' ==================================

        Public Shared AiguilleurTableau As Integer

        Public Shared TabRéfInfo(5000, 6) As String
        ' Les colonnes correspondent à celles du fichier Référentiel Paramètres QuadriSPEL

        Public Shared TabRéfPara(500, 25) As String
        ' Col 0  : Famille du paramètre
        ' Col 1  : Nom du paramètre
        ' Col 2  : Libellé du paramètre
        ' Col 3  : Code Sandre du paramètre
        ' Col 4  : Dénomination Analyseur
        ' Col 5  : Libellé Analyseur
        ' Col 6  : Dénomination méthode
        ' Col 7  : Engin d'analyse
        ' Col 8  : Dénomination fraction
        ' Col 9  : Libellé support
        ' Col 10 : Dénomination engin de prélèvement
        ' Col 11 : Précision
        ' Col 12 : Type Précision
        ' Col 13 : Niveau de saisie
        ' Col 14 : Seuil de détection
        ' Col 15 : Libellé Résultat qualitatif
        ' Col 16 : Numéro individu
        ' Col 17 : Résultat Numérique
        ' Col 18 : Unité
        ' Col 19 : Commentaire mesure
        ' Col 20 : Remarque
        ' Col 21 : Index de tri
        ' Modif Suite V 1.6 Ifremer
        ' Col 22 : Réserve
        ' Col 23 : Code Sandre Résultat Qualitatif
        ' Col 24 : Code Sandre Groupe Taxon Résultat
        ' Col 25 : Code Sandre Taxon Résultat

        Public Shared AdresseTabPara As Integer

        ' ==================================
        ' Variables Familles
        ' ==================================

        Public Shared PointeurFamille As Integer
        Public Shared PointeurParamètre As Integer
        Public Shared NbrIndex As Integer

        ' ==================================
        ' Variables Fichiers
        ' ==================================
        Public Shared NomSelect As String

        ' ==================================
        ' Variables Recopie Informations
        ' ==================================

        Public Shared Info1 As String
        Public Shared Info2 As String
        Public Shared Info3 As String
        Public Shared Info4 As String
        Public Shared Info5 As String
        Public Shared Info6 As String
        Public Shared Info7 As String
        Public Shared Info8 As String
        Public Shared Info9 As String
        Public Shared Info10 As String
        Public Shared Info11 As String
        Public Shared Info12 As String
        Public Shared Info13 As String
        Public Shared Info14 As String
        Public Shared Info15 As String
        Public Shared Info16 As String
        Public Shared Info17 As String

        Public Shared Info18 As String
        Public Shared Info19 As String
        Public Shared Info20 As String


        Public Shared ValeurPrécédente As String

        ' ==================================
        ' Variables Informations Règles Structureles
        ' ==================================

        Public Shared Feuille As String
        Public Shared Champ As String

        Public Shared TabRèglesInv(40) As String

    End Class

End Module
