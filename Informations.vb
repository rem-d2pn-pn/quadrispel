﻿Public Class Informations

    '=============================================================================================================================================
    ' Procédure lors de la fermeture de la feuille par la croix
    Private Sub Informations_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        '==============================================
        ' Blocage de la procédure de fermeture automatique
        e.Cancel = True
        '==============================================
        ' Demande de fermeture par Fondu
        Fermeture()
    End Sub

    ' ==============================================================================================================================================
    ' Procédure au chargement de la page
    Private Sub Informations_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button2.Select()
        '==============================================
        ' Demande d'ouverture par Fondu
        Me.Opacity = 0
        Timer1.Enabled = True
    End Sub

    '=============================================================================================================================================
    ' PROCÉDURES DIVERSES
    '=============================================================================================================================================
    ' Traitement du repositionnement automatique de la feuille quand on l'a déplacée
    Private Sub Informations_MouseEnter(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.MouseEnter
        Me.CenterToScreen()
    End Sub

    Private Sub TextBox1_MouseEnter(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox1.MouseEnter
        Me.CenterToScreen()
    End Sub

    ' ==============================================================================================================================================
    ' Bouton de fermeture de l'application
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button2.Select()
        ' ==================================
        ' Demande de fermeture par Fondu
        Fermeture()
    End Sub

    '=============================================================================================================================================
    ' PROCÉDURE D'OUVERTURE ET DE FERMETURE
    '=============================================================================================================================================
    ' Procédure d'ouverture par Fondu
    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        Me.Opacity = Me.Opacity + 0.06
        If Me.Opacity >= 0.95 Then
            Timer1.Enabled = False
            Me.Opacity = 1
        End If
    End Sub

    Private Sub Fermeture()
        ' ============================================
        Me.Visible = False
    End Sub



End Class