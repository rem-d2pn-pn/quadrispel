﻿' Importation des systèmes utilisés

Imports System
Imports System.IO
Imports System.IO.Directory
Imports System.Text

Public Class Charge_Masque

    '=============================================================================================================================================
    ' Procédure lors de la fermeture de la feuille par la croix
    Private Sub Charge_Masque_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        ' Blocage de la procédure de fermeture automatique
        e.Cancel = True
        '==============================================
        Fermeture()
    End Sub

    ' ==============================================================================================================================================
    ' Procédure au chargement de la feuille
    Private Sub Charge_Masque_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        '==============================================
        ' Changement visuel de sélection de bouton
        Button3.Select()
        ' ==================================
        ' Recherche des fichiers de Base
        ListBox1.Items.Clear()
        For Each FichiersSelect As String In My.Computer.FileSystem.GetFiles(My.Computer.FileSystem.CurrentDirectory & "\Masques\", FileIO.SearchOption.SearchTopLevelOnly, "*.msq")
            ' ==================================
            ' Mise en forme des noms des fichiers trouvés
            FichiersSelect = "   " & Strings.Right(FichiersSelect, Len(FichiersSelect) - Strings.InStrRev(FichiersSelect, "\"))
            ' ==================================
            ' Suppression de l'extension de fichier
            FichiersSelect = Strings.Left(FichiersSelect, Len(FichiersSelect) - 4)
            ' ==================================
            ' Rangement des noms de fichiers dans la liste
            ListBox1.Items.Add(FichiersSelect)
        Next
        '==============================================
        ' Demande d'ouverture par Fondu
        Me.Opacity = 0
        Timer1.Enabled = True
    End Sub

    ' ==============================================================================================================================================
    ' Procédure de fermeture de la feuille
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        '==============================================
        ' Changement visuel de sélection de bouton
        Button3.Select()
        '==============================================
        Fermeture()
    End Sub

    ' ==============================================================================================================================================
    ' Procédure de sélection du nom du fichier de sélection dans la liste
    Private Sub ListBox1_MouseDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles ListBox1.MouseDoubleClick
        '==============================================
        ' Sortie si la sélection est vide
        If ListBox1.SelectedItem = "" Then Exit Sub
        '==============================================
        ' Récupération du nom du fichier sélectionné
        Variables.NomSelect = "\Masques\" & Strings.LTrim(ListBox1.SelectedItem) & ".msq"
        '==============================================
        ' Changement visuel de sélection de bouton
        Button3.Select()
        ' ==================================
        ' Initialisation des variables
        Dim NomFichier As StreamReader
        Dim i As Integer
        Dim j As Integer
        Dim k As Integer
        Dim TabMin As Integer
        Dim TabMax As Integer
        Dim LectureLigne As String
        Dim CodeSandre As String = ""
        Dim IndexTabInfo As Integer
        ' ==================================
        ' Traitement des erreurs
        On Error GoTo TraitErreur2
        ' ====================================================================================
        ' Vérification si la sélection d'un nom de fichier a été réalisée
        If Variables.NomSelect = "" Then
            MsgBox("Vous n'avez pas réalisé de sélection de fichier !", MsgBoxStyle.Exclamation, " Absence de sélection")
            Exit Sub
        End If
        ' ==================================
        ' Ouverture du fichier de chargement
        NomFichier = New StreamReader(Variables.Repertoire & Variables.NomSelect, System.Text.Encoding.Default)
        ' ==================================
        ' Lecture du fichier 
        Do Until NomFichier.EndOfStream = True
            ' ==================================
            ' Chargement des informations Passage
            Passage.TextBox10.Text = NomFichier.ReadLine
            Passage.TextBox24.Text = NomFichier.ReadLine
            Passage.TextBox3.Text = NomFichier.ReadLine
            Passage.TextBox2.Text = NomFichier.ReadLine
            Passage.TextBox1.Text = NomFichier.ReadLine
            Passage.TextBox5.Text = NomFichier.ReadLine
            Passage.TextBox8.Text = NomFichier.ReadLine
            Passage.TextBox23.Text = NomFichier.ReadLine
            Passage.TextBox7.Text = NomFichier.ReadLine
            Passage.TextBox9.Text = NomFichier.ReadLine
            Passage.TextBox11.Text = NomFichier.ReadLine
            Passage.TextBox12.Text = NomFichier.ReadLine
            Passage.TextBox14.Text = NomFichier.ReadLine
            Passage.TextBox15.Text = NomFichier.ReadLine
            Passage.TextBox17.Text = NomFichier.ReadLine
            Passage.TextBox18.Text = NomFichier.ReadLine
            Passage.TextBox19.Text = NomFichier.ReadLine
            Passage.TextBox20.Text = NomFichier.ReadLine

            ' ==================================
            ' Mise à jour de la liste des Lieux de Surveillance si le numéro de département est indiqué
            If Passage.TextBox10.Text <> "" Then
                ' ==================================
                ' Renseignement de la liste 2 en fonction de la Dénomination des Départements
                Passage.ListBox2.Items.Clear()
                ' ==================================
                ' Recherche du point d'entrée dans le tableau des informations pour les Lieux de Surveillance
                For i = 0 To 5000
                    If Variables.TabRéfInfo(i, 0) = "LIEU DE SURVEILLANCE" Then
                        IndexTabInfo = i + 1
                        Exit For
                    End If
                Next
                ' ==================================
                ' Chargement des dénomination des Lieux de Surveillance du Département dans la liste 2
                For i = IndexTabInfo To 5000
                    ' ==================================
                    ' Sortie de la boucle si fin de tableau
                    If Variables.TabRéfInfo(i, 0) = "" Then Exit For
                    ' ==================================
                    ' Vérification si le numéro de département correspond
                    If Variables.TabRéfInfo(i, 0) = Passage.TextBox10.Text Then
                        ' ==================================
                        ' Affectation d'une désignation dans la liste
                        Passage.ListBox2.Items.Add(" " & Variables.TabRéfInfo(i, 1))
                    End If
                Next
            End If

            ' ==================================
            ' Chargement des informations Prélèvement
            Prélèvement.TextBox1.Text = NomFichier.ReadLine
            Prélèvement.TextBox2.Text = NomFichier.ReadLine
            Prélèvement.TextBox8.Text = NomFichier.ReadLine
            Prélèvement.TextBox23.Text = NomFichier.ReadLine
            Prélèvement.TextBox3.Text = NomFichier.ReadLine
            Prélèvement.TextBox5.Text = NomFichier.ReadLine
            Prélèvement.TextBox6.Text = NomFichier.ReadLine
            Prélèvement.TextBox7.Text = NomFichier.ReadLine
            Prélèvement.TextBox9.Text = NomFichier.ReadLine
            Prélèvement.TextBox10.Text = NomFichier.ReadLine
            Prélèvement.TextBox11.Text = NomFichier.ReadLine
            Prélèvement.TextBox12.Text = NomFichier.ReadLine
            Prélèvement.TextBox14.Text = NomFichier.ReadLine
            Prélèvement.TextBox15.Text = NomFichier.ReadLine
            Prélèvement.TextBox16.Text = NomFichier.ReadLine
            Prélèvement.TextBox17.Text = NomFichier.ReadLine
            ' ==================================
            ' Chargement des informations Échantillon
            Échantillon.TextBox1.Text = NomFichier.ReadLine
            Échantillon.TextBox2.Text = NomFichier.ReadLine
            Échantillon.TextBox3.Text = NomFichier.ReadLine
            Échantillon.TextBox4.Text = NomFichier.ReadLine
            Échantillon.TextBox5.Text = NomFichier.ReadLine
            Échantillon.TextBox6.Text = NomFichier.ReadLine
            Échantillon.TextBox7.Text = NomFichier.ReadLine
            Échantillon.TextBox8.Text = NomFichier.ReadLine
            Échantillon.TextBox9.Text = NomFichier.ReadLine
            ' ==================================
            ' Effacement de la Liste 2 des familles sélectionnées
            Familles.ListBox2.Items.Clear()
            ' ==================================
            ' Nombre de noms de Familles sélectionnées
            j = Val(NomFichier.ReadLine)
            ' ==================================
            ' Chargement de la liste des noms de Familles sélectionnées de la Liste 2
            For i = 1 To j
                ' ==================================
                ' Vérification si la fin de liste est atteinte
                Familles.ListBox2.Items.Add(NomFichier.ReadLine)
            Next
            ' ==================================
            ' Effacement des informations des Paramètres du Tableau des Paramètres
            For i = 0 To 500
                For j = 4 To 20
                    Variables.TabRéfPara(i, j) = ""
                Next
                Variables.TabRéfPara(i, 21) = 0
            Next
            ' ==================================
            ' Chargement du Tableau des Paramètres
            For i = 0 To 500
                ' ==================================
                ' Extraction de l'enregistrement
                LectureLigne = NomFichier.ReadLine
                ' ==================================
                ' Vérification si la fin de liste est atteinte
                If LectureLigne = "Fin" Then Exit Do

                ' ==================================
                ' Recherche du code Sandre du Paramètre
                ' ==================================
                ' Positionnement du pointeur sur le premier caractère de la ligne
                TabMin = 1
                ' ==================================
                ' Boucle de récupération du code Sandre
                For j = 0 To 3
                    ' ==================================
                    ' Recherche de la prochaine position de la tabulation
                    TabMax = Strings.InStr(TabMin, LectureLigne, Chr("9"))
                    ' ==================================
                    ' Récupération du code Sandre du Paramètre
                    If j = 3 Then
                        CodeSandre = Strings.Mid(LectureLigne, TabMin, TabMax - TabMin)
                    End If
                    ' ==================================
                    ' Positionnement de la prochaine recherche de tabulation
                    TabMin = TabMax + 1
                Next

                ' ==================================
                ' Recherche du Paramètre à partir du code Sandre
                ' ==================================
                ' Balayage du tableau des Paramètres
                For k = 0 To 500
                    ' ==================================
                    ' Vérification si le code Sandre du Paramètre dans le Tableau correspond à celui recherché
                    If Variables.TabRéfPara(k, 3) = CodeSandre Then
                        ' ==================================
                        ' Affectation des informations au paramètre trouvé
                        ' ==================================
                        ' Positionnement du pointeur sur le premier caractère de la ligne
                        TabMin = 1
                        ' ==================================
                        ' Boucle d'affectation des informations pour les 21 colonnes
                        For j = 0 To 20
                            ' ==================================
                            ' Recherche de la prochaine position de la tabulation
                            TabMax = Strings.InStr(TabMin, LectureLigne, Chr("9"))
                            ' ==================================
                            ' Chargement de la colonne définie avec ses informations
                            Variables.TabRéfPara(k, j) = Strings.Mid(LectureLigne, TabMin, TabMax - TabMin)
                            ' ==================================
                            ' Positionnement de la prochaine recherche de tabulation
                            TabMin = TabMax + 1
                        Next


                        ' ==================================
                        ' Modifications version 1.6 Ifremer

                        ' ==================================
                        ' Vérification si ancien fichier V 1.5 Ifremer ou nouveau fichier V 1.6 Ifremer
                        If Strings.InStr(TabMin, LectureLigne, Chr("9")) = 0 Then
                            ' ==================================
                            ' Chargement de la dernière colonne définie avec ses informations V 1.5 Ifremer
                            Variables.TabRéfPara(k, 21) = Strings.Right(LectureLigne, Len(LectureLigne) - TabMax)
                        Else
                            ' ==================================
                            ' Chargement des trois colonnes ajoutées avec leurs informations V 1.6 Ifremer


                            '         MsgBox(TabMin & "      " & TabMax & "      " & Strings.InStr(TabMin, LectureLigne, Chr("9")))


                            ' ==================================
                            ' Recherche de la prochaine position de la tabulation - Col 21 : Index
                            TabMax = Strings.InStr(TabMin, LectureLigne, Chr("9"))
                            ' ==================================
                            ' Chargement de la colonne définie avec ses informations
                            Variables.TabRéfPara(k, 21) = Strings.Mid(LectureLigne, TabMin, TabMax - TabMin)
                            ' ==================================
                            ' Positionnement de la prochaine recherche de tabulation
                            TabMin = TabMax + 1

                            ' ==================================
                            ' Recherche de la prochaine position de la tabulation - Col 23 : Code Resultat Qualitatif
                            TabMax = Strings.InStr(TabMin, LectureLigne, Chr("9"))
                            ' ==================================
                            ' Chargement de la colonne définie avec ses informations
                            Variables.TabRéfPara(k, 23) = Strings.Mid(LectureLigne, TabMin, TabMax - TabMin)
                            ' ==================================
                            ' Positionnement de la prochaine recherche de tabulation
                            TabMin = TabMax + 1

                            ' ==================================
                            ' Recherche de la prochaine position de la tabulation - Col 24 : Code Groupe Taxon Résultat
                            TabMax = Strings.InStr(TabMin, LectureLigne, Chr("9"))
                            ' ==================================
                            ' Chargement de la colonne définie avec ses informations
                            Variables.TabRéfPara(k, 24) = Strings.Mid(LectureLigne, TabMin, TabMax - TabMin)

                            ' ==================================
                            ' Chargement de la colonne définie avec ses informations - Col 25 : Code Taxon Résultat
                            Variables.TabRéfPara(k, 25) = Strings.Right(LectureLigne, Len(LectureLigne) - TabMax)


                        End If

                    End If
                Next
            Next
        Loop
        ' ==================================
        ' Fermeture du fichier
        NomFichier.Close()
        ' ==================================
        ' Recopie du nom du fichier de sélection chargé
        TextBox1.Text = Strings.LTrim(ListBox1.SelectedItem)
        ' ==================================
        ' Proposition du nom du fichier pour le nom du fichier Masque
        Sauve_Masque.TextBox1.Text = LTrim(ListBox1.SelectedItem)
        ' ==================================
        ' Proposition du nom du fichier pour le nom du fichier Résultat
        Sauve_Résultat.TextBox1.Text = LTrim(ListBox1.SelectedItem)
        ' ==================================
        ' Proposition du nom du fichier pour le nom du fichier Ifremer
        Sauve_Ifremer.TextBox1.Text = LTrim(ListBox1.SelectedItem)
        ' ==================================
        ' Effacement du nom du fichier de sélection chargé
        Variables.NomSelect = ""
        ' ==================================
        ' Invalidation sauvegarde à la fermeture
        Variables.SauveFichierMasque = False
        ' ==================================
        ' Indication du type de fichier chargé
        Variables.TypeFichierChargé = "Masque"

        ' ==================================
        ' Effacement des lignes du DataGridView
        Form1.DataGridView1.Rows.Clear()

        ' ==================================
        ' Fermeture de l'application
        Fermeture()


        Exit Sub

        ' ==================================
TraitErreur2:
        If Err.Number <> 0 Then
            MsgBox(Err.Number & "    " & Err.Description, MsgBoxStyle.Exclamation, " Erreur de chargement")
        End If
    End Sub

    ' ==============================================================================================================================================
    ' Procédure de fermeture
    Private Sub Fermeture()
        ' ============================================
        ' Essai pour éviter qu'une application ne passe devant la feuille ayant demandé celle-ci
        Form1.Activate()
        ' ============================================
        Me.Visible = False
    End Sub

    ' ==============================================================================================================================================
    ' Procédure d'ouverture par Fondu
    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        Me.Opacity = Me.Opacity + 0.06
        If Me.Opacity >= 0.95 Then
            Timer1.Enabled = False
            Me.Opacity = 1
        End If
    End Sub

    '=============================================================================================================================================
    ' Traitement du repositionnement automatique de la feuille quand on l'a déplacée
    Private Sub Charge_Masque_MouseEnter(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.MouseEnter
        Me.CenterToScreen()
    End Sub

    '=============================================================================================================================================
    ' Traitement de réduction des fenêtres
    Private Sub Charge_Masque_Move(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Move
        ' ============================================
        ' Passage de la feuille principale transparente quand on réduit cette feuille
        If Me.WindowState = 1 Then
            Form1.Opacity = 0
        End If
        ' ============================================
        ' Passage de la feuille principale opaque quand on rétablit cette feuille
        If Me.WindowState = 0 Then
            Form1.Opacity = 1
        End If
    End Sub

End Class