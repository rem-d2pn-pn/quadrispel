﻿Public Class Paramètres


    '=============================================================================================================================================
    ' Procédure lors de la fermeture de la feuille par la croix
    Private Sub Paramètres_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        '==============================================
        ' Blocage de la procédure de fermeture automatique
        e.Cancel = True
        '==============================================
        ' Demande de fermeture par Fondu
        Fermeture()
    End Sub

    ' ==============================================================================================================================================
    ' Procédure au chargement de la page
    Private Sub Paramètres_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        '==============================================
        ' Blocage des boutons à l'ouverture
        Button4.Enabled = False
        Button5.Enabled = False
        Button6.Enabled = False


        ' ==================================
        ' Sélection de la première Famille
        Variables.PointeurFamille = 0
        ' ==================================
        ' Invalidation du bouton de Famille précédente
        Button8.Enabled = False
        ' ==================================
        ' Validation du bouton de Famille suivante
        If Familles.ListBox2.Items.Count - 1 > 0 Then
            Button9.Enabled = True
        End If
        ' ==================================
        ' Transfert des paramètres des Familles
        TransfertParamDansList1()


        ' ==================================
        ' Transfert des informations du Tableau des Paramètres Référentiels
        '       TransfertDuTabInfRésParFam()
        ' ==================================
        ' Transfert des informations du Tableau des Paramètres
        '       TransfertDuTabSélParFam()

        '==============================================
        ' Demande d'ouverture par Fondu
        Me.Opacity = 0
        Timer1.Enabled = True
    End Sub


    '=============================================================================================================================================
    ' PROCÉDURES BOUTONS
    '=============================================================================================================================================
    ' Bouton Transférer
    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button2.Select()
        ' ==================================
        ' Vérification que des sélections n'ont pas déjà été transférées
        For i = 0 To ListBox1.Items.Count - 1
            If ListBox1.GetSelected(i) = True Then
                ' ==================================
                ' Vérification si le nom pointé a été déjà été transféré
                For j = 0 To ListBox2.Items.Count - 1
                    If ListBox1.Items.Item(i) = ListBox2.Items(j) Then
                        ' ==================================
                        ' Message indiquant une erreur de transfert
                        MsgBox("Le paramètre " & ListBox2.Items(j) & " a déjà été transféré ! ", MsgBoxStyle.Exclamation, " QUADRISPEL - Erreur de transfert")
                        Exit Sub
                    End If
                Next
            End If
        Next
        ' ==================================
        ' Transfert dans la liste 2 des noms de Familles sélectionnés dans la liste 1
        For i = 0 To ListBox1.Items.Count - 1
            ' ==================================
            ' Vérification si le nom pointé a été sélectionné
            If ListBox1.GetSelected(i) = True Then
                ListBox2.Items.Add(ListBox1.Items.Item(i))
            End If
        Next
        ' ==================================
        ' Désélection de toute la liste 1
        For i = 0 To ListBox1.Items.Count - 1
            ListBox1.SetSelected(i, False)
        Next
        ' ==================================
        ' Désélection de toute la liste 2
        For i = 0 To ListBox2.Items.Count - 1
            ListBox2.SetSelected(i, False)
        Next
        ' ==================================
        ' Desactivation des boutons Monter, Descendre et Supprimer
        Button4.Enabled = False
        Button5.Enabled = False
        Button6.Enabled = False
        ' ==================================
        ' Initialisation du bouton Tous
        Button7.Text = "Tous"


        '=============================================================================================================================================
        ' Procédure de Transfert de la Liste 2 des Paramètres Sélectionnés dans le Tableau des Paramètres des Familles
        TransfertList2DansParam()

        ' ==================================
        ' Validation sauvegarde à la fermeture
        Variables.SauveFichierMasque = True

    End Sub

    '=============================================================================================================================================
    ' Bouton Supprimer
    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click

        ' ==================================
        ' Sortie si l'index = -1
        If ListBox2.SelectedIndex = -1 Then Exit Sub

        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button2.Select()
        ' ==================================
        ' Suppression du nom du Paramètre sélectionné
        ListBox2.Items.RemoveAt(ListBox2.SelectedIndex)
        ' ==================================
        ' Invalidation du bouton supprimer
        Button6.Enabled = False
        ' ==================================
        ' Desactivation s'il ne reste qu'une ligne
        '        If ListBox2.Items.Count < 2 Then
        'Button4.Enabled = False
        '        Button5.Enabled = False
        '        End If
        ' ==================================
        ' Desactivation des boutons Monter, Descendre et Supprimer
        Button4.Enabled = False
        Button5.Enabled = False
        Button6.Enabled = False


        '=============================================================================================================================================
        ' Procédure de Transfert de la Liste 2 des Paramètres Sélectionnés dans le Tableau des Paramètres des Familles
        TransfertList2DansParam()

        ' ==================================
        ' Validation sauvegarde à la fermeture
        Variables.SauveFichierMasque = True

    End Sub

    '=============================================================================================================================================
    ' Bouton Monter
    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Dim i As Integer
        ' ==================================
        ' Sortie si l'index = -1
        If ListBox2.SelectedIndex = -1 Then Exit Sub

        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button2.Select()
        ' ==================================
        ' Récupération de l'index sélectionné
        i = ListBox2.SelectedIndex
        ' ==================================
        ' Sortie si pas de sélection ou sélection du premier nom
        If i = -1 Or i = 0 Then Exit Sub
        ' ==================================
        ' Insertion du nom sélectionné
        ListBox2.Items.Insert(i - 1, ListBox2.SelectedItem)
        ' ==================================
        ' Effacement de l'index sélectionné
        ListBox2.Items.RemoveAt(i + 1)
        ' ==================================
        ' Sélection du nouvel index sélectionné
        i = i - 1
        ListBox2.SetSelected(i, True)
        ' ==================================
        ' Validation des boutons de sélections des Familles
        Button5.Enabled = True
        If i < 1 Then
            Button4.Enabled = False
        End If


        '=============================================================================================================================================
        ' Procédure de Transfert de la Liste 2 des Paramètres Sélectionnés dans le Tableau des Paramètres des Familles
        TransfertList2DansParam()

        ' ==================================
        ' Validation sauvegarde à la fermeture
        Variables.SauveFichierMasque = True

    End Sub

    '=============================================================================================================================================
    ' Bouton Descendre
    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Dim i As Integer
        ' ==================================
        ' Sortie si l'index = -1
        If ListBox2.SelectedIndex = -1 Then Exit Sub

        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button2.Select()
        ' ==================================
        ' Récupération de l'index sélectionné
        i = ListBox2.SelectedIndex
        ' ==================================
        ' Sortie si pas de sélection ou sélection du dernier nom
        If i = -1 Or i = ListBox2.Items.Count - 1 Then Exit Sub
        ' ==================================
        ' Insertion du nom sélectionné
        ListBox2.Items.Insert(i + 2, ListBox2.SelectedItem)
        ' ==================================
        ' Effacement de l'index sélectionné
        ListBox2.Items.RemoveAt(i)
        ' ==================================
        ' Sélection du nouvel index sélectionné
        i = i + 1
        ListBox2.SetSelected(i, True)
        ' ==================================
        ' Validation des boutons de sélections des Familles
        Button4.Enabled = True
        If i >= ListBox2.Items.Count - 1 Then
            Button5.Enabled = False
        End If


        '=============================================================================================================================================
        ' Procédure de Transfert de la Liste 2 des Paramètres Sélectionnés dans le Tableau des Paramètres des Familles
        TransfertList2DansParam()

        ' ==================================
        ' Validation sauvegarde à la fermeture
        Variables.SauveFichierMasque = True

    End Sub

    '=============================================================================================================================================
    ' Bouton Tous / Aucun de la liste 1
    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button2.Select()
        ' ==================================
        ' Vérification du texte du bouton
        If Button7.Text = "Tous" Then
            ' ==================================
            ' MAJ du texte du bouton
            Button7.Text = "Aucun"
            ' ==================================
            ' Sélection de toute la liste 1
            For i = 0 To ListBox1.Items.Count - 1
                ListBox1.SetSelected(i, True)
            Next
            Exit Sub
        End If
        ' ==================================
        ' MAJ du texte du bouton
        Button7.Text = "Tous"
        ' ==================================
        ' Désélection de toute la liste 1
        For i = 0 To ListBox1.Items.Count - 1
            ListBox1.SetSelected(i, False)
        Next
    End Sub

    '=============================================================================================================================================
    ' Bouton Famille Précédente
    Private Sub Button8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button8.Click
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button2.Select()
        ' ==================================
        ' Décrémentation du Pointeur de Familles
        Variables.PointeurFamille = Variables.PointeurFamille - 1
        ' ==================================
        ' Vérification si première Famille réalisée
        If Variables.PointeurFamille = 0 Then
            Button8.Enabled = False
        End If
        ' ==================================
        ' Validation du bouton de Famille suivante
        If Variables.PointeurFamille < Familles.ListBox2.Items.Count - 1 Then
            Button9.Enabled = True
        End If
        ' ==================================
        ' Transfert du Tableau des Paramètres des Familles dans la  Liste 1
        TransfertParamDansList1()
    End Sub

    '=============================================================================================================================================
    ' Bouton Famille Suivante
    Private Sub Button9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button9.Click
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button2.Select()
        ' ==================================
        ' Décrémentation du Pointeur de Familles
        Variables.PointeurFamille = Variables.PointeurFamille + 1
        ' ==================================
        ' Vérification si dernière Famille réalisée
        If Variables.PointeurFamille = Familles.ListBox2.Items.Count - 1 Then
            Button9.Enabled = False
        End If
        ' ==================================
        ' Validation du bouton de Famille précédente
        If Variables.PointeurFamille > 0 Then
            Button8.Enabled = True
        End If
        ' ==================================
        ' Transfert du Tableau des Paramètres des Familles dans la  Liste 1
        TransfertParamDansList1()
    End Sub

    '=============================================================================================================================================
    ' Procédure de Transfert du Tableau des Paramètres des Familles dans la Liste 1
    Private Sub TransfertParamDansList1()
        ' ==================================
        ' Initialisation
        Dim i As Integer
        ' ==================================
        ' Récupération du nom de Famille de la List 2 des Familles
        TextBox1.Text = LTrim(Familles.ListBox2.Items.Item(Variables.PointeurFamille))
        ' ==================================
        ' MAJ du texte du bouton
        Button7.Text = "Tous"
        ' ==================================
        ' RAZ de la liste des noms des Paramètres
        ListBox1.Items.Clear()
        ' ==================================
        ' RAZ de la liste des noms des Paramètres Sélectionnés
        ListBox2.Items.Clear()
        ' ==================================
        ' Transfert des paramètres de la famille sélectionnée
        For i = 0 To 500
            ' ==================================
            ' Sortie de la boucle si fin de tableau
            If Variables.TabRéfPara(i, 0) = "" Then Exit For
            ' ==================================
            ' Transfert des paramètres de la Famille sélectionnée
            If Variables.TabRéfPara(i, 0) = LTrim(Familles.ListBox2.Items.Item(Variables.PointeurFamille)) Then
                ' ==================================
                ' Affectation dans la liste des paramètres de la Famille sélectionnée
                ListBox1.Items.Add(" " & Variables.TabRéfPara(i, 1))
            End If
        Next
        ' ==================================
        ' Procédure de Transfert du paramètres sélectionnés de la Famille sélectionnée dans la Liste 2
        TransfertParamDansList2()
    End Sub

    '=============================================================================================================================================
    ' Procédure de Transfert du Tableau des Paramètres Sélectionnés des Familles dans la Liste 2
    Private Sub TransfertParamDansList2()
        ' ==================================
        ' Initialisation
        Dim i As Integer
        Dim j As Integer
        Dim PremierParam As Integer
        Dim PointeurParam As Integer = 1
        ' ==================================
        ' RAZ de la liste des noms des Paramètres Sélectionnés
        ListBox2.Items.Clear()
        ' ==================================
        ' Recherche de l'adresse de la famille sélectionnée
        For i = 0 To 500
            If TextBox1.Text = Variables.TabRéfPara(i, 0) Then
                PremierParam = i
                Exit For
            End If
        Next
        ' ==================================
        ' Transfert des paramètres sélectionnés de la famille sélectionnée dans la Liste 2
        For i = PremierParam To 500
            ' ==================================
            ' Sortie si changement de famille
            If TextBox1.Text <> Variables.TabRéfPara(i, 0) Then
                Exit For
            End If
            ' ==================================
            ' Balayage des paramètres sélectionnés
            For j = PremierParam To 500
                ' ==================================
                ' Sortie si changement de famille
                If TextBox1.Text <> Variables.TabRéfPara(j, 0) Then
                    Exit For
                End If
                ' ==================================
                ' Recherche du paramètre dont l'index correspond au pointeur de paramètres
                If PointeurParam = Val(Variables.TabRéfPara(j, 21)) Then
                    ' ==================================
                    ' Affectation dans la liste 2 des paramètres de la Famille sélectionnée
                    ListBox2.Items.Add(" " & Variables.TabRéfPara(j, 1))
                    ' ==================================
                    ' Incrémentation du pointeur de paramètres
                    PointeurParam = PointeurParam + 1
                End If
            Next
        Next
    End Sub

    '=============================================================================================================================================
    ' Procédure de Transfert de la Liste 2 des Paramètres Sélectionnés dans le Tableau des Paramètres des Familles
    Private Sub TransfertList2DansParam()
        ' ==================================
        ' Initialisation
        Dim i As Integer
        Dim j As Integer
        Dim PremierParam As Integer
        ' ==================================
        ' Recherche de l'adresse de la famille sélectionnée
        For i = 0 To 500
            If TextBox1.Text = Variables.TabRéfPara(i, 0) Then
                PremierParam = i
                Exit For
            End If
        Next
        ' ==================================
        ' Effacement de l'index des paramétres de la famille sélectionnée
        For i = PremierParam To 500
            If TextBox1.Text = Variables.TabRéfPara(i, 0) Then
                Variables.TabRéfPara(i, 21) = 0
            Else
                Exit For
            End If
        Next
        ' ==================================
        ' Balayage de la Liste 2 pour le transfert des paramètres sélectionnés de la famille sélectionnée dans le Tableau des Paramètres des Familles
        For i = 0 To ListBox2.Items.Count - 1
            ' ==================================
            ' Balayage des paramètres sélectionnés
            For j = PremierParam To 500
                ' ==================================
                ' Sortie si changement de famille
                If TextBox1.Text <> Variables.TabRéfPara(j, 0) Then
                    Exit For
                End If
                ' ==================================
                ' Recherche du paramètre dont le nom correspond au paramètre pointé dans la Liste 2
                If Variables.TabRéfPara(j, 1) = LTrim(ListBox2.Items(i)) Then
                    ' ==================================
                    ' Affectation dans le Tableau des Paramètres des Familles de l'index du paramètre sélectionné
                    Variables.TabRéfPara(j, 21) = i + 1
                End If
            Next
        Next
        '       MsgBox(Variables.TabRéfPara(72, 21) & "  " & Variables.TabRéfPara(73, 21) & "  " & Variables.TabRéfPara(74, 21) & "  " & Variables.TabRéfPara(75, 21) & "  " & Variables.TabRéfPara(76, 21) & "  " & Variables.TabRéfPara(77, 21))
    End Sub

    '=============================================================================================================================================
    ' Activation des boutons Monter, Descendre et Supprimer en fonction de la sélection dans la Liste
    Private Sub ListBox2_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListBox2.SelectedIndexChanged
        If ListBox2.SelectedItem = "" Then Exit Sub
        ' ============================================
        ' Activation du bouton Supprimer
        Button6.Enabled = True
        ' ============================================
        ' Activation des boutons Monter et Descendre
        If ListBox2.Items.Count > 1 Then
            Button4.Enabled = True
            Button5.Enabled = True
        End If
        ' ============================================
        ' Desactivation du bouton Monter
        If ListBox2.SelectedIndex = 0 Then
            Button4.Enabled = False
        End If
        ' ============================================
        ' Desactivation du bouton Descendre
        If ListBox2.SelectedIndex >= ListBox2.Items.Count - 1 Then
            Button5.Enabled = False
        End If
    End Sub


    '=============================================================================================================================================
    ' PROCÉDURES DIVERSES
    '=============================================================================================================================================
    ' Traitement du repositionnement automatique de la feuille quand on l'a déplacée
    Private Sub Paramètres_MouseEnter(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.MouseEnter
        Me.CenterToScreen()
    End Sub

    '=============================================================================================================================================
    ' Traitement de réduction des fenêtres
    Private Sub Paramètres_Move(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Move
        ' ============================================
        ' Passage de la feuille principale transparente quand on réduit cette feuille
        If Me.WindowState = 1 Then
            Form1.Opacity = 0
        End If
        ' ============================================
        ' Passage de la feuille principale opaque quand on rétablit cette feuille
        If Me.WindowState = 0 Then
            Form1.Opacity = 1
        End If
    End Sub

    ' ==============================================================================================================================================
    ' Bouton de fermeture de l'application
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button2.Select()
        ' ==================================
        ' Demande de fermeture par Fondu
        Fermeture()
    End Sub

    ' ==============================================================================================================================================
    ' Renvoi du focus sur le bouton caché
    Private Sub TextBox1_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox1.GotFocus
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button2.Select()
    End Sub


    '=============================================================================================================================================
    ' PROCÉDURE D'OUVERTURE ET DE FERMETURE
    '=============================================================================================================================================
    ' Procédure d'ouverture par Fondu
    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        Me.Opacity = Me.Opacity + 0.06
        If Me.Opacity >= 0.95 Then
            Timer1.Enabled = False
            Me.Opacity = 1
        End If
    End Sub

    Private Sub Fermeture()
        ' ============================================
        ' Essai pour éviter qu'une application ne passe devant la feuille ayant demandé celle-ci
        Form1.Activate()
        ' ============================================
        Me.Visible = False
    End Sub




End Class
