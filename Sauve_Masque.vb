﻿' Importation des systèmes utilisés
Imports System
Imports System.IO
Imports System.IO.Directory
Imports System.Text

Public Class Sauve_Masque


    '=============================================================================================================================================
    ' Procédure lors de la fermeture de la feuille par la croix
    Private Sub Sauve_Masque_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        ' Blocage de la procédure de fermeture automatique
        e.Cancel = True
        '==============================================
        Fermeture()
    End Sub

    ' ==============================================================================================================================================
    ' Procédure au chargement de la feuille
    Private Sub Sauve_Masque_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        '==============================================
        ' Changement visuel de sélection de bouton
        Button3.Select()
        '==============================================
        ' Suppression de la date si elle existe
        If Strings.Mid(TextBox1.Text, 5, 1) = "-" And Strings.Mid(TextBox1.Text, 8, 1) = "-" Then
            If Strings.Mid(TextBox1.Text, 11, 1) = " " Then
                TextBox1.Text = Strings.Right(TextBox1.Text, Len(TextBox1.Text) - 11)
            Else
                TextBox1.Text = Strings.Right(TextBox1.Text, Len(TextBox1.Text) - 10)
            End If
        End If
        '==============================================
        ' Demande d'ouverture par Fondu
        Me.Opacity = 0
        Timer1.Enabled = True
    End Sub

    ' ==============================================================================================================================================
    ' Procédure de fermeture de la feuille
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        '==============================================
        ' Changement visuel de sélection de bouton
        Button3.Select()
        '==============================================
        Fermeture()
    End Sub

    ' ==============================================================================================================================================
    ' Procédure de validation
    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        '==============================================
        ' Changement visuel de sélection de bouton
        Button3.Select()
        ' ==================================
        ' Initialisation des variables
        Dim InfoFichier As FileInfo
        Dim NomFichier As StreamWriter
        Dim LigneTableau As String
        Dim i As Integer
        ' ==================================
        ' Traitement des erreurs
        On Error GoTo TraitErreur1
        ' ==================================
        ' Vérification si la sélection d'un nom de fichier a été réalisée
        If TextBox1.Text = "" Then
            MsgBox("Vous n'avez pas défini un nom de fichier !", MsgBoxStyle.Exclamation, " Absence de sélection")
            Exit Sub
        End If
        ' ==================================
        ' Vérification si le fichier existe déjà
        ' ==================================
        ' Récupération du nom du fichier de la base Fairplay
        InfoFichier = New FileInfo(Variables.Repertoire & "\Masques\" & TextBox1.Text & ".msq")
        ' ==================================
        ' Vérification de l'existence de la base Fairplay
        If InfoFichier.Exists = True Then
            i = MsgBox("          Le fichier existe déjà !" & vbCrLf & vbCrLf & "  Confirmez-vous la sauvegarde ? ", MsgBoxStyle.Exclamation + MsgBoxStyle.YesNo + MsgBoxStyle.DefaultButton2, " QUADRISPEL - Demande de confirmation")
            If i = 7 Then
                Exit Sub
            End If
        End If
        ' ==================================
        ' Ouverture du fichier de sauvegarde
        NomFichier = New StreamWriter(Variables.Repertoire & "\Masques\" & TextBox1.Text & ".msq", False, System.Text.Encoding.UTF8)
        ' ==================================
        ' Sauvegarde des informations Passage
        NomFichier.WriteLine(Passage.TextBox10.Text)
        NomFichier.WriteLine(Passage.TextBox24.Text)
        NomFichier.WriteLine(Passage.TextBox3.Text)
        NomFichier.WriteLine(Passage.TextBox2.Text)
        NomFichier.WriteLine(Passage.TextBox1.Text)
        NomFichier.WriteLine(Passage.TextBox5.Text)
        NomFichier.WriteLine(Passage.TextBox8.Text)
        NomFichier.WriteLine(Passage.TextBox23.Text)
        NomFichier.WriteLine(Passage.TextBox7.Text)
        NomFichier.WriteLine(Passage.TextBox9.Text)
        NomFichier.WriteLine(Passage.TextBox11.Text)
        NomFichier.WriteLine(Passage.TextBox12.Text)
        NomFichier.WriteLine(Passage.TextBox14.Text)
        NomFichier.WriteLine(Passage.TextBox15.Text)
        NomFichier.WriteLine(Passage.TextBox17.Text)
        NomFichier.WriteLine(Passage.TextBox18.Text)
        NomFichier.WriteLine(Passage.TextBox19.Text)
        NomFichier.WriteLine(Passage.TextBox20.Text)
        ' ==================================
        ' Sauvegarde des informations Prélèvement
        NomFichier.WriteLine(Prélèvement.TextBox1.Text)
        NomFichier.WriteLine(Prélèvement.TextBox2.Text)
        NomFichier.WriteLine(Prélèvement.TextBox8.Text)
        NomFichier.WriteLine(Prélèvement.TextBox23.Text)
        NomFichier.WriteLine(Prélèvement.TextBox3.Text)
        NomFichier.WriteLine(Prélèvement.TextBox5.Text)
        NomFichier.WriteLine(Prélèvement.TextBox6.Text)
        NomFichier.WriteLine(Prélèvement.TextBox7.Text)
        NomFichier.WriteLine(Prélèvement.TextBox9.Text)
        NomFichier.WriteLine(Prélèvement.TextBox10.Text)
        NomFichier.WriteLine(Prélèvement.TextBox11.Text)
        NomFichier.WriteLine(Prélèvement.TextBox12.Text)
        NomFichier.WriteLine(Prélèvement.TextBox14.Text)
        NomFichier.WriteLine(Prélèvement.TextBox15.Text)
        NomFichier.WriteLine(Prélèvement.TextBox16.Text)
        NomFichier.WriteLine(Prélèvement.TextBox17.Text)
        ' ==================================
        ' Sauvegarde des informations Échantillon
        NomFichier.WriteLine(Échantillon.TextBox1.Text)
        NomFichier.WriteLine(Échantillon.TextBox2.Text)
        NomFichier.WriteLine(Échantillon.TextBox3.Text)
        NomFichier.WriteLine(Échantillon.TextBox4.Text)
        NomFichier.WriteLine(Échantillon.TextBox5.Text)
        NomFichier.WriteLine(Échantillon.TextBox6.Text)
        NomFichier.WriteLine(Échantillon.TextBox7.Text)
        NomFichier.WriteLine(Échantillon.TextBox8.Text)
        NomFichier.WriteLine(Échantillon.TextBox9.Text)
        ' ==================================
        ' Sauvegarde du nombre de familles sélectionnées
        NomFichier.WriteLine(Familles.ListBox2.Items.Count)
        ' ==================================
        ' Sauvegarde de la liste des noms de Familles sélectionnées de la Liste 2
        For i = 0 To Familles.ListBox2.Items.Count - 1
            NomFichier.WriteLine(Familles.ListBox2.Items(i))
        Next
        ' ==================================
        ' Sauvegarde du Tableau des Paramètres
        For i = 0 To 500
            ' ==================================
            ' Sortie si plus de ligne à sauvegarder
            If Variables.TabRéfPara(i, 0) = "" Then Exit For
            ' ==================================
            ' Sauvegarde du paramètre dont l'index est valide
            If Val(Variables.TabRéfPara(i, 21)) <> 0 Then
                ' ==================================
                ' Constitution d'une ligne
                LigneTableau = Variables.TabRéfPara(i, 0) & Chr(9)
                ' Col 1  : Nom du paramètre
                LigneTableau = LigneTableau & Variables.TabRéfPara(i, 1) & Chr(9)
                ' Col 2  : Libellé du paramètre
                LigneTableau = LigneTableau & Variables.TabRéfPara(i, 2) & Chr(9)
                ' Col 3  : Code Sandre du paramètre
                LigneTableau = LigneTableau & Variables.TabRéfPara(i, 3) & Chr(9)
                ' Col 4  : Dénomination Analyseur
                LigneTableau = LigneTableau & Variables.TabRéfPara(i, 4) & Chr(9)
                ' Col 5  : Libellé Analyseur
                LigneTableau = LigneTableau & Variables.TabRéfPara(i, 5) & Chr(9)
                ' Col 6  : Dénomination méthode
                LigneTableau = LigneTableau & Variables.TabRéfPara(i, 6) & Chr(9)
                ' Col 7  : Engin d'analyse
                LigneTableau = LigneTableau & Variables.TabRéfPara(i, 7) & Chr(9)
                ' Col 8  : Dénomination fraction
                LigneTableau = LigneTableau & Variables.TabRéfPara(i, 8) & Chr(9)
                ' Col 9  : Libellé support
                LigneTableau = LigneTableau & Variables.TabRéfPara(i, 9) & Chr(9)
                ' Col 10 : Dénomination engin de prélèvement
                LigneTableau = LigneTableau & Variables.TabRéfPara(i, 10) & Chr(9)
                ' Col 11 : Précision
                LigneTableau = LigneTableau & Variables.TabRéfPara(i, 11) & Chr(9)
                ' Col 12 : Type Précision
                LigneTableau = LigneTableau & Variables.TabRéfPara(i, 12) & Chr(9)
                ' Col 13 : Niveau de saisie
                LigneTableau = LigneTableau & Variables.TabRéfPara(i, 13) & Chr(9)
                ' Col 14 : Seuil de détection
                LigneTableau = LigneTableau & Variables.TabRéfPara(i, 14) & Chr(9)
                ' Col 15 : Résultat qualitatif
                LigneTableau = LigneTableau & Variables.TabRéfPara(i, 15) & Chr(9)
                ' Col 16 : Numéro individu
                LigneTableau = LigneTableau & Variables.TabRéfPara(i, 16) & Chr(9)
                ' Col 17 : Résultat
                LigneTableau = LigneTableau & Chr(9)
                ' Col 18 : Unité
                LigneTableau = LigneTableau & Variables.TabRéfPara(i, 18) & Chr(9)
                ' Col 19 : Commentaire mesure
                LigneTableau = LigneTableau & Variables.TabRéfPara(i, 19) & Chr(9)
                ' Col 20 : Remarque
                LigneTableau = LigneTableau & Variables.TabRéfPara(i, 20) & Chr(9)
                ' Col 21 : Index de tri
                LigneTableau = LigneTableau & Variables.TabRéfPara(i, 21) & Chr(9)



                ' Modifications V 1.6

                ' Col 23 : Code Resultat Qualitatif
                LigneTableau = LigneTableau & Variables.TabRéfPara(i, 23) & Chr(9)
                ' Col 24 : Code Groupe Taxon Résultat
                LigneTableau = LigneTableau & Variables.TabRéfPara(i, 24) & Chr(9)
                ' Col 25 : Code Taxon Résultat
                LigneTableau = LigneTableau & Variables.TabRéfPara(i, 25)






                ' ==================================
                ' Sauvegarde d'une ligne
                NomFichier.WriteLine(LigneTableau)
            End If
        Next
        ' ==================================
        ' Indication de la fin du fichier
        NomFichier.WriteLine("Fin")
        ' ==================================
        ' Fermeture du fichier
        NomFichier.Close()
        ' ==================================
        ' Effacement de la vérification de sauvegarde à la fermeture de l'application
        Variables.SauveFichierMasque = False
        ' ==================================
        ' Fermeture de l'application
        Fermeture()

        Exit Sub
        '=========================================================================================================
TraitErreur1:
        If Err.Number <> 0 Then
            MsgBox(Err.Number & "    " & Err.Description, MsgBoxStyle.Exclamation, " Erreur de sauvegarde")
        End If
    End Sub

    ' ==============================================================================================================================================
    ' Procédure d'effacement du texte
    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        '==============================================
        ' Changement visuel de sélection de bouton
        Button3.Select()
        '==============================================
        TextBox1.Text = ""
    End Sub

    ' ==============================================================================================================================================
    ' Procédure de fermeture
    Private Sub Fermeture()
        ' ============================================
        ' Essai pour éviter qu'une application ne passe devant la feuille ayant demandé celle-ci
        Form1.Activate()
        ' ============================================
        Me.Visible = False
    End Sub

    ' ==============================================================================================================================================
    ' Procédure d'ouverture par Fondu
    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        Me.Opacity = Me.Opacity + 0.06
        If Me.Opacity >= 0.95 Then
            Timer1.Enabled = False
            Me.Opacity = 1
        End If
    End Sub

    '=============================================================================================================================================
    ' Traitement du repositionnement automatique de la feuille quand on l'a déplacée
    Private Sub Sauve_Masque_MouseEnter(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.MouseEnter
        Me.CenterToScreen()
    End Sub

    '=============================================================================================================================================
    ' Traitement de réduction des fenêtres
    Private Sub Sauve_Masque_Move(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Move
        ' ============================================
        ' Passage de la feuille principale transparente quand on réduit cette feuille
        If Me.WindowState = 1 Then
            Form1.Opacity = 0
        End If
        ' ============================================
        ' Passage de la feuille principale opaque quand on rétablit cette feuille
        If Me.WindowState = 0 Then
            Form1.Opacity = 1
        End If
    End Sub

End Class