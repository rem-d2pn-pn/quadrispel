﻿Public Class Passage

    '=============================================================================================================================================
    ' Procédure lors de la fermeture de la feuille par la croix
    Private Sub Passage_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        '==============================================
        ' Blocage de la procédure de fermeture automatique
        e.Cancel = True
        '==============================================
        ' Demande de fermeture par Fondu
        Fermeture()
    End Sub

    ' ==============================================================================================================================================
    ' Procédure au chargement de la page
    Private Sub Passage_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button2.Select()
        '==============================================
        ' Demande d'ouverture par Fondu
        Me.Opacity = 0
        Timer1.Enabled = True
        ' ==================================
        ' Effacement du nombre de caractères autorisé
        GroupBox13.Text = " Mnémonique Passage "
        ' ==================================
        ' Effacement du nombre de caractères autorisé
        GroupBox14.Text = " Commentaires Passage "

    End Sub

    '=============================================================================================================================================
    ' PROCÉDURES DIVERSES
    '=============================================================================================================================================
    ' Traitement du repositionnement automatique de la feuille quand on l'a déplacée
    Private Sub Passage_MouseEnter(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.MouseEnter
        Me.CenterToScreen()
    End Sub


    '=============================================================================================================================================
    ' Traitement de réduction des fenêtres
    Private Sub Passage_Move(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Move
        ' ============================================
        ' Passage de la feuille principale transparente quand on réduit cette feuille
        If Me.WindowState = 1 Then
            Form1.Opacity = 0
        End If
        ' ============================================
        ' Passage de la feuille principale opaque quand on rétablit cette feuille
        If Me.WindowState = 0 Then
            Form1.Opacity = 1
        End If
    End Sub

    ' ==============================================================================================================================================
    ' Bouton de fermeture de l'application
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button2.Select()
        ' ==================================
        ' Demande de fermeture par Fondu
        Fermeture()
    End Sub


    '=============================================================================================================================================
    ' PROCÉDURE D'OUVERTURE ET DE FERMETURE
    '=============================================================================================================================================
    ' Procédure d'ouverture par Fondu
    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        Me.Opacity = Me.Opacity + 0.06
        If Me.Opacity >= 0.95 Then
            Timer1.Enabled = False
            Me.Opacity = 1
        End If
    End Sub

    Private Sub Fermeture()
        ' ============================================
        ' Essai pour éviter qu'une application ne passe devant la feuille ayant demandé celle-ci
        Form1.Activate()
        ' ============================================
        Me.Visible = False
    End Sub


    '=============================================================================================================================================
    ' PROCÉDURE POUR ÉVITER LE CURSEUR CLIGNOTANT DANS LES CHAMPS
    '=============================================================================================================================================
 
    Private Sub TextBox1_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox1.GotFocus
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button2.Select()
    End Sub

    Private Sub TextBox2_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox2.GotFocus
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button2.Select()
    End Sub

    Private Sub TextBox3_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox3.GotFocus
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button2.Select()
    End Sub

    Private Sub TextBox8_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox8.GotFocus
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button2.Select()
    End Sub

    Private Sub TextBox10_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox10.GotFocus
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button2.Select()
    End Sub

    Private Sub TextBox15_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox15.GotFocus
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button2.Select()
    End Sub

    Private Sub TextBox19_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox19.GotFocus
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button2.Select()
    End Sub

    Private Sub TextBox23_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox23.GotFocus
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button2.Select()
    End Sub

    Private Sub TextBox24_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox24.GotFocus
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button2.Select()
    End Sub


    '=============================================================================================================================================
    ' PROCÉDURE POUR LE NOMBRE DE CARACTÈRES DANS LES CHAMPS
    '=============================================================================================================================================
    ' Affichage du nombre de caractères autorisé
    Private Sub TextBox17_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox17.GotFocus
        ' ==================================
        ' Affichage du nombre de caractères autorisé
        GroupBox13.Text = " Mnémonique Passage (" & 50 - Len(TextBox17.Text) & ") "
    End Sub

    '=============================================================================================================================================
    ' Affichage du nombre de caractères autorisé
    Private Sub TextBox17_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox17.TextChanged
        ' ==================================
        ' Validation sauvegarde à la fermeture
        Variables.SauveFichierMasque = True
        ' ==================================
        ' Affichage du nombre de caractères autorisé
        GroupBox13.Text = " Mnémonique Passage (" & 50 - Len(TextBox17.Text) & ") "
    End Sub

    '=============================================================================================================================================
    ' Effacement du nombre de caractères autorisé
    Private Sub TextBox17_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox17.LostFocus
        ' ==================================
        ' Effacement du nombre de caractères autorisé
        GroupBox13.Text = " Mnémonique Passage "
    End Sub

    '=============================================================================================================================================
    ' Affichage du nombre de caractères autorisé
    Private Sub TextBox18_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox18.GotFocus
        ' ==================================
        ' Affichage du nombre de caractères autorisé
        GroupBox14.Text = " Commentaires Passage (" & 2000 - Len(TextBox18.Text) & ") "
    End Sub

    '=============================================================================================================================================
    ' Affichage du nombre de caractères autorisé
    Private Sub TextBox18_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox18.TextChanged
        ' ==================================
        ' Validation sauvegarde à la fermeture
        Variables.SauveFichierMasque = True
        ' ==================================
        ' Affichage du nombre de caractères autorisé
        GroupBox14.Text = " Commentaires Passage (" & 2000 - Len(TextBox18.Text) & ") "
    End Sub

    '=============================================================================================================================================
    ' Effacement du nombre de caractères autorisé
    Private Sub TextBox18_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox18.LostFocus
        ' ==================================
        ' Effacement du nombre de caractères autorisé
        GroupBox14.Text = " Commentaires Passage "
    End Sub


    '=============================================================================================================================================
    ' PROCÉDURES D'APPARITION DES SÉLECTEURS
    '=============================================================================================================================================
    ' Sélection de la liste des Réseaux
    Private Sub TextBox3_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox3.MouseClick
        ' ==================================
        ' Traitement pour la liste des sujets si une autre liste est en cours
        If Variables.NumeroList <> 0 Then
            Variables.NumeroListSuivant = 5
            ' ==================================
            ' Arrêt du timer de la procédure d'apparition des listes
            Timer2.Enabled = False
            ' Arrêt du timer de la procédure d'apparition des listes
            Timer3.Enabled = False
            ' ==================================
            ' Déclenchement du retrait du sélecteur
            Timer4.Enabled = True
            Exit Sub
        End If
        ' ==================================
        ' Variable d'affichage du Listbox de la liste des sujets
        Variables.NumeroList = 5
        ' ==================================
        ' Déclenchement du timer de la procédure d'apparition des listes
        Timer2.Enabled = True
    End Sub

    '=============================================================================================================================================
    ' Sélection de la liste des Libellés des Saisisseurs
    Private Sub TextBox8_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox8.MouseClick
        ' ==================================
        ' Traitement pour la liste des sujets si une autre liste est en cours
        If Variables.NumeroList <> 0 Then
            Variables.NumeroListSuivant = 3
            ' ==================================
            ' Arrêt du timer de la procédure d'apparition des listes
            Timer2.Enabled = False
            ' Arrêt du timer de la procédure d'apparition des listes
            Timer3.Enabled = False
            ' ==================================
            ' Déclenchement du retrait du sélecteur
            Timer4.Enabled = True
            Exit Sub
        End If
        ' ==================================
        ' Variable d'affichage du Listbox de la liste des sujets
        Variables.NumeroList = 3
        ' ==================================
        ' Déclenchement du timer de la procédure d'apparition des listes
        Timer2.Enabled = True
    End Sub

    '=============================================================================================================================================
    ' Sélection de la liste des Départements des Lieux de Surveillance
    Private Sub TextBox10_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox10.MouseClick
        ' ==================================
        ' Traitement pour la liste des sujets si une autre liste est en cours
        If Variables.NumeroList <> 0 Then
            Variables.NumeroListSuivant = 4
            ' ==================================
            ' Arrêt du timer de la procédure d'apparition des listes
            Timer2.Enabled = False
            ' Arrêt du timer de la procédure d'apparition des listes
            Timer3.Enabled = False
            ' ==================================
            ' Déclenchement du retrait du sélecteur
            Timer4.Enabled = True
            Exit Sub
        End If
        ' ==================================
        ' Variable d'affichage du Listbox de la liste des sujets
        Variables.NumeroList = 4
        ' ==================================
        ' Déclenchement du timer de la procédure d'apparition des listes
        Timer2.Enabled = True
    End Sub

    '=============================================================================================================================================
    ' Sélection de la liste Unité Sonde
    Private Sub TextBox15_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox15.MouseClick
        ' ==================================
        ' Traitement pour la liste des sujets si une autre liste est en cours
        If Variables.NumeroList <> 0 Then
            Variables.NumeroListSuivant = 6
            ' ==================================
            ' Arrêt du timer de la procédure d'apparition des listes
            Timer2.Enabled = False
            ' Arrêt du timer de la procédure d'apparition des listes
            Timer3.Enabled = False
            ' ==================================
            ' Déclenchement du retrait du sélecteur
            Timer4.Enabled = True
            Exit Sub
        End If
        ' ==================================
        ' Variable d'affichage du Listbox de la liste des sujets
        Variables.NumeroList = 6
        ' ==================================
        ' Déclenchement du timer de la procédure d'apparition des listes
        Timer2.Enabled = True
    End Sub

    '=============================================================================================================================================
    ' Sélection de la liste Positionnement Passage
    Private Sub TextBox19_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox19.MouseClick
        ' ==================================
        ' Traitement pour la liste des sujets si une autre liste est en cours
        If Variables.NumeroList <> 0 Then
            Variables.NumeroListSuivant = 7
            ' ==================================
            ' Arrêt du timer de la procédure d'apparition des listes
            Timer2.Enabled = False
            ' Arrêt du timer de la procédure d'apparition des listes
            Timer3.Enabled = False
            ' ==================================
            ' Déclenchement du retrait du sélecteur
            Timer4.Enabled = True
            Exit Sub
        End If
        ' ==================================
        ' Variable d'affichage du Listbox de la liste des sujets
        Variables.NumeroList = 7
        ' ==================================
        ' Déclenchement du timer de la procédure d'apparition des listes
        Timer2.Enabled = True
    End Sub

    '=============================================================================================================================================
    ' Sélection de la liste des Dénominations des Saisisseurs
    Private Sub TextBox23_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox23.MouseClick
        ' ==================================
        ' Vérification si le texte du Libellé est vide
        '       If TextBox8.Text = "" Then
        ' ==================================
        ' Traitement pour la liste des sujets si une autre liste est en cours
        If Variables.NumeroList <> 0 Then
            Variables.NumeroListSuivant = 1
            ' ==================================
            ' Arrêt du timer de la procédure d'apparition des listes
            Timer2.Enabled = False
            ' Arrêt du timer de la procédure d'apparition des listes
            Timer3.Enabled = False
            ' ==================================
            ' Déclenchement du retrait du sélecteur
            Timer4.Enabled = True
            Exit Sub
        End If
        ' ==================================
        ' Variable d'affichage du Listbox de la liste des sujets
        Variables.NumeroList = 1
        ' ==================================
        ' Déclenchement du timer de la procédure d'apparition des listes
        Timer2.Enabled = True



        '        Else

        ' ==================================
        ' Affectation de la Dénomination, éventuellement suivante, en fonction du Libellé


        ' Vérification du nombre de Dénominations dans le tableau





        '       End If
    End Sub

    '=============================================================================================================================================
    ' Sélection de la liste des Dénominations des Lieux de Surveillance
    Private Sub TextBox24_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox24.MouseClick
        ' ==================================
        ' Vérification si le texte du Libellé est vide
        '        If TextBox10.Text = "" Then
        ' ==================================
        ' Traitement pour la liste des sujets si une autre liste est en cours
        If Variables.NumeroList <> 0 Then
            Variables.NumeroListSuivant = 2
            ' ==================================
            ' Arrêt du timer de la procédure d'apparition des listes
            Timer2.Enabled = False
            ' Arrêt du timer de la procédure d'apparition des listes
            Timer3.Enabled = False
            ' ==================================
            ' Déclenchement du retrait du sélecteur
            Timer4.Enabled = True
            Exit Sub
        End If
        ' ==================================
        ' Variable d'affichage du Listbox de la liste des sujets
        Variables.NumeroList = 2
        ' ==================================
        ' Déclenchement du timer de la procédure d'apparition des listes
        Timer2.Enabled = True
        '       End If
    End Sub

    '=============================================================================================================================================
    ' Déclenche la sortie d'un sélecteur
    Private Sub Timer2_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer2.Tick
        ' ==================================
        ' Procédure lorsque le sélecteur de la liste des Dénominations des Saisisseurs est arrivé à sa position
        If Variables.NumeroList = 1 And Panel1.Top >= 0 Then
            ' ==================================
            ' Arrêt du timer
            Timer2.Enabled = False
            ' ==================================
            ' Déclenchement du timer de la temporisation de lecture
            Timer3.Enabled = True
            ' ==================================
            ' Sortie
            Exit Sub
        End If
        ' ==================================
        ' Procédure lorsque le sélecteur de la liste des Dénominations des Départements est arrivé à sa position
        If Variables.NumeroList = 2 And Panel2.Top >= 0 Then
            ' ==================================
            ' Arrêt du timer
            Timer2.Enabled = False
            ' ==================================
            ' Déclenchement du timer de la temporisation de lecture
            Timer3.Enabled = True
            ' ==================================
            ' Sortie
            Exit Sub
        End If

        ' ==================================
        ' Procédure lorsque le sélecteur de la liste des Libellés des Saisisseurs est arrivé à sa position
        If Variables.NumeroList = 3 And Panel3.Top >= 0 Then
            ' ==================================
            ' Arrêt du timer
            Timer2.Enabled = False
            ' ==================================
            ' Déclenchement du timer de la temporisation de lecture
            Timer3.Enabled = True
            ' ==================================
            ' Sortie
            Exit Sub
        End If
        ' ==================================
        ' Procédure lorsque le sélecteur de la liste des Libellés des Départements est arrivé à sa position
        If Variables.NumeroList = 4 And Panel4.Top >= 0 Then
            ' ==================================
            ' Arrêt du timer
            Timer2.Enabled = False
            ' ==================================
            ' Déclenchement du timer de la temporisation de lecture
            Timer3.Enabled = True
            ' ==================================
            ' Sortie
            Exit Sub
        End If
        ' ==================================
        ' Procédure lorsque le sélecteur de la liste des Réseaux est arrivé à sa position
        If Variables.NumeroList = 5 And Panel5.Top >= 0 Then
            ' ==================================
            ' Arrêt du timer
            Timer2.Enabled = False
            ' ==================================
            ' Déclenchement du timer de la temporisation de lecture
            Timer3.Enabled = True
            ' ==================================
            ' Sortie
            Exit Sub
        End If
        ' ==================================
        ' Procédure lorsque le sélecteur de la liste Unité Sonde est arrivé à sa position
        If Variables.NumeroList = 6 And Panel6.Top >= 0 Then
            ' ==================================
            ' Arrêt du timer
            Timer2.Enabled = False
            ' ==================================
            ' Déclenchement du timer de la temporisation de lecture
            Timer3.Enabled = True
            ' ==================================
            ' Sortie
            Exit Sub
        End If
        ' ==================================
        ' Procédure lorsque le sélecteur de la liste Positionnement Passage est arrivé à sa position
        If Variables.NumeroList = 7 And Panel7.Top >= 0 Then
            ' ==================================
            ' Arrêt du timer
            Timer2.Enabled = False
            ' ==================================
            ' Déclenchement du timer de la temporisation de lecture
            Timer3.Enabled = True
            ' ==================================
            ' Sortie
            Exit Sub
        End If


        ' ==================================
        ' Incrémentation du déplacement du sélecteur de la liste des Dénominations des Saisisseurs
        If Variables.NumeroList = 1 Then Panel1.Top = Panel1.Top + 2
        ' ==================================
        ' Incrémentation du déplacement du sélecteur de la liste des Dénominations des Départements
        If Variables.NumeroList = 2 Then Panel2.Top = Panel2.Top + 2

        ' ==================================
        ' Incrémentation du déplacement du sélecteur de la liste des Libellés des Saisisseurs
        If Variables.NumeroList = 3 Then Panel3.Top = Panel3.Top + 2
        ' ==================================
        ' Incrémentation du déplacement du sélecteur de la liste des Libellés des Départements
        If Variables.NumeroList = 4 Then Panel4.Top = Panel4.Top + 2
        ' ==================================
        ' Incrémentation du déplacement du sélecteur de la liste des Réseaux
        If Variables.NumeroList = 5 Then Panel5.Top = Panel5.Top + 2
        ' ==================================
        ' Incrémentation du déplacement du sélecteur de la liste Unité Sonde
        If Variables.NumeroList = 6 Then Panel6.Top = Panel6.Top + 2
        ' ==================================
        ' Incrémentation du déplacement du sélecteur de la liste Positionnement Passage
        If Variables.NumeroList = 7 Then Panel7.Top = Panel7.Top + 2

    End Sub

    ' ==============================================================================================================================================
    ' Déclenche l'attente de lecture et l'affichage de la liste
    Private Sub Timer3_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer3.Tick
        ' ==================================
        ' Incrémentation du déplacement du sélecteur de la liste des Dénominations des Saisisseurs
        If Variables.NumeroList = 1 Then
            Panel1.Height = Panel1.Height + 16
            ' ==================================
            ' Apparition de la liste correspondante
            ListBox1.Visible = True
        End If
        ' ==================================
        ' Incrémentation du déplacement du sélecteur de la liste des Dénominations des Départements
        If Variables.NumeroList = 2 Then
            Panel2.Height = Panel2.Height + 16
            ' ==================================
            ' Apparition de la liste correspondante
            ListBox2.Visible = True
        End If

        ' ==================================
        ' Incrémentation du déplacement du sélecteur de la liste des Libellés des Saisisseurs
        If Variables.NumeroList = 3 Then
            Panel3.Height = Panel3.Height + 16
            ' ==================================
            ' Apparition de la liste correspondante
            ListBox3.Visible = True
        End If
        ' ==================================
        ' Incrémentation du déplacement du sélecteur de la liste des Libellés des Départements
        If Variables.NumeroList = 4 Then
            Panel4.Height = Panel4.Height + 16
            ' ==================================
            ' Apparition de la liste correspondante
            ListBox4.Visible = True
        End If
        ' ==================================
        ' Incrémentation du déplacement du sélecteur de la liste des Réseaux
        If Variables.NumeroList = 5 Then
            Panel5.Height = Panel5.Height + 16
            ' ==================================
            ' Apparition de la liste correspondante
            ListBox5.Visible = True
        End If
        ' ==================================
        ' Incrémentation du déplacement du sélecteur de la liste Unité Sonde
        If Variables.NumeroList = 6 Then
            Panel6.Height = Panel6.Height + 16
            ' ==================================
            ' Apparition de la liste correspondante
            ListBox6.Visible = True
        End If
        ' ==================================
        ' Incrémentation du déplacement du sélecteur de la liste Positionnement Passage
        If Variables.NumeroList = 7 Then
            Panel7.Height = Panel7.Height + 16
            ' ==================================
            ' Apparition de la liste correspondante
            ListBox7.Visible = True
        End If



        ' ==================================
        ' Vérification de la fin du déplacement du sélecteur de la liste des Dénominations des Saisisseurs
        If Variables.NumeroList = 1 And Panel1.Height >= 430 Then
            ' ==================================
            ' Arrête du timer de temporisation de lecture
            Timer3.Enabled = False
            ' ==================================
            ' Déclenchement du timer de déclenchement de la procédure de retrait
            Timer6.Enabled = True
            ' ==================================
            ' Déclenchement du timer de retrait automatique
            Timer7.Enabled = True
        End If
        ' ==================================
        ' Vérification de la fin du déplacement du sélecteur de la liste des Dénominations des Départements
        If Variables.NumeroList = 2 And Panel2.Height >= 430 Then
            '''''''''''''' Panel2.Height = 168
            ' ==================================
            ' Arrête du timer de temporisation de lecture
            Timer3.Enabled = False
            ' ==================================
            ' Déclenchement du timer de déclenchement de la procédure de retrait
            Timer6.Enabled = True
            ' ==================================
            ' Déclenchement du timer de retrait automatique
            Timer7.Enabled = True
        End If
        ' ==================================
        ' Vérification de la fin du déplacement du sélecteur de la liste des Libellés des Saisisseurs
        If Variables.NumeroList = 3 And Panel3.Height >= 430 Then
            ' ==================================
            ' Arrête du timer de temporisation de lecture
            Timer3.Enabled = False
            ' ==================================
            ' Déclenchement du timer de déclenchement de la procédure de retrait
            Timer6.Enabled = True
            ' ==================================
            ' Déclenchement du timer de retrait automatique
            Timer7.Enabled = True
        End If
        ' ==================================
        ' Vérification de la fin du déplacement du sélecteur de la liste des Libellés des Départements
        If Variables.NumeroList = 4 And Panel4.Height >= 430 Then
            '''''''''''' Panel2.Height = 168
            ' ==================================
            ' Arrête du timer de temporisation de lecture
            Timer3.Enabled = False
            ' ==================================
            ' Déclenchement du timer de déclenchement de la procédure de retrait
            Timer6.Enabled = True
            ' ==================================
            ' Déclenchement du timer de retrait automatique
            Timer7.Enabled = True
        End If
        ' ==================================
        ' Vérification de la fin du déplacement du sélecteur de la liste des Réseaux
        If Variables.NumeroList = 5 And Panel5.Height >= 289 Then
            ' ==================================
            ' Arrête du timer de temporisation de lecture
            Timer3.Enabled = False
            ' ==================================
            ' Déclenchement du timer de déclenchement de la procédure de retrait
            Timer6.Enabled = True
            ' ==================================
            ' Déclenchement du timer de retrait automatique
            Timer7.Enabled = True
        End If
        ' ==================================
        ' Vérification de la fin du déplacement du sélecteur de la liste Unité Sonde
        If Variables.NumeroList = 6 And Panel6.Height >= 289 Then
            ' ==================================
            ' Arrête du timer de temporisation de lecture
            Timer3.Enabled = False
            ' ==================================
            ' Déclenchement du timer de déclenchement de la procédure de retrait
            Timer6.Enabled = True
            ' ==================================
            ' Déclenchement du timer de retrait automatique
            Timer7.Enabled = True
        End If
        ' ==================================
        ' Vérification de la fin du déplacement du sélecteur de la liste Positionnement Passage
        If Variables.NumeroList = 7 And Panel7.Height >= 289 Then
            ' ==================================
            ' Arrête du timer de temporisation de lecture
            Timer3.Enabled = False
            ' ==================================
            ' Déclenchement du timer de déclenchement de la procédure de retrait
            Timer6.Enabled = True
            ' ==================================
            ' Déclenchement du timer de retrait automatique
            Timer7.Enabled = True
        End If

    End Sub


    '=============================================================================================================================================
    ' PROCÉDURES DES DISPARITION DES SÉLECTEURS
    '=============================================================================================================================================
    ' Déclenche la disparition du sélecteur
    Private Sub Timer4_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer4.Tick
        ' ==================================
        ' Décrémentation du déplacement du sélecteur de la liste des Dénominations des Saisisseurs
        If Variables.NumeroList = 1 And Panel1.Height > 50 Then
            Panel1.Height = Panel1.Height - 16
        End If
        ' ==================================
        ' Décrémentation du déplacement du sélecteur de la liste des Dénominations des Départements
        If Variables.NumeroList = 2 And Panel2.Height > 50 Then
            Panel2.Height = Panel2.Height - 16
        End If

        ' ==================================
        ' Décrémentation du déplacement du sélecteur de la liste des Libellés des Saisisseurs
        If Variables.NumeroList = 3 And Panel3.Height > 50 Then
            Panel3.Height = Panel3.Height - 16
        End If
        ' ==================================
        ' Décrémentation du déplacement du sélecteur de la liste des Libellés des Départements
        If Variables.NumeroList = 4 And Panel4.Height > 50 Then
            Panel4.Height = Panel4.Height - 16
        End If
        ' ==================================
        ' Décrémentation du déplacement du sélecteur de la liste des Réseaux
        If Variables.NumeroList = 5 And Panel5.Height > 50 Then
            Panel5.Height = Panel5.Height - 16
        End If
        ' ==================================
        ' Décrémentation du déplacement du sélecteur de la liste Unité Sonde
        If Variables.NumeroList = 6 And Panel6.Height > 50 Then
            Panel6.Height = Panel6.Height - 16
        End If
        ' ==================================
        ' Décrémentation du déplacement du sélecteur de la liste Positionnement Passage
        If Variables.NumeroList = 7 And Panel7.Height > 50 Then
            Panel7.Height = Panel7.Height - 16
        End If



        ' ==================================
        ' Vérification de la fin du déplacement du sélecteur de la liste des Dénominations des Saisisseurs
        If Variables.NumeroList = 1 And Panel1.Height <= 50 Then
            ' ==================================
            ' Retrait de la liste correspondante
            ListBox1.Visible = False
            ' ==================================
            ' Arrêt du timer
            Timer4.Enabled = False
            ' Déclenchement du timer de retrait du conteneur de la liste correspondante et lancement éventuel d'une liste demandée
            Timer5.Enabled = True
        End If
        ' ==================================
        ' Vérification de la fin du déplacement du sélecteur de la liste des Dénominations des Départements
        If Variables.NumeroList = 2 And Panel2.Height <= 50 Then
            ' ==================================
            ' Retrait de la liste correspondante
            ListBox2.Visible = False
            ' ==================================
            ' Ajustement du conteneur
            ''''''''''''''''' Panel2.Height = 50
            ' ==================================
            ' Arrêt du timer
            Timer4.Enabled = False
            ' Déclenchement du timer de retrait du conteneur de la liste correspondante et lancement éventuel d'une liste demandée
            Timer5.Enabled = True
        End If

        ' ==================================
        ' Vérification de la fin du déplacement du sélecteur de la liste des Libellés des Saisisseurs
        If Variables.NumeroList = 3 And Panel3.Height <= 50 Then
            ' ==================================
            ' Retrait de la liste correspondante
            ListBox3.Visible = False
            ' ==================================
            ' Arrêt du timer
            Timer4.Enabled = False
            ' Déclenchement du timer de retrait du conteneur de la liste correspondante et lancement éventuel d'une liste demandée
            Timer5.Enabled = True
        End If
        ' ==================================
        ' Vérification de la fin du déplacement du sélecteur de la liste des Libellés des Départements
        If Variables.NumeroList = 4 And Panel4.Height <= 50 Then
            ' ==================================
            ' Retrait de la liste correspondante
            ListBox4.Visible = False
            ' ==================================
            ' Arrêt du timer
            Timer4.Enabled = False
            ' Déclenchement du timer de retrait du conteneur de la liste correspondante et lancement éventuel d'une liste demandée
            Timer5.Enabled = True
        End If
        ' ==================================
        ' Vérification de la fin du déplacement du sélecteur de la liste des Réseaux
        If Variables.NumeroList = 5 And Panel5.Height <= 50 Then
            ' ==================================
            ' Retrait de la liste correspondante
            ListBox5.Visible = False
            ' ==================================
            ' Arrêt du timer
            Timer4.Enabled = False
            ' Déclenchement du timer de retrait du conteneur de la liste correspondante et lancement éventuel d'une liste demandée
            Timer5.Enabled = True
        End If
        ' ==================================
        ' Vérification de la fin du déplacement du sélecteur de la liste Unité Sonde
        If Variables.NumeroList = 6 And Panel6.Height <= 50 Then
            ' ==================================
            ' Retrait de la liste correspondante
            ListBox6.Visible = False
            ' ==================================
            ' Arrêt du timer
            Timer4.Enabled = False
            ' Déclenchement du timer de retrait du conteneur de la liste correspondante et lancement éventuel d'une liste demandée
            Timer5.Enabled = True
        End If
        ' ==================================
        ' Vérification de la fin du déplacement du sélecteur de la liste Positionnement Passage
        If Variables.NumeroList = 7 And Panel7.Height <= 50 Then
            ' ==================================
            ' Retrait de la liste correspondante
            ListBox7.Visible = False
            ' ==================================
            ' Arrêt du timer
            Timer4.Enabled = False
            ' Déclenchement du timer de retrait du conteneur de la liste correspondante et lancement éventuel d'une liste demandée
            Timer5.Enabled = True
        End If

    End Sub

    ' ==============================================================================================================================================
    ' Timer de retrait du conteneur de la liste correspondante et lancement éventuel d'une liste demandée
    Private Sub Timer5_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer5.Tick
        ' ==================================
        ' Décrémentation du déplacement du sélecteur de la liste des Dénominations des Saisisseurs
        If Variables.NumeroList = 1 Then Panel1.Top = Panel1.Top - 2
        ' ==================================
        ' Décrémentation du déplacement du sélecteur de la liste des Dénominations des Départements
        If Variables.NumeroList = 2 Then Panel2.Top = Panel2.Top - 2

        ' ==================================
        ' Décrémentation du déplacement du sélecteur de la liste des Libellés des Saisisseurs
        If Variables.NumeroList = 3 Then Panel3.Top = Panel3.Top - 2
        ' ==================================
        ' Décrémentation du déplacement du sélecteur de la liste des Libellés des Départements
        If Variables.NumeroList = 4 Then Panel4.Top = Panel4.Top - 2
        ' ==================================
        ' Décrémentation du déplacement du sélecteur de la liste des Réseaux
        If Variables.NumeroList = 5 Then Panel5.Top = Panel5.Top - 2
        ' ==================================
        ' Décrémentation du déplacement du sélecteur de la liste Unité Sonde
        If Variables.NumeroList = 6 Then Panel6.Top = Panel6.Top - 2
        ' ==================================
        ' Décrémentation du déplacement du sélecteur de la liste Positionnement Passage
        If Variables.NumeroList = 7 Then Panel7.Top = Panel7.Top - 2

        ' ==================================
        ' Traitement pour la liste des Dénominations des Saisisseurs
        If Variables.NumeroList = 1 And Panel1.Top <= -50 Then
            ' ==================================
            ' Remise à zéro de la variable de numéro de la liste
            Variables.NumeroList = 0
            ' ==================================
            ' Arrêt du timer
            Timer5.Enabled = False
            ' ==================================
            ' Arrêt du timer de déclenchement de la procédure de retrait
            Timer6.Enabled = False
            ' ==================================
            ' Vérification si l'apparition d'une liste a été demandée
            If Variables.NumeroListSuivant <> 0 Then
                ' ==================================
                ' Transfert du numéro de la liste suivante dans celui de la liste
                Variables.NumeroList = Variables.NumeroListSuivant
                ' ==================================
                ' Remise à zéro de la variable de numéro de la liste suivante
                Variables.NumeroListSuivant = 0
                ' ==================================
                ' Déclenchement du timer de la procédure d'apparition des listes
                Timer2.Enabled = True
            End If
            Exit Sub
        End If
        ' ==================================
        ' Traitement pour la liste des Dénominations des Départements
        If Variables.NumeroList = 2 And Panel2.Top <= -50 Then
            ' ==================================
            ' Remise à zéro de la variable de numéro de la liste
            Variables.NumeroList = 0
            ' ==================================
            ' Arrêt du timer
            Timer5.Enabled = False
            ' ==================================
            ' Arrêt du timer de déclenchement de la procédure de retrait
            Timer6.Enabled = False
            ' ==================================
            ' Vérification si l'apparition d'une liste a été demandée
            If Variables.NumeroListSuivant <> 0 Then
                ' ==================================
                ' Transfert du numéro de la liste suivante dans celui de la liste
                Variables.NumeroList = Variables.NumeroListSuivant
                ' ==================================
                ' Remise à zéro de la variable de numéro de la liste suivante
                Variables.NumeroListSuivant = 0
                ' ==================================
                ' Déclenchement du timer de la procédure d'apparition des listes
                Timer2.Enabled = True
            End If
            Exit Sub
        End If


        ' ==================================
        ' Traitement pour la liste des Libellés des Saisisseurs
        If Variables.NumeroList = 3 And Panel3.Top <= -50 Then
            ' ==================================
            ' Remise à zéro de la variable de numéro de la liste
            Variables.NumeroList = 0
            ' ==================================
            ' Arrêt du timer
            Timer5.Enabled = False
            ' ==================================
            ' Arrêt du timer de déclenchement de la procédure de retrait
            Timer6.Enabled = False
            ' ==================================
            ' Vérification si l'apparition d'une liste a été demandée
            If Variables.NumeroListSuivant <> 0 Then
                ' ==================================
                ' Transfert du numéro de la liste suivante dans celui de la liste
                Variables.NumeroList = Variables.NumeroListSuivant
                ' ==================================
                ' Remise à zéro de la variable de numéro de la liste suivante
                Variables.NumeroListSuivant = 0
                ' ==================================
                ' Déclenchement du timer de la procédure d'apparition des listes
                Timer2.Enabled = True
            End If
            Exit Sub
        End If
        ' ==================================
        ' Traitement pour la liste des Libellés des Départements
        If Variables.NumeroList = 4 And Panel4.Top <= -50 Then
            ' ==================================
            ' Remise à zéro de la variable de numéro de la liste
            Variables.NumeroList = 0
            ' ==================================
            ' Arrêt du timer
            Timer5.Enabled = False
            ' ==================================
            ' Arrêt du timer de déclenchement de la procédure de retrait
            Timer6.Enabled = False
            ' ==================================
            ' Vérification si l'apparition d'une liste a été demandée
            If Variables.NumeroListSuivant <> 0 Then
                ' ==================================
                ' Transfert du numéro de la liste suivante dans celui de la liste
                Variables.NumeroList = Variables.NumeroListSuivant
                ' ==================================
                ' Remise à zéro de la variable de numéro de la liste suivante
                Variables.NumeroListSuivant = 0
                ' ==================================
                ' Déclenchement du timer de la procédure d'apparition des listes
                Timer2.Enabled = True
            End If
            Exit Sub
        End If
        ' ==================================
        ' Traitement pour la liste des Réseaux
        If Variables.NumeroList = 5 And Panel5.Top <= -50 Then
            ' ==================================
            ' Remise à zéro de la variable de numéro de la liste
            Variables.NumeroList = 0
            ' ==================================
            ' Arrêt du timer
            Timer5.Enabled = False
            ' ==================================
            ' Arrêt du timer de déclenchement de la procédure de retrait
            Timer6.Enabled = False
            ' ==================================
            ' Vérification si l'apparition d'une liste a été demandée
            If Variables.NumeroListSuivant <> 0 Then
                ' ==================================
                ' Transfert du numéro de la liste suivante dans celui de la liste
                Variables.NumeroList = Variables.NumeroListSuivant
                ' ==================================
                ' Remise à zéro de la variable de numéro de la liste suivante
                Variables.NumeroListSuivant = 0
                ' ==================================
                ' Déclenchement du timer de la procédure d'apparition des listes
                Timer2.Enabled = True
            End If
            Exit Sub
        End If
        ' ==================================
        ' Traitement pour la liste Unité Sonde
        If Variables.NumeroList = 6 And Panel6.Top <= -50 Then
            ' ==================================
            ' Remise à zéro de la variable de numéro de la liste
            Variables.NumeroList = 0
            ' ==================================
            ' Arrêt du timer
            Timer5.Enabled = False
            ' ==================================
            ' Arrêt du timer de déclenchement de la procédure de retrait
            Timer6.Enabled = False
            ' ==================================
            ' Vérification si l'apparition d'une liste a été demandée
            If Variables.NumeroListSuivant <> 0 Then
                ' ==================================
                ' Transfert du numéro de la liste suivante dans celui de la liste
                Variables.NumeroList = Variables.NumeroListSuivant
                ' ==================================
                ' Remise à zéro de la variable de numéro de la liste suivante
                Variables.NumeroListSuivant = 0
                ' ==================================
                ' Déclenchement du timer de la procédure d'apparition des listes
                Timer2.Enabled = True
            End If
            Exit Sub
        End If
        ' ==================================
        ' Traitement pour la liste Positionnement Passage
        If Variables.NumeroList = 7 And Panel7.Top <= -50 Then
            ' ==================================
            ' Remise à zéro de la variable de numéro de la liste
            Variables.NumeroList = 0
            ' ==================================
            ' Arrêt du timer
            Timer5.Enabled = False
            ' ==================================
            ' Arrêt du timer de déclenchement de la procédure de retrait
            Timer6.Enabled = False
            ' ==================================
            ' Vérification si l'apparition d'une liste a été demandée
            If Variables.NumeroListSuivant <> 0 Then
                ' ==================================
                ' Transfert du numéro de la liste suivante dans celui de la liste
                Variables.NumeroList = Variables.NumeroListSuivant
                ' ==================================
                ' Remise à zéro de la variable de numéro de la liste suivante
                Variables.NumeroListSuivant = 0
                ' ==================================
                ' Déclenchement du timer de la procédure d'apparition des listes
                Timer2.Enabled = True
            End If
            Exit Sub
        End If


    End Sub

    ' ==============================================================================================================================================
    ' Timer de déclenchement de la procédure de retrait de la liste affichée
    Private Sub Timer6_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer6.Tick
        ' ==================================
        ' Déclenchement du timer de la procédure de retrait
        Timer4.Enabled = True
        ' ==================================
        ' Arrêt du timer
        Timer6.Enabled = False
        ' ==================================
        ' Arrêt du timer de retrait automatique
        Timer7.Enabled = False
    End Sub

    ' ==============================================================================================================================================
    ' Timer de retrait automatique lorsque le curseur n'est pas sur la zone correspondante
    Private Sub Timer7_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer7.Tick

        '    TextBox3.Text = "X " & MousePosition.X - Me.Left
        '    TextBox8.Text = "Y " & MousePosition.Y - Me.Top

        ' ==================================
        ' Traitement pour la liste des Dénominations des Saisisseurs
        If Variables.NumeroList = 1 Then
            ' ==================================
            ' Vérification si le curseur est dans la zone correspondante
            If (MousePosition.X - Me.Left) > 152 And (MousePosition.X - Me.Left) < 695 And (MousePosition.Y - Me.Top) > 30 And (MousePosition.Y - Me.Top) < 463 Then
                ' ==================================
                ' Arrêt du timer de déclenchement de la procédure de retrait
                Timer6.Enabled = False
                ' ==================================
                ' Sortie
                Exit Sub
            End If
        End If
        ' ==================================
        ' Traitement pour la liste des Dénominations des Départements
        If Variables.NumeroList = 2 Then
            ' ==================================
            ' Vérification si le curseur est dans la zone correspondante
            If (MousePosition.X - Me.Left) > 182 And (MousePosition.X - Me.Left) < 663 And (MousePosition.Y - Me.Top) > 30 And (MousePosition.Y - Me.Top) < 463 Then
                ' ==================================
                ' Arrêt du timer de déclenchement de la procédure de retrait
                Timer6.Enabled = False
                ' ==================================
                ' Sortie
                Exit Sub
            End If
        End If

        ' ==================================
        ' Traitement pour la liste des Libellés des Saisisseurs
        If Variables.NumeroList = 3 Then
            ' ==================================
            ' Vérification si le curseur est dans la zone correspondante
            If (MousePosition.X - Me.Left) > 292 And (MousePosition.X - Me.Left) < 553 And (MousePosition.Y - Me.Top) > 30 And (MousePosition.Y - Me.Top) < 463 Then
                ' ==================================
                ' Arrêt du timer de déclenchement de la procédure de retrait
                Timer6.Enabled = False
                ' ==================================
                ' Sortie
                Exit Sub
            End If
        End If
        ' ==================================
        ' Traitement pour la liste des Numéros des Départements
        If Variables.NumeroList = 4 Then
            ' ==================================
            ' Vérification si le curseur est dans la zone correspondante
            If (MousePosition.X - Me.Left) > 322 And (MousePosition.X - Me.Left) < 523 And (MousePosition.Y - Me.Top) > 30 And (MousePosition.Y - Me.Top) < 463 Then
                ' ==================================
                ' Arrêt du timer de déclenchement de la procédure de retrait
                Timer6.Enabled = False
                ' ==================================
                ' Sortie
                Exit Sub
            End If
        End If
        ' ==================================
        ' Traitement pour la liste des Libellés des Réseaux
        If Variables.NumeroList = 5 Then
            ' ==================================
            ' Vérification si le curseur est dans la zone correspondante
            If (MousePosition.X - Me.Left) > 322 And (MousePosition.X - Me.Left) < 523 And (MousePosition.Y - Me.Top) > 30 And (MousePosition.Y - Me.Top) < 319 Then
                ' ==================================
                ' Arrêt du timer de déclenchement de la procédure de retrait
                Timer6.Enabled = False
                ' ==================================
                ' Sortie
                Exit Sub
            End If
        End If
        ' ==================================
        ' Traitement pour la liste des Libellés des Unité Sonde
        If Variables.NumeroList = 6 Then
            ' ==================================
            ' Vérification si le curseur est dans la zone correspondante
            If (MousePosition.X - Me.Left) > 322 And (MousePosition.X - Me.Left) < 523 And (MousePosition.Y - Me.Top) > 30 And (MousePosition.Y - Me.Top) < 319 Then
                ' ==================================
                ' Arrêt du timer de déclenchement de la procédure de retrait
                Timer6.Enabled = False
                ' ==================================
                ' Sortie
                Exit Sub
            End If
        End If
        ' ==================================
        ' Traitement pour la liste des Libellés des Positionnement Passage
        If Variables.NumeroList = 7 Then
            ' ==================================
            ' Vérification si le curseur est dans la zone correspondante
            If (MousePosition.X - Me.Left) > 152 And (MousePosition.X - Me.Left) < 695 And (MousePosition.Y - Me.Top) > 30 And (MousePosition.Y - Me.Top) < 319 Then
                ' ==================================
                ' Arrêt du timer de déclenchement de la procédure de retrait
                Timer6.Enabled = False
                ' ==================================
                ' Sortie
                Exit Sub
            End If
        End If


        ' ==================================
        ' Déclenchement du timer de déclenchement de la procédure de retrait
        Timer6.Enabled = True
    End Sub

    '=============================================================================================================================================
    ' PROCÉDURES DE SÉLECTION DANS LES LISTES
    '=============================================================================================================================================
    ' Sélection d'une dénomination de Saisisseur
    Private Sub ListBox1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ListBox1.SelectedIndexChanged
        ' ==================================
        ' Sortie si sélection vide
        If ListBox1.SelectedItem = "" Then Exit Sub
        ' ==================================
        Dim IndexTabInfo As Integer
        ' ==================================
        ' Arrêt du timer de temporisation de lecture pour éviter un conflit lorsque la liste se déroule et que l'on a cliqué sur une sélection
        Timer3.Enabled = False

        ' ==================================
        ' Transfert de la sélection
        If ListBox1.SelectedItem = " - Effacement du champ -" Then
            TextBox8.Text = ""
            TextBox23.Text = ""
        Else
            TextBox23.Text = Strings.LTrim(ListBox1.SelectedItem)
            ' ==================================
            ' Recherche du point d'entrée dans le tableau des informations pour les Saisisseurs
            For i = 0 To 5000
                If Variables.TabRéfInfo(i, 0) = "SAISISSEUR" Then
                    IndexTabInfo = i + 1
                    Exit For
                End If
            Next
            TextBox8.Text = ""
            ' ==================================
            ' Récupération du libellé du Saisisseur
            For i = IndexTabInfo To 5000
                ' ==================================
                ' Sortie de la boucle si fin de tableau
                If Variables.TabRéfInfo(i, 0) = "" Then Exit For
                ' ==================================
                ' Rangement du libellé  du saisisseur
                If Variables.TabRéfInfo(i, 0) = TextBox23.Text Then
                    ' ==================================
                    ' Affectation de la désignation
                    TextBox8.Text = Variables.TabRéfInfo(i, 1)
                End If
            Next
        End If

        ' ==================================
        ' Déclenchement du retrait du sélecteur
        Timer4.Enabled = True
        ' ==================================
        ' Arrêt du timer de retrait automatique
        Timer7.Enabled = False
        ' ==================================
        ' Désélection 
        ListBox1.ClearSelected()
    End Sub

    '=============================================================================================================================================
    ' Sélection d'une dénomination des Lieux de surveillance
    Private Sub ListBox2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ListBox2.SelectedIndexChanged
        ' ==================================
        ' Sortie si sélection vide
        If ListBox2.SelectedItem = "" Then Exit Sub
        ' ==================================
        Dim IndexTabInfo As Integer
        ' ==================================
        ' Arrêt du timer de temporisation de lecture pour éviter un conflit lorsque la liste se déroule et que l'on a cliqué sur une sélection
        Timer3.Enabled = False
        ' ==================================
        ' Transfert de la sélection
        If ListBox2.SelectedItem = " - Effacement du champ -" Then
            TextBox10.Text = ""
            TextBox24.Text = ""
        Else
            TextBox24.Text = Strings.LTrim(ListBox2.SelectedItem)
        End If
        ' ==================================
        ' Recherche du point d'entrée dans le tableau des informations pour les Lieux de Surveillance
        For i = 0 To 5000
            If Variables.TabRéfInfo(i, 1) = "LIEU DE SURVEILLANCE" Then
                IndexTabInfo = i + 1
                Exit For
            End If
        Next
        ' ==================================
        ' Récupération du numéro des Départements
        For i = IndexTabInfo To 5000
            If Variables.TabRéfInfo(i, 1) = TextBox24.Text Then
                ' ==================================
                ' Affectation du numéro de département
                TextBox10.Text = Variables.TabRéfInfo(i, 0)
                ' ==================================
                ' Affectation latitude et longitude et Position du Passage
                TextBox1.Text = Variables.TabRéfInfo(i, 4)
                TextBox2.Text = Variables.TabRéfInfo(i, 3)
                TextBox19.Text = Variables.TabRéfInfo(i, 5)
                Exit For
            End If
        Next

        ' ==================================
        ' Déclenchement du retrait du sélecteur
        Timer4.Enabled = True
        ' ==================================
        ' Arrêt du timer de retrait automatique
        Timer7.Enabled = False
        ' ==================================
        ' Désélection 
        ListBox2.ClearSelected()
    End Sub

    '=============================================================================================================================================
    ' Sélection d'un libellé de Saisisseur
    Private Sub ListBox3_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ListBox3.SelectedIndexChanged
        ' ==================================
        ' Sortie si sélection vide
        If ListBox3.SelectedItem = "" Then Exit Sub
        ' ==================================
        Dim IndexTabInfo As Integer
        ' ==================================
        ' Arrêt du timer de temporisation de lecture pour éviter un conflit lorsque la liste se déroule et que l'on a cliqué sur une sélection
        Timer3.Enabled = False
        ' ==================================
        ' Transfert de la sélection
        If ListBox3.SelectedItem = " - Effacement du champ -" Then
            TextBox8.Text = ""
            TextBox23.Text = ""
        Else
            TextBox8.Text = Strings.LTrim(ListBox3.SelectedItem)
            ' ==================================
            ' Recherche du point d'entrée dans le tableau des informations pour les Saisisseurs
            For i = 0 To 5000
                If Variables.TabRéfInfo(i, 0) = "SAISISSEUR" Then
                    IndexTabInfo = i + 1
                    Exit For
                End If
            Next
            TextBox23.Text = ""
            ' ==================================
            ' Récupération de la dénomination du Saisisseur
            For i = IndexTabInfo To 5000
                ' ==================================
                ' Sortie de la boucle si fin de tableau
                If Variables.TabRéfInfo(i, 0) = "" Then Exit For
                ' ==================================
                ' Rangement de la dénomination  du saisisseur
                If Variables.TabRéfInfo(i, 1) = TextBox8.Text Then
                    ' ==================================
                    ' Affectation de la désignation
                    TextBox23.Text = Variables.TabRéfInfo(i, 0)
                End If
            Next
        End If
        ' ==================================
        ' Déclenchement du retrait du sélecteur
        Timer4.Enabled = True
        ' ==================================
        ' Arrêt du timer de retrait automatique
        Timer7.Enabled = False
        ' ==================================
        ' Désélection 
        ListBox3.ClearSelected()
    End Sub

    '=============================================================================================================================================
    ' Sélection d'un numéro des Départements
    Private Sub ListBox4_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ListBox4.SelectedIndexChanged
        ' ==================================
        ' Sortie si sélection vide
        If ListBox4.SelectedItem = "" Then Exit Sub
        ' ==================================
        Dim IndexTabInfo As Integer
        ' ==================================
        ' Arrêt du timer de temporisation de lecture pour éviter un conflit lorsque la liste se déroule et que l'on a cliqué sur une sélection
        Timer3.Enabled = False
        ' ==================================
        ' Transfert de la sélection
        If ListBox4.SelectedItem = " - Effacement du champ -" Then
            TextBox10.Text = ""
            TextBox24.Text = ""
            TextBox1.Text = ""
            TextBox2.Text = ""
            TextBox19.Text = ""
            ' ==================================
            ' Renseignement de la liste 2 avec tous les lieux de surveillance
            ListBox2.Items.Clear()
            ' ==================================
            ' Recherche du point d'entrée dans le tableau des informations pour les Lieux de Surveillance
            For i = 0 To 5000
                If Variables.TabRéfInfo(i, 0) = "LIEU DE SURVEILLANCE" Then
                    IndexTabInfo = i + 1
                    Exit For
                End If
            Next
            ' ==================================
            ' Chargement des dénomination des Lieux de Surveillance dans la liste 2
            For i = IndexTabInfo To 5000
                ' ==================================
                ' Sortie de la boucle si fin de tableau
                If Variables.TabRéfInfo(i, 0) = "" Then Exit For
                ' ==================================
                ' Rangement des dénomination des Lieux de Surveillance dans la liste
                ListBox2.Items.Add(" " & Variables.TabRéfInfo(i, 1))
            Next

        Else
            TextBox10.Text = Strings.LTrim(ListBox4.SelectedItem)
            TextBox24.Text = ""
            TextBox1.Text = ""
            TextBox2.Text = ""
            TextBox19.Text = ""
            ' ==================================
            ' Renseignement de la liste 2 en fonction de la Dénomination des Départements
            ListBox2.Items.Clear()
            ' ==================================
            ' Recherche du point d'entrée dans le tableau des informations pour les Lieux de Surveillance
            For i = 0 To 5000
                If Variables.TabRéfInfo(i, 0) = "LIEU DE SURVEILLANCE" Then
                    IndexTabInfo = i + 1
                    Exit For
                End If
            Next
            ' ==================================
            ' Chargement des dénomination des Lieux de Surveillance du Département dans la liste 2
            For i = IndexTabInfo To 5000
                ' ==================================
                ' Sortie de la boucle si fin de tableau
                If Variables.TabRéfInfo(i, 0) = "" Then Exit For
                ' ==================================
                ' Vérification si le numéro de département correspond
                If Variables.TabRéfInfo(i, 0) = TextBox10.Text Then
                    ' ==================================
                    ' Affectation d'une désignation dans la liste
                    ListBox2.Items.Add(" " & Variables.TabRéfInfo(i, 1))
                End If
            Next

        End If
        ' ==================================
        ' Déclenchement du retrait du sélecteur
        Timer4.Enabled = True
        ' ==================================
        ' Arrêt du timer de retrait automatique
        Timer7.Enabled = False
        ' ==================================
        ' Désélection 
        ListBox4.ClearSelected()
    End Sub

    '=============================================================================================================================================
    ' Sélection d'un Réseau
    Private Sub ListBox5_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ListBox5.SelectedIndexChanged
        ' ==================================
        ' Sortie si sélection vide
        If ListBox5.SelectedItem = "" Then Exit Sub
        ' ==================================
        ' Arrêt du timer de temporisation de lecture pour éviter un conflit lorsque la liste se déroule et que l'on a cliqué sur une sélection
        Timer3.Enabled = False
        ' ==================================
        ' Transfert de la sélection
        If ListBox5.SelectedItem = " - Effacement du champ -" Then
            TextBox3.Text = ""
        Else
            TextBox3.Text = Strings.LTrim(ListBox5.SelectedItem)
        End If
        ' ==================================
        ' Déclenchement du retrait du sélecteur
        Timer4.Enabled = True
        ' ==================================
        ' Arrêt du timer de retrait automatique
        Timer7.Enabled = False
        ' ==================================
        ' Désélection 
        ListBox5.ClearSelected()
    End Sub

    '=============================================================================================================================================
    ' Sélection d'une Unité Sonde
    Private Sub ListBox6_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ListBox6.SelectedIndexChanged
        ' ==================================
        ' Sortie si sélection vide
        If ListBox6.SelectedItem = "" Then Exit Sub
        ' ==================================
        ' Arrêt du timer de temporisation de lecture pour éviter un conflit lorsque la liste se déroule et que l'on a cliqué sur une sélection
        Timer3.Enabled = False
        ' ==================================
        ' Transfert de la sélection
        If ListBox6.SelectedItem = " - Effacement du champ -" Then
            TextBox15.Text = ""
        Else
            TextBox15.Text = Strings.LTrim(ListBox6.SelectedItem)
        End If
        ' ==================================
        ' Déclenchement du retrait du sélecteur
        Timer4.Enabled = True
        ' ==================================
        ' Arrêt du timer de retrait automatique
        Timer7.Enabled = False
        ' ==================================
        ' Désélection 
        ListBox6.ClearSelected()
    End Sub

    '=============================================================================================================================================
    ' Sélection d'une Positionnement Passage
    Private Sub ListBox7_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ListBox7.SelectedIndexChanged
        ' ==================================
        ' Sortie si sélection vide
        If ListBox7.SelectedItem = "" Then Exit Sub
        ' ==================================
        ' Arrêt du timer de temporisation de lecture pour éviter un conflit lorsque la liste se déroule et que l'on a cliqué sur une sélection
        Timer3.Enabled = False
        ' ==================================
        ' Transfert de la sélection
        If ListBox7.SelectedItem = " - Effacement du champ -" Then
            TextBox19.Text = ""
        Else
            TextBox19.Text = Strings.LTrim(ListBox7.SelectedItem)
        End If
        ' ==================================
        ' Déclenchement du retrait du sélecteur
        Timer4.Enabled = True
        ' ==================================
        ' Arrêt du timer de retrait automatique
        Timer7.Enabled = False
        ' ==================================
        ' Désélection 
        ListBox7.ClearSelected()
    End Sub


    '=============================================================================================================================================
    ' PROCÉDURES DE FORMATAGES
    '=============================================================================================================================================
    ' Vérification du formatage de la Date de Passage
    Private Sub TextBox11_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox11.LostFocus
        If TextBox11.Text = "" Then Exit Sub

        If Len(TextBox11.Text) <> 10 Then
            MsgBox("Le format est jj/mm/aaaa ! ", MsgBoxStyle.Critical, " QUADRISPEL - Erreur")
            TextBox11.Select()
            Exit Sub
        End If

        If Val(Strings.Left(TextBox11.Text, 2)) < 1 Or Val(Strings.Left(TextBox11.Text, 2)) > 31 Then
            MsgBox("La valeur du jour est erronée ! ", MsgBoxStyle.Critical, " QUADRISPEL - Erreur")
            TextBox11.Select()
            Exit Sub
        End If

        If Val(Strings.Mid(TextBox11.Text, 4, 2)) < 1 Or Val(Strings.Mid(TextBox11.Text, 4, 2)) > 12 Then
            MsgBox("La valeur du mois est erronée ! ", MsgBoxStyle.Critical, " QUADRISPEL - Erreur")
            TextBox11.Select()
            Exit Sub
        End If

        If Val(Strings.Right(TextBox11.Text, 4)) < 2000 Or Val(Strings.Right(TextBox11.Text, 4)) > 2100 Then
            MsgBox("La valeur de l'année est erronée ! ", MsgBoxStyle.Critical, " QUADRISPEL - Erreur")
            TextBox11.Select()
            Exit Sub
        End If

        TextBox11.Text = Strings.Left(TextBox11.Text, 2) & "/" & Strings.Mid(TextBox11.Text, 4, 2) & "/" & Strings.Right(TextBox11.Text, 4)

    End Sub

    '=============================================================================================================================================
    ' Vérification du formatage de l'Heure de Passage
    Private Sub TextBox12_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox12.LostFocus
        If TextBox12.Text = "" Then Exit Sub

        If Len(TextBox12.Text) <> 5 Then
            MsgBox("Le format est hh:mm ! ", MsgBoxStyle.Critical, " QUADRISPEL - Erreur")
            TextBox12.Select()
            Exit Sub
        End If

        If Val(Strings.Left(TextBox12.Text, 2)) < 0 Or Val(Strings.Left(TextBox12.Text, 2)) > 23 Then
            MsgBox("La valeur de l'heure est erronée ! ", MsgBoxStyle.Critical, " QUADRISPEL - Erreur")
            TextBox12.Select()
            Exit Sub
        End If

        If Val(Strings.Right(TextBox12.Text, 2)) < 0 Or Val(Strings.Right(TextBox12.Text, 2)) > 59 Then
            MsgBox("La valeur des minutes est erronée ! ", MsgBoxStyle.Critical, " QUADRISPEL - Erreur")
            TextBox12.Select()
            Exit Sub
        End If

        TextBox12.Text = Strings.Left(TextBox12.Text, 2) & ":" & Strings.Right(TextBox12.Text, 2)

    End Sub


    '=============================================================================================================================================
    ' Demande de sauvegarde du masque lors de la fermeture si oubli

    Private Sub TextBox10_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox10.TextChanged
        ' ==================================
        ' Validation sauvegarde à la fermeture
        Variables.SauveFichierMasque = True
    End Sub

    Private Sub TextBox24_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox24.TextChanged
        ' ==================================
        ' Validation sauvegarde à la fermeture
        Variables.SauveFichierMasque = True
    End Sub

    Private Sub TextBox3_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox3.TextChanged
        ' ==================================
        ' Validation sauvegarde à la fermeture
        Variables.SauveFichierMasque = True
    End Sub

    Private Sub TextBox1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox1.TextChanged
        ' ==================================
        ' Changement couleur obligation champ
        If TextBox1.Text <> "" And TextBox2.Text <> "" Then
            TextBox19.BackColor = Color.FromArgb(255, 255, 200)
        Else
            TextBox19.BackColor = Color.White
        End If
        ' ==================================
        ' Validation sauvegarde à la fermeture
        Variables.SauveFichierMasque = True
    End Sub

    Private Sub TextBox2_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox2.TextChanged
        ' ==================================
        ' Changement couleur obligation champ
        If TextBox1.Text <> "" And TextBox2.Text <> "" Then
            TextBox19.BackColor = Color.FromArgb(255, 255, 200)
        Else
            TextBox19.BackColor = Color.White
        End If
        ' ==================================
        ' Validation sauvegarde à la fermeture
        Variables.SauveFichierMasque = True
    End Sub

    Private Sub TextBox5_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox5.TextChanged
        ' ==================================
        ' Validation sauvegarde à la fermeture
        Variables.SauveFichierMasque = True
    End Sub

    Private Sub TextBox8_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox8.TextChanged
        ' ==================================
        ' Validation sauvegarde à la fermeture
        Variables.SauveFichierMasque = True
    End Sub

    Private Sub TextBox23_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox23.TextChanged
        ' ==================================
        ' Validation sauvegarde à la fermeture
        Variables.SauveFichierMasque = True
    End Sub

    Private Sub TextBox7_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox7.TextChanged
        ' ==================================
        ' Validation sauvegarde à la fermeture
        Variables.SauveFichierMasque = True
    End Sub

    Private Sub TextBox9_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox9.TextChanged
        ' ==================================
        ' Validation sauvegarde à la fermeture
        Variables.SauveFichierMasque = True
    End Sub

    Private Sub TextBox11_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox11.TextChanged
        ' ==================================
        ' Validation sauvegarde à la fermeture
        Variables.SauveFichierMasque = True
    End Sub

    Private Sub TextBox12_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox12.TextChanged
        ' ==================================
        ' Validation sauvegarde à la fermeture
        Variables.SauveFichierMasque = True
    End Sub

    Private Sub TextBox14_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox14.TextChanged
        ' ==================================
        ' Changement couleur obligation champ
        If TextBox14.Text <> "" Then
            TextBox15.BackColor = Color.FromArgb(255, 255, 200)
        Else
            TextBox15.BackColor = Color.White
        End If
        ' ==================================
        ' Validation sauvegarde à la fermeture
        Variables.SauveFichierMasque = True
    End Sub

    Private Sub TextBox15_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox15.TextChanged
        ' ==================================
        ' Validation sauvegarde à la fermeture
        Variables.SauveFichierMasque = True
    End Sub

    Private Sub TextBox19_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox19.TextChanged
        ' ==================================
        ' Validation sauvegarde à la fermeture
        Variables.SauveFichierMasque = True
    End Sub

    Private Sub TextBox20_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox20.TextChanged
        ' ==================================
        ' Validation sauvegarde à la fermeture
        Variables.SauveFichierMasque = True
    End Sub


    '=============================================================================================================================================
    ' PROCÉDURES D'INFORMATION POUR LES CHAMPS
    '=============================================================================================================================================
    ' Passage en demande d'information
    Private Sub Button3_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Button3.MouseDown
        ' ==================================
        ' Passage en curseur d'aide
        Me.Cursor = Cursors.Help
    End Sub

    '=============================================================================================================================================
    ' Traitements des champs sélectionnés
    Private Sub Button3_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Button3.MouseUp
        ' ==================================
        ' Retour au curseur normal
        Me.Cursor = Cursors.Default
        ' ==================================
        ' Repérage
        '        TextBox18.Text = " X = " & e.X & "    Y = " & e.Y
        ' ==================================
        ' Vérification si le bouton Information est sélectionné
        If e.X > 1 And e.X < 114 And e.Y > 1 And e.Y < 24 Then
            MsgBox("- Cliquer sur le bouton " & Chr(171) & "Information" & Chr(187) & "," & vbCrLf & "- Maintenir appuyer," & vbCrLf & "- Déplacer le " & Chr(171) & "?" & Chr(187) & " sur le champ désiré,    " & vbCrLf & "- Relacher.", MsgBoxStyle.Information, "QUADRISPEL - Procédure d'utilisation")
            ' ==================================
            ' Renvoi du select sur le bouton caché
            Button2.Select()
            Exit Sub
        End If
        ' ==================================
        ' Vérification si le champ Département est sélectionné
        If e.X > -175 And e.X < -46 And e.Y < -354 And e.Y > -375 Then
            Informations.TextBox1.Height = 40 + 16
            Informations.Button1.Top = 60 + 16
            Informations.Height = 128 + 16
            Informations.Text = " Information du champ " & Chr(171) & " Département " & Chr(187)
            Informations.TextBox1.Text = "La sélection du département ne fait apparaître que les dénominations de ses lieux de surveillance." & vbCrLf & "Si le numéro de département est omis, la liste de dénomination des lieux de surveillance présente l'ensemble des lieux de surveillance."
            Informations.ShowDialog()
            ' ==================================
            ' Renvoi du select sur le bouton caché
            Button2.Select()
            Exit Sub
        End If
        ' ==================================
        ' Vérification si le champ Dénomination Lieu de Surveillance est sélectionné
        If e.X > 0 And e.X < 604 And e.Y < -354 And e.Y > -375 Then
            Informations.TextBox1.Height = 40 + 16
            Informations.Button1.Top = 60 + 16
            Informations.Height = 128 + 16
            Informations.Text = " Information du champ " & Chr(171) & " Dénomination Lieu de Surveillance " & Chr(187)
            Informations.TextBox1.Text = "C'est la dénomination du lieu de surveillance où s'exécute le passage." & vbCrLf & "Si le numéro de département est omis, la liste présente l'ensemble des lieux de surveillance." & vbCrLf & "À la sélection de la Désignation du Lieu de Surveillance les champs Latitude et Longitude sont renseignés."
            Informations.ShowDialog()
            ' ==================================
            ' Renvoi du select sur le bouton caché
            Button2.Select()
            Exit Sub
        End If
        ' ==================================
        ' Vérification si le champ Réseau est sélectionné
        If e.X > -175 And e.X < -46 And e.Y < -289 And e.Y > -310 Then
            Informations.TextBox1.Height = 40 + 80
            Informations.Button1.Top = 60 + 80
            Informations.Height = 128 + 80
            Informations.Text = " Information du champ " & Chr(171) & " Réseau " & Chr(187)
            Informations.TextBox1.Text = "Désigne les activités qui sont à l'origine de la collecte d'un ensemble cohérent de données, que ce soit pour les réseaux de surveillance ou pour des études limitées dans le temps." & vbCrLf & "La quantité de données rattachées à un programme peut être variable, selon qu'il s'agit d'une activité longue ou intensive, ou d'une opération plus ponctuelle (étude) mais toujours mise en œuvre selon un schéma décidé à l'avance." & vbCrLf & "Ce terme correspond à la notion de réseau pour le SANDRE, à la différence près que le SANDRE n'inclut pas les études ponctuelles."
            Informations.ShowDialog()
            ' ==================================
            ' Renvoi du select sur le bouton caché
            Button2.Select()
            Exit Sub
        End If
        ' ==================================
        ' Vérification si le champ Latitude de Passage est sélectionné
        If e.X > 0 And e.X < 109 And e.Y < -289 And e.Y > -310 Then
            Informations.TextBox1.Height = 40 + 32
            Informations.Button1.Top = 60 + 32
            Informations.Height = 128 + 32
            Informations.Text = " Information du champ " & Chr(171) & " Latitude de Passage " & Chr(187)
            Informations.TextBox1.Text = "Exprimée au format WGS84 en degrés décimaux." & vbCrLf & "C'est, par héritage la latitude du lieu de surveillance où la latitude réelle du passage recueillie sur le terrain au moment du passage." & vbCrLf & "Le champ est renseigné à la sélection de la Dénomination du Lieu de Surveillance."
            Informations.ShowDialog()
            ' ==================================
            ' Renvoi du select sur le bouton caché
            Button2.Select()
            Exit Sub
        End If
        ' ==================================
        ' Vérification si le champ Longitude de Passage est sélectionné
        If e.X > 155 And e.X < 259 And e.Y < -289 And e.Y > -310 Then
            Informations.TextBox1.Height = 40 + 32
            Informations.Button1.Top = 60 + 32
            Informations.Height = 128 + 32
            Informations.Text = " Information du champ " & Chr(171) & " Longitude de Passage " & Chr(187)
            Informations.TextBox1.Text = "Exprimée au format WGS84 en degrés décimaux." & vbCrLf & "C'est, par héritage la longitude du lieu de surveillance où la longitude réelle du passage recueillie sur le terrain au moment du passage." & vbCrLf & "Le champ est renseigné à la sélection de la Dénomination du Lieu de Surveillance."
            Informations.ShowDialog()
            ' ==================================
            ' Renvoi du select sur le bouton caché
            Button2.Select()
            Exit Sub
        End If
        ' ==================================
        ' Vérification si le champ Positionnement du Passage est sélectionné
        If e.X > 305 And e.X < 604 And e.Y < -289 And e.Y > -310 Then
            Informations.TextBox1.Height = 40 + 80
            Informations.Button1.Top = 60 + 80
            Informations.Height = 128 + 80
            Informations.Text = " Information du champ " & Chr(171) & " Positionnement du Passage " & Chr(187)
            Informations.TextBox1.Text = "Le positionnement s'intègre dans les métadonnées de la norme ISO 19115." & vbCrLf & "Il correspond à la méthodologie employée pour localiser des entités géographiques." & vbCrLf & "Il se base sur un engin de positionnement (GPS,Ortholittorale, image SPOT ...), et défini la façon dont cet engin a été utilisé pour positionner l'entité." & vbCrLf & "Il définit notamment la précision des données positionnées." & vbCrLf & vbCrLf & "Ce champ est obligatoire si la Latitude et la Longitude du Passage sont renseignées."
            Informations.ShowDialog()
            ' ==================================
            ' Renvoi du select sur le bouton caché
            Button2.Select()
            Exit Sub
        End If
        ' ==================================
        ' Vérification si le champ Acronyme Saisisseur est sélectionné
        If e.X > -175 And e.X < 9 And e.Y < -225 And e.Y > -245 Then
            Informations.TextBox1.Height = 40
            Informations.Button1.Top = 60
            Informations.Height = 128
            Informations.Text = " Information du champ " & Chr(171) & " Acronyme Saisisseur " & Chr(187)
            Informations.TextBox1.Text = "C'est l'acronyme de l'organisme ou du service saisisseur." & vbCrLf & "La sélection d'un Acronyme Saisisseur renseigne automatiquement le champ du Libellé Saisisseur."
            Informations.ShowDialog()
            ' ==================================
            ' Renvoi du select sur le bouton caché
            Button2.Select()
            Exit Sub
        End If
        ' ==================================
        ' Vérification si le champ Libellé Saisisseur est sélectionné
        If e.X > 55 And e.X < 604 And e.Y < -225 And e.Y > -245 Then
            Informations.TextBox1.Height = 40
            Informations.Button1.Top = 60
            Informations.Height = 128
            Informations.Text = " Information du champ " & Chr(171) & " Libellé Saisisseur " & Chr(187)
            Informations.TextBox1.Text = "C'est le libellé de l'organisme ou du service saisisseur." & vbCrLf & "La sélection d'un Libellé Saisisseur renseigne automatiquement le champ de l'Acronyme Saisisseur."
            Informations.ShowDialog()
            ' ==================================
            ' Renvoi du select sur le bouton caché
            Button2.Select()
            Exit Sub
        End If
        ' ==================================
        ' Vérification si le champ Campagne est sélectionné
        If e.X > -175 And e.X < 194 And e.Y < -159 And e.Y > -180 Then
            Informations.TextBox1.Height = 40 + 128
            Informations.Button1.Top = 60 + 128
            Informations.Height = 128 + 128
            Informations.Text = " Information du champ " & Chr(171) & " Campagne " & Chr(187)
            Informations.TextBox1.Text = "Les campagnes correspondent à deux notions différentes :" & vbCrLf & "- ce sont d'une part les campagnes à la mer gérées par le SISMER" & vbCrLf & "- d'autre part, pour le REBENT sectoriel interdital par exemple, il s'agit de l'ensemble des sorties sur le terrain sur un secteur en un laps de temps donné, sur une période continue (une marée par exemple) et nécessitant une logistique particulière." & vbCrLf & "En effet, chaque secteur REBENT suivi (lieu de surveillance dans Quadrige²) ne peut être couvert en une seule fois." & vbCrLf & "Plusieurs équipes se déplacent donc dans une zone définie du secteur pendant une période de temps données pour en faire le suivi exhaustif." & vbCrLf & "C'est une campagne REBENT sectoriel interdital."
            Informations.ShowDialog()
            ' ==================================
            ' Renvoi du select sur le bouton caché
            Button2.Select()
            Exit Sub
        End If
        ' ==================================
        ' Vérification si le champ Sortie est sélectionné
        If e.X > 240 And e.X < 604 And e.Y < -159 And e.Y > -180 Then
            Informations.TextBox1.Height = 40
            Informations.Button1.Top = 60
            Informations.Height = 128
            Informations.Text = " Information du champ " & Chr(171) & " Sortie " & Chr(187)
            Informations.TextBox1.Text = "Une sortie correspond à une équipe de participants à une date donnée et sur une même zone géographique." & vbCrLf & "Cette notion est typique du suivi sectoriel interdital REBENT."
            Informations.ShowDialog()
            ' ==================================
            ' Renvoi du select sur le bouton caché
            Button2.Select()
            Exit Sub
        End If
        ' ==================================
        ' Vérification si le champ Date Passage est sélectionné
        If e.X > -175 And e.X < -61 And e.Y < -94 And e.Y > -115 Then
            Informations.TextBox1.Height = 40
            Informations.Button1.Top = 60
            Informations.Height = 128
            Informations.Text = " Information du champ " & Chr(171) & " Date Passage " & Chr(187)
            Informations.TextBox1.Text = "La date de passage est au format jj/mm/aaaa."
            Informations.ShowDialog()
            ' ==================================
            ' Renvoi du select sur le bouton caché
            Button2.Select()
            Exit Sub
        End If
        ' ==================================
        ' Vérification si le champ Heure Passage est sélectionné
        If e.X > -15 And e.X < 84 And e.Y < -94 And e.Y > -115 Then
            Informations.TextBox1.Height = 40
            Informations.Button1.Top = 60
            Informations.Height = 128
            Informations.Text = " Information du champ " & Chr(171) & " Heure Passage " & Chr(187)
            Informations.TextBox1.Text = "L'heure de passage est au format hh/mm."
            Informations.ShowDialog()
            ' ==================================
            ' Renvoi du select sur le bouton caché
            Button2.Select()
            Exit Sub
        End If
        ' ==================================
        ' Vérification si le champ Sonde est sélectionné
        If e.X > 130 And e.X < 194 And e.Y < -94 And e.Y > -115 Then
            Informations.TextBox1.Height = 40
            Informations.Button1.Top = 60
            Informations.Height = 128
            Informations.Text = " Information du champ " & Chr(171) & " Sonde " & Chr(187)
            Informations.TextBox1.Text = "C'est la hauteur d'eau sous le bateau."
            Informations.ShowDialog()
            ' ==================================
            ' Renvoi du select sur le bouton caché
            Button2.Select()
            Exit Sub
        End If
        ' ==================================
        ' Vérification si le champ Unité Sonde est sélectionné
        If e.X > 240 And e.X < 304 And e.Y < -94 And e.Y > -115 Then
            Informations.TextBox1.Height = 40 + 16
            Informations.Button1.Top = 60 + 16
            Informations.Height = 128 + 16
            Informations.Text = " Information du champ " & Chr(171) & " Unité Sonde " & Chr(187)
            Informations.TextBox1.Text = "C'est l'unité de la hauteur d'eau sous le bateau." & vbCrLf & vbCrLf & "Ce champ est obligatoire si le champ Sonde est renseigné."
            Informations.ShowDialog()
            ' ==================================
            ' Renvoi du select sur le bouton caché
            Button2.Select()
            Exit Sub
        End If
        ' ==================================
        ' Vérification si le champ Mnémonique du Passage est sélectionné
        If e.X > 350 And e.X < 604 And e.Y < -94 And e.Y > -115 Then
            Informations.TextBox1.Height = 40
            Informations.Button1.Top = 60
            Informations.Height = 128
            Informations.Text = " Information du champ " & Chr(171) & " Mnémonique du Passage " & Chr(187)
            Informations.TextBox1.Text = "Texte libre d'une longueur maximale de 50 caractères." & vbCrLf & "Un décompteur indique le nombre de caractères restants."
            Informations.ShowDialog()
            ' ==================================
            ' Renvoi du select sur le bouton caché
            Button2.Select()
            Exit Sub
        End If
        ' ==================================
        ' Vérification si le champ Commentaires Passage est sélectionné
        If e.X > -170 And e.X < 164 And e.Y < -29 And e.Y > -50 Then
            Informations.TextBox1.Height = 40
            Informations.Button1.Top = 60
            Informations.Height = 128
            Informations.Text = " Information du champ " & Chr(171) & " Commentaires Passage " & Chr(187)
            Informations.TextBox1.Text = "Texte libre d'une longueur maximale de 2000 caractères." & vbCrLf & "Un décompteur indique le nombre de caractères restants."
            Informations.ShowDialog()
            ' ==================================
            ' Renvoi du select sur le bouton caché
            Button2.Select()
            Exit Sub
        End If
        ' ==================================
        ' Vérification si le champ Zone Destination Dragage est sélectionné
        If e.X > 210 And e.X < 429 And e.Y < -29 And e.Y > -50 Then
            Informations.TextBox1.Height = 40
            Informations.Button1.Top = 60
            Informations.Height = 128
            Informations.Text = " Information du champ " & Chr(171) & " Zone Destination Dragage " & Chr(187)
            Informations.TextBox1.Text = "Ce champ est destiné au programme DRAGAGE."
            Informations.ShowDialog()
            ' ==================================
            ' Renvoi du select sur le bouton caché
            Button2.Select()
            Exit Sub
        End If
        ' ==================================
        ' Vérification si le champ Nombre Individu Passage est sélectionné
        If e.X > 475 And e.X < 604 And e.Y < -29 And e.Y > -50 Then
            Informations.TextBox1.Height = 40 + 80
            Informations.Button1.Top = 60 + 80
            Informations.Height = 128 + 80
            Informations.Text = " Information du champ " & Chr(171) & " Nombre Individu Passage " & Chr(187)
            Informations.TextBox1.Text = "Les individus sont des entités de même type constituant un prélèvement ou un échantillon, et sur lesquels sont répétés plusieurs mesures identiques (exemple : mesure de poids, longueur, largeur sur les 30 huîtres d'un échantillon : l'échantillon comporte 30 individus)." & vbCrLf & "Dans Quadrige², il est indispensable pour saisir des résultats en mode colonne :" & vbCrLf & "- que, dans la stratégie applicable, le PSFM soit noté " & Chr(171) & " Saisie sur individu " & Chr(187) & vbCrLf & "- qu'un nombre d'individus soit renseigné au niveau in situ (prélèvement, échantillon) sur lequel sont saisis les résultats pour permettre la création d'un tableau lignes/colonnes."
            Informations.ShowDialog()
            ' ==================================
            ' Renvoi du select sur le bouton caché
            Button2.Select()
            Exit Sub
        End If
    End Sub

 
End Class