﻿Public Class Échantillon


    '=============================================================================================================================================
    ' Procédure lors de la fermeture de la feuille par la croix
    Private Sub Échantillon_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles MyBase.FormClosing
        '==============================================
        ' Blocage de la procédure de fermeture automatique
        e.Cancel = True
        '==============================================
        ' Demande de fermeture par Fondu
        Fermeture()
    End Sub

    ' ==============================================================================================================================================
    ' Procédure au chargement de la page
    Private Sub Échantillon_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button2.Select()
        '==============================================
        ' Demande d'ouverture par Fondu
        Me.Opacity = 0
        Timer1.Enabled = True
        ' ==================================
        ' Effacement du nombre de caractères autorisé
        GroupBox9.Text = " Commentaires Échantillon "
    End Sub


    '=============================================================================================================================================
    ' PROCÉDURES DIVERSES
    '=============================================================================================================================================
    ' Traitement du repositionnement automatique de la feuille quand on l'a déplacée
    Private Sub Échantillon_MouseEnter(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.MouseEnter
        Me.CenterToScreen()
    End Sub

    '=============================================================================================================================================
    ' Traitement de réduction des fenêtres
    Private Sub Échantillon_Move(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Move
        ' ============================================
        ' Passage de la feuille principale transparente quand on réduit cette feuille
        If Me.WindowState = 1 Then
            Form1.Opacity = 0
        End If
        ' ============================================
        ' Passage de la feuille principale opaque quand on rétablit cette feuille
        If Me.WindowState = 0 Then
            Form1.Opacity = 1
        End If
    End Sub

    ' ==============================================================================================================================================
    ' Bouton de fermeture de l'application
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button2.Select()
        ' ==================================
        ' Demande de fermeture par Fondu
        Fermeture()
    End Sub


    '=============================================================================================================================================
    ' PROCÉDURE D'OUVERTURE ET DE FERMETURE
    '=============================================================================================================================================
    ' Procédure d'ouverture par Fondu
    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        Me.Opacity = Me.Opacity + 0.06
        If Me.Opacity >= 0.95 Then
            Timer1.Enabled = False
            Me.Opacity = 1
        End If
    End Sub

    Private Sub Fermeture()
        ' ============================================
        ' Essai pour éviter qu'une application ne passe devant la feuille ayant demandé celle-ci
        Form1.Activate()
        ' ============================================
        Me.Visible = False
    End Sub


    '=============================================================================================================================================
    ' PROCÉDURE POUR ÉVITER LE CURSEUR CLIGNOTANT DANS LES CHAMPS
    '=============================================================================================================================================

    Private Sub TextBox2_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox2.GotFocus
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button2.Select()
    End Sub

    Private Sub TextBox4_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox4.GotFocus
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button2.Select()
    End Sub

    Private Sub TextBox7_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox7.GotFocus
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button2.Select()
    End Sub


    '=============================================================================================================================================
    ' PROCÉDURE POUR LE NOMBRE DE CARACTÈRES DANS LES CHAMPS
    '=============================================================================================================================================
    ' Affichage du nombre de caractères autorisé
    Private Sub TextBox9_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox9.GotFocus
        ' ==================================
        ' Affichage du nombre de caractères autorisé
        GroupBox9.Text = " Commentaires Échantillon (" & 2000 - Len(TextBox9.Text) & ") "
    End Sub

    '=============================================================================================================================================
    ' Affichage du nombre de caractères autorisé
    Private Sub TextBox9_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox9.TextChanged
        ' ==================================
        ' Validation sauvegarde à la fermeture
        Variables.SauveFichierMasque = True
        ' ==================================
        ' Affichage du nombre de caractères autorisé
        GroupBox9.Text = " Commentaires Échantillon (" & 2000 - Len(TextBox9.Text) & ") "
    End Sub

    '=============================================================================================================================================
    ' Effacement du nombre de caractères autorisé
    Private Sub TextBox9_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox9.LostFocus
        ' ==================================
        ' Effacement du nombre de caractères autorisé
        GroupBox9.Text = " Commentaires Échantillon "
    End Sub


    '=============================================================================================================================================
    ' PROCÉDURES D'APPARITION DES SÉLECTEURS
    '=============================================================================================================================================
    ' Sélection de la liste Support Échantillon
    Private Sub TextBox2_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox2.MouseClick
        ' ==================================
        ' Traitement pour la liste des sujets si une autre liste est en cours
        If Variables.NumeroList <> 0 Then
            Variables.NumeroListSuivant = 1
            ' ==================================
            ' Arrêt du timer de la procédure d'apparition des listes
            Timer2.Enabled = False
            ' Arrêt du timer de la procédure d'apparition des listes
            Timer3.Enabled = False
            ' ==================================
            ' Déclenchement du retrait du sélecteur
            Timer4.Enabled = True
            Exit Sub
        End If
        ' ==================================
        ' Variable d'affichage du Listbox de la liste des sujets
        Variables.NumeroList = 1
        ' ==================================
        ' Déclenchement du timer de la procédure d'apparition des listes
        Timer2.Enabled = True
    End Sub

    ' Sélection de la liste Groupe Taxon Support Échantillon
    Private Sub TextBox4_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox4.MouseClick
        ' ==================================
        ' Traitement pour la liste des sujets si une autre liste est en cours
        If Variables.NumeroList <> 0 Then
            Variables.NumeroListSuivant = 2
            ' ==================================
            ' Arrêt du timer de la procédure d'apparition des listes
            Timer2.Enabled = False
            ' Arrêt du timer de la procédure d'apparition des listes
            Timer3.Enabled = False
            ' ==================================
            ' Déclenchement du retrait du sélecteur
            Timer4.Enabled = True
            Exit Sub
        End If
        ' ==================================
        ' Variable d'affichage du Listbox de la liste des sujets
        Variables.NumeroList = 2
        ' ==================================
        ' Déclenchement du timer de la procédure d'apparition des listes
        Timer2.Enabled = True
    End Sub

    '=============================================================================================================================================
    ' Sélection de la liste Unité Immersion
    Private Sub TextBox7_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox7.MouseClick
        ' ==================================
        ' Traitement pour la liste des sujets si une autre liste est en cours
        If Variables.NumeroList <> 0 Then
            Variables.NumeroListSuivant = 3
            ' ==================================
            ' Arrêt du timer de la procédure d'apparition des listes
            Timer2.Enabled = False
            ' Arrêt du timer de la procédure d'apparition des listes
            Timer3.Enabled = False
            ' ==================================
            ' Déclenchement du retrait du sélecteur
            Timer4.Enabled = True
            Exit Sub
        End If
        ' ==================================
        ' Variable d'affichage du Listbox de la liste des sujets
        Variables.NumeroList = 3
        ' ==================================
        ' Déclenchement du timer de la procédure d'apparition des listes
        Timer2.Enabled = True
    End Sub

    '=============================================================================================================================================
    ' Déclenche la sortie d'un sélecteur
    Private Sub Timer2_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer2.Tick
        ' ==================================
        ' Procédure lorsque le sélecteur de la liste des Support Échantillon est arrivé à sa position
        If Variables.NumeroList = 1 And Panel1.Top >= 0 Then
            ' ==================================
            ' Arrêt du timer
            Timer2.Enabled = False
            ' ==================================
            ' Déclenchement du timer de la temporisation de lecture
            Timer3.Enabled = True
            ' ==================================
            ' Sortie
            Exit Sub
        End If
        ' ==================================
        ' Procédure lorsque le sélecteur de la liste Groupe Taxon Support Échantillon est arrivé à sa position
        If Variables.NumeroList = 2 And Panel2.Top >= 0 Then
            ' ==================================
            ' Arrêt du timer
            Timer2.Enabled = False
            ' ==================================
            ' Déclenchement du timer de la temporisation de lecture
            Timer3.Enabled = True
            ' ==================================
            ' Sortie
            Exit Sub
        End If
        ' ==================================
        ' Procédure lorsque le sélecteur de la liste des Unité Taille Échantillon est arrivé à sa position
        If Variables.NumeroList = 3 And Panel3.Top >= 0 Then
            ' ==================================
            ' Arrêt du timer
            Timer2.Enabled = False
            ' ==================================
            ' Déclenchement du timer de la temporisation de lecture
            Timer3.Enabled = True
            ' ==================================
            ' Sortie
            Exit Sub
        End If


        ' ==================================
        ' Incrémentation du déplacement du sélecteur de la liste des Support Échantillon
        If Variables.NumeroList = 1 Then Panel1.Top = Panel1.Top + 2
        ' ==================================
        ' Incrémentation du déplacement du sélecteur de la liste des Groupe Taxon Support Échantillon
        If Variables.NumeroList = 2 Then Panel2.Top = Panel2.Top + 2
        ' ==================================
        ' Incrémentation du déplacement du sélecteur de la liste des Unité Taille Échantillon
        If Variables.NumeroList = 3 Then Panel3.Top = Panel3.Top + 2

    End Sub

    ' ==============================================================================================================================================
    ' Déclenche l'attente de lecture et l'affichage de la liste
    Private Sub Timer3_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer3.Tick
        ' ==================================
        ' Incrémentation du déplacement du sélecteur de la liste des Support Échantillon
        If Variables.NumeroList = 1 Then
            Panel1.Height = Panel1.Height + 16
            ' ==================================
            ' Apparition de la liste correspondante
            ListBox1.Visible = True
        End If
        ' ==================================
        ' Incrémentation du déplacement du sélecteur de la liste Groupe Taxon Support Échantillon
        If Variables.NumeroList = 2 Then
            Panel2.Height = Panel2.Height + 16
            ' ==================================
            ' Apparition de la liste correspondante
            ListBox2.Visible = True
        End If
        ' ==================================
        ' Incrémentation du déplacement du sélecteur de la liste des Unité Taille Échantillon
        If Variables.NumeroList = 3 Then
            Panel3.Height = Panel3.Height + 16
            ' ==================================
            ' Apparition de la liste correspondante
            ListBox3.Visible = True
        End If


        ' ==================================
        ' Vérification de la fin du déplacement du sélecteur de la liste des Support Échantillon
        If Variables.NumeroList = 1 And Panel1.Height >= 241 Then
            ' ==================================
            ' Arrête du timer de temporisation de lecture
            Timer3.Enabled = False
            ' ==================================
            ' Déclenchement du timer de déclenchement de la procédure de retrait
            Timer6.Enabled = True
            ' ==================================
            ' Déclenchement du timer de retrait automatique
            Timer7.Enabled = True
        End If
        ' ==================================
        ' Vérification de la fin du déplacement du sélecteur de la liste Groupe Taxon Support Échantillon
        If Variables.NumeroList = 2 And Panel2.Height >= 241 Then
            ' ==================================
            ' Arrête du timer de temporisation de lecture
            Timer3.Enabled = False
            ' ==================================
            ' Déclenchement du timer de déclenchement de la procédure de retrait
            Timer6.Enabled = True
            ' ==================================
            ' Déclenchement du timer de retrait automatique
            Timer7.Enabled = True
        End If
        ' ==================================
        ' Vérification de la fin du déplacement du sélecteur de la liste des Unité Taille Échantillon
        If Variables.NumeroList = 3 And Panel3.Height >= 241 Then
            ' ==================================
            ' Arrête du timer de temporisation de lecture
            Timer3.Enabled = False
            ' ==================================
            ' Déclenchement du timer de déclenchement de la procédure de retrait
            Timer6.Enabled = True
            ' ==================================
            ' Déclenchement du timer de retrait automatique
            Timer7.Enabled = True
        End If


    End Sub


    '=============================================================================================================================================
    ' PROCÉDURES DES DISPARITION DES SÉLECTEURS
    '=============================================================================================================================================
    ' Déclenche la disparition du sélecteur
    Private Sub Timer4_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer4.Tick
        ' ==================================
        ' Décrémentation du déplacement du sélecteur de la liste des Support Échantillon
        If Variables.NumeroList = 1 And Panel1.Height > 50 Then
            Panel1.Height = Panel1.Height - 16
        End If
        ' ==================================
        ' Décrémentation du déplacement du sélecteur de la liste Groupe Taxon Support Échantillon
        If Variables.NumeroList = 2 And Panel2.Height > 50 Then
            Panel2.Height = Panel2.Height - 16
        End If
        ' ==================================
        ' Décrémentation du déplacement du sélecteur de la liste des Unité Taille Échantillon
        If Variables.NumeroList = 3 And Panel3.Height > 50 Then
            Panel3.Height = Panel3.Height - 16
        End If

        ' ==================================
        ' Vérification de la fin du déplacement du sélecteur de la liste des Support Échantillon
        If Variables.NumeroList = 1 And Panel1.Height <= 50 Then
            ' ==================================
            ' Retrait de la liste correspondante
            ListBox1.Visible = False
            ' ==================================
            ' Arrêt du timer
            Timer4.Enabled = False
            ' Déclenchement du timer de retrait du conteneur de la liste correspondante et lancement éventuel d'une liste demandée
            Timer5.Enabled = True
        End If
        ' ==================================
        ' Vérification de la fin du déplacement du sélecteur de la liste Groupe Taxon Support Échantillon
        If Variables.NumeroList = 2 And Panel2.Height <= 50 Then
            ' ==================================
            ' Retrait de la liste correspondante
            ListBox2.Visible = False
            ' ==================================
            ' Arrêt du timer
            Timer4.Enabled = False
            ' Déclenchement du timer de retrait du conteneur de la liste correspondante et lancement éventuel d'une liste demandée
            Timer5.Enabled = True
        End If

        ' ==================================
        ' Vérification de la fin du déplacement du sélecteur de la liste des Unité Taille Échantillon
        If Variables.NumeroList = 3 And Panel3.Height <= 50 Then
            ' ==================================
            ' Retrait de la liste correspondante
            ListBox3.Visible = False
            ' ==================================
            ' Arrêt du timer
            Timer4.Enabled = False
            ' Déclenchement du timer de retrait du conteneur de la liste correspondante et lancement éventuel d'une liste demandée
            Timer5.Enabled = True
        End If
    End Sub

    ' ==============================================================================================================================================
    ' Timer de retrait du conteneur de la liste correspondante et lancement éventuel d'une liste demandée
    Private Sub Timer5_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer5.Tick
        ' ==================================
        ' Décrémentation du déplacement du sélecteur de la liste des Support Échantillon
        If Variables.NumeroList = 1 Then Panel1.Top = Panel1.Top - 2
        ' ==================================
        ' Décrémentation du déplacement du sélecteur de la liste Groupe Taxon Support Échantillon
        If Variables.NumeroList = 2 Then Panel2.Top = Panel2.Top - 2
        ' ==================================
        ' Décrémentation du déplacement du sélecteur de la liste des Unité Taille Échantillon
        If Variables.NumeroList = 3 Then Panel3.Top = Panel3.Top - 2


        ' ==================================
        ' Traitement pour la liste des Support Échantillon
        If Variables.NumeroList = 1 And Panel1.Top <= -50 Then
            ' ==================================
            ' Remise à zéro de la variable de numéro de la liste
            Variables.NumeroList = 0
            ' ==================================
            ' Arrêt du timer
            Timer5.Enabled = False
            ' ==================================
            ' Arrêt du timer de déclenchement de la procédure de retrait
            Timer6.Enabled = False
            ' ==================================
            ' Vérification si l'apparition d'une liste a été demandée
            If Variables.NumeroListSuivant <> 0 Then
                ' ==================================
                ' Transfert du numéro de la liste suivante dans celui de la liste
                Variables.NumeroList = Variables.NumeroListSuivant
                ' ==================================
                ' Remise à zéro de la variable de numéro de la liste suivante
                Variables.NumeroListSuivant = 0
                ' ==================================
                ' Déclenchement du timer de la procédure d'apparition des listes
                Timer2.Enabled = True
            End If
            Exit Sub
        End If
        ' ==================================
        ' Traitement pour la liste Groupe Taxon Support Échantillon
        If Variables.NumeroList = 2 And Panel2.Top <= -50 Then
            ' ==================================
            ' Remise à zéro de la variable de numéro de la liste
            Variables.NumeroList = 0
            ' ==================================
            ' Arrêt du timer
            Timer5.Enabled = False
            ' ==================================
            ' Arrêt du timer de déclenchement de la procédure de retrait
            Timer6.Enabled = False
            ' ==================================
            ' Vérification si l'apparition d'une liste a été demandée
            If Variables.NumeroListSuivant <> 0 Then
                ' ==================================
                ' Transfert du numéro de la liste suivante dans celui de la liste
                Variables.NumeroList = Variables.NumeroListSuivant
                ' ==================================
                ' Remise à zéro de la variable de numéro de la liste suivante
                Variables.NumeroListSuivant = 0
                ' ==================================
                ' Déclenchement du timer de la procédure d'apparition des listes
                Timer2.Enabled = True
            End If
            Exit Sub
        End If
        ' ==================================
        ' Traitement pour la liste des Unité Taille Échantillon
        If Variables.NumeroList = 3 And Panel3.Top <= -50 Then
            ' ==================================
            ' Remise à zéro de la variable de numéro de la liste
            Variables.NumeroList = 0
            ' ==================================
            ' Arrêt du timer
            Timer5.Enabled = False
            ' ==================================
            ' Arrêt du timer de déclenchement de la procédure de retrait
            Timer6.Enabled = False
            ' ==================================
            ' Vérification si l'apparition d'une liste a été demandée
            If Variables.NumeroListSuivant <> 0 Then
                ' ==================================
                ' Transfert du numéro de la liste suivante dans celui de la liste
                Variables.NumeroList = Variables.NumeroListSuivant
                ' ==================================
                ' Remise à zéro de la variable de numéro de la liste suivante
                Variables.NumeroListSuivant = 0
                ' ==================================
                ' Déclenchement du timer de la procédure d'apparition des listes
                Timer2.Enabled = True
            End If
            Exit Sub
        End If

    End Sub

    ' ==============================================================================================================================================
    ' Timer de déclenchement de la procédure de retrait de la liste affichée
    Private Sub Timer6_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer6.Tick
        ' ==================================
        ' Déclenchement du timer de la procédure de retrait
        Timer4.Enabled = True
        ' ==================================
        ' Arrêt du timer
        Timer6.Enabled = False
        ' ==================================
        ' Arrêt du timer de retrait automatique
        Timer7.Enabled = False
    End Sub

    ' ==============================================================================================================================================
    ' Timer de retrait automatique lorsque le curseur n'est pas sur la zone correspondante
    Private Sub Timer7_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer7.Tick

        '       TextBox1.Text = "X " & MousePosition.X - Me.Left
        '       TextBox8.Text = "Y " & MousePosition.Y - Me.Top

        ' ==================================
        ' Traitement pour la liste des Support Échantillon
        If Variables.NumeroList = 1 Then
            ' ==================================
            ' Vérification si le curseur est dans la zone correspondante
            If (MousePosition.X - Me.Left) > 202 And (MousePosition.X - Me.Left) < 463 And (MousePosition.Y - Me.Top) > 30 And (MousePosition.Y - Me.Top) < 271 Then
                ' ==================================
                ' Arrêt du timer de déclenchement de la procédure de retrait
                Timer6.Enabled = False
                ' ==================================
                ' Sortie
                Exit Sub
            End If
        End If
        ' ==================================
        ' Traitement pour la liste Groupe Taxon Support Échantillon
        If Variables.NumeroList = 2 Then
            ' ==================================
            ' Vérification si le curseur est dans la zone correspondante
            If (MousePosition.X - Me.Left) > 202 And (MousePosition.X - Me.Left) < 463 And (MousePosition.Y - Me.Top) > 30 And (MousePosition.Y - Me.Top) < 271 Then
                ' ==================================
                ' Arrêt du timer de déclenchement de la procédure de retrait
                Timer6.Enabled = False
                ' ==================================
                ' Sortie
                Exit Sub
            End If
        End If
        ' ==================================
        ' Traitement pour la liste des Unité Taille Échantillon
        If Variables.NumeroList = 3 Then
            ' ==================================
            ' Vérification si le curseur est dans la zone correspondante
            If (MousePosition.X - Me.Left) > 232 And (MousePosition.X - Me.Left) < 433 And (MousePosition.Y - Me.Top) > 30 And (MousePosition.Y - Me.Top) < 271 Then
                ' ==================================
                ' Arrêt du timer de déclenchement de la procédure de retrait
                Timer6.Enabled = False
                ' ==================================
                ' Sortie
                Exit Sub
            End If
        End If

        ' ==================================
        ' Déclenchement du timer de déclenchement de la procédure de retrait
        Timer6.Enabled = True
    End Sub


    '=============================================================================================================================================
    ' PROCÉDURES DE SÉLECTION DANS LES LISTES
    '=============================================================================================================================================
    ' Sélection d'un Support Échantillon
    Private Sub ListBox1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ListBox1.SelectedIndexChanged
        ' ==================================
        ' Sortie si sélection vide
        If ListBox1.SelectedItem = "" Then Exit Sub
        ' ==================================
        ' Arrêt du timer de temporisation de lecture pour éviter un conflit lorsque la liste se déroule et que l'on a cliqué sur une sélection et que le DataGridView1 se déroule en arrière
        Timer3.Enabled = False
        ' ==================================
        ' Transfert de la sélection
        If ListBox1.SelectedItem = " - Effacement du champ -" Then
            TextBox2.Text = ""
        Else
            TextBox2.Text = Strings.LTrim(ListBox1.SelectedItem)
        End If
        ' ==================================
        ' Déclenchement du retrait du sélecteur
        Timer4.Enabled = True
        ' ==================================
        ' Arrêt du timer de retrait automatique
        Timer7.Enabled = False
        ' ==================================
        ' Désélection 
        ListBox1.ClearSelected()
    End Sub

    '=============================================================================================================================================
    ' Sélection d'une dénomination d'un Groupe Taxon Support Échantillon
    Private Sub ListBox2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ListBox2.SelectedIndexChanged
        ' ==================================
        ' Sortie si sélection vide
        If ListBox2.SelectedItem = "" Then Exit Sub
        ' ==================================
        ' Arrêt du timer de temporisation de lecture pour éviter un conflit lorsque la liste se déroule et que l'on a cliqué sur une sélection et que le DataGridView1 se déroule en arrière
        Timer3.Enabled = False
        ' ==================================
        ' Transfert de la sélection
        If ListBox2.SelectedItem = " - Effacement du champ -" Then
            TextBox4.Text = ""
        Else
            TextBox4.Text = Strings.LTrim(ListBox2.SelectedItem)
        End If
        ' ==================================
        ' Déclenchement du retrait du sélecteur
        Timer4.Enabled = True
        ' ==================================
        ' Arrêt du timer de retrait automatique
        Timer7.Enabled = False
        ' ==================================
        ' Désélection 
        ListBox2.ClearSelected()
    End Sub

    '=============================================================================================================================================
    ' Sélection d'une Unité Taille Échantillon
    Private Sub ListBox3_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ListBox3.SelectedIndexChanged
        ' ==================================
        ' Sortie si sélection vide
        If ListBox3.SelectedItem = "" Then Exit Sub
        ' ==================================
        ' Arrêt du timer de temporisation de lecture pour éviter un conflit lorsque la liste se déroule et que l'on a cliqué sur une sélection et que le DataGridView1 se déroule en arrière
        Timer3.Enabled = False
        ' ==================================
        ' Transfert de la sélection
        If ListBox3.SelectedItem = " - Effacement du champ -" Then
            TextBox7.Text = ""
        Else
            TextBox7.Text = Strings.LTrim(ListBox3.SelectedItem)
        End If
        ' ==================================
        ' Déclenchement du retrait du sélecteur
        Timer4.Enabled = True
        ' ==================================
        ' Arrêt du timer de retrait automatique
        Timer7.Enabled = False
        ' ==================================
        ' Désélection 
        ListBox3.ClearSelected()
        ' ==================================
        ' Renvoi du select sur le bouton caché sinon Bouton Fermer valider ?
        Button2.Select()
    End Sub


    '=============================================================================================================================================
    ' Demande de sauvegarde du masque lors de la fermeture si oubli

    Private Sub TextBox1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox1.TextChanged
        ' ==================================
        ' Validation sauvegarde à la fermeture
        Variables.SauveFichierMasque = True
    End Sub

    Private Sub TextBox2_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox2.TextChanged
        ' ==================================
        ' Validation sauvegarde à la fermeture
        Variables.SauveFichierMasque = True
    End Sub

    Private Sub TextBox3_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox3.TextChanged
        ' ==================================
        ' Validation sauvegarde à la fermeture
        Variables.SauveFichierMasque = True
    End Sub

    Private Sub TextBox4_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox4.TextChanged
        ' ==================================
        ' Validation sauvegarde à la fermeture
        Variables.SauveFichierMasque = True
    End Sub

    Private Sub TextBox5_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox5.TextChanged
        ' ==================================
        ' Validation sauvegarde à la fermeture
        Variables.SauveFichierMasque = True
    End Sub

    Private Sub TextBox6_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox6.TextChanged
        ' ==================================
        ' Changement couleur obligation champ
        If TextBox6.Text <> "" And TextBox2.Text <> "" Then
            TextBox7.BackColor = Color.FromArgb(255, 255, 200)
        Else
            TextBox7.BackColor = Color.White
        End If
        ' ==================================
        ' Validation sauvegarde à la fermeture
        Variables.SauveFichierMasque = True
    End Sub

    Private Sub TextBox7_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox7.TextChanged
        ' ==================================
        ' Validation sauvegarde à la fermeture
        Variables.SauveFichierMasque = True
    End Sub

    Private Sub TextBox8_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox8.TextChanged
        ' ==================================
        ' Validation sauvegarde à la fermeture
        Variables.SauveFichierMasque = True
    End Sub


    '=============================================================================================================================================
    ' PROCÉDURES D'INFORMATION POUR LES CHAMPS
    '=============================================================================================================================================
    ' Passage en demande d'information
    Private Sub Button3_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Button3.MouseDown
        ' ==================================
        ' Passage en curseur d'aide
        Me.Cursor = Cursors.Help
    End Sub

    '=============================================================================================================================================
    ' Traitements des champs sélectionnés
    Private Sub Button3_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Button3.MouseUp
        ' ==================================
        ' Retour au curseur normal
        Me.Cursor = Cursors.Default
        ' ==================================
        ' Repérage
        '        TextBox9.Text = " X = " & e.X & "    Y = " & e.Y
        '        Exit Sub
        ' ==================================
        ' Vérification si le bouton Information est sélectionné
        If e.X > 1 And e.X < 114 And e.Y > 1 And e.Y < 24 Then
            MsgBox("- Cliquer sur le bouton " & Chr(34) & "Information" & Chr(34) & "," & vbCrLf & "- Maintenir appuyer," & vbCrLf & "- Déplacer le " & Chr(34) & "?" & Chr(34) & " sur le champ désiré,    " & vbCrLf & "- Relacher.", MsgBoxStyle.Information, "QUADRISPEL - Procédure d'utilisation")
            ' ==================================
            ' Renvoi du select sur le bouton caché
            Button2.Select()
            Exit Sub
        End If
        ' ==================================
        ' Vérification si le champ Lot Aquacole est sélectionné
        If e.X > -120 And e.X < 64 And e.Y < -224 And e.Y > -245 Then
            Informations.TextBox1.Height = 40
            Informations.Button1.Top = 60
            Informations.Height = 128
            Informations.Text = " Information du champ " & Chr(171) & " Lot Aquacole " & Chr(187)
            Informations.TextBox1.Text = "Champ spécifique aux données aquacoles."
            Informations.ShowDialog()
            ' ==================================
            ' Renvoi du select sur le bouton caché
            Button2.Select()
            Exit Sub
        End If
        ' ==================================
        ' Vérification si le champ Dénomination Support Échantillon est sélectionné
        If e.X > 110 And e.X < 479 And e.Y < -224 And e.Y > -245 Then
            Informations.TextBox1.Height = 40
            Informations.Button1.Top = 60
            Informations.Height = 128
            Informations.Text = " Information du champ " & Chr(171) & " Dénomination Support Échantillon " & Chr(187)
            Informations.TextBox1.Text = "Ce champ désigne la matière du support de l'échantillon." & vbCrLf & "Ce champ est obligatoire."
            Informations.ShowDialog()
            ' ==================================
            ' Renvoi du select sur le bouton caché
            Button2.Select()
            Exit Sub
        End If
        ' ==================================
        ' Vérification si le champ Taxon Support Échantillon est sélectionné
        If e.X > -120 And e.X < 159 And e.Y < -159 And e.Y > -180 Then
            Informations.TextBox1.Height = 40 + 176
            Informations.Button1.Top = 60 + 176
            Informations.Height = 128 + 176
            Informations.Text = " Information du champ " & Chr(171) & " Taxon Support Échantillon " & Chr(187)
            Informations.TextBox1.Text = "Entité biologique de valeur systématique = unité systématique = ensemble d'entités biologiques appartenant au même phylum et occupant un niveau précis dans la classification systématique adoptée ; un taxon désigne ainsi un niveau systématique donné dans un phylum donné." & vbCrLf & "Un taxon est désigné par un nom ; ce nom est unique pour un règne donné (par exemple, le genre Spinachia est employé à la fois dans le nom d'espèce de l'épinoche de mer Spinachia spinachia et dans celui de l'épinard Spinachia olacera, une plante de la famille des Chénopodiacées)." & vbCrLf & "Une espèce donnée, un genre donné, une famille donnée, etc. sont des taxons." & vbCrLf & "Très précisèment, la taxinomie établit la nomenclature qui régit la dénomination des taxons." & vbCrLf & "Il est possible de préciser le taxon échantillonné. La classification utilisée est celle du WORMS." & vbCrLf & "Dans le cas de résultats de " & Chr(171) & " dénombrements " & Chr(187) & " ce champ (ou Groupe Taxon Support Échantillon) doit être renseigné." & vbCrLf & "Si le champ " & Chr(171) & " Groupe Taxon Support Échantillon " & Chr(187) & " est renseigné le champ " & Chr(171) & " Taxon Support Échantillon " & Chr(187) & " doit être vide."
            Informations.ShowDialog()
            ' ==================================
            ' Renvoi du select sur le bouton caché
            Button2.Select()
            Exit Sub
        End If
        ' ==================================
        ' Vérification si le champ Dénomination Groupe Taxon Support Échantillon est sélectionné
        If e.X > 205 And e.X < 479 And e.Y < -159 And e.Y > -180 Then
            Informations.TextBox1.Height = 40 + 96
            Informations.Button1.Top = 60 + 96
            Informations.Height = 128 + 96
            Informations.Text = " Information du champ " & Chr(171) & " Dénomination Groupe Taxon Support Échantillon " & Chr(187)
            Informations.TextBox1.Text = "Il désigne un ensemble de taxons ayant les mêmes caractéristiques pour un critère donné." & vbCrLf & "Ce critère peut être morpho-anatomique (par exemple les strates algales ou la taille des organismes), comportementale (par exemple des groupes trophiques ou des modes de déplacement), ou encore basé sur des notions plus complexes comme la polluo-sensibilité (exemple des groupes écologiques définis pour les macroinvertébrés benthiques)." & vbCrLf & "Pour un critère donné, les groupes de taxons sont rassemblés dans un Regroupement de Taxons." & vbCrLf & "Si le champ " & Chr(171) & " Taxon Support Échantillon " & Chr(187) & " est renseigné le champ " & Chr(171) & " Groupe Taxon Support Échantillon " & Chr(187) & " doit être vide."
            Informations.ShowDialog()
            ' ==================================
            ' Renvoi du select sur le bouton caché
            Button2.Select()
            Exit Sub
        End If
        ' ==================================
        ' Vérification si le champ Numéro Échantillon est sélectionné
        If e.X > -120 And e.X < 4 And e.Y < -94 And e.Y > -115 Then
            Informations.TextBox1.Height = 40
            Informations.Button1.Top = 60
            Informations.Height = 128
            Informations.Text = " Information du champ " & Chr(171) & " Numéro Échantillon " & Chr(187)
            Informations.TextBox1.Text = "Ce champ, obligatoire, correspond à un identifiant de l'échantillon prélevé propre au laboratoire d'analyse."
            Informations.ShowDialog()
            ' ==================================
            ' Renvoi du select sur le bouton caché
            Button2.Select()
            Exit Sub
        End If
        ' ==================================
        ' Vérification si le champ Taille Échantillon est sélectionné
        If e.X > 50 And e.X < 154 And e.Y < -94 And e.Y > -115 Then
            Informations.TextBox1.Height = 40
            Informations.Button1.Top = 60
            Informations.Height = 128
            Informations.Text = " Information du champ " & Chr(171) & " Taille Échantillon " & Chr(187)
            Informations.TextBox1.Text = "Cette taille peut être exprimée soit en longueur, surface, volume ou poids."
            Informations.ShowDialog()
            ' ==================================
            ' Renvoi du select sur le bouton caché
            Button2.Select()
            Exit Sub
        End If
        ' ==================================
        ' Vérification si le champ Unité Taille Échantillon est sélectionné
        If e.X > 200 And e.X < 304 And e.Y < -94 And e.Y > -115 Then
            Informations.TextBox1.Height = 40
            Informations.Button1.Top = 60
            Informations.Height = 128
            Informations.Text = " Information du champ " & Chr(171) & " Unité Taille Échantillon " & Chr(187)
            Informations.TextBox1.Text = "Ce champ est obligatoire lorsque la taille de l'échantillon est renseignée."
            Informations.ShowDialog()
            ' ==================================
            ' Renvoi du select sur le bouton caché
            Button2.Select()
            Exit Sub
        End If
        ' ==================================
        ' Vérification si le champ Nombre Individu Échantillon est sélectionné
        If e.X > 350 And e.X < 479 And e.Y < -94 And e.Y > -115 Then
            Informations.TextBox1.Height = 40
            Informations.Button1.Top = 60
            Informations.Height = 128
            Informations.Text = " Information du champ " & Chr(171) & " Nombre Individu Échantillon " & Chr(187)
            Informations.TextBox1.Text = "Nombre d'individus constituant précisément l'échantillon."
            Informations.ShowDialog()
            ' ==================================
            ' Renvoi du select sur le bouton caché
            Button2.Select()
            Exit Sub
        End If
        ' ==================================
        ' Vérification si le champ Commentaires Échantillon est sélectionné
        If e.X > -120 And e.X < 479 And e.Y < -29 And e.Y > -50 Then
            Informations.TextBox1.Height = 40
            Informations.Button1.Top = 60
            Informations.Height = 128
            Informations.Text = " Information du champ " & Chr(171) & " Commentaires Échantillon " & Chr(187)
            Informations.TextBox1.Text = "Texte libre d'une longueur maximale de 2000 caractères." & vbCrLf & "Un décompteur indique le nombre de caractères restants."
            Informations.ShowDialog()
            ' ==================================
            ' Renvoi du select sur le bouton caché
            Button2.Select()
            Exit Sub
        End If
    End Sub

End Class