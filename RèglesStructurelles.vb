﻿Public Class RèglesStructurelles


    '=============================================================================================================================================
    ' Procédure lors de la fermeture de la feuille par la croix
    Private Sub RèglesStructurelles_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        '==============================================
        ' Blocage de la procédure de fermeture automatique
        e.Cancel = True
        '==============================================
        ' Demande de fermeture par Fondu
        Fermeture()
    End Sub

    ' ==============================================================================================================================================
    ' Procédure au chargement de la page
    Private Sub RèglesStructurelles_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ' ==================================
        ' Effacement de la couleur de fond
        If DataGridView1.Item(0, 0).Value = "Pas d'invalidation de Règles Structurelles !" Then
            DataGridView1.Item(0, 0).Style.SelectionBackColor = Color.LightGreen
        Else
            DataGridView1.Item(0, 0).Style.SelectionBackColor = Color.White
        End If
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button2.Select()
        '==============================================
        ' Demande d'ouverture par Fondu
        Me.Opacity = 0
        Timer1.Enabled = True
    End Sub


    '=============================================================================================================================================
    ' PROCÉDURE DU CLIC SUR UNE ERREUR
    '=============================================================================================================================================
    ' Traitement de complément sur une règle structurelle
    Private Sub DataGridView1_CellMouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellMouseEventArgs) Handles DataGridView1.CellMouseDown


        ' ==================================
        ' Sortie si clic sur le titre de colonne
        If e.RowIndex < 0 Then Exit Sub
        ' ==================================

        '       MsgBox(e.ColumnIndex & "   " & e.RowIndex)

        '        MsgBox(DataGridView1.Item(e.ColumnIndex, e.RowIndex).Value)

        ' 0171 = «   0187 = »

        ' ==================================
        ' Sortie si pas d'invalidation de règles structurelle
        If DataGridView1.Item(0, 0).Value = "Pas d'invalidation de Règles Structurelles !" Then Exit Sub
        ' ==================================
        ' Coloration en rouge de la règle invalidée
        DataGridView1.Item(e.ColumnIndex, e.RowIndex).Style.SelectionBackColor = Color.Pink
        ' ==================================
        ' Absence de la « Dénomination du Lieu de Surveillance ».
        If DataGridView1.Item(e.ColumnIndex, e.RowIndex).Value = "Absence de la « Dénomination du Lieu de Surveillance »" Then
            Variables.Feuille = "Feuille « Passage »  "
            Variables.Champ = "Champ « Dénomination du Lieu de Surveillance »    "
            Timer2.Enabled = True
        End If
        ' ==================================
        ' Absence de la « Dénomination du Réseau ».
        If DataGridView1.Item(e.ColumnIndex, e.RowIndex).Value = "Absence de la « Dénomination du Réseau »" Then
            Variables.Feuille = "Feuille « Passage »  "
            Variables.Champ = "Champ « Réseau »"
            Timer2.Enabled = True
        End If
        ' ==================================
        ' Absence du « Libellé du Saisisseur ».
        If DataGridView1.Item(e.ColumnIndex, e.RowIndex).Value = "Absence du « Libellé du Saisisseur »" Then
            Variables.Feuille = "Feuille « Passage »  "
            Variables.Champ = "Champ « Libellé Saisisseur »    "
            Timer2.Enabled = True
        End If
        ' ==================================
        ' Absence de la « Date de Passage ».
        If DataGridView1.Item(e.ColumnIndex, e.RowIndex).Value = "Absence de la « Date de Passage »" Then
            Variables.Feuille = "Feuille « Passage »  "
            Variables.Champ = "Champ « Date de Passage »    "
            Timer2.Enabled = True
        End If
        ' ==================================
        ' Présence d’une « Valeur de Sonde » sans « Unité de Sonde ».
        If DataGridView1.Item(e.ColumnIndex, e.RowIndex).Value = "Présence d’une « Valeur de Sonde » sans « Unité de Sonde »" Then
            Variables.Feuille = "Feuille « Passage »  "
            Variables.Champ = "Champ « Unité Sonde »    "
            Timer2.Enabled = True
        End If
        ' ==================================
        ' Absence de « Positionnement Passage » avec présence de la « Latitude Passage » ou de la « Longitude Passage » ou des deux.
        If DataGridView1.Item(e.ColumnIndex, e.RowIndex).Value = "Absence de « Positionnement Passage » avec présence de la « Latitude Passage » ou de la « Longitude Passage » ou des deux" Then
            Variables.Feuille = "Feuille « Passage »  "
            Variables.Champ = "Champ « Positionnement du Passage »    "
            Timer2.Enabled = True
        End If
        ' ==================================
        ' Absence de la « Dénomination de l’Engin de Prélèvement ».
        If DataGridView1.Item(e.ColumnIndex, e.RowIndex).Value = "Absence de la « Dénomination de l’Engin de Prélèvement »" Then
            Variables.Feuille = "Feuille « Propriétés et Résultats »  "
            Variables.Champ = "Champ « Dénomination Engin de Prélèvement »    "
            Timer2.Enabled = True
        End If
        ' ==================================
        ' Absence du « Libellé du Préleveur ».
        If DataGridView1.Item(e.ColumnIndex, e.RowIndex).Value = "Absence du « Libellé du Préleveur »" Then
            Variables.Feuille = "Feuille « Prélèvement »  "
            Variables.Champ = "Champ « Libellé Préleveur »    "
            Timer2.Enabled = True
        End If
        ' ==================================
        ' Présence de l’« Immersion de Prélèvement » avec l’« Immersion Max » ou avec l’« Immersion Min » ou sans l’« Unité de Prélèvement ».
        If DataGridView1.Item(e.ColumnIndex, e.RowIndex).Value = "Présence de l’ « Immersion de Prélèvement » avec l' « Immersion Max » ou avec l’ « Immersion Min » ou sans l’ « Unité d'Immersion »" Then
            Variables.Feuille = "Feuille « Prélèvement »  "
            Variables.Champ = "Champ « Immersion Prélèvement »    "
            Timer2.Enabled = True
        End If
        ' ==================================
        ' Présence de l’« Immersion Max de Prélèvement » sans l’« Immersion Min de Prélèvement » ou sans l’« Unité de Prélèvement ».
        If DataGridView1.Item(e.ColumnIndex, e.RowIndex).Value = "Présence de l’ « Immersion Max de Prélèvement » sans l' « Immersion Min de Prélèvement » ou sans l’ « Unité d'Immersion »" Then
            Variables.Feuille = "Feuille « Prélèvement »  "
            Variables.Champ = "Champ « Immersion Max Prélèvement »    "
            Timer2.Enabled = True
        End If
        ' ==================================
        ' Présence de l’« Immersion Min de Prélèvement » sans l’« Immersion Max de Prélèvement » ou sans l’« Unité de Prélèvement ».
        If DataGridView1.Item(e.ColumnIndex, e.RowIndex).Value = "Présence de l’ « Immersion Min de Prélèvement » sans l' « Immersion Max de Prélèvement » ou sans l’ « Unité d'Immersion »" Then
            Variables.Feuille = "Feuille « Prélèvement »  "
            Variables.Champ = "Champ « Immersion Min Prélèvement »    "
            Timer2.Enabled = True
        End If
        ' ==================================
        ' Présence de la « Taille de Prélèvement » sans l’« Unité de Taille de Prélèvement ».
        If DataGridView1.Item(e.ColumnIndex, e.RowIndex).Value = "Présence de la « Taille de Prélèvement » sans l' « Unité de Taille de Prélèvement »" Then
            Variables.Feuille = "Feuille « Prélèvement »  "
            Variables.Champ = "Champ « Taille Prélèvement »    "
            Timer2.Enabled = True
        End If
        ' ==================================
        ' Absence de « Positionnement de Prélèvement » avec présence de la « Latitude » ou de la « Longitude de Prélèvement » ou des deux.
        If DataGridView1.Item(e.ColumnIndex, e.RowIndex).Value = "Absence de « Positionnement de Prélèvement » avec présence de la « Latitude » ou de la « Longitude de Prélèvement » ou des deux" Then
            Variables.Feuille = "Feuille « Prélèvement »  "
            Variables.Champ = "Champ « Positionnement Prélèvement »    "
            Timer2.Enabled = True
        End If
        ' ==================================
        ' Absence de la « Dénomination du Support Échantillon ».
        If DataGridView1.Item(e.ColumnIndex, e.RowIndex).Value = "Absence de la « Dénomination du Support Échantillon »" Then
            Variables.Feuille = "Feuille « Échantillon »  "
            Variables.Champ = "Champ « Dénomination Support Échantillon »    "
            Timer2.Enabled = True
        End If
        ' ==================================
        ' Présence du « Taxon Support Échantillon » et du « Groupe Taxon Support Échantillon ».
        If DataGridView1.Item(e.ColumnIndex, e.RowIndex).Value = "Présence du « Taxon Support Échantillon » et du « Groupe Taxon Support Échantillon »" Then
            Variables.Feuille = "Feuille « Échantillon »  "
            Variables.Champ = "Champ « Taxon Support Échantillon »    "
            Timer2.Enabled = True
        End If
        ' ==================================
        ' Absence du « Numéro d’Échantillon ».
        If DataGridView1.Item(e.ColumnIndex, e.RowIndex).Value = "Absence du « Numéro d’Échantillon »" Then
            Variables.Feuille = "Feuille « Échantillon »  "
            Variables.Champ = "Champ « Numéro Échantillon »    "
            Timer2.Enabled = True
        End If
        ' ==================================
        ' Présence de la « Taille d’Échantillon » sans l’« Unité de Taille d’Échantillon ».
        If DataGridView1.Item(e.ColumnIndex, e.RowIndex).Value = "Présence de la « Taille d’Échantillon » sans l' « Unité de Taille d’Échantillon »" Then
            Variables.Feuille = "Feuille « Échantillon »  "
            Variables.Champ = "Champ « Taille Échantillon »    "
            Timer2.Enabled = True
        End If
        ' ==================================
        ' Absence du « Niveau de Saisie Résultat ».
        If DataGridView1.Item(e.ColumnIndex, e.RowIndex).Value = "Absence du « Niveau de Saisie Résultat »" Then
            Variables.Feuille = "Feuille « Propriétés et Résultats »  "
            Variables.Champ = "Champ « Niveau de Saisie »    "
            Timer2.Enabled = True
        End If
        ' ==================================
        ' Absence du « Code Sandre Paramètre ».
        If DataGridView1.Item(e.ColumnIndex, e.RowIndex).Value = "Absence du « Code Sandre Paramètre »" Then
            Variables.Feuille = "Fichier « Référentiel Paramètres Quadrispel »  "
            Variables.Champ = "Information « Code Sandre Paramètre »    "
            Timer2.Enabled = True
        End If
        ' ==================================
        ' Absence de la « Dénomination du Paramètre ».
        If DataGridView1.Item(e.ColumnIndex, e.RowIndex).Value = "Absence de la « Dénomination du Paramètre »" Then
            Variables.Feuille = "Fichier « Référentiel Paramètres Quadrispel »  "
            Variables.Champ = "Information « Dénomination du Paramètre »    "
            Timer2.Enabled = True
        End If
        ' ==================================
        ' Absence de la « Dénomination du Support ».
        If DataGridView1.Item(e.ColumnIndex, e.RowIndex).Value = "Absence de la « Dénomination du Support »" Then
            Variables.Feuille = "Feuille « Propriétés et Résultats »  "
            Variables.Champ = "Champ « Dénomination Support »    "
            Timer2.Enabled = True
        End If
        ' ==================================
        ' Différence entre la « Dénomination Support Échantillon » et une « Dénomination du Support ».
        If DataGridView1.Item(e.ColumnIndex, e.RowIndex).Value = "Différence entre la « Dénomination Support Échantillon » et une « Dénomination du Support »" Then
            Variables.Feuille = "Feuille « Échantillon »  "
            Variables.Champ = "Champ « Dénomination Support Échantillon »    "
            Timer2.Enabled = True
        End If
        ' ==================================
        ' Absence de la « Dénomination de la Fraction ».
        If DataGridView1.Item(e.ColumnIndex, e.RowIndex).Value = "Absence de la « Dénomination de la Fraction »" Then
            Variables.Feuille = "Feuille « Propriétés et Résultats »    "
            Variables.Champ = "Champ « Dénomination Fraction »    "
            Timer2.Enabled = True
        End If
        ' ==================================
        ' Absence de la « Dénomination de la Méthode ».
        If DataGridView1.Item(e.ColumnIndex, e.RowIndex).Value = "Absence de la « Dénomination de la Méthode »" Then
            Variables.Feuille = "Feuille « Propriétés et Résultats »    "
            Variables.Champ = "Champ « Dénomination Méthode »    "
            Timer2.Enabled = True
        End If



        ' ==================================
        ' Absence du « Résultat Numérique ».
        If DataGridView1.Item(e.ColumnIndex, e.RowIndex).Value = "Absence du « Résultat Numérique »" Then
            Variables.Feuille = "Feuille « Propriétés et Résultats »    "
            Variables.Champ = "Champ « Résultat Numérique »    "
            Timer2.Enabled = True
        End If



        ' ==================================
        ' Absence du « Code Résultat Qualitatif ».
        If DataGridView1.Item(e.ColumnIndex, e.RowIndex).Value = "Absence du « Code Résultat Qualitatif »" Then
            Variables.Feuille = "Feuille « Propriétés et Résultats »    "
            Variables.Champ = "Champ « Code Résultat Qualitatif »    "
            Timer2.Enabled = True
        End If


        ' ==================================
        ' Présence du signe « < » dans la valeur du Résultat.
        If DataGridView1.Item(e.ColumnIndex, e.RowIndex).Value = "Présence du signe « < » dans la valeur du Résultat" Then
            Variables.Feuille = "Feuille « Propriétés et Résultats »    "
            Variables.Champ = "Champ « Résultat »    "
            Timer2.Enabled = True
        End If
        ' ==================================
        ' Absence de l’« Unité du Résultat ».
        If DataGridView1.Item(e.ColumnIndex, e.RowIndex).Value = "Absence de l’ « Unité du Résultat »" Then
            Variables.Feuille = "Feuille « Propriétés et Résultats »    "
            Variables.Champ = "Champ « Unité »    "
            Timer2.Enabled = True
        End If
        ' ==================================
        ' Absence de la « Libellé Analyste ».
        If DataGridView1.Item(e.ColumnIndex, e.RowIndex).Value = "Absence du « Libellé de l’Analyste »" Then
            Variables.Feuille = "Feuille « Propriétés et Résultats »    "
            Variables.Champ = "Champ « Libellé Analyste »    "
            Timer2.Enabled = True
        End If
        ' ==================================
        ' Absence du « Code Remarque ».
        If DataGridView1.Item(e.ColumnIndex, e.RowIndex).Value = "Absence du « Code Remarque »" Then
            Variables.Feuille = "Feuille « Propriétés et Résultats »    "
            Variables.Champ = "Champ « Code Remarque »    "
            Timer2.Enabled = True
        End If
        ' ==================================
        ' Présence d’une « Valeur de Précision » sans le « Type de Précision ».
        If DataGridView1.Item(e.ColumnIndex, e.RowIndex).Value = "Présence de la « Valeur de Précision » sans le « Type de Précision »" Then
            '           RèglesStructurelles.DataGridView1.Item(NomColonne, IndexRègleStructurelle).Value = "Présence de la " & Chr(171) & " Valeur de Précision " & Chr(187) & " sans le " & Chr(171) & " Type de Précision " & Chr(187)
            Variables.Feuille = "Feuille « Propriétés et Résultats »  "
            Variables.Champ = "Champ « Type de Précision »    "
            Timer2.Enabled = True
        End If
    End Sub

    Private Sub Timer2_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer2.Tick
        Timer2.Enabled = False
        '       MsgBox(Variables.Feuille & vbCrLf & vbCrLf & Variables.Champ, MsgBoxStyle.Information, " Information à vérifier")
        MsgBox(Variables.Feuille & vbCrLf & vbCrLf & Variables.Champ, , " Information à vérifier")
    End Sub

    '=============================================================================================================================================
    ' PROCÉDURE D'AFFICHAGE DES RÈGLES STRUCTURELLES
    '=============================================================================================================================================
    ' Liste des Règles Structurelles
    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Me.Text = " Liste des Règles Structurelles"
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button2.Select()
        ' ==================================
        ' Effacement des lignes du DataGridView et affectation de l'information de validation
        DataGridView1.Rows.Clear()
        DataGridView1.Rows.Add()
        DataGridView1.Item(0, 0).Value = "Absence de la " & Chr(171) & " Dénomination du Lieu de Surveillance " & Chr(187)
        DataGridView1.Rows.Add()
        DataGridView1.Item(0, 1).Value = "Absence de la " & Chr(171) & " Dénomination du Réseau " & Chr(187)
        DataGridView1.Rows.Add()
        DataGridView1.Item(0, 2).Value = "Absence du " & Chr(171) & " Libellé du Saisisseur " & Chr(187)
        DataGridView1.Rows.Add()
        DataGridView1.Item(0, 3).Value = "Absence de la " & Chr(171) & " Date de Passage " & Chr(187)
        DataGridView1.Rows.Add()
        DataGridView1.Item(0, 4).Value = "Présence d’une " & Chr(171) & " Valeur de Sonde " & Chr(187) & " sans " & Chr(171) & " Unité de Sonde " & Chr(187)
        DataGridView1.Rows.Add()
        DataGridView1.Item(0, 5).Value = "Absence de " & Chr(171) & " Positionnement Passage " & Chr(187) & " avec présence de la " & Chr(171) & " Latitude Passage " & Chr(187) & " ou de la " & Chr(171) & " Longitude Passage " & Chr(187) & " ou des deux"
        DataGridView1.Rows.Add()
        DataGridView1.Item(0, 6).Value = "Absence de la " & Chr(171) & " Dénomination de l’Engin de Prélèvement " & Chr(187)
        DataGridView1.Rows.Add()
        DataGridView1.Item(0, 7).Value = "Absence du " & Chr(171) & " Libellé du Préleveur " & Chr(187)
        DataGridView1.Rows.Add()
        DataGridView1.Item(0, 8).Value = "Présence de l’ " & Chr(171) & " Immersion de Prélèvement " & Chr(187) & " avec l' " & Chr(171) & " Immersion Max " & Chr(187) & " ou avec l’ " & Chr(171) & " Immersion Min " & Chr(187) & " ou sans l’ " & Chr(171) & " Unité d'Immersion " & Chr(187)
        DataGridView1.Rows.Add()
        DataGridView1.Item(0, 9).Value = "Présence de l’ " & Chr(171) & " Immersion Max de Prélèvement " & Chr(187) & " sans l' " & Chr(171) & " Immersion Min de Prélèvement " & Chr(187) & " ou sans l’ " & Chr(171) & " Unité d'Immersion " & Chr(187)
        DataGridView1.Rows.Add()
        DataGridView1.Item(0, 10).Value = "Présence de l’ " & Chr(171) & " Immersion Min de Prélèvement " & Chr(187) & " sans l' " & Chr(171) & " Immersion Max de Prélèvement " & Chr(187) & " ou sans l’ " & Chr(171) & " Unité d'Immersion " & Chr(187)
        DataGridView1.Rows.Add()
        DataGridView1.Item(0, 11).Value = "Présence de la " & Chr(171) & " Taille de Prélèvement " & Chr(187) & " sans l' " & Chr(171) & " Unité de Taille de Prélèvement " & Chr(187)
        DataGridView1.Rows.Add()
        DataGridView1.Item(0, 12).Value = "Absence de " & Chr(171) & " Positionnement de Prélèvement " & Chr(187) & " avec présence de la " & Chr(171) & " Latitude " & Chr(187) & " ou de la " & Chr(171) & " Longitude de Prélèvement " & Chr(187) & " ou des deux"
        DataGridView1.Rows.Add()
        DataGridView1.Item(0, 13).Value = "Absence de la " & Chr(171) & " Dénomination du Support Échantillon " & Chr(187)
        DataGridView1.Rows.Add()
        DataGridView1.Item(0, 14).Value = "Présence du " & Chr(171) & " Taxon Support Échantillon " & Chr(187) & " et du " & Chr(171) & " Groupe Taxon Support Échantillon " & Chr(187)
        DataGridView1.Rows.Add()
        DataGridView1.Item(0, 15).Value = "Absence du " & Chr(171) & " Numéro d’Échantillon " & Chr(187)
        DataGridView1.Rows.Add()
        DataGridView1.Item(0, 16).Value = "Présence de la " & Chr(171) & " Taille d’Échantillon " & Chr(187) & " sans l' " & Chr(171) & " Unité de Taille d’Échantillon " & Chr(187)
        DataGridView1.Rows.Add()
        DataGridView1.Item(0, 17).Value = "Absence du " & Chr(171) & " Niveau de Saisie Résultat " & Chr(187)
        DataGridView1.Rows.Add()
        DataGridView1.Item(0, 18).Value = "Absence du " & Chr(171) & " Code Sandre Paramètre " & Chr(187)
        DataGridView1.Rows.Add()
        DataGridView1.Item(0, 19).Value = "Absence de la " & Chr(171) & " Dénomination du Paramètre " & Chr(187)
        DataGridView1.Rows.Add()
        DataGridView1.Item(0, 20).Value = "Absence de la " & Chr(171) & " Dénomination du Support " & Chr(187)
        DataGridView1.Rows.Add()
        DataGridView1.Item(0, 21).Value = "Différence entre la " & Chr(171) & " Dénomination Support Échantillon " & Chr(187) & " et une " & Chr(171) & " Dénomination du Support " & Chr(187)
        DataGridView1.Rows.Add()
        DataGridView1.Item(0, 22).Value = "Absence de la " & Chr(171) & " Dénomination de la Fraction " & Chr(187)
        DataGridView1.Rows.Add()
        DataGridView1.Item(0, 23).Value = "Absence de la " & Chr(171) & " Dénomination de la Méthode " & Chr(187)
        DataGridView1.Rows.Add()
        DataGridView1.Item(0, 24).Value = "Absence du " & Chr(171) & " Résultat Numérique " & Chr(187)
        DataGridView1.Rows.Add()
        DataGridView1.Item(0, 25).Value = "Présence du signe " & Chr(171) & " < " & Chr(187) & " dans la valeur du Résultat"
        DataGridView1.Rows.Add()
        DataGridView1.Item(0, 26).Value = "Absence de l’ " & Chr(171) & " Unité du Résultat " & Chr(187)
        DataGridView1.Rows.Add()
        DataGridView1.Item(0, 27).Value = "Absence du " & Chr(171) & " Libellé de l’Analyste " & Chr(187)
        DataGridView1.Rows.Add()
        DataGridView1.Item(0, 28).Value = "Absence du " & Chr(171) & " Code Remarque " & Chr(187)
        DataGridView1.Rows.Add()
        DataGridView1.Item(0, 29).Value = "Présence de la " & Chr(171) & " Valeur de Précision " & Chr(187) & " sans le " & Chr(171) & " Type de Précision " & Chr(187)
        DataGridView1.Rows.Add()
        DataGridView1.Item(0, 30).Value = "Absence du " & Chr(171) & " Code Résultat Qualitatif " & Chr(187)


        DataGridView1.Item(0, 0).Style.SelectionBackColor = Color.White

    End Sub

    '=============================================================================================================================================
    ' Liste des Règles Structurelles non valides
    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Me.Text = " Liste des Règles Structurelles non valides"
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button2.Select()
        ' ==================================
        ' Remplissage 
        DataGridView1.Rows.Clear()
        For i = 0 To 40
            If Variables.TabRèglesInv(i) <> "" Then
                DataGridView1.Rows.Add()
                DataGridView1.Item(0, i).Value = Variables.TabRèglesInv(i)
            End If
        Next
        ' ==================================
        ' Effacement de la couleur de fond
        If DataGridView1.Item(0, 0).Value = "Pas d'invalidation de Règles Structurelles !" Then
            DataGridView1.Item(0, 0).Style.SelectionBackColor = Color.LightGreen
        Else
            DataGridView1.Item(0, 0).Style.SelectionBackColor = Color.White
        End If

    End Sub

    '=============================================================================================================================================
    ' PROCÉDURES DIVERSES
    '=============================================================================================================================================
    ' Traitement du repositionnement automatique de la feuille quand on l'a déplacée
    Private Sub RèglesStructurelles_MouseEnter(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.MouseEnter
        Me.CenterToScreen()
    End Sub

    Private Sub DataGridView1_MouseEnter(ByVal sender As Object, ByVal e As System.EventArgs) Handles DataGridView1.MouseEnter
        Me.CenterToScreen()
    End Sub

    '=============================================================================================================================================
    ' Traitement de réduction des fenêtres
    Private Sub RèglesStructurelles_Move(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Move
        ' ============================================
        ' Passage de la feuille principale transparente quand on réduit cette feuille
        If Me.WindowState = 1 Then
            Form1.Opacity = 0
        End If
        ' ============================================
        ' Passage de la feuille principale opaque quand on rétablit cette feuille
        If Me.WindowState = 0 Then
            Form1.Opacity = 1
        End If
    End Sub

    ' ==============================================================================================================================================
    ' Bouton de fermeture de l'application
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button2.Select()
        ' ==================================
        ' Demande de fermeture par Fondu
        Fermeture()
    End Sub

    '=============================================================================================================================================
    ' PROCÉDURE D'OUVERTURE ET DE FERMETURE
    '=============================================================================================================================================
    ' Procédure d'ouverture par Fondu
    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        Me.Opacity = Me.Opacity + 0.06
        If Me.Opacity >= 0.95 Then
            Timer1.Enabled = False
            Me.Opacity = 1
        End If
    End Sub

    Private Sub Fermeture()
        ' ============================================
        ' Essai pour éviter qu'une application ne passe devant la feuille ayant demandé celle-ci
        Form1.Activate()
        ' ============================================
        Me.Visible = False
    End Sub


End Class