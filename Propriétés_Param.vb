﻿Public Class Propriétés_Param

    '=============================================================================================================================================
    ' Procédure lors de la fermeture de la feuille par la croix
    Private Sub Propriétés_Param_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        '==============================================
        ' Blocage de la procédure de fermeture automatique
        e.Cancel = True
        '==============================================
        ' Demande de fermeture par Fondu
        Fermeture()
    End Sub

    ' ==============================================================================================================================================
    ' Procédure au chargement de la page
    Private Sub Propriétés_Param_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        ' ==================================
        ' Création d'un buffer pour le Drag and Drop du champ Résultat
        '       AddHandler TextBox11.DragEnter, AddressOf TextBox11_DragEnter

        ' Finalement qu'il existe ou pas c'est pareil
        ' ==================================

        ' ==================================
        ' Création d'un buffer pour le Drag and Drop du champ Résultat
        '       AddHandler TextBox11.DragDrop, AddressOf TextBox11_DragDrop

        ' Attention !!!  s'il existe cela double l'exécution , on s'en aprerçoit avec un textbox11.text = textbox11.text & ... 
        ' ==================================

        ' ==================================
        ' Initialisation du pointeur de paramètre sur l'index de paramètre égal à 1
        Variables.PointeurParamètre = 1
        ' ==================================
        ' Procédure de Transfert des Propriétés du Paramètre Sélectionné dans la Feuille des Paramètres
        TransfertParamDansFeuille()

        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button2.Select()

        '==============================================
        ' Demande d'ouverture par Fondu
        Me.Opacity = 0
        Timer1.Enabled = True
    End Sub

    '=============================================================================================================================================
    ' PROCÉDURES BOUTONS FAMILLES ET PARAMÈTRES
    '=============================================================================================================================================
    ' Bouton Famille Précédente
    Private Sub Button8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button8.Click
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button2.Select()
        ' ==================================
        ' Définition des variables
        '        Dim i As Integer
        ' ==================================
        ' Vérification d'une demande de sauvegarde d'informations
        '        If Variables.SauveInfosParam = True Then
        '        i = MsgBox(" Les informations du paramètre n'ont pas été validées ! " & vbCrLf & vbCrLf & "                           Continuez ?", MsgBoxStyle.Exclamation + MsgBoxStyle.YesNo + MsgBoxStyle.DefaultButton2, " QUADRISPEL - Erreur de validation")
        '        If i = 7 Then
        '        Exit Sub
        '        End If
        '        Variables.SauveInfosParam = False
        '        End If
        ' ==================================
        ' Vérification d'une demande de sauvegarde de résultat
        '        If Variables.SauveRésulParam = True Then
        '        i = MsgBox(" Le résultat du paramètre n'a pas été validé ! " & vbCrLf & vbCrLf & "                  Continuez ?", MsgBoxStyle.Exclamation + MsgBoxStyle.YesNo + MsgBoxStyle.DefaultButton2, " QUADRISPEL - Erreur de validation")
        '        If i = 7 Then
        '        Exit Sub
        '        End If
        '        Variables.SauveRésulParam = False
        '        End If
        ' ==================================
        ' Décrémentation du pointeur de tableau de Famille de Paramètres
        Variables.AiguilleurTableau = Variables.AiguilleurTableau - 1
        ' ==================================
        ' Vérification que le pointage est valide
        If Variables.AiguilleurTableau < 0 Then
            Variables.AiguilleurTableau = 0
            Exit Sub
        End If
        ' ==================================
        ' Récupération du nom de Famille de la feuille des Paramètres
        TextBox1.Text = LTrim(Familles.ListBox2.Items.Item(Variables.AiguilleurTableau))
        ' ==================================
        ' Validation des boutons de sélections des Familles
        Button9.Enabled = True
        If Variables.AiguilleurTableau < 1 Then
            Button8.Enabled = False
        End If


        ' ==================================
        ' Initialisation du pointeur de paramètre sur l'index de paramètre égal à 1
        Variables.PointeurParamètre = 1
        ' ==================================
        ' Procédure de Transfert des Propriétés du Paramètre Sélectionné dans la Feuille des Paramètres
        TransfertParamDansFeuille()

        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button2.Select()

    End Sub

    '=============================================================================================================================================
    ' Bouton Famille Suivante
    Private Sub Button9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button9.Click
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button2.Select()
        ' ==================================
        ' Définition des variables
        '        Dim i As Integer
        ' ==================================
        ' Vérification d'une demande de sauvegarde d'informations
        '        If Variables.SauveInfosParam = True Then
        '        i = MsgBox(" Les informations du paramètre n'ont pas été validées ! " & vbCrLf & vbCrLf & "                           Continuez ?", MsgBoxStyle.Exclamation + MsgBoxStyle.YesNo + MsgBoxStyle.DefaultButton2, " QUADRISPEL - Erreur de validation")
        '        If i = 7 Then
        '        Exit Sub
        '        End If
        '        Variables.SauveInfosParam = False
        '        End If
        ' ==================================
        ' Vérification d'une demande de sauvegarde de résultat
        '        If Variables.SauveRésulParam = True Then
        '        i = MsgBox(" Le résultat du paramètre n'a pas été validé ! " & vbCrLf & vbCrLf & "                  Continuez ?", MsgBoxStyle.Exclamation + MsgBoxStyle.YesNo + MsgBoxStyle.DefaultButton2, " QUADRISPEL - Erreur de validation")
        '        If i = 7 Then
        '        Exit Sub
        '        End If
        '        Variables.SauveRésulParam = False
        '        End If
        ' ==================================
        ' Incrémentation du pointeur de tableau de Famille de Paramètres
        Variables.AiguilleurTableau = Variables.AiguilleurTableau + 1
        ' ==================================
        ' Vérification que le pointage est valide
        If Variables.AiguilleurTableau >= Familles.ListBox2.Items.Count Then
            Variables.AiguilleurTableau = Familles.ListBox2.Items.Count - 1
            Exit Sub
        End If
        ' ==================================
        ' Récupération du nom de Famille de la feuille des Paramètres
        TextBox1.Text = LTrim(Familles.ListBox2.Items.Item(Variables.AiguilleurTableau))
        ' ==================================
        ' Validation des boutons de sélections des Familles
        Button8.Enabled = True
        If Variables.AiguilleurTableau = Familles.ListBox2.Items.Count - 1 Then
            Button9.Enabled = False
        End If
        ' ==================================
        ' Initialisation du pointeur de paramètre sur l'index de paramètre égal à 1
        Variables.PointeurParamètre = 1
        ' ==================================
        ' Procédure de Transfert des Propriétés du Paramètre Sélectionné dans la Feuille des Paramètres
        TransfertParamDansFeuille()
    End Sub

    '=============================================================================================================================================
    ' Bouton Paramètre Précédent
    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button2.Select()
        ' ==================================
        ' Définition des variables
        '        Dim i As Integer
        ' ==================================
        ' Vérification d'une demande de sauvegarde d'informations
        '        If Variables.SauveInfosParam = True Then
        '        i = MsgBox(" Les informations du paramètre n'ont pas été validées ! " & vbCrLf & vbCrLf & "                           Continuez ?", MsgBoxStyle.Exclamation + MsgBoxStyle.YesNo + MsgBoxStyle.DefaultButton2, " QUADRISPEL - Erreur de validation")
        '        If i = 7 Then
        '        Exit Sub
        '        End If
        '        Variables.SauveInfosParam = False
        '        End If
        ' ==================================
        ' Vérification d'une demande de sauvegarde de résultat
        '        If Variables.SauveRésulParam = True Then
        '            i = MsgBox(" Le résultat du paramètre n'a pas été validé ! " & vbCrLf & vbCrLf & "                  Continuez ?", MsgBoxStyle.Exclamation + MsgBoxStyle.YesNo + MsgBoxStyle.DefaultButton2, " QUADRISPEL - Erreur de validation")
        '            If i = 7 Then
        '                Exit Sub
        '            End If
        '                Variables.SauveRésulParam = False
        '        End If


        ' ==================================
        ' Décrémentation du pointeur de paramètre sur l'index de paramètre précédent
        Variables.PointeurParamètre = Variables.PointeurParamètre - 1
        ' ==================================
        ' Procédure de Transfert des Propriétés du Paramètre Sélectionné dans la Feuille des Paramètres
        TransfertParamDansFeuille()
    End Sub

    '=============================================================================================================================================
    ' Bouton Paramètre Suivant
    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button2.Select()
        ' ==================================
        ' Définition des variables
        '        Dim i As Integer
        ' ==================================
        ' Vérification d'une demande de sauvegarde d'informations
        '        If Variables.SauveInfosParam = True Then
        '            i = MsgBox(" Les informations du paramètre n'ont pas été validées ! " & vbCrLf & vbCrLf & "                           Continuez ?", MsgBoxStyle.Exclamation + MsgBoxStyle.YesNo + MsgBoxStyle.DefaultButton2, " QUADRISPEL - Erreur de validation")
        '            If i = 7 Then
        '                Exit Sub
        '            End If
        '            Variables.SauveInfosParam = False
        '        End If
        ' ==================================
        ' Vérification d'une demande de sauvegarde de résultat
        '        If Variables.SauveRésulParam = True Then
        '            i = MsgBox(" Le résultat du paramètre n'a pas été validé ! " & vbCrLf & vbCrLf & "                  Continuez ?", MsgBoxStyle.Exclamation + MsgBoxStyle.YesNo + MsgBoxStyle.DefaultButton2, " QUADRISPEL - Erreur de validation")
        '            If i = 7 Then
        '                Exit Sub
        '            End If
        '            Variables.SauveRésulParam = False
        '        End If

        ' ==================================
        ' Incrémentation du pointeur de paramètre sur l'index de paramètre suivant
        Variables.PointeurParamètre = Variables.PointeurParamètre + 1
        ' ==================================
        ' Procédure de Transfert des Propriétés du Paramètre Sélectionné dans la Feuille des Paramètres
        TransfertParamDansFeuille()

    End Sub

    '=============================================================================================================================================
    ' Procédure de Transfert des Propriétés du Paramètre Sélectionné dans la Feuille des Paramètres
    Private Sub TransfertParamDansFeuille()
        ' ==================================
        ' Initialisation
        Dim i As Integer
        '       Dim j As Integer
        Dim PremierParam As Integer
        Variables.NbrIndex = 0
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button2.Select()
        ' ==================================
        ' Recherche de l'adresse de la famille sélectionnée
        For i = 0 To 500
            If TextBox1.Text = Variables.TabRéfPara(i, 0) Then
                PremierParam = i
                Exit For
            End If
        Next
        ' ==================================
        ' Balayage des index pour le transfert des informations du paramètre sélectionné de la famille sélectionnée dans la Feuille des Paramètres
        For i = PremierParam To 500
            ' ==================================
            ' Sortie si changement de famille
            If TextBox1.Text <> Variables.TabRéfPara(i, 0) Then
                Exit For
            End If
            ' ==================================
            ' Incrémentation de l'indicateur du nombre d'index si valeur d'index valide
            If Val(Variables.TabRéfPara(i, 21)) <> 0 Then
                Variables.NbrIndex = Variables.NbrIndex + 1
            End If
            ' ==================================
            ' Recherche du paramètre dont l'index correspond au Pointeur de Paramètre
            If Variables.TabRéfPara(i, 21) = Variables.PointeurParamètre Then
                ' ==================================
                ' Récupération de l'adresse du Paramètre dans le Tableau
                Variables.AdresseTabPara = i
                ' ==================================
                ' Col 1  : Nom du paramètre
                TextBox2.Text = Variables.TabRéfPara(i, 1)
                ' ==================================
                ' Col 2  : Libellé du paramètre
                TextBox19.Text = Variables.TabRéfPara(i, 2)
                ' ==================================
                ' Proposition de conserver les informations des champs du Paramètre précédent
                '                If Variables.PointeurParamètre <> 1 And Variables.TabRéfPara(i, 4) = "" And Variables.TabRéfPara(i, 5) = "" And Variables.TabRéfPara(i, 6) = "" And Variables.TabRéfPara(i, 7) = "" And Variables.TabRéfPara(i, 8) = "" And Variables.TabRéfPara(i, 9) = "" And Variables.TabRéfPara(i, 10) = "" And Variables.TabRéfPara(i, 11) = "" And Variables.TabRéfPara(i, 12) = "" And Variables.TabRéfPara(i, 13) = "" And Variables.TabRéfPara(i, 14) = "" And Variables.TabRéfPara(i, 15) = "" And Variables.TabRéfPara(i, 16) = "" And Variables.TabRéfPara(i, 17) = "" And Variables.TabRéfPara(i, 18) = "" And Variables.TabRéfPara(i, 19) = "" And Variables.TabRéfPara(i, 20) = "" Then
                ' ==================================
                ' Proposition valider si tous les champs sont vides
                '               If TextBox24.Text <> "" Or TextBox10.Text <> "" Or TextBox5.Text <> "" Or TextBox26.Text <> "" Or TextBox7.Text <> "" Or TextBox9.Text <> "" Or TextBox15.Text <> "" Or TextBox18.Text <> "" Or TextBox22.Text <> "" Or TextBox21.Text <> "" Or TextBox20.Text <> "" Or TextBox14.Text <> "" Or TextBox27.Text <> "" Or TextBox11.Text <> "" Or TextBox12.Text <> "" Or TextBox17.Text <> "" Or TextBox25.Text <> "" Then
                ' ==================================
                ' Message de proposition
                '               j = MsgBox(" Désirez-vous utiliser les informations affichées ? ", MsgBoxStyle.Information + MsgBoxStyle.YesNo + MsgBoxStyle.DefaultButton1, " QUADRISPEL - Informations du Paramètre précédent")
                ' ==================================
                ' Traitement si non à la question
                '               If j = 7 Then
                ' ==================================
                ' Affectation dans le Tableau des Paramètres des Familles de l'index du paramètre sélectionné
                ' Col 3  : Code Sandre du paramètre

                ' Col 4  : Dénomination Analyseur
                '                TextBox24.Text = Variables.TabRéfPara(i, 4)
                ' Col 5  : Libellé Analyseur
                '                TextBox10.Text = Variables.TabRéfPara(i, 5)
                ' Col 6  : Dénomination méthode
                '                TextBox5.Text = Variables.TabRéfPara(i, 6)
                ' Col 7  : Engin d'analyse
                '               TextBox26.Text = Variables.TabRéfPara(i, 7)
                ' Col 8  : Dénomination fraction
                '                TextBox7.Text = Variables.TabRéfPara(i, 8)
                ' Col 9  : Libellé support
                '                TextBox9.Text = Variables.TabRéfPara(i, 9)
                ' Col 10 : Dénomination engin de prélèvement
                '                TextBox15.Text = Variables.TabRéfPara(i, 10)
                ' Col 11 : Précision
                '                TextBox18.Text = Variables.TabRéfPara(i, 11)
                ' Col 12 : Type Précision
                '                TextBox22.Text = Variables.TabRéfPara(i, 12)
                ' Col 13 : Niveau de saisie
                '                TextBox21.Text = Variables.TabRéfPara(i, 13)
                ' Col 14 : Seuil de détection
                '                TextBox20.Text = Variables.TabRéfPara(i, 14)
                ' Col 15 : Résultat qualitatif
                '                TextBox14.Text = Variables.TabRéfPara(i, 15)
                ' Col 16 : Numéro individu
                '                TextBox27.Text = Variables.TabRéfPara(i, 16)
                ' Col 17 : Résultat
                '                TextBox11.Text = Variables.TabRéfPara(i, 17)
                ' Col 18 : Unité
                '                TextBox12.Text = Variables.TabRéfPara(i, 18)
                ' Col 19 : Commentaire mesure
                '                TextBox17.Text = Variables.TabRéfPara(i, 19)
                ' Col 20 : Remarque
                '                TextBox25.Text = Variables.TabRéfPara(i, 20)
                '            End If
                '            End If
                '            Else
                ' ==================================
                ' Affectation dans le Tableau des Paramètres des Familles de l'index du paramètre sélectionné
                ' Col 3  : Code Sandre du paramètre

                ' Col 4  : Dénomination Analyseur
                TextBox24.Text = Variables.TabRéfPara(i, 4)
                ' Col 5  : Libellé Analyseur
                TextBox10.Text = Variables.TabRéfPara(i, 5)
                ' Col 6  : Dénomination méthode
                TextBox5.Text = Variables.TabRéfPara(i, 6)
                ' Col 7  : Engin d'analyse
                TextBox26.Text = Variables.TabRéfPara(i, 7)
                ' Col 8  : Dénomination fraction
                TextBox7.Text = Variables.TabRéfPara(i, 8)
                ' Col 9  : Libellé support
                TextBox9.Text = Variables.TabRéfPara(i, 9)
                ' Col 10 : Dénomination engin de prélèvement
                TextBox15.Text = Variables.TabRéfPara(i, 10)
                ' Col 11 : Précision
                TextBox18.Text = Variables.TabRéfPara(i, 11)
                ' Col 12 : Type Précision
                TextBox22.Text = Variables.TabRéfPara(i, 12)
                ' Col 13 : Niveau de saisie
                TextBox21.Text = Variables.TabRéfPara(i, 13)
                ' Col 14 : Seuil de détection
                TextBox20.Text = Variables.TabRéfPara(i, 14)
                ' Col 15 : Libellé Résultat qualitatif
                TextBox14.Text = Variables.TabRéfPara(i, 15)
                ' Col 16 : Numéro individu
                TextBox27.Text = Variables.TabRéfPara(i, 16)
                ' Col 17 : Résultat
                TextBox11.Text = Variables.TabRéfPara(i, 17)
                ' Col 18 : Unité
                TextBox12.Text = Variables.TabRéfPara(i, 18)
                ' Col 19 : Commentaire mesure
                TextBox17.Text = Variables.TabRéfPara(i, 19)
                ' Col 20 : Remarque
                TextBox25.Text = Variables.TabRéfPara(i, 20)


                ' Col 23 : Code Résultat Qualitatif
                TextBox33.Text = Variables.TabRéfPara(i, 23)
                ' Col 24 : Code Groupe Taxon Résultat
                TextBox34.Text = Variables.TabRéfPara(i, 24)
                ' Col 25 : Code Taxon Résultat
                TextBox35.Text = Variables.TabRéfPara(i, 25)

                '           End If
            End If
        Next
        ' ==================================
        ' Vérification si c'est le premier paramètre
        Button4.Enabled = True
        If Variables.PointeurParamètre = 1 Then
            Button4.Enabled = False
        End If
        ' ==================================
        ' Vérification si c'est le dernier paramètre
        Button3.Enabled = True
        If Variables.PointeurParamètre = Variables.NbrIndex Then
            Button3.Enabled = False
        End If
        ' ============================================
        ' Réinitialisation de la variable de sauvegarde des informations
        '        Variables.SauveInfosParam = False
        ' ============================================
        ' Réinitialisation de la variable de sauvegarde du résultat
        '        Variables.SauveRésulParam = False
        ' ==================================
        ' Effacement du nombre de caractères autorisé
        GroupBox18.Text = " Commentaires Mesure "

    End Sub

    '=============================================================================================================================================
    ' Bouton de validation de transfert des informations paramètres vers le paramètre sélectionné
    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button2.Select()
        ' ==================================
        TransfertFeuilleDansParam()
        ' ==================================
        ' Vérification sauvegarde à la fermeture
        Variables.SauveFichierRésultat = True
    End Sub

    '=============================================================================================================================================
    ' Procédure de Transfert des Informations de la Feuille des Paramètres dans les Propriétés du Paramètre Sélectionné
    Private Sub TransfertFeuilleDansParam()
        ' ==================================
        ' Initialisation
        Dim i As Integer
        Dim PremierParam As Integer
        ' ==================================
        ' Recherche de l'adresse de la famille sélectionnée
        For i = 0 To 500
            If TextBox1.Text = Variables.TabRéfPara(i, 0) Then
                PremierParam = i
                Exit For
            End If
        Next
        ' ==================================
        ' Balayage des index pour le transfert des informations de la Feuille des Paramètres dans le paramètre sélectionné de la famille sélectionnée
        For i = PremierParam To 500
            ' ==================================
            ' Recherche du paramètre dont l'index correspond au Pointeur de Paramètre
            If Variables.TabRéfPara(i, 21) = Variables.PointeurParamètre Then
                ' ==================================
                ' Affectation dans le Tableau des Paramètres des Familles de l'index du paramètre sélectionné
                ' Col 1  : Nom du paramètre
                ' Col 2  : Libellé du paramètre
                ' Col 3  : Code Sandre du paramètre

                ' Col 4  : Dénomination Analyseur
                Variables.TabRéfPara(i, 4) = TextBox24.Text
                ' Col 5  : Libellé Analyseur
                Variables.TabRéfPara(i, 5) = TextBox10.Text
                ' Col 6  : Dénomination méthode
                Variables.TabRéfPara(i, 6) = TextBox5.Text
                ' Col 7  : Engin d'analyse
                Variables.TabRéfPara(i, 7) = TextBox26.Text
                ' Col 8  : Dénomination fraction
                Variables.TabRéfPara(i, 8) = TextBox7.Text
                ' Col 9  : Libellé support
                Variables.TabRéfPara(i, 9) = TextBox9.Text
                ' Col 10 : Dénomination engin de prélèvement
                Variables.TabRéfPara(i, 10) = TextBox15.Text
                ' Col 11 : Précision
                Variables.TabRéfPara(i, 11) = TextBox18.Text
                ' Col 12 : Type Précision
                Variables.TabRéfPara(i, 12) = TextBox22.Text
                ' Col 13 : Niveau de saisie
                Variables.TabRéfPara(i, 13) = TextBox21.Text
                ' Col 14 : Seuil de détection
                Variables.TabRéfPara(i, 14) = TextBox20.Text
                ' Col 15 : Libellé Résultat qualitatif
                Variables.TabRéfPara(i, 15) = TextBox14.Text
                ' Col 16 : Numéro individu
                Variables.TabRéfPara(i, 16) = TextBox27.Text
                ' Col 17 : Résultat
                Variables.TabRéfPara(i, 17) = TextBox11.Text
                ' Col 18 : Unité
                Variables.TabRéfPara(i, 18) = TextBox12.Text
                ' Col 19 : Commentaire mesure
                Variables.TabRéfPara(i, 19) = TextBox17.Text
                ' Col 20 : Remarque
                Variables.TabRéfPara(i, 20) = TextBox25.Text


                ' Col 23 : Code Résultat Qualitatif
                Variables.TabRéfPara(i, 23) = TextBox33.Text
                ' Col 24 : Code Groupe Taxon Résultat
                Variables.TabRéfPara(i, 24) = TextBox34.Text
                ' Col 25 : Code Taxon Résultat
                Variables.TabRéfPara(i, 25) = TextBox35.Text


                Exit For

            End If
        Next
        ' ============================================
        ' Réinitialisation de la variable de sauvegarde des informations
        '        Variables.SauveInfosParam = False
        ' ============================================
        ' Réinitialisation de la variable de sauvegarde du résultat
        '        Variables.SauveRésulParam = False
    End Sub



    '=============================================================================================================================================
    ' Procédure Drag and Drop sur le champ Résultat à partir d'une information extérieure à l'application
    Private Sub TextBox11_DragDrop(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles TextBox11.DragDrop

        Dim i As Integer

        ' ==================================
        If e.Data.GetDataPresent(DataFormats.Text) Then
            ' ==================================
            ' Copie l'information dans le texte au format string
            TextBox11.Text = e.Data.GetData(DataFormats.Text).ToString



            ' ==================================
            ' Vérification de la non position du point décimal à gauche ou à droite
            If Strings.Left(TextBox11.Text, 1) = "." Or Strings.Left(TextBox11.Text, 1) = "," Then
                MsgBox("Il manque la partie entière du Résultat !", MsgBoxStyle.Critical, "QUADRISPEL - Erreur de Résultat")
                Exit Sub
            End If
            If Strings.Right(TextBox11.Text, 1) = "." Or Strings.Right(TextBox11.Text, 1) = "," Then
                MsgBox("Il manque la partie décimale du Résultat !", MsgBoxStyle.Critical, "QUADRISPEL - Erreur de Résultat")
                Exit Sub
            End If


            ' ==================================
            ' Traitement de la vérification numérique
            If Len(TextBox11.Text) <> 0 Then
                For i = 1 To Len(TextBox11.Text)
                    If Strings.Mid(TextBox11.Text, i, 1) <> "1" And Strings.Mid(TextBox11.Text, i, 1) <> "2" And Strings.Mid(TextBox11.Text, i, 1) <> "3" And Strings.Mid(TextBox11.Text, i, 1) <> "4" And Strings.Mid(TextBox11.Text, i, 1) <> "5" And Strings.Mid(TextBox11.Text, i, 1) <> "6" And Strings.Mid(TextBox11.Text, i, 1) <> "7" And Strings.Mid(TextBox11.Text, i, 1) <> "8" And Strings.Mid(TextBox11.Text, i, 1) <> "9" And Strings.Mid(TextBox11.Text, i, 1) <> "0" And Strings.Mid(TextBox11.Text, i, 1) <> "," And Strings.Mid(TextBox11.Text, i, 1) <> "." Then
                        '                    MsgBox("Le " & i & "° caractère n'est pas valide !", MsgBoxStyle.Critical, "QUADRISPEL - Erreur de Résultat")

                        TextBox11.Text = Strings.Left(TextBox11.Text, i - 1)
                        '               TextBox11.Undo()
                        SendKeys.Send("{END}")

                        Exit Sub
                    End If
                Next
            End If


        End If
    End Sub

    '=============================================================================================================================================
    ' Procédure Drag and Drop sur le champ Résultat à partir d'une information extérieure à l'application
    Private Sub TextBox11_DragEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles TextBox11.DragEnter
        ' ==================================
        If e.Data.GetDataPresent(DataFormats.Text) Then
            ' ==================================
            ' Affecte la forme du curseur sur le champ texte
            e.Effect = DragDropEffects.Copy
        Else
            ' ==================================
            ' Ramène la forme du curseur par défaut
            e.Effect = DragDropEffects.None
        End If
    End Sub


    '=============================================================================================================================================
    ' Traitements de la valeur du résultat après la touche "Entrée" sur le champ Résultat
    Private Sub TextBox11_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TextBox11.KeyDown


        ' ==================================
        ' Traitement de la vérification numérique
        '       If e.KeyCode <> Asc("1") And e.KeyCode <> Asc("2") And e.KeyCode <> Asc("3") And e.KeyCode <> Asc("4") And e.KeyCode <> Asc("5") And e.KeyCode <> Asc("6") And e.KeyCode <> Asc("7") And e.KeyCode <> Asc("8") And e.KeyCode <> Asc("9") And e.KeyCode <> Asc("0") And e.KeyCode <> Asc(",") And e.KeyCode <> Asc(".") Then
        '           MsgBox("Le caractère n'est pas valide !", MsgBoxStyle.Critical, "QUADRISPEL - Erreur de Résultat")
        '       Exit Sub
        '       End If


        ' ==================================
        ' Traitement de la valeur du résultat après la touche "Entrée"
        If e.KeyCode = Asc(Chr(13)) Then

            ' ==================================
            ' Vérification de la non position du point décimal à gauche ou à droite
            If Strings.Left(TextBox11.Text, 1) = "." Or Strings.Left(TextBox11.Text, 1) = "," Then
                MsgBox("Il manque la partie entière du Résultat !", MsgBoxStyle.Critical, "QUADRISPEL - Erreur de Résultat")
                Exit Sub
            End If
            If Strings.Right(TextBox11.Text, 1) = "." Or Strings.Right(TextBox11.Text, 1) = "," Then
                MsgBox("Il manque la partie décimale du Résultat !", MsgBoxStyle.Critical, "QUADRISPEL - Erreur de Résultat")
                Exit Sub
            End If



            ' ==================================
            ' Traitement de la vérification numérique
            If Len(TextBox11.Text) <> 0 Then
                For i = 1 To Len(TextBox11.Text)
                    If Strings.Mid(TextBox11.Text, i, 1) <> "1" And Strings.Mid(TextBox11.Text, i, 1) <> "2" And Strings.Mid(TextBox11.Text, i, 1) <> "3" And Strings.Mid(TextBox11.Text, i, 1) <> "4" And Strings.Mid(TextBox11.Text, i, 1) <> "5" And Strings.Mid(TextBox11.Text, i, 1) <> "6" And Strings.Mid(TextBox11.Text, i, 1) <> "7" And Strings.Mid(TextBox11.Text, i, 1) <> "8" And Strings.Mid(TextBox11.Text, i, 1) <> "9" And Strings.Mid(TextBox11.Text, i, 1) <> "0" And Strings.Mid(TextBox11.Text, i, 1) <> "," And Strings.Mid(TextBox11.Text, i, 1) <> "." Then
                        '                    MsgBox("Le " & i & "° caractère n'est pas valide !", MsgBoxStyle.Critical, "QUADRISPEL - Erreur de Résultat")
                        Exit Sub
                    End If
                Next
            End If





            ' ==================================
            ' Remplacement du point par la virgule
            TextBox11.Text = Replace(TextBox11.Text, ".", ",")
            ' ==================================
            ' Procédure de Transfert des Informations de la Feuille des Paramètres dans les Propriétés du Paramètre Sélectionné
            TransfertFeuilleDansParam()
            ' ==================================
            ' Sauvegarde de la nouvelle valeur de résultat dans la variable dédiée
            Variables.ValeurPrécédente = TextBox11.Text

            Exit Sub

        End If



    End Sub

    Private Sub TextBox11_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TextBox11.KeyUp
        ' ==================================
        ' Traitement de la valeur du résultat après la touche "Entrée"
        If e.KeyCode = Asc(Chr(13)) Then

            ' ==================================
            ' Vérification de la non position du point décimal à gauche ou à droite au cas ou le message KeyDown est validé par la touche Enter
            If Strings.Right(TextBox11.Text, 1) = "." Or Strings.Right(TextBox11.Text, 1) = "," Then
                Exit Sub
            End If
            If Strings.Right(TextBox11.Text, 1) = "." Or Strings.Right(TextBox11.Text, 1) = "," Then
                Exit Sub
            End If

            ' ==================================
            ' Traitement de la vérification numérique
            '            If e.KeyCode <> Asc("1") And e.KeyCode <> Asc("2") And e.KeyCode <> Asc("3") And e.KeyCode <> Asc("4") And e.KeyCode <> Asc("5") And e.KeyCode <> Asc("6") And e.KeyCode <> Asc("7") And e.KeyCode <> Asc("8") And e.KeyCode <> Asc("9") And e.KeyCode <> Asc("0") And e.KeyCode <> Asc(",") And e.KeyCode <> Asc(".") Then
            '            Exit Sub
            'End If



            ' ==================================
            ' Traitement de la vérification numérique
            If Len(TextBox11.Text) <> 0 Then
                For i = 1 To Len(TextBox11.Text)
                    If Strings.Mid(TextBox11.Text, i, 1) <> "1" And Strings.Mid(TextBox11.Text, i, 1) <> "2" And Strings.Mid(TextBox11.Text, i, 1) <> "3" And Strings.Mid(TextBox11.Text, i, 1) <> "4" And Strings.Mid(TextBox11.Text, i, 1) <> "5" And Strings.Mid(TextBox11.Text, i, 1) <> "6" And Strings.Mid(TextBox11.Text, i, 1) <> "7" And Strings.Mid(TextBox11.Text, i, 1) <> "8" And Strings.Mid(TextBox11.Text, i, 1) <> "9" And Strings.Mid(TextBox11.Text, i, 1) <> "0" And Strings.Mid(TextBox11.Text, i, 1) <> "," And Strings.Mid(TextBox11.Text, i, 1) <> "." Then
                        '                    MsgBox("Le " & i & "° caractère n'est pas valide !", MsgBoxStyle.Critical, "QUADRISPEL - Erreur de Résultat")

                        TextBox11.Text = Strings.Left(TextBox11.Text, i - 1)
                        '               TextBox11.Undo()
                        SendKeys.Send("{END}")
                        Exit Sub
                    End If
                Next
            End If






            ' ==================================
            ' Vérification si c'est le dernier Paramètre de la Famille
            If Variables.PointeurParamètre + 1 <= Variables.NbrIndex Then
                ' ==================================
                ' Incrémentation du pointeur de paramètre sur l'index de paramètre suivant
                Variables.PointeurParamètre = Variables.PointeurParamètre + 1
                ' ==================================
                ' Procédure de Transfert des Propriétés du Paramètre Sélectionné dans la Feuille des Paramètres
                TransfertParamDansFeuille()
                ' ==================================
                ' Vérification si la valeur précédente est proposée
                If TextBox11.Text = "" Then
                    ' ==================================
                    ' Chargement dans le champ de valeur du résultat par la valeur précédente
                    TextBox11.Text = Variables.ValeurPrécédente
                End If
                ' ==================================
                ' Sélection du champ Résultat

                TextBox11.Select()

                ' MsgBox("coucou")
                ' ==================================
                ' Traitement si dernier Paramètre d'une Famille atteint
            Else
                ' ==================================
                ' Renvoi du select sur le bouton caché
                Button2.Select()
                ' ==================================
                ' Passage à la Famille suivante si elle existe
                If Button9.Enabled <> False Then
                    ' ==================================
                    ' Incrémentation du pointeur de tableau de Famille de Paramètres
                    Variables.AiguilleurTableau = Variables.AiguilleurTableau + 1
                    ' ==================================
                    ' Vérification que le pointage est valide
                    If Variables.AiguilleurTableau >= Familles.ListBox2.Items.Count Then
                        Variables.AiguilleurTableau = Familles.ListBox2.Items.Count - 1
                        Exit Sub
                    End If
                    ' ==================================
                    ' Récupération du nom de Famille de la feuille des Paramètres
                    TextBox1.Text = LTrim(Familles.ListBox2.Items.Item(Variables.AiguilleurTableau))
                    ' ==================================
                    ' Validation des boutons de sélections des Familles
                    Button8.Enabled = True
                    If Variables.AiguilleurTableau = Familles.ListBox2.Items.Count - 1 Then
                        Button9.Enabled = False
                    End If
                    ' ==================================
                    ' Initialisation du pointeur de paramètre sur l'index de paramètre égal à 1
                    Variables.PointeurParamètre = 1
                    ' ==================================
                    ' Procédure de Transfert des Propriétés du Paramètre Sélectionné dans la Feuille des Paramètres
                    TransfertParamDansFeuille()
                    ' ==================================
                    ' Sélection du champ Résultat
                    TextBox11.Select()
                End If
            End If


        End If









        '       Exit Sub











        ' ==================================
        ' Vérification si fin des paramètres pour une famille sélectionnée
        '           If Button3.Enabled = False Then
        ' ==================================
        ' Passage à la Famille suivante si elle existe
        '           If Button9.Enabled <> False Then
        ' ==================================
        ' Incrémentation du pointeur de tableau de Famille de Paramètres
        '              Variables.AiguilleurTableau = Variables.AiguilleurTableau + 1
        ' ==================================
        ' Vérification que le pointage est valide
        '               If Variables.AiguilleurTableau >= Familles.ListBox2.Items.Count Then
        '                   Variables.AiguilleurTableau = Familles.ListBox2.Items.Count - 1
        '                   Exit Sub
        '               End If
        ' ==================================
        ' Récupération du nom de Famille de la feuille des Paramètres
        '               TextBox1.Text = LTrim(Familles.ListBox2.Items.Item(Variables.AiguilleurTableau))
        ' ==================================
        ' Validation des boutons de sélections des Familles
        '               Button8.Enabled = True
        '               If Variables.AiguilleurTableau = Familles.ListBox2.Items.Count - 1 Then
        '                   Button9.Enabled = False
        '               End If
        ' ==================================
        ' Initialisation du pointeur de paramètre sur l'index de paramètre égal à 1
        '               Variables.PointeurParamètre = 1
        ' ==================================
        ' Procédure de Transfert des Propriétés du Paramètre Sélectionné dans la Feuille des Paramètres
        '               TransfertParamDansFeuille()
        ' ==================================
        ' Effacement de la valeur précèdente si changement de famille
        '              Variables.ValeurPrécédente = ""
        'End If
        'End If

    End Sub


    '=============================================================================================================================================
    ' PROCÉDURE SUR LES CHAMPS DE PROPRIÉTÉS
    '=============================================================================================================================================
    ' Effacement des champs de Propriétés
    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button2.Select()
        ' ==================================
        ' Initialisation
        Dim j As Integer
        ' ==================================
        ' Vérification si tous les champs sont vides
        If TextBox24.Text <> "" Or TextBox10.Text <> "" Or TextBox5.Text <> "" Or TextBox26.Text <> "" Or TextBox7.Text <> "" Or TextBox9.Text <> "" Or TextBox15.Text <> "" Or TextBox18.Text <> "" Or TextBox22.Text <> "" Or TextBox21.Text <> "" Or TextBox20.Text <> "" Or TextBox14.Text <> "" Or TextBox27.Text <> "" Or TextBox11.Text <> "" Or TextBox12.Text <> "" Or TextBox17.Text <> "" Or TextBox25.Text <> "" Or TextBox33.Text <> "" Or TextBox34.Text <> "" Or TextBox35.Text <> "" Then
            ' ==================================
            ' Message d'information
            j = MsgBox(" Des champs contiennent des informations !" & vbCrLf & vbCrLf & "          Confirmez-vous l'effacement ? ", MsgBoxStyle.Exclamation + MsgBoxStyle.YesNo + MsgBoxStyle.DefaultButton2, " QUADRISPEL - Validation du collage")
            ' ==================================
            ' Traitement si non à la question
            If j = 7 Then
                Exit Sub
            End If
        End If
        ' ==================================
        ' Collage des champs de Propriétés
        ' Col 4  : Dénomination Analyseur
        TextBox24.Text = ""
        ' Col 5  : Libellé Analyseur
        TextBox10.Text = ""
        ' Col 6  : Dénomination méthode
        TextBox5.Text = ""
        ' Col 7  : Engin d'analyse
        TextBox26.Text = ""
        ' Col 8  : Dénomination fraction
        TextBox7.Text = ""
        ' Col 9  : Libellé support
        TextBox9.Text = ""
        ' Col 10 : Dénomination engin de prélèvement
        TextBox15.Text = ""
        ' Col 11 : Précision
        TextBox18.Text = ""
        ' Col 12 : Type Précision
        TextBox22.Text = ""
        ' Col 13 : Niveau de saisie
        TextBox21.Text = ""
        ' Col 14 : Seuil de détection
        TextBox20.Text = ""
        ' Col 15 : Libellé Résultat qualitatif
        TextBox14.Text = ""
        ' Col 16 : Numéro individu
        TextBox27.Text = ""
        ' Col 17 : Résultat
        TextBox11.Text = ""
        ' Col 18 : Unité
        TextBox12.Text = ""
        ' Col 19 : Commentaire mesure
        TextBox17.Text = ""
        ' Col 20 : Remarque
        TextBox25.Text = ""

        ' Col 23 : Code Résultat Qualitatif
        TextBox33.Text = ""
        ' Col 24 : Code Groupe Taxon Résultat
        TextBox34.Text = ""
        ' Col 25 : Code Taxon Résultat
        TextBox35.Text = ""


        ' ============================================
        ' Réinitialisation de la variable de sauvegarde des informations
        '        Variables.SauveInfosParam = True
        ' ============================================
        ' Réinitialisation de la variable de sauvegarde du résultat
        '        Variables.SauveRésulParam = True

    End Sub

    '=============================================================================================================================================
    ' Copie des champs de Propriétés
    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button2.Select()
        ' ==================================
        ' Vérification si tous les champs sont vides
        If TextBox24.Text = "" And TextBox10.Text = "" And TextBox5.Text = "" And TextBox26.Text = "" And TextBox7.Text = "" And TextBox9.Text = "" And TextBox15.Text = "" And TextBox18.Text = "" And TextBox22.Text = "" And TextBox21.Text = "" And TextBox20.Text = "" And TextBox14.Text = "" And TextBox27.Text = "" And TextBox11.Text = "" And TextBox12.Text = "" And TextBox17.Text = "" And TextBox25.Text = "" Then
            ' ==================================
            ' Invalidation du bouton de collage
            Button10.Enabled = False
            Exit Sub
        End If
        ' ==================================
        ' Copiage des champs de Propriétés
        ' Col 4  : Dénomination Analyseur
        Variables.Info1 = TextBox24.Text
        ' Col 5  : Libellé Analyseur
        Variables.Info2 = TextBox10.Text
        ' Col 6  : Dénomination méthode
        Variables.Info3 = TextBox5.Text
        ' Col 7  : Engin d'analyse
        Variables.Info4 = TextBox26.Text
        ' Col 8  : Dénomination fraction
        Variables.Info5 = TextBox7.Text
        ' Col 9  : Libellé support
        Variables.Info6 = TextBox9.Text
        ' Col 10 : Dénomination engin de prélèvement
        Variables.Info7 = TextBox15.Text
        ' Col 11 : Précision
        Variables.Info8 = TextBox18.Text
        ' Col 12 : Type Précision
        Variables.Info9 = TextBox22.Text
        ' Col 13 : Niveau de saisie
        Variables.Info10 = TextBox21.Text
        ' Col 14 : Seuil de détection
        Variables.Info11 = TextBox20.Text
        ' Col 15 : Libellé Résultat qualitatif
        Variables.Info12 = TextBox14.Text
        ' Col 16 : Numéro individu
        Variables.Info13 = TextBox27.Text
        ' Col 17 : Résultat
        Variables.Info14 = TextBox11.Text
        ' Col 18 : Unité
        Variables.Info15 = TextBox12.Text
        ' Col 19 : Commentaire mesure
        Variables.Info16 = TextBox17.Text
        ' Col 20 : Remarque
        Variables.Info17 = TextBox25.Text


        ' Col 23 : Code Résultat Qualitatif
        Variables.Info18 = TextBox33.Text
        ' Col 24 : Code Groupe Taxon Résultat
        Variables.Info19 = TextBox34.Text
        ' Col 25 : Code Taxon Résultat
        Variables.Info20 = TextBox35.Text


        ' ==================================
        '  Validation du bouton de collage
        Button10.Enabled = True

    End Sub

    '=============================================================================================================================================
    ' Collage des champs de Propriétés
    Private Sub Button10_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button10.Click
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button2.Select()
        ' ==================================
        ' Initialisation
        Dim j As Integer
        ' ==================================
        ' Vérification si tous les champs sont vides
        If TextBox24.Text <> "" Or TextBox10.Text <> "" Or TextBox5.Text <> "" Or TextBox26.Text <> "" Or TextBox7.Text <> "" Or TextBox9.Text <> "" Or TextBox15.Text <> "" Or TextBox18.Text <> "" Or TextBox22.Text <> "" Or TextBox21.Text <> "" Or TextBox20.Text <> "" Or TextBox14.Text <> "" Or TextBox27.Text <> "" Or TextBox11.Text <> "" Or TextBox12.Text <> "" Or TextBox17.Text <> "" Or TextBox25.Text <> "" Or TextBox33.Text <> "" Or TextBox34.Text <> "" Or TextBox35.Text <> "" Then
            ' ==================================
            ' Message d'information
            j = MsgBox(" Des champs contiennent des informations !" & vbCrLf & vbCrLf & "          Confirmez-vous le collage ? ", MsgBoxStyle.Exclamation + MsgBoxStyle.YesNo + MsgBoxStyle.DefaultButton2, " QUADRISPEL - Validation du collage")
            ' ==================================
            ' Traitement si non à la question
            If j = 7 Then
                Exit Sub
            End If
        End If
        ' ==================================
        ' Collage des champs de Propriétés
        ' Col 4  : Dénomination Analyseur
        TextBox24.Text = Variables.Info1
        ' Col 5  : Libellé Analyseur
        TextBox10.Text = Variables.Info2
        ' Col 6  : Dénomination méthode
        TextBox5.Text = Variables.Info3
        ' Col 7  : Engin d'analyse
        TextBox26.Text = Variables.Info4
        ' Col 8  : Dénomination fraction
        TextBox7.Text = Variables.Info5
        ' Col 9  : Libellé support
        TextBox9.Text = Variables.Info6
        ' Col 10 : Dénomination engin de prélèvement
        TextBox15.Text = Variables.Info7
        ' Col 11 : Précision
        TextBox18.Text = Variables.Info8
        ' Col 12 : Type Précision
        TextBox22.Text = Variables.Info9
        ' Col 13 : Niveau de saisie
        TextBox21.Text = Variables.Info10
        ' Col 14 : Seuil de détection
        TextBox20.Text = Variables.Info11
        ' Col 15 : Libellé Résultat qualitatif
        TextBox14.Text = Variables.Info12
        ' Col 16 : Numéro individu
        TextBox27.Text = Variables.Info13
        ' Col 17 : Résultat
        TextBox11.Text = Variables.Info14
        ' Col 18 : Unité
        TextBox12.Text = Variables.Info15
        ' Col 19 : Commentaire mesure
        TextBox17.Text = Variables.Info16
        ' Col 20 : Remarque
        TextBox25.Text = Variables.Info17


        ' Col 23 : Code Résultat Qualitatif
        TextBox33.Text = Variables.Info18
        ' Col 24 : Code Groupe Taxon Résultat
        TextBox34.Text = Variables.Info19
        ' Col 25 : Code Taxon Résultat
        TextBox35.Text = Variables.Info20


        ' ============================================
        ' Réinitialisation de la variable de sauvegarde des informations
        '        Variables.SauveInfosParam = True
        ' ============================================
        ' Réinitialisation de la variable de sauvegarde du résultat
        '        Variables.SauveRésulParam = True

    End Sub


    '=============================================================================================================================================
    ' PROCÉDURES DIVERSES
    '=============================================================================================================================================
    ' Traitement du repositionnement automatique de la feuille quand on l'a déplacée
    Private Sub Propriétés_Param_MouseEnter(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.MouseEnter
        ' ============================================
        ' Vérification si on est en positionnement libre de la feuille Propriétés et Résultats
        If Variables.FeuiProResLibre = True Then Exit Sub
        ' ============================================
        ' Recentrage de la feuille
        Me.CenterToScreen()
    End Sub

    '=============================================================================================================================================
    ' Traitement de réduction des fenêtres
    Private Sub Propriétés_Param_Move(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Move
        ' ============================================
        ' Vérification si on est en positionnement libre de la feuille Propriétés et Résultats
        If Variables.FeuiProResLibre = True Then Exit Sub
        ' ============================================
        ' Passage de la feuille principale transparente quand on réduit cette feuille
        If Me.WindowState = 1 Then
            Form1.Opacity = 0
        End If
        ' ============================================
        ' Passage de la feuille principale opaque quand on rétablit cette feuille
        If Me.WindowState = 0 Then
            Form1.Opacity = 1
        End If
    End Sub

    ' ==============================================================================================================================================
    ' Bouton de fermeture de l'application
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button2.Select()
        ' ==================================
        ' Demande de fermeture par Fondu
        Fermeture()
    End Sub


    '=============================================================================================================================================
    ' PROCÉDURE D'OUVERTURE ET DE FERMETURE
    '=============================================================================================================================================
    ' Procédure d'ouverture par Fondu
    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        Me.Opacity = Me.Opacity + 0.06
        If Me.Opacity >= 0.95 Then
            Timer1.Enabled = False
            Me.Opacity = 1
        End If
    End Sub

    Private Sub Fermeture()
        '        Dim i As Integer
        '==============================================
        ' Remise de la feuille en centrage automatique
        Variables.FeuiProResLibre = False
        ' ============================================
        ' Passage de la feuille principale opaque
        Form1.Opacity = 1

        '==============================================
        ' Évite d'avoir une seconde fois le message
        If Me.Visible = False Then Exit Sub
        ' ==================================
        ' Vérification d'une demande de sauvegarde d'informations
        '        If Variables.SauveInfosParam = True Then
        '            i = MsgBox(" Les informations du paramètre n'ont pas été validées ! " & vbCrLf & vbCrLf & "                           Continuez ?", MsgBoxStyle.Exclamation + MsgBoxStyle.YesNo + MsgBoxStyle.DefaultButton2, " QUADRISPEL - Erreur de validation")
        '            If i = 7 Then
        '                Exit Sub
        '            End If
        '            Variables.SauveInfosParam = False
        '        End If
        ' ==================================
        ' Vérification d'une demande de sauvegarde de résultat
        '        If Variables.SauveRésulParam = True Then
        '           i = MsgBox(" Le résultat du paramètre n'a pas été validé ! " & vbCrLf & vbCrLf & "                  Continuez ?", MsgBoxStyle.Exclamation + MsgBoxStyle.YesNo + MsgBoxStyle.DefaultButton2, " QUADRISPEL - Erreur de validation")
        '           If i = 7 Then
        '               Exit Sub
        '           End If
        '           Variables.SauveRésulParam = False
        '       End If
        ' ============================================
        ' Essai pour éviter qu'une application ne passe devant la feuille ayant demandé celle-ci
        Form1.Activate()

        ' ============================================
        Me.Visible = False
    End Sub


    '=============================================================================================================================================
    ' PROCÉDURE POUR ÉVITER LE CURSEUR CLIGNOTANT DANS LES CHAMPS
    '=============================================================================================================================================

    Private Sub TextBox1_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox1.GotFocus
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button2.Select()
    End Sub

    Private Sub TextBox2_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox2.GotFocus
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button2.Select()
    End Sub

    Private Sub TextBox4_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox4.GotFocus
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button2.Select()
    End Sub

    Private Sub TextBox5_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox5.GotFocus
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button2.Select()
    End Sub

    Private Sub TextBox6_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox6.GotFocus
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button2.Select()
    End Sub

    Private Sub TextBox7_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox7.GotFocus
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button2.Select()
    End Sub

    Private Sub TextBox9_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox9.GotFocus
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button2.Select()
    End Sub

    Private Sub TextBox10_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox10.GotFocus
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button2.Select()
    End Sub

    Private Sub TextBox12_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox12.GotFocus
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button2.Select()
    End Sub

    Private Sub TextBox13_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox13.GotFocus
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button2.Select()
    End Sub

    Private Sub TextBox15_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox15.GotFocus
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button2.Select()
    End Sub

    Private Sub TextBox16_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox16.GotFocus
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button2.Select()
    End Sub

    Private Sub TextBox18_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox18.GotFocus
        ' ==================================
        ' Renvoi du select sur le bouton caché
        '       Button2.Select()
    End Sub

    Private Sub TextBox19_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox19.GotFocus
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button2.Select()
    End Sub

    Private Sub TextBox21_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox21.GotFocus
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button2.Select()
    End Sub

    Private Sub TextBox22_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox22.GotFocus
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button2.Select()
    End Sub

    Private Sub TextBox24_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox24.GotFocus
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button2.Select()
    End Sub

    Private Sub TextBox25_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox25.GotFocus
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button2.Select()
    End Sub

    Private Sub TextBox26_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox26.GotFocus
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button2.Select()
    End Sub



    '=============================================================================================================================================
    ' PROCÉDURE POUR LE NOMBRE DE CARACTÈRES DANS LES CHAMPS
    '=============================================================================================================================================
    ' Affichage du nombre de caractères autorisé
    Private Sub TextBox17_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox17.GotFocus
        ' ==================================
        ' Affichage du nombre de caractères autorisé
        GroupBox18.Text = " Commentaires Mesure (" & 2000 - Len(TextBox17.Text) & ") "
    End Sub

    '=============================================================================================================================================
    ' Affichage du nombre de caractères autorisé
    Private Sub TextBox17_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox17.TextChanged
        '       Variables.SauveInfosParam = True
        ' ==================================
        ' Affichage du nombre de caractères autorisé
        GroupBox18.Text = " Commentaires Mesure (" & 2000 - Len(TextBox17.Text) & ") "
        ' ==================================
        ' Validation automatique - Col 19 : Commentaire mesure
        If TextBox17.Text = Variables.TabRéfPara(Variables.AdresseTabPara, 19) Then Exit Sub
        ' ==================================
        ' Sauvegarde du Paramètre
        Variables.TabRéfPara(Variables.AdresseTabPara, 19) = TextBox17.Text
        ' ==================================
        ' Proposition sauvegarde à la fermeture
        Variables.SauveFichierMasque = True
    End Sub

    '=============================================================================================================================================
    ' Effacement du nombre de caractères autorisé
    Private Sub TextBox17_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox17.LostFocus
        ' ==================================
        ' Effacement du nombre de caractères autorisé
        GroupBox18.Text = " Commentaires Mesure "
    End Sub


    '=============================================================================================================================================
    ' PROCÉDURES DE SÉLECTIONS DES CHAMPS À RECOPIER
    '=============================================================================================================================================
    ' Sélection du texte Méthodes
    Private Sub TextBox5_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox5.MouseDown
        ' ============================================
        ' Traitement de la demande du bouton Droit
        If e.Button = Windows.Forms.MouseButtons.Right Then
            ' ============================================
            ' Text de la couleur de fond du texte
            If TextBox5.BackColor <> Color.Pink Then
                ' ============================================
                ' Changement de la couleur de fond du texte
                TextBox5.BackColor = Color.Pink
            Else
                ' ============================================
                ' Changement de la couleur de fond du texte
                TextBox5.BackColor = Color.FromArgb(255, 255, 200)
            End If
            ' ============================================
            ' Ascenseur des boutons
            Superviseur_Boutons()
        End If
    End Sub

    '=============================================================================================================================================
    ' Sélection du texte Dénomination Engin Analyse
    Private Sub TextBox26_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox26.MouseDown
        ' ============================================
        ' Traitement de la demande du bouton Droit
        If e.Button = Windows.Forms.MouseButtons.Right Then
            ' ============================================
            ' Text de la couleur de fond du texte
            If TextBox26.BackColor <> Color.Pink Then
                ' ============================================
                ' Changement de la couleur de fond du texte
                TextBox26.BackColor = Color.Pink
            Else
                ' ============================================
                ' Changement de la couleur de fond du texte
                TextBox26.BackColor = Color.White
            End If
            ' ============================================
            ' Ascenseur des boutons
            Superviseur_Boutons()
        End If
    End Sub

    '=============================================================================================================================================
    ' Sélection du texte Libellé Analyseur
    Private Sub TextBox10_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox10.MouseDown
        ' ============================================
        ' Traitement de la demande du bouton Droit
        If e.Button = Windows.Forms.MouseButtons.Right Then
            ' ============================================
            ' Text de la couleur de fond du texte
            If TextBox10.BackColor <> Color.Pink Then
                ' ============================================
                ' Changement de la couleur de fond du texte
                TextBox10.BackColor = Color.Pink
                TextBox24.BackColor = Color.Pink
            Else
                ' ============================================
                ' Changement de la couleur de fond du texte
                TextBox10.BackColor = Color.FromArgb(255, 255, 200)
                TextBox24.BackColor = Color.FromArgb(255, 255, 200)
            End If
            ' ============================================
            ' Ascenseur des boutons
            Superviseur_Boutons()
        End If
    End Sub

    '=============================================================================================================================================
    ' Sélection du texte Dénomination Analyseur
    Private Sub TextBox24_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox24.MouseDown
        ' ============================================
        ' Traitement de la demande du bouton Droit
        If e.Button = Windows.Forms.MouseButtons.Right Then
            ' ============================================
            ' Text de la couleur de fond du texte
            If TextBox24.BackColor <> Color.Pink Then
                ' ============================================
                ' Changement de la couleur de fond du texte
                TextBox24.BackColor = Color.Pink
                TextBox10.BackColor = Color.Pink
            Else
                ' ============================================
                ' Changement de la couleur de fond du texte
                TextBox24.BackColor = Color.FromArgb(255, 255, 200)
                TextBox10.BackColor = Color.FromArgb(255, 255, 200)
            End If
            ' ============================================
            ' Ascenseur des boutons
            Superviseur_Boutons()
        End If
    End Sub

    '=============================================================================================================================================
    ' Sélection du texte Dénomination Fraction
    Private Sub TextBox7_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox7.MouseDown
        ' ============================================
        ' Traitement de la demande du bouton Droit
        If e.Button = Windows.Forms.MouseButtons.Right Then
            ' ============================================
            ' Text de la couleur de fond du texte
            If TextBox7.BackColor <> Color.Pink Then
                ' ============================================
                ' Changement de la couleur de fond du texte
                TextBox7.BackColor = Color.Pink
            Else
                ' ============================================
                ' Changement de la couleur de fond du texte
                TextBox7.BackColor = Color.FromArgb(255, 255, 200)
            End If
            ' ============================================
            ' Ascenseur des boutons
            Superviseur_Boutons()
        End If
    End Sub

    '=============================================================================================================================================
    ' Sélection du texte Dénomination Support
    Private Sub TextBox9_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox9.MouseDown
        ' ============================================
        ' Traitement de la demande du bouton Droit
        If e.Button = Windows.Forms.MouseButtons.Right Then
            ' ============================================
            ' Text de la couleur de fond du texte
            If TextBox9.BackColor <> Color.Pink Then
                ' ============================================
                ' Changement de la couleur de fond du texte
                TextBox9.BackColor = Color.Pink
            Else
                ' ============================================
                ' Changement de la couleur de fond du texte
                TextBox9.BackColor = Color.FromArgb(255, 255, 200)
            End If
            ' ============================================
            ' Ascenseur des boutons
            Superviseur_Boutons()
        End If
    End Sub

    '=============================================================================================================================================
    ' Sélection du texte Dénomination Engin de Prélèvement
    Private Sub TextBox15_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox15.MouseDown
        ' ============================================
        ' Traitement de la demande du bouton Droit
        If e.Button = Windows.Forms.MouseButtons.Right Then
            ' ============================================
            ' Text de la couleur de fond du texte
            If TextBox15.BackColor <> Color.Pink Then
                ' ============================================
                ' Changement de la couleur de fond du texte
                TextBox15.BackColor = Color.Pink
            Else
                ' ============================================
                ' Changement de la couleur de fond du texte
                TextBox15.BackColor = Color.FromArgb(255, 255, 200)
            End If
            ' ============================================
            ' Ascenseur des boutons
            Superviseur_Boutons()
        End If
    End Sub

    '=============================================================================================================================================
    ' Sélection du texte Dénomination Précision
    Private Sub TextBox18_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox18.MouseDown
        ' ============================================
        ' Traitement de la demande du bouton Droit
        If e.Button = Windows.Forms.MouseButtons.Right Then
            ' ============================================
            ' Text de la couleur de fond du texte
            If TextBox18.BackColor <> Color.Pink Then
                ' ============================================
                ' Changement de la couleur de fond du texte
                TextBox18.BackColor = Color.Pink
            Else
                ' ============================================
                ' Changement de la couleur de fond du texte
                TextBox18.BackColor = Color.White
            End If
            ' ============================================
            ' Ascenseur des boutons
            Superviseur_Boutons()
        End If
    End Sub

    '=============================================================================================================================================
    ' Sélection du texte Dénomination Type Précision
    Private Sub TextBox22_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox22.MouseDown
        ' ============================================
        ' Traitement de la demande du bouton Droit
        If e.Button = Windows.Forms.MouseButtons.Right Then
            ' ============================================
            ' Text de la couleur de fond du texte
            If TextBox22.BackColor <> Color.Pink Then
                ' ============================================
                ' Changement de la couleur de fond du texte
                TextBox22.BackColor = Color.Pink
            Else
                ' ============================================
                ' Changement de la couleur de fond du texte
                TextBox22.BackColor = Color.White
            End If
            ' ============================================
            ' Ascenseur des boutons
            Superviseur_Boutons()
        End If
    End Sub

    '=============================================================================================================================================
    ' Sélection du texte Dénomination Niveau de Saisie
    Private Sub TextBox21_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox21.MouseDown
        ' ============================================
        ' Traitement de la demande du bouton Droit
        If e.Button = Windows.Forms.MouseButtons.Right Then
            ' ============================================
            ' Text de la couleur de fond du texte
            If TextBox21.BackColor <> Color.Pink Then
                ' ============================================
                ' Changement de la couleur de fond du texte
                TextBox21.BackColor = Color.Pink
            Else
                ' ============================================
                ' Changement de la couleur de fond du texte
                TextBox21.BackColor = Color.FromArgb(255, 255, 200)
            End If
            ' ============================================
            ' Ascenseur des boutons
            Superviseur_Boutons()
        End If
    End Sub

    '=============================================================================================================================================
    ' Sélection du texte Dénomination Seuil de Détection
    Private Sub TextBox20_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox20.MouseDown
        ' ============================================
        ' Traitement de la demande du bouton Droit
        If e.Button = Windows.Forms.MouseButtons.Right Then
            ' ============================================
            ' Text de la couleur de fond du texte
            If TextBox20.BackColor <> Color.Pink Then
                ' ============================================
                ' Changement de la couleur de fond du texte
                TextBox20.BackColor = Color.Pink
            Else
                ' ============================================
                ' Changement de la couleur de fond du texte
                TextBox20.BackColor = Color.White
            End If
            ' ============================================
            ' Ascenseur des boutons
            Superviseur_Boutons()
        End If
    End Sub

    Private Sub TextBox20_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox20.LostFocus
        ' ==================================
        ' Remplacement du point par la virgule
        TextBox20.Text = Replace(TextBox20.Text, ".", ",")
    End Sub

    '=============================================================================================================================================
    ' Sélection du texte Dénomination Libellé Résultat Qualitatif
    Private Sub TextBox14_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox14.MouseDown
        ' ============================================
        ' Traitement de la demande du bouton Droit
        If e.Button = Windows.Forms.MouseButtons.Right Then
            ' ============================================
            ' Text de la couleur de fond du texte
            If TextBox14.BackColor <> Color.Pink Then
                ' ============================================
                ' Changement de la couleur de fond du texte
                TextBox14.BackColor = Color.Pink
            Else
                ' ============================================
                ' Changement de la couleur de fond du texte
                TextBox14.BackColor = Color.White
            End If
            ' ============================================
            ' Ascenseur des boutons
            Superviseur_Boutons()
        End If
    End Sub

    '=============================================================================================================================================
    ' Sélection du texte Dénomination Numéro Individu
    Private Sub TextBox27_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox27.MouseDown
        ' ============================================
        ' Traitement de la demande du bouton Droit
        If e.Button = Windows.Forms.MouseButtons.Right Then
            ' ============================================
            ' Text de la couleur de fond du texte
            If TextBox27.BackColor <> Color.Pink Then
                ' ============================================
                ' Changement de la couleur de fond du texte
                TextBox27.BackColor = Color.Pink
            Else
                ' ============================================
                ' Changement de la couleur de fond du texte
                TextBox27.BackColor = Color.White
            End If
            ' ============================================
            ' Ascenseur des boutons
            Superviseur_Boutons()
        End If
    End Sub

    '=============================================================================================================================================
    ' Sélection du texte Dénomination Commentaire Mesure
    Private Sub TextBox17_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox17.MouseDown
        ' ============================================
        ' Traitement de la demande du bouton Droit
        If e.Button = Windows.Forms.MouseButtons.Right Then
            ' ============================================
            ' Text de la couleur de fond du texte
            If TextBox17.BackColor <> Color.Pink Then
                ' ============================================
                ' Changement de la couleur de fond du texte
                TextBox17.BackColor = Color.Pink
            Else
                ' ============================================
                ' Changement de la couleur de fond du texte
                TextBox17.BackColor = Color.White
            End If
            ' ============================================
            ' Ascenseur des boutons
            Superviseur_Boutons()
        End If
    End Sub

    '=============================================================================================================================================
    ' Sélection du texte Dénomination Code Remarque
    Private Sub TextBox25_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox25.MouseDown
        ' ============================================
        ' Traitement de la demande du bouton Droit
        If e.Button = Windows.Forms.MouseButtons.Right Then
            ' ============================================
            ' Text de la couleur de fond du texte
            If TextBox25.BackColor <> Color.Pink Then
                ' ============================================
                ' Changement de la couleur de fond du texte
                TextBox25.BackColor = Color.Pink
            Else
                ' ============================================
                ' Changement de la couleur de fond du texte
                TextBox25.BackColor = Color.FromArgb(255, 255, 200)
            End If
            ' ============================================
            ' Ascenseur des boutons
            Superviseur_Boutons()
        End If
    End Sub

    '=============================================================================================================================================
    ' Sélection du texte Dénomination Unité
    Private Sub TextBox12_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox12.MouseDown
        ' ============================================
        ' Traitement de la demande du bouton Droit
        If e.Button = Windows.Forms.MouseButtons.Right Then
            ' ============================================
            ' Text de la couleur de fond du texte
            If TextBox12.BackColor <> Color.Pink Then
                ' ============================================
                ' Changement de la couleur de fond du texte
                TextBox12.BackColor = Color.Pink
            Else
                ' ============================================
                ' Changement de la couleur de fond du texte
                TextBox12.BackColor = Color.FromArgb(255, 255, 200)
            End If
            ' ============================================
            ' Ascenseur des boutons
            Superviseur_Boutons()
        End If
    End Sub

    '=============================================================================================================================================
    ' Sélection du texte Dénomination Résultat
    Private Sub TextBox11_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox11.MouseDown
        ' ============================================
        ' Traitement de la demande du bouton Droit
        If e.Button = Windows.Forms.MouseButtons.Right Then
            ' ============================================
            ' Text de la couleur de fond du texte
            If TextBox11.BackColor <> Color.Pink Then
                ' ============================================
                ' Changement de la couleur de fond du texte
                TextBox11.BackColor = Color.Pink
            Else
                ' ============================================
                ' Changement de la couleur de fond du texte
                TextBox11.BackColor = Color.FromArgb(255, 255, 200)
            End If
            ' ============================================
            ' Ascenseur des boutons
            Superviseur_Boutons()
        End If


        ' ============================================
        ' Traitement de la demande du bouton Gauche
        ' ============================================
        ' Vérification si on est en positionnement libre de la feuille Propriétés et Résultats
        If Variables.FeuiProResLibre = False Then
            ' ============================================
            ' Validation de la variable de blocage de positionnement automatique
            Variables.FeuiProResLibre = True
            ' ============================================
            ' Disparition de la feuille principale par passage de la feuille principale transparente 
            Form1.Opacity = 0
        End If

    End Sub




    '=============================================================================================================================================
    ' Sélection du texte Code Résultat Qualitatif
    Private Sub TextBox33_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox33.MouseDown
        ' ============================================
        ' Traitement de la demande du bouton Droit
        If e.Button = Windows.Forms.MouseButtons.Right Then
            ' ============================================
            ' Text de la couleur de fond du texte
            If TextBox33.BackColor <> Color.Pink Then
                ' ============================================
                ' Changement de la couleur de fond du texte
                TextBox33.BackColor = Color.Pink
            Else
                ' ============================================
                ' Changement de la couleur de fond du texte
                TextBox33.BackColor = Color.FromArgb(255, 255, 200)
            End If
            ' ============================================
            ' Ascenseur des boutons
            Superviseur_Boutons()
        End If
    End Sub

    '=============================================================================================================================================
    ' Sélection du texte Code Groupe Taxon Résultat
    Private Sub TextBox34_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox34.MouseDown
        ' ============================================
        ' Traitement de la demande du bouton Droit
        If e.Button = Windows.Forms.MouseButtons.Right Then
            ' ============================================
            ' Text de la couleur de fond du texte
            If TextBox34.BackColor <> Color.Pink Then
                ' ============================================
                ' Changement de la couleur de fond du texte
                TextBox34.BackColor = Color.Pink
            Else
                ' ============================================
                ' Changement de la couleur de fond du texte
                TextBox34.BackColor = Color.White
            End If
            ' ============================================
            ' Ascenseur des boutons
            Superviseur_Boutons()
        End If
    End Sub

    '=============================================================================================================================================
    ' Sélection du texte Code Taxon Résultat
    Private Sub TextBox35_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox35.MouseDown
        ' ============================================
        ' Traitement de la demande du bouton Droit
        If e.Button = Windows.Forms.MouseButtons.Right Then
            ' ============================================
            ' Text de la couleur de fond du texte
            If TextBox35.BackColor <> Color.Pink Then
                ' ============================================
                ' Changement de la couleur de fond du texte
                TextBox35.BackColor = Color.Pink
            Else
                ' ============================================
                ' Changement de la couleur de fond du texte
                TextBox35.BackColor = Color.White
            End If
            ' ============================================
            ' Ascenseur des boutons
            Superviseur_Boutons()
        End If
    End Sub



    '=============================================================================================================================================
    ' Superviseur de la montée et de la descente des boutons
    Private Sub Superviseur_Boutons()
        ' ============================================
        ' Vérification des couleurs de fond pour la sélection des boutons
        If TextBox5.BackColor = Color.Pink Or TextBox24.BackColor = Color.Pink Or TextBox10.BackColor = Color.Pink Or TextBox26.BackColor = Color.Pink Or TextBox7.BackColor = Color.Pink Or TextBox9.BackColor = Color.Pink Or TextBox15.BackColor = Color.Pink Or TextBox18.BackColor = Color.Pink Or TextBox22.BackColor = Color.Pink Or TextBox21.BackColor = Color.Pink Or TextBox20.BackColor = Color.Pink Or TextBox14.BackColor = Color.Pink Or TextBox27.BackColor = Color.Pink Or TextBox17.BackColor = Color.Pink Or TextBox25.BackColor = Color.Pink Or TextBox12.BackColor = Color.Pink Or TextBox11.BackColor = Color.Pink Or TextBox33.BackColor = Color.Pink Or TextBox34.BackColor = Color.Pink Or TextBox35.BackColor = Color.Pink Then
            ' ============================================
            ' Sélections des boutons
            Variables.AffichageBoutons = True
            ' ============================================
            ' Vérification si les boutons correspondants sont affichés
            If Button12.Visible = False Then
                ' ============================================
                ' Sortie des boutons spécifiques
                Timer8.Enabled = False
                Timer9.Enabled = True
            End If
        Else
            ' ============================================
            ' Sélections des boutons
            Variables.AffichageBoutons = False
            ' ============================================
            ' Vérification si les boutons correspondants sont affichés
            If Button5.Visible = False Then
                ' ============================================
                ' Sortie des boutons spécifiques
                Timer8.Enabled = False
                Timer9.Enabled = True
            End If
        End If
    End Sub

    '=============================================================================================================================================
    ' Timer des montées des boutons
    Private Sub Timer8_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer8.Tick
        ' ============================================
        ' Vérification si le panel est arrivé à sa position
        If Panel12.Top < 581 Then   ' avant 516
            ' ============================================
            ' Arrêt du timer de montée
            Timer8.Enabled = False
            ' ============================================
            ' Sortie
            Exit Sub
        End If
        ' ============================================
        ' Décrémentation du top du panel
        Panel12.Top = Panel12.Top - 1
    End Sub

    '=============================================================================================================================================
    ' Timer des descentes des boutons
    Private Sub Timer9_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer9.Tick
        ' ============================================
        ' Vérification si le panel est arrivé à sa position
        If Panel12.Top > 624 Then   ' avant 559
            ' ============================================
            ' Arrêt du timer de descente
            Timer9.Enabled = False
            ' ============================================
            ' Vérification si boutons de recopie demandés
            If Variables.AffichageBoutons = False Then
                ' ============================================
                ' Basculement des boutons
                Button1.Visible = True
                '               Button5.Visible = True
                Button6.Visible = True
                Button7.Visible = True
                Button10.Visible = True
                Button11.Visible = False
                Button12.Visible = False
                Button13.Visible = False
                Button14.Visible = False
            Else
                ' ============================================
                ' Basculement des boutons
                Button1.Visible = False
                '                Button5.Visible = False
                Button6.Visible = False
                Button7.Visible = False
                Button10.Visible = False
                Button11.Visible = True
                Button12.Visible = True
                Button13.Visible = True
                Button14.Visible = True
            End If
            ' ============================================
            ' Validation du timer de montée
            Timer8.Enabled = True
            ' ============================================
            ' Sortie
            Exit Sub
        End If
        ' ============================================
        ' Incrémentation du top du panel
        Panel12.Top = Panel12.Top + 1
    End Sub

    '=============================================================================================================================================
    ' Bouton d'effacement de la couleur de fond des champs
    Private Sub Button11_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button11.Click
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button2.Select()
        ' ============================================
        ' Rétablissement de la couleur de fond des boutons en blanc
        TextBox5.BackColor = Color.FromArgb(255, 255, 200)
        TextBox24.BackColor = Color.FromArgb(255, 255, 200)
        TextBox10.BackColor = Color.FromArgb(255, 255, 200)
        TextBox26.BackColor = Color.White
        TextBox7.BackColor = Color.FromArgb(255, 255, 200)
        TextBox9.BackColor = Color.FromArgb(255, 255, 200)
        TextBox15.BackColor = Color.FromArgb(255, 255, 200)
        TextBox18.BackColor = Color.White
        TextBox22.BackColor = Color.White
        TextBox21.BackColor = Color.FromArgb(255, 255, 200)
        TextBox20.BackColor = Color.White
        TextBox14.BackColor = Color.White
        TextBox27.BackColor = Color.White
        TextBox17.BackColor = Color.White
        TextBox25.BackColor = Color.FromArgb(255, 255, 200)
        TextBox12.BackColor = Color.FromArgb(255, 255, 200)
        TextBox11.BackColor = Color.FromArgb(255, 255, 200)

        TextBox33.BackColor = Color.FromArgb(255, 255, 200)
        TextBox34.BackColor = Color.White
        TextBox35.BackColor = Color.White

        ' ============================================
        ' Ascenseur des boutons
        Superviseur_Boutons()
    End Sub

    '=============================================================================================================================================
    ' Bouton de recopier dans tous les paramètres de toutes les familles
    Private Sub Button12_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button12.Click
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button2.Select()

        ' ==================================
        ' Définition des variables
        Dim i As Integer
        Dim DernierParam As Integer

        ' ==================================
        ' Message de confirmation
        i = MsgBox(" Recopie dans toutes les familles !" & vbCrLf & vbCrLf & "     Confirmez-vous la recopie ? ", MsgBoxStyle.Exclamation + MsgBoxStyle.YesNo + MsgBoxStyle.DefaultButton2, " QUADRISPEL - Validation de recopie")
        ' ==================================
        ' Traitement si non à la question
        If i = 7 Then Exit Sub

        ' ==================================
        ' Recherche de l'adresse du dernier paramètre
        For i = 0 To 500
            If Variables.TabRéfPara(i, 0) = "" Then
                DernierParam = i - 1
                Exit For
            End If
        Next
        ' ==================================
        ' Balayage du Tableau des Paramètres
        For i = 0 To DernierParam
            ' ==================================
            ' Sélection du texte Méthodes
            If TextBox5.BackColor = Color.Pink Then Variables.TabRéfPara(i, 6) = TextBox5.Text
            ' ==================================
            ' Sélection du texte Dénomination Engin Analyse
            If TextBox26.BackColor = Color.Pink Then Variables.TabRéfPara(i, 7) = TextBox26.Text
            ' ==================================
            ' Sélection du texte Libellé Analyseur
            If TextBox10.BackColor = Color.Pink Then Variables.TabRéfPara(i, 5) = TextBox10.Text
            ' ==================================
            ' Sélection du texte Dénomination Analyseur 
            If TextBox24.BackColor = Color.Pink Then Variables.TabRéfPara(i, 4) = TextBox24.Text
            ' ==================================
            ' Sélection du texte Dénomination Fraction
            If TextBox7.BackColor = Color.Pink Then Variables.TabRéfPara(i, 8) = TextBox7.Text
            ' ==================================
            ' Sélection du texte Dénomination Support
            If TextBox9.BackColor = Color.Pink Then Variables.TabRéfPara(i, 9) = TextBox9.Text
            ' ==================================
            ' Sélection du texte Dénomination Engin de Prélèvement
            If TextBox15.BackColor = Color.Pink Then Variables.TabRéfPara(i, 10) = TextBox15.Text
            ' ==================================
            ' Sélection du texte Dénomination Précision
            If TextBox18.BackColor = Color.Pink Then Variables.TabRéfPara(i, 11) = TextBox18.Text
            ' ==================================
            ' Sélection du texte Dénomination Type Précision
            If TextBox22.BackColor = Color.Pink Then Variables.TabRéfPara(i, 12) = TextBox22.Text
            ' ==================================
            ' Sélection du texte Dénomination Niveau de Saisie
            If TextBox21.BackColor = Color.Pink Then Variables.TabRéfPara(i, 13) = TextBox21.Text
            ' ==================================
            ' Sélection du texte Dénomination Seuil de Détection
            If TextBox20.BackColor = Color.Pink Then Variables.TabRéfPara(i, 14) = TextBox20.Text
            ' ==================================
            ' Sélection du texte Dénomination Libellé Résultat Qualitatif
            If TextBox14.BackColor = Color.Pink Then Variables.TabRéfPara(i, 15) = TextBox14.Text
            ' ==================================
            ' Sélection du texte Dénomination Numéro Individu
            If TextBox27.BackColor = Color.Pink Then Variables.TabRéfPara(i, 16) = TextBox27.Text
            ' ==================================
            ' Sélection du texte Dénomination Commentaire Mesure
            If TextBox17.BackColor = Color.Pink Then Variables.TabRéfPara(i, 19) = TextBox17.Text
            ' ==================================
            ' Sélection du texte Dénomination Remarque
            If TextBox25.BackColor = Color.Pink Then Variables.TabRéfPara(i, 20) = TextBox25.Text
            ' ==================================
            ' Sélection du texte Dénomination Unité
            If TextBox12.BackColor = Color.Pink Then Variables.TabRéfPara(i, 18) = TextBox12.Text
            ' ==================================
            ' Sélection du texte Dénomination Résultat
            If TextBox11.BackColor = Color.Pink Then Variables.TabRéfPara(i, 17) = TextBox11.Text


            ' ==================================
            ' Sélection du texte Code Résultat Qualitatif
            If TextBox33.BackColor = Color.Pink Then Variables.TabRéfPara(i, 23) = TextBox33.Text
            ' ==================================
            ' Sélection du texte Code Groupe Taxon Résultat
            If TextBox34.BackColor = Color.Pink Then Variables.TabRéfPara(i, 24) = TextBox34.Text
            ' ==================================
            ' Sélection du texte Code Taxon Résultat
            If TextBox35.BackColor = Color.Pink Then Variables.TabRéfPara(i, 25) = TextBox35.Text


        Next
        ' ==================================
        ' Vérification sauvegarde à la fermeture
        Variables.SauveFichierMasque = True
        If TextBox11.BackColor = Color.Pink Then
            Variables.SauveFichierRésultat = True
        End If
    End Sub

    '=============================================================================================================================================
    ' Bouton de recopier dans tous les paramètres d'une même famille
    Private Sub Button13_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button13.Click
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button2.Select()

        ' ==================================
        ' Définition des variables
        Dim i As Integer
        Dim PremierParam As Integer

        ' ==================================
        ' Message de confirmation
        i = MsgBox(" Recopie dans toute la famille !" & vbCrLf & vbCrLf & "   Confirmez-vous la recopie ? ", MsgBoxStyle.Exclamation + MsgBoxStyle.YesNo + MsgBoxStyle.DefaultButton2, " QUADRISPEL - Validation de recopie")
        ' ==================================
        ' Traitement si non à la question
        If i = 7 Then Exit Sub

        ' ==================================
        ' Recherche de la première adresse de la famille sélectionnée
        For i = 0 To 500
            If TextBox1.Text = Variables.TabRéfPara(i, 0) Then
                PremierParam = i
                Exit For
            End If
        Next
        ' ==================================
        ' Balayage du Tableau des Paramètres
        For i = PremierParam To 500
            ' ==================================
            ' Sortie si changement de famille
            If TextBox1.Text <> Variables.TabRéfPara(i, 0) Then
                Exit For
            End If
            ' ==================================
            ' Sélection du texte Méthodes
            If TextBox5.BackColor = Color.Pink Then Variables.TabRéfPara(i, 6) = TextBox5.Text
            ' ==================================
            ' Sélection du texte Dénomination Engin Analyse
            If TextBox26.BackColor = Color.Pink Then Variables.TabRéfPara(i, 7) = TextBox26.Text
            ' ==================================
            ' Sélection du texte Libellé Analyseur
            If TextBox10.BackColor = Color.Pink Then Variables.TabRéfPara(i, 5) = TextBox10.Text
            ' ==================================
            ' Sélection du texte Dénomination Analyseur 
            If TextBox24.BackColor = Color.Pink Then Variables.TabRéfPara(i, 4) = TextBox24.Text
            ' ==================================
            ' Sélection du texte Dénomination Fraction
            If TextBox7.BackColor = Color.Pink Then Variables.TabRéfPara(i, 8) = TextBox7.Text
            ' ==================================
            ' Sélection du texte Dénomination Support
            If TextBox9.BackColor = Color.Pink Then Variables.TabRéfPara(i, 9) = TextBox9.Text
            ' ==================================
            ' Sélection du texte Dénomination Engin de Prélèvement
            If TextBox15.BackColor = Color.Pink Then Variables.TabRéfPara(i, 10) = TextBox15.Text
            ' ==================================
            ' Sélection du texte Dénomination Précision
            If TextBox18.BackColor = Color.Pink Then Variables.TabRéfPara(i, 11) = TextBox18.Text
            ' ==================================
            ' Sélection du texte Dénomination Type Précision
            If TextBox22.BackColor = Color.Pink Then Variables.TabRéfPara(i, 12) = TextBox22.Text
            ' ==================================
            ' Sélection du texte Dénomination Niveau de Saisie
            If TextBox21.BackColor = Color.Pink Then Variables.TabRéfPara(i, 13) = TextBox21.Text
            ' ==================================
            ' Sélection du texte Dénomination Seuil de Détection
            If TextBox20.BackColor = Color.Pink Then Variables.TabRéfPara(i, 14) = TextBox20.Text
            ' ==================================
            ' Sélection du texte Dénomination Libellé Résultat Qualitatif
            If TextBox14.BackColor = Color.Pink Then Variables.TabRéfPara(i, 15) = TextBox14.Text
            ' ==================================
            ' Sélection du texte Dénomination Numéro Individu
            If TextBox27.BackColor = Color.Pink Then Variables.TabRéfPara(i, 16) = TextBox27.Text
            ' ==================================
            ' Sélection du texte Dénomination Commentaire Mesure
            If TextBox17.BackColor = Color.Pink Then Variables.TabRéfPara(i, 19) = TextBox17.Text
            ' ==================================
            ' Sélection du texte Dénomination Remarque
            If TextBox25.BackColor = Color.Pink Then Variables.TabRéfPara(i, 20) = TextBox25.Text
            ' ==================================
            ' Sélection du texte Dénomination Unité
            If TextBox12.BackColor = Color.Pink Then Variables.TabRéfPara(i, 18) = TextBox12.Text
            ' ==================================
            ' Sélection du texte Dénomination Résultat
            If TextBox11.BackColor = Color.Pink Then Variables.TabRéfPara(i, 17) = TextBox11.Text


            ' ==================================
            ' Sélection du texte Code Résultat Qualitatif
            If TextBox33.BackColor = Color.Pink Then Variables.TabRéfPara(i, 23) = TextBox33.Text
            ' ==================================
            ' Sélection du texte Code Groupe Taxon Résultat
            If TextBox34.BackColor = Color.Pink Then Variables.TabRéfPara(i, 24) = TextBox34.Text
            ' ==================================
            ' Sélection du texte Code Taxon Résultat
            If TextBox35.BackColor = Color.Pink Then Variables.TabRéfPara(i, 25) = TextBox35.Text


        Next
        ' ==================================
        ' Vérification sauvegarde à la fermeture
        Variables.SauveFichierMasque = True
        If TextBox11.BackColor = Color.Pink Then
            Variables.SauveFichierRésultat = True
        End If
    End Sub

    '=============================================================================================================================================
    ' Bouton de sélection de tous les champs
    Private Sub Button14_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button14.Click
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button2.Select()
        ' ============================================
        ' Rétablissement de la couleur de fond des boutons en blanc
        TextBox5.BackColor = Color.Pink
        TextBox24.BackColor = Color.Pink
        TextBox10.BackColor = Color.Pink
        TextBox26.BackColor = Color.Pink
        TextBox7.BackColor = Color.Pink
        TextBox9.BackColor = Color.Pink
        TextBox15.BackColor = Color.Pink
        TextBox18.BackColor = Color.Pink
        TextBox22.BackColor = Color.Pink
        TextBox21.BackColor = Color.Pink
        TextBox20.BackColor = Color.Pink
        TextBox14.BackColor = Color.Pink
        TextBox27.BackColor = Color.Pink
        TextBox17.BackColor = Color.Pink
        TextBox25.BackColor = Color.Pink
        TextBox12.BackColor = Color.Pink
        TextBox11.BackColor = Color.Pink

        TextBox33.BackColor = Color.Pink
        TextBox34.BackColor = Color.Pink
        TextBox35.BackColor = Color.Pink

        ' ============================================
        ' Ascenseur des boutons
        Superviseur_Boutons()
    End Sub

    '=============================================================================================================================================
    ' PROCÉDURES D'APPARITION DES SÉLECTEURS
    '=============================================================================================================================================
    ' Sélection de la liste des Méthodes
    Private Sub TextBox5_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox5.MouseClick
        ' ==================================
        ' Traitement pour la liste des sujets si une autre liste est en cours
        If Variables.NumeroList <> 0 Then
            Variables.NumeroListSuivant = 1
            ' ==================================
            ' Arrêt du timer de la procédure d'apparition des listes
            Timer2.Enabled = False
            ' Arrêt du timer de la procédure d'apparition des listes
            Timer3.Enabled = False
            ' ==================================
            ' Déclenchement du retrait du sélecteur
            Timer4.Enabled = True
            Exit Sub
        End If
        ' ==================================
        ' Variable d'affichage du Listbox de la liste des sujets
        Variables.NumeroList = 1
        ' ==================================
        ' Déclenchement du timer de la procédure d'apparition des listes
        Timer2.Enabled = True
    End Sub

    '=============================================================================================================================================
    ' Sélection de la liste Fraction
    Private Sub TextBox7_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox7.MouseClick
        ' ==================================
        ' Traitement pour la liste des sujets si une autre liste est en cours
        If Variables.NumeroList <> 0 Then
            Variables.NumeroListSuivant = 5
            ' ==================================
            ' Arrêt du timer de la procédure d'apparition des listes
            Timer2.Enabled = False
            ' Arrêt du timer de la procédure d'apparition des listes
            Timer3.Enabled = False
            ' ==================================
            ' Déclenchement du retrait du sélecteur
            Timer4.Enabled = True
            Exit Sub
        End If
        ' ==================================
        ' Variable d'affichage du Listbox de la liste des sujets
        Variables.NumeroList = 5
        ' ==================================
        ' Déclenchement du timer de la procédure d'apparition des listes
        Timer2.Enabled = True
    End Sub

    '=============================================================================================================================================
    ' Sélection de la liste Support
    Private Sub TextBox9_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox9.MouseClick
        ' ==================================
        ' Traitement pour la liste des sujets si une autre liste est en cours
        If Variables.NumeroList <> 0 Then
            Variables.NumeroListSuivant = 6
            ' ==================================
            ' Arrêt du timer de la procédure d'apparition des listes
            Timer2.Enabled = False
            ' Arrêt du timer de la procédure d'apparition des listes
            Timer3.Enabled = False
            ' ==================================
            ' Déclenchement du retrait du sélecteur
            Timer4.Enabled = True
            Exit Sub
        End If
        ' ==================================
        ' Variable d'affichage du Listbox de la liste des sujets
        Variables.NumeroList = 6
        ' ==================================
        ' Déclenchement du timer de la procédure d'apparition des listes
        Timer2.Enabled = True
    End Sub

    '=============================================================================================================================================
    ' Sélection de la liste des Libellés des Analyseurs
    Private Sub TextBox10_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox10.MouseClick
        ' ==================================
        ' Traitement pour la liste des sujets si une autre liste est en cours
        If Variables.NumeroList <> 0 Then
            Variables.NumeroListSuivant = 4
            ' ==================================
            ' Arrêt du timer de la procédure d'apparition des listes
            Timer2.Enabled = False
            ' Arrêt du timer de la procédure d'apparition des listes
            Timer3.Enabled = False
            ' ==================================
            ' Déclenchement du retrait du sélecteur
            Timer4.Enabled = True
            Exit Sub
        End If
        ' ==================================
        ' Variable d'affichage du Listbox de la liste des sujets
        Variables.NumeroList = 4
        ' ==================================
        ' Déclenchement du timer de la procédure d'apparition des listes
        Timer2.Enabled = True
    End Sub

    '=============================================================================================================================================
    ' Sélection de la liste Unité
    Private Sub TextBox12_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox12.MouseClick
        ' ==================================
        ' Traitement pour la liste des sujets si une autre liste est en cours
        If Variables.NumeroList <> 0 Then
            Variables.NumeroListSuivant = 11
            ' ==================================
            ' Arrêt du timer de la procédure d'apparition des listes
            Timer2.Enabled = False
            ' Arrêt du timer de la procédure d'apparition des listes
            Timer3.Enabled = False
            ' ==================================
            ' Déclenchement du retrait du sélecteur
            Timer4.Enabled = True
            Exit Sub
        End If
        ' ==================================
        ' Variable d'affichage du Listbox de la liste des sujets
        Variables.NumeroList = 11
        ' ==================================
        ' Déclenchement du timer de la procédure d'apparition des listes
        Timer2.Enabled = True
    End Sub

    '=============================================================================================================================================
    ' Sélection de la liste des Libellés des Engin de Prélèvement
    Private Sub TextBox15_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox15.MouseClick
        ' ==================================
        ' Traitement pour la liste des sujets si une autre liste est en cours
        If Variables.NumeroList <> 0 Then
            Variables.NumeroListSuivant = 7
            ' ==================================
            ' Arrêt du timer de la procédure d'apparition des listes
            Timer2.Enabled = False
            ' Arrêt du timer de la procédure d'apparition des listes
            Timer3.Enabled = False
            ' ==================================
            ' Déclenchement du retrait du sélecteur
            Timer4.Enabled = True
            Exit Sub
        End If
        ' ==================================
        ' Variable d'affichage du Listbox de la liste des sujets
        Variables.NumeroList = 7
        ' ==================================
        ' Déclenchement du timer de la procédure d'apparition des listes
        Timer2.Enabled = True
    End Sub

    '=============================================================================================================================================
    ' Sélection de la liste Précision
    Private Sub TextBox18_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox18.MouseClick
        ' ==================================
        ' Traitement pour la liste des sujets si une autre liste est en cours
        '       If Variables.NumeroList <> 0 Then
        '       Variables.NumeroListSuivant = 8
        ' ==================================
        ' Arrêt du timer de la procédure d'apparition des listes
        '       Timer2.Enabled = False
        ' Arrêt du timer de la procédure d'apparition des listes
        '       Timer3.Enabled = False
        ' ==================================
        ' Déclenchement du retrait du sélecteur
        '       Timer4.Enabled = True
        '       Exit Sub
        '        End If
        ' ==================================
        ' Variable d'affichage du Listbox de la liste des sujets
        '        Variables.NumeroList = 8
        ' ==================================
        ' Déclenchement du timer de la procédure d'apparition des listes
        '        Timer2.Enabled = True
    End Sub

    '=============================================================================================================================================
    ' Sélection de la liste Niveau Saisie
    Private Sub TextBox21_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox21.MouseClick
        ' ==================================
        ' Traitement pour la liste des sujets si une autre liste est en cours
        If Variables.NumeroList <> 0 Then
            Variables.NumeroListSuivant = 10
            ' ==================================
            ' Arrêt du timer de la procédure d'apparition des listes
            Timer2.Enabled = False
            ' Arrêt du timer de la procédure d'apparition des listes
            Timer3.Enabled = False
            ' ==================================
            ' Déclenchement du retrait du sélecteur
            Timer4.Enabled = True
            Exit Sub
        End If
        ' ==================================
        ' Variable d'affichage du Listbox de la liste des sujets
        Variables.NumeroList = 10
        ' ==================================
        ' Déclenchement du timer de la procédure d'apparition des listes
        Timer2.Enabled = True
    End Sub

    '=============================================================================================================================================
    ' Sélection de la liste Type Précision
    Private Sub TextBox22_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox22.MouseClick
        ' ==================================
        ' Traitement pour la liste des sujets si une autre liste est en cours
        If Variables.NumeroList <> 0 Then
            Variables.NumeroListSuivant = 9
            ' ==================================
            ' Arrêt du timer de la procédure d'apparition des listes
            Timer2.Enabled = False
            ' Arrêt du timer de la procédure d'apparition des listes
            Timer3.Enabled = False
            ' ==================================
            ' Déclenchement du retrait du sélecteur
            Timer4.Enabled = True
            Exit Sub
        End If
        ' ==================================
        ' Variable d'affichage du Listbox de la liste des sujets
        Variables.NumeroList = 9
        ' ==================================
        ' Déclenchement du timer de la procédure d'apparition des listes
        Timer2.Enabled = True
    End Sub

    '=============================================================================================================================================
    ' Sélection de la liste des Dénominations des Analyseurs
    Private Sub TextBox24_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox24.MouseClick
        ' ==================================
        ' Traitement pour la liste des sujets si une autre liste est en cours
        If Variables.NumeroList <> 0 Then
            Variables.NumeroListSuivant = 2
            ' ==================================
            ' Arrêt du timer de la procédure d'apparition des listes
            Timer2.Enabled = False
            ' Arrêt du timer de la procédure d'apparition des listes
            Timer3.Enabled = False
            ' ==================================
            ' Déclenchement du retrait du sélecteur
            Timer4.Enabled = True
            Exit Sub
        End If
        ' ==================================
        ' Variable d'affichage du Listbox de la liste des sujets
        Variables.NumeroList = 2
        ' ==================================
        ' Déclenchement du timer de la procédure d'apparition des listes
        Timer2.Enabled = True
    End Sub

    '=============================================================================================================================================
    ' Sélection de la liste des Dénominations Code Remarque
    Private Sub TextBox25_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox25.MouseClick
        ' ==================================
        ' Traitement pour la liste des sujets si une autre liste est en cours
        If Variables.NumeroList <> 0 Then
            Variables.NumeroListSuivant = 12
            ' ==================================
            ' Arrêt du timer de la procédure d'apparition des listes
            Timer2.Enabled = False
            ' Arrêt du timer de la procédure d'apparition des listes
            Timer3.Enabled = False
            ' ==================================
            ' Déclenchement du retrait du sélecteur
            Timer4.Enabled = True
            Exit Sub
        End If
        ' ==================================
        ' Variable d'affichage du Listbox de la liste des sujets
        Variables.NumeroList = 12
        ' ==================================
        ' Déclenchement du timer de la procédure d'apparition des listes
        Timer2.Enabled = True
    End Sub

    '=============================================================================================================================================
    ' Sélection de la liste des Dénominations des Engin Analyse
    Private Sub TextBox26_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox26.MouseClick
        ' ==================================
        ' Traitement pour la liste des sujets si une autre liste est en cours
        If Variables.NumeroList <> 0 Then
            Variables.NumeroListSuivant = 3
            ' ==================================
            ' Arrêt du timer de la procédure d'apparition des listes
            Timer2.Enabled = False
            ' Arrêt du timer de la procédure d'apparition des listes
            Timer3.Enabled = False
            ' ==================================
            ' Déclenchement du retrait du sélecteur
            Timer4.Enabled = True
            Exit Sub
        End If
        ' ==================================
        ' Variable d'affichage du Listbox de la liste des sujets
        Variables.NumeroList = 3
        ' ==================================
        ' Déclenchement du timer de la procédure d'apparition des listes
        Timer2.Enabled = True
    End Sub

    '=============================================================================================================================================
    ' Déclenche la sortie d'un sélecteur
    Private Sub Timer2_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer2.Tick
        ' ==================================
        ' Procédure lorsque le sélecteur de la liste des Dénominations des Méthodes est arrivé à sa position
        If Variables.NumeroList = 1 And Panel1.Top >= 0 Then
            ' ==================================
            ' Arrêt du timer
            Timer2.Enabled = False
            ' ==================================
            ' Déclenchement du timer de la temporisation de lecture
            Timer3.Enabled = True
            ' ==================================
            ' Sortie
            Exit Sub
        End If
        ' ==================================
        ' Procédure lorsque le sélecteur de la liste des Dénominations des Analystes est arrivé à sa position
        If Variables.NumeroList = 2 And Panel2.Top >= 0 Then
            ' ==================================
            ' Arrêt du timer
            Timer2.Enabled = False
            ' ==================================
            ' Déclenchement du timer de la temporisation de lecture
            Timer3.Enabled = True
            ' ==================================
            ' Sortie
            Exit Sub
        End If
        ' ==================================
        ' Procédure lorsque le sélecteur de la liste Engin Analyse est arrivé à sa position
        If Variables.NumeroList = 3 And Panel3.Top >= 0 Then
            ' ==================================
            ' Arrêt du timer
            Timer2.Enabled = False
            ' ==================================
            ' Déclenchement du timer de la temporisation de lecture
            Timer3.Enabled = True
            ' ==================================
            ' Sortie
            Exit Sub
        End If
        ' ==================================
        ' Procédure lorsque le sélecteur de la liste des Libellés des Analystes est arrivé à sa position
        If Variables.NumeroList = 4 And Panel4.Top >= 0 Then
            ' ==================================
            ' Arrêt du timer
            Timer2.Enabled = False
            ' ==================================
            ' Déclenchement du timer de la temporisation de lecture
            Timer3.Enabled = True
            ' ==================================
            ' Sortie
            Exit Sub
        End If
        ' ==================================
        ' Procédure lorsque le sélecteur de la liste Fraction est arrivé à sa position
        If Variables.NumeroList = 5 And Panel5.Top >= 0 Then
            ' ==================================
            ' Arrêt du timer
            Timer2.Enabled = False
            ' ==================================
            ' Déclenchement du timer de la temporisation de lecture
            Timer3.Enabled = True
            ' ==================================
            ' Sortie
            Exit Sub
        End If
        ' ==================================
        ' Procédure lorsque le sélecteur de la liste Support est arrivé à sa position
        If Variables.NumeroList = 6 And Panel6.Top >= 0 Then
            ' ==================================
            ' Arrêt du timer
            Timer2.Enabled = False
            ' ==================================
            ' Déclenchement du timer de la temporisation de lecture
            Timer3.Enabled = True
            ' ==================================
            ' Sortie
            Exit Sub
        End If
        ' ==================================
        ' Procédure lorsque le sélecteur de la liste Engin de Prélèvement est arrivé à sa position
        If Variables.NumeroList = 7 And Panel7.Top >= 0 Then
            ' ==================================
            ' Arrêt du timer
            Timer2.Enabled = False
            ' ==================================
            ' Déclenchement du timer de la temporisation de lecture
            Timer3.Enabled = True
            ' ==================================
            ' Sortie
            Exit Sub
        End If
        ' ==================================
        ' Procédure lorsque le sélecteur de la liste Précision est arrivé à sa position
        If Variables.NumeroList = 8 And Panel8.Top >= 0 Then
            ' ==================================
            ' Arrêt du timer
            Timer2.Enabled = False
            ' ==================================
            ' Déclenchement du timer de la temporisation de lecture
            Timer3.Enabled = True
            ' ==================================
            ' Sortie
            Exit Sub
        End If
        ' ==================================
        ' Procédure lorsque le sélecteur de la liste Type Précision est arrivé à sa position
        If Variables.NumeroList = 9 And Panel9.Top >= 0 Then
            ' ==================================
            ' Arrêt du timer
            Timer2.Enabled = False
            ' ==================================
            ' Déclenchement du timer de la temporisation de lecture
            Timer3.Enabled = True
            ' ==================================
            ' Sortie
            Exit Sub
        End If
        ' ==================================
        ' Procédure lorsque le sélecteur de la liste Niveau de Saisie est arrivé à sa position
        If Variables.NumeroList = 10 And Panel10.Top >= 0 Then
            ' ==================================
            ' Arrêt du timer
            Timer2.Enabled = False
            ' ==================================
            ' Déclenchement du timer de la temporisation de lecture
            Timer3.Enabled = True
            ' ==================================
            ' Sortie
            Exit Sub
        End If
        ' ==================================
        ' Procédure lorsque le sélecteur de la liste Unité est arrivé à sa position
        If Variables.NumeroList = 11 And Panel11.Top >= 0 Then
            ' ==================================
            ' Arrêt du timer
            Timer2.Enabled = False
            ' ==================================
            ' Déclenchement du timer de la temporisation de lecture
            Timer3.Enabled = True
            ' ==================================
            ' Sortie
            Exit Sub
        End If
        ' ==================================
        ' Procédure lorsque le sélecteur de la liste Code Remarque est arrivé à sa position
        If Variables.NumeroList = 12 And Panel13.Top >= 0 Then
            ' ==================================
            ' Arrêt du timer
            Timer2.Enabled = False
            ' ==================================
            ' Déclenchement du timer de la temporisation de lecture
            Timer3.Enabled = True
            ' ==================================
            ' Sortie
            Exit Sub
        End If


        ' ==================================
        ' Incrémentation du déplacement du sélecteur de la liste des Dénominations des Méthodes
        If Variables.NumeroList = 1 Then Panel1.Top = Panel1.Top + 2
        ' ==================================
        ' Incrémentation du déplacement du sélecteur de la liste des Dénominations des Analystes
        If Variables.NumeroList = 2 Then Panel2.Top = Panel2.Top + 2
        ' ==================================
        ' Incrémentation du déplacement du sélecteur de la liste des Libellés des Préleveurs
        If Variables.NumeroList = 3 Then Panel3.Top = Panel3.Top + 2
        ' ==================================
        ' Incrémentation du déplacement du sélecteur de la liste des Libellés des Analystes
        If Variables.NumeroList = 4 Then Panel4.Top = Panel4.Top + 2
        ' ==================================
        ' Incrémentation du déplacement du sélecteur de la liste Fraction
        If Variables.NumeroList = 5 Then Panel5.Top = Panel5.Top + 2
        ' ==================================
        ' Incrémentation du déplacement du sélecteur de la liste Support
        If Variables.NumeroList = 6 Then Panel6.Top = Panel6.Top + 2
        ' ==================================
        ' Incrémentation du déplacement du sélecteur de la liste Engin de Prélèvement
        If Variables.NumeroList = 7 Then Panel7.Top = Panel7.Top + 2
        ' ==================================
        ' Incrémentation du déplacement du sélecteur de la liste Précision
        If Variables.NumeroList = 8 Then Panel8.Top = Panel8.Top + 2
        ' ==================================
        ' Incrémentation du déplacement du sélecteur de la liste Type Précision
        If Variables.NumeroList = 9 Then Panel9.Top = Panel9.Top + 2
        ' ==================================
        ' Incrémentation du déplacement du sélecteur de la liste Niveau de Saisie
        If Variables.NumeroList = 10 Then Panel10.Top = Panel10.Top + 2
        ' ==================================
        ' Incrémentation du déplacement du sélecteur de la liste Unité
        If Variables.NumeroList = 11 Then Panel11.Top = Panel11.Top + 2
        ' ==================================
        ' Incrémentation du déplacement du sélecteur de la liste Code Remarque
        If Variables.NumeroList = 12 Then Panel13.Top = Panel13.Top + 2

    End Sub

    ' ==============================================================================================================================================
    ' Déclenche l'attente de lecture et l'affichage de la liste
    Private Sub Timer3_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer3.Tick
        ' ==================================
        ' Incrémentation du déplacement du sélecteur de la liste des Dénominations des Méthodes
        If Variables.NumeroList = 1 Then
            Panel1.Height = Panel1.Height + 16
            ' ==================================
            ' Apparition de la liste correspondante
            ListBox1.Visible = True
        End If
        ' ==================================
        ' Incrémentation du déplacement du sélecteur de la liste des Dénominations des Analystes
        If Variables.NumeroList = 2 Then
            Panel2.Height = Panel2.Height + 16
            ' ==================================
            ' Apparition de la liste correspondante
            ListBox2.Visible = True
        End If
        ' ==================================
        ' Incrémentation du déplacement du sélecteur de la liste des Engin Analyse
        If Variables.NumeroList = 3 Then
            Panel3.Height = Panel3.Height + 16
            ' ==================================
            ' Apparition de la liste correspondante
            ListBox3.Visible = True
        End If
        ' ==================================
        ' Incrémentation du déplacement du sélecteur de la liste des Libellés des Analystes
        If Variables.NumeroList = 4 Then
            Panel4.Height = Panel4.Height + 16
            ' ==================================
            ' Apparition de la liste correspondante
            ListBox4.Visible = True
        End If
        ' ==================================
        ' Incrémentation du déplacement du sélecteur de la liste Fraction
        If Variables.NumeroList = 5 Then
            Panel5.Height = Panel5.Height + 16
            ' ==================================
            ' Apparition de la liste correspondante
            ListBox5.Visible = True
        End If
        ' ==================================
        ' Incrémentation du déplacement du sélecteur de la liste Support
        If Variables.NumeroList = 6 Then
            Panel6.Height = Panel6.Height + 16
            ' ==================================
            ' Apparition de la liste correspondante
            ListBox6.Visible = True
        End If
        ' ==================================
        ' Incrémentation du déplacement du sélecteur de la liste Engin de Prélèvement
        If Variables.NumeroList = 7 Then
            Panel7.Height = Panel7.Height + 16
            ' ==================================
            ' Apparition de la liste correspondante
            ListBox7.Visible = True
        End If
        ' ==================================
        ' Incrémentation du déplacement du sélecteur de la liste Précision
        If Variables.NumeroList = 8 Then
            Panel8.Height = Panel8.Height + 16
            ' ==================================
            ' Apparition de la liste correspondante
            ListBox8.Visible = True
        End If
        ' ==================================
        ' Incrémentation du déplacement du sélecteur de la liste Type Précision
        If Variables.NumeroList = 9 Then
            Panel9.Height = Panel9.Height + 16
            ' ==================================
            ' Apparition de la liste correspondante
            ListBox9.Visible = True
        End If
        ' ==================================
        ' Incrémentation du déplacement du sélecteur de la liste Niveau de Saisie
        If Variables.NumeroList = 10 Then
            Panel10.Height = Panel10.Height + 16
            ' ==================================
            ' Apparition de la liste correspondante
            ListBox10.Visible = True
        End If
        ' ==================================
        ' Incrémentation du déplacement du sélecteur de la liste Unité
        If Variables.NumeroList = 11 Then
            Panel11.Height = Panel11.Height + 16
            ' ==================================
            ' Apparition de la liste correspondante
            ListBox11.Visible = True
        End If
        ' ==================================
        ' Incrémentation du déplacement du sélecteur de la liste Code Remarque
        If Variables.NumeroList = 12 Then
            Panel13.Height = Panel13.Height + 16
            ' ==================================
            ' Apparition de la liste correspondante
            ListBox12.Visible = True
        End If


        ' ==================================
        ' Vérification de la fin du déplacement du sélecteur de la liste des Dénominations des Méthodes
        If Variables.NumeroList = 1 And Panel1.Height >= 545 Then       '   610 Then
            ' ==================================
            ' Arrête du timer de temporisation de lecture
            Timer3.Enabled = False
            ' ==================================
            ' Déclenchement du timer de déclenchement de la procédure de retrait
            Timer6.Enabled = True
            ' ==================================
            ' Déclenchement du timer de retrait automatique
            Timer7.Enabled = True
        End If
        ' ==================================
        ' Vérification de la fin du déplacement du sélecteur de la liste des Dénominations des Analystes
        If Variables.NumeroList = 2 And Panel2.Height >= 545 Then
            ' ==================================
            ' Arrête du timer de temporisation de lecture
            Timer3.Enabled = False
            ' ==================================
            ' Déclenchement du timer de déclenchement de la procédure de retrait
            Timer6.Enabled = True
            ' ==================================
            ' Déclenchement du timer de retrait automatique
            Timer7.Enabled = True
        End If
        ' ==================================
        ' Vérification de la fin du déplacement du sélecteur de la liste des Engin Analyse
        If Variables.NumeroList = 3 And Panel3.Height >= 321 Then
            ' ==================================
            ' Arrête du timer de temporisation de lecture
            Timer3.Enabled = False
            ' ==================================
            ' Déclenchement du timer de déclenchement de la procédure de retrait
            Timer6.Enabled = True
            ' ==================================
            ' Déclenchement du timer de retrait automatique
            Timer7.Enabled = True
        End If
        ' ==================================
        ' Vérification de la fin du déplacement du sélecteur de la liste des Libellés des Analystes
        If Variables.NumeroList = 4 And Panel4.Height >= 545 Then
            ' ==================================
            ' Arrête du timer de temporisation de lecture
            Timer3.Enabled = False
            ' ==================================
            ' Déclenchement du timer de déclenchement de la procédure de retrait
            Timer6.Enabled = True
            ' ==================================
            ' Déclenchement du timer de retrait automatique
            Timer7.Enabled = True
        End If
        ' ==================================
        ' Vérification de la fin du déplacement du sélecteur de la liste Fraction
        If Variables.NumeroList = 5 And Panel5.Height >= 321 Then
            ' ==================================
            ' Arrête du timer de temporisation de lecture
            Timer3.Enabled = False
            ' ==================================
            ' Déclenchement du timer de déclenchement de la procédure de retrait
            Timer6.Enabled = True
            ' ==================================
            ' Déclenchement du timer de retrait automatique
            Timer7.Enabled = True
        End If
        ' ==================================
        ' Vérification de la fin du déplacement du sélecteur de la liste Support
        If Variables.NumeroList = 6 And Panel6.Height >= 321 Then
            ' ==================================
            ' Arrête du timer de temporisation de lecture
            Timer3.Enabled = False
            ' ==================================
            ' Déclenchement du timer de déclenchement de la procédure de retrait
            Timer6.Enabled = True
            ' ==================================
            ' Déclenchement du timer de retrait automatique
            Timer7.Enabled = True
        End If
        ' ==================================
        ' Vérification de la fin du déplacement du sélecteur de la liste Engin de Prélèvement
        If Variables.NumeroList = 7 And Panel7.Height >= 321 Then
            ' ==================================
            ' Arrête du timer de temporisation de lecture
            Timer3.Enabled = False
            ' ==================================
            ' Déclenchement du timer de déclenchement de la procédure de retrait
            Timer6.Enabled = True
            ' ==================================
            ' Déclenchement du timer de retrait automatique
            Timer7.Enabled = True
        End If
        ' ==================================
        ' Vérification de la fin du déplacement du sélecteur de la liste Précision
        If Variables.NumeroList = 8 And Panel8.Height >= 321 Then
            ' ==================================
            ' Arrête du timer de temporisation de lecture
            Timer3.Enabled = False
            ' ==================================
            ' Déclenchement du timer de déclenchement de la procédure de retrait
            Timer6.Enabled = True
            ' ==================================
            ' Déclenchement du timer de retrait automatique
            Timer7.Enabled = True
        End If
        ' ==================================
        ' Vérification de la fin du déplacement du sélecteur de la liste Type Précision
        If Variables.NumeroList = 9 And Panel9.Height >= 321 Then
            ' ==================================
            ' Arrête du timer de temporisation de lecture
            Timer3.Enabled = False
            ' ==================================
            ' Déclenchement du timer de déclenchement de la procédure de retrait
            Timer6.Enabled = True
            ' ==================================
            ' Déclenchement du timer de retrait automatique
            Timer7.Enabled = True
        End If
        ' ==================================
        ' Vérification de la fin du déplacement du sélecteur de la liste Niveau de Saisie
        If Variables.NumeroList = 10 And Panel10.Height >= 321 Then
            ' ==================================
            ' Arrête du timer de temporisation de lecture
            Timer3.Enabled = False
            ' ==================================
            ' Déclenchement du timer de déclenchement de la procédure de retrait
            Timer6.Enabled = True
            ' ==================================
            ' Déclenchement du timer de retrait automatique
            Timer7.Enabled = True
        End If
        ' ==================================
        ' Vérification de la fin du déplacement du sélecteur de la liste Unité
        If Variables.NumeroList = 11 And Panel11.Height >= 321 Then
            ' ==================================
            ' Arrête du timer de temporisation de lecture
            Timer3.Enabled = False
            ' ==================================
            ' Déclenchement du timer de déclenchement de la procédure de retrait
            Timer6.Enabled = True
            ' ==================================
            ' Déclenchement du timer de retrait automatique
            Timer7.Enabled = True
        End If
        ' ==================================
        ' Vérification de la fin du déplacement du sélecteur de la liste Code Remarque
        If Variables.NumeroList = 12 And Panel13.Height >= 321 Then
            ' ==================================
            ' Arrête du timer de temporisation de lecture
            Timer3.Enabled = False
            ' ==================================
            ' Déclenchement du timer de déclenchement de la procédure de retrait
            Timer6.Enabled = True
            ' ==================================
            ' Déclenchement du timer de retrait automatique
            Timer7.Enabled = True
        End If

    End Sub


    '=============================================================================================================================================
    ' PROCÉDURES DE DISPARITION DES SÉLECTEURS
    '=============================================================================================================================================
    ' Déclenche la disparition du sélecteur
    Private Sub Timer4_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer4.Tick
        ' ==================================
        ' Décrémentation du déplacement du sélecteur de la liste des Dénominations des Méthodes
        If Variables.NumeroList = 1 And Panel1.Height > 50 Then
            Panel1.Height = Panel1.Height - 16
        End If
        ' ==================================
        ' Décrémentation du déplacement du sélecteur de la liste des Dénominations des Analystes
        If Variables.NumeroList = 2 And Panel2.Height > 50 Then
            Panel2.Height = Panel2.Height - 16
        End If
        ' ==================================
        ' Décrémentation du déplacement du sélecteur de la liste des Engin Analyse
        If Variables.NumeroList = 3 And Panel3.Height > 50 Then
            Panel3.Height = Panel3.Height - 16
        End If
        ' ==================================
        ' Décrémentation du déplacement du sélecteur de la liste des Libellés des Analystes
        If Variables.NumeroList = 4 And Panel4.Height > 50 Then
            Panel4.Height = Panel4.Height - 16
        End If
        ' ==================================
        ' Décrémentation du déplacement du sélecteur de la liste Fraction
        If Variables.NumeroList = 5 And Panel5.Height > 50 Then
            Panel5.Height = Panel5.Height - 16
        End If
        ' ==================================
        ' Décrémentation du déplacement du sélecteur de la liste Support
        If Variables.NumeroList = 6 And Panel6.Height > 50 Then
            Panel6.Height = Panel6.Height - 16
        End If
        ' ==================================
        ' Décrémentation du déplacement du sélecteur de la liste Engin de Prélèvement
        If Variables.NumeroList = 7 And Panel7.Height > 50 Then
            Panel7.Height = Panel7.Height - 16
        End If
        ' ==================================
        ' Décrémentation du déplacement du sélecteur de la liste Précision
        If Variables.NumeroList = 8 And Panel8.Height > 50 Then
            Panel8.Height = Panel8.Height - 16
        End If
        ' ==================================
        ' Décrémentation du déplacement du sélecteur de la liste Type Précision
        If Variables.NumeroList = 9 And Panel9.Height > 50 Then
            Panel9.Height = Panel9.Height - 16
        End If
        ' ==================================
        ' Décrémentation du déplacement du sélecteur de la liste Niveau de Saisie
        If Variables.NumeroList = 10 And Panel10.Height > 50 Then
            Panel10.Height = Panel10.Height - 16
        End If
        ' ==================================
        ' Décrémentation du déplacement du sélecteur de la liste Unité
        If Variables.NumeroList = 11 And Panel11.Height > 50 Then
            Panel11.Height = Panel11.Height - 16
        End If
        ' ==================================
        ' Décrémentation du déplacement du sélecteur de la liste Code Remarque
        If Variables.NumeroList = 12 And Panel13.Height > 50 Then
            Panel13.Height = Panel13.Height - 16
        End If


        ' ==================================
        ' Vérification de la fin du déplacement du sélecteur de la liste des Dénominations des Méthodes
        If Variables.NumeroList = 1 And Panel1.Height <= 50 Then
            ' ==================================
            ' Retrait de la liste correspondante
            ListBox1.Visible = False
            ' ==================================
            ' Arrêt du timer
            Timer4.Enabled = False
            ' Déclenchement du timer de retrait du conteneur de la liste correspondante et lancement éventuel d'une liste demandée
            Timer5.Enabled = True
        End If
        ' ==================================
        ' Vérification de la fin du déplacement du sélecteur de la liste des Dénominations des Analystes
        If Variables.NumeroList = 2 And Panel2.Height <= 50 Then
            ' ==================================
            ' Retrait de la liste correspondante
            ListBox2.Visible = False
            ' ==================================
            ' Arrêt du timer
            Timer4.Enabled = False
            ' Déclenchement du timer de retrait du conteneur de la liste correspondante et lancement éventuel d'une liste demandée
            Timer5.Enabled = True
        End If
        ' ==================================
        ' Vérification de la fin du déplacement du sélecteur de la liste des Engin Analyse
        If Variables.NumeroList = 3 And Panel3.Height <= 50 Then
            ' ==================================
            ' Retrait de la liste correspondante
            ListBox3.Visible = False
            ' ==================================
            ' Arrêt du timer
            Timer4.Enabled = False
            ' Déclenchement du timer de retrait du conteneur de la liste correspondante et lancement éventuel d'une liste demandée
            Timer5.Enabled = True
        End If
        ' ==================================
        ' Vérification de la fin du déplacement du sélecteur de la liste des Libellés des Analystes
        If Variables.NumeroList = 4 And Panel4.Height <= 50 Then
            ' ==================================
            ' Retrait de la liste correspondante
            ListBox4.Visible = False
            ' ==================================
            ' Arrêt du timer
            Timer4.Enabled = False
            ' Déclenchement du timer de retrait du conteneur de la liste correspondante et lancement éventuel d'une liste demandée
            Timer5.Enabled = True
        End If
        ' ==================================
        ' Vérification de la fin du déplacement du sélecteur de la liste Fraction
        If Variables.NumeroList = 5 And Panel5.Height <= 50 Then
            ' ==================================
            ' Retrait de la liste correspondante
            ListBox5.Visible = False
            ' ==================================
            ' Arrêt du timer
            Timer4.Enabled = False
            ' Déclenchement du timer de retrait du conteneur de la liste correspondante et lancement éventuel d'une liste demandée
            Timer5.Enabled = True
        End If
        ' ==================================
        ' Vérification de la fin du déplacement du sélecteur de la liste Support
        If Variables.NumeroList = 6 And Panel6.Height <= 50 Then
            ' ==================================
            ' Retrait de la liste correspondante
            ListBox6.Visible = False
            ' ==================================
            ' Arrêt du timer
            Timer4.Enabled = False
            ' Déclenchement du timer de retrait du conteneur de la liste correspondante et lancement éventuel d'une liste demandée
            Timer5.Enabled = True
        End If
        ' ==================================
        ' Vérification de la fin du déplacement du sélecteur de la liste Engin de Prélèvement
        If Variables.NumeroList = 7 And Panel7.Height <= 50 Then
            ' ==================================
            ' Retrait de la liste correspondante
            ListBox7.Visible = False
            ' ==================================
            ' Arrêt du timer
            Timer4.Enabled = False
            ' Déclenchement du timer de retrait du conteneur de la liste correspondante et lancement éventuel d'une liste demandée
            Timer5.Enabled = True
        End If
        ' ==================================
        ' Vérification de la fin du déplacement du sélecteur de la liste Précision
        If Variables.NumeroList = 8 And Panel8.Height <= 50 Then
            ' ==================================
            ' Retrait de la liste correspondante
            ListBox8.Visible = False
            ' ==================================
            ' Arrêt du timer
            Timer4.Enabled = False
            ' Déclenchement du timer de retrait du conteneur de la liste correspondante et lancement éventuel d'une liste demandée
            Timer5.Enabled = True
        End If
        ' ==================================
        ' Vérification de la fin du déplacement du sélecteur de la liste Type Précision
        If Variables.NumeroList = 9 And Panel9.Height <= 50 Then
            ' ==================================
            ' Retrait de la liste correspondante
            ListBox9.Visible = False
            ' ==================================
            ' Arrêt du timer
            Timer4.Enabled = False
            ' Déclenchement du timer de retrait du conteneur de la liste correspondante et lancement éventuel d'une liste demandée
            Timer5.Enabled = True
        End If
        ' ==================================
        ' Vérification de la fin du déplacement du sélecteur de la liste Niveau de Saisie
        If Variables.NumeroList = 10 And Panel10.Height <= 50 Then
            ' ==================================
            ' Retrait de la liste correspondante
            ListBox10.Visible = False
            ' ==================================
            ' Arrêt du timer
            Timer4.Enabled = False
            ' Déclenchement du timer de retrait du conteneur de la liste correspondante et lancement éventuel d'une liste demandée
            Timer5.Enabled = True
        End If
        ' ==================================
        ' Vérification de la fin du déplacement du sélecteur de la liste Unité
        If Variables.NumeroList = 11 And Panel11.Height <= 50 Then
            ' ==================================
            ' Retrait de la liste correspondante
            ListBox11.Visible = False
            ' ==================================
            ' Arrêt du timer
            Timer4.Enabled = False
            ' Déclenchement du timer de retrait du conteneur de la liste correspondante et lancement éventuel d'une liste demandée
            Timer5.Enabled = True
        End If
        ' ==================================
        ' Vérification de la fin du déplacement du sélecteur de la liste Code Remarque
        If Variables.NumeroList = 12 And Panel13.Height <= 50 Then
            ' ==================================
            ' Retrait de la liste correspondante
            ListBox12.Visible = False
            ' ==================================
            ' Arrêt du timer
            Timer4.Enabled = False
            ' Déclenchement du timer de retrait du conteneur de la liste correspondante et lancement éventuel d'une liste demandée
            Timer5.Enabled = True
        End If

    End Sub

    ' ==============================================================================================================================================
    ' Timer de retrait du conteneur de la liste correspondante et lancement éventuel d'une liste demandée
    Private Sub Timer5_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer5.Tick
        ' ==================================
        ' Décrémentation du déplacement du sélecteur de la liste des Dénominations des Méthodes
        If Variables.NumeroList = 1 Then Panel1.Top = Panel1.Top - 2
        ' ==================================
        ' Décrémentation du déplacement du sélecteur de la liste des Dénominations des Analystes
        If Variables.NumeroList = 2 Then Panel2.Top = Panel2.Top - 2
        ' ==================================
        ' Décrémentation du déplacement du sélecteur de la liste des Engin Analyse
        If Variables.NumeroList = 3 Then Panel3.Top = Panel3.Top - 2
        ' ==================================
        ' Décrémentation du déplacement du sélecteur de la liste des Libellés des Analystes
        If Variables.NumeroList = 4 Then Panel4.Top = Panel4.Top - 2
        ' ==================================
        ' Décrémentation du déplacement du sélecteur de la liste Fraction
        If Variables.NumeroList = 5 Then Panel5.Top = Panel5.Top - 2
        ' ==================================
        ' Décrémentation du déplacement du sélecteur de la liste Support
        If Variables.NumeroList = 6 Then Panel6.Top = Panel6.Top - 2
        ' ==================================
        ' Décrémentation du déplacement du sélecteur de la liste Engin de Prélèvement
        If Variables.NumeroList = 7 Then Panel7.Top = Panel7.Top - 2
        ' ==================================
        ' Décrémentation du déplacement du sélecteur de la liste Précision
        If Variables.NumeroList = 8 Then Panel8.Top = Panel8.Top - 2
        ' ==================================
        ' Décrémentation du déplacement du sélecteur de la liste Type Précision
        If Variables.NumeroList = 9 Then Panel9.Top = Panel9.Top - 2
        ' ==================================
        ' Décrémentation du déplacement du sélecteur de la liste Niveau de Saisie
        If Variables.NumeroList = 10 Then Panel10.Top = Panel10.Top - 2
        ' ==================================
        ' Décrémentation du déplacement du sélecteur de la liste Unité
        If Variables.NumeroList = 11 Then Panel11.Top = Panel11.Top - 2
        ' ==================================
        ' Décrémentation du déplacement du sélecteur de la liste Code Remarque
        If Variables.NumeroList = 12 Then Panel13.Top = Panel13.Top - 2


        ' ==================================
        ' Traitement pour la liste des Dénominations des Méthodes
        If Variables.NumeroList = 1 And Panel1.Top <= -50 Then
            ' ==================================
            ' Remise à zéro de la variable de numéro de la liste
            Variables.NumeroList = 0
            ' ==================================
            ' Arrêt du timer
            Timer5.Enabled = False
            ' ==================================
            ' Arrêt du timer de déclenchement de la procédure de retrait
            Timer6.Enabled = False
            ' ==================================
            ' Vérification si l'apparition d'une liste a été demandée
            If Variables.NumeroListSuivant <> 0 Then
                ' ==================================
                ' Transfert du numéro de la liste suivante dans celui de la liste
                Variables.NumeroList = Variables.NumeroListSuivant
                ' ==================================
                ' Remise à zéro de la variable de numéro de la liste suivante
                Variables.NumeroListSuivant = 0
                ' ==================================
                ' Déclenchement du timer de la procédure d'apparition des listes
                Timer2.Enabled = True
            End If
            Exit Sub
        End If
        ' ==================================
        ' Traitement pour la liste des Dénominations des Analystes
        If Variables.NumeroList = 2 And Panel2.Top <= -50 Then
            ' ==================================
            ' Remise à zéro de la variable de numéro de la liste
            Variables.NumeroList = 0
            ' ==================================
            ' Arrêt du timer
            Timer5.Enabled = False
            ' ==================================
            ' Arrêt du timer de déclenchement de la procédure de retrait
            Timer6.Enabled = False
            ' ==================================
            ' Vérification si l'apparition d'une liste a été demandée
            If Variables.NumeroListSuivant <> 0 Then
                ' ==================================
                ' Transfert du numéro de la liste suivante dans celui de la liste
                Variables.NumeroList = Variables.NumeroListSuivant
                ' ==================================
                ' Remise à zéro de la variable de numéro de la liste suivante
                Variables.NumeroListSuivant = 0
                ' ==================================
                ' Déclenchement du timer de la procédure d'apparition des listes
                Timer2.Enabled = True
            End If
            Exit Sub
        End If
        ' ==================================
        ' Traitement pour la liste des Engin Analyse
        If Variables.NumeroList = 3 And Panel3.Top <= -50 Then
            ' ==================================
            ' Remise à zéro de la variable de numéro de la liste
            Variables.NumeroList = 0
            ' ==================================
            ' Arrêt du timer
            Timer5.Enabled = False
            ' ==================================
            ' Arrêt du timer de déclenchement de la procédure de retrait
            Timer6.Enabled = False
            ' ==================================
            ' Vérification si l'apparition d'une liste a été demandée
            If Variables.NumeroListSuivant <> 0 Then
                ' ==================================
                ' Transfert du numéro de la liste suivante dans celui de la liste
                Variables.NumeroList = Variables.NumeroListSuivant
                ' ==================================
                ' Remise à zéro de la variable de numéro de la liste suivante
                Variables.NumeroListSuivant = 0
                ' ==================================
                ' Déclenchement du timer de la procédure d'apparition des listes
                Timer2.Enabled = True
            End If
            Exit Sub
        End If
        ' ==================================
        ' Traitement pour la liste des Libellés des Analystes
        If Variables.NumeroList = 4 And Panel4.Top <= -50 Then
            ' ==================================
            ' Remise à zéro de la variable de numéro de la liste
            Variables.NumeroList = 0
            ' ==================================
            ' Arrêt du timer
            Timer5.Enabled = False
            ' ==================================
            ' Arrêt du timer de déclenchement de la procédure de retrait
            Timer6.Enabled = False
            ' ==================================
            ' Vérification si l'apparition d'une liste a été demandée
            If Variables.NumeroListSuivant <> 0 Then
                ' ==================================
                ' Transfert du numéro de la liste suivante dans celui de la liste
                Variables.NumeroList = Variables.NumeroListSuivant
                ' ==================================
                ' Remise à zéro de la variable de numéro de la liste suivante
                Variables.NumeroListSuivant = 0
                ' ==================================
                ' Déclenchement du timer de la procédure d'apparition des listes
                Timer2.Enabled = True
            End If
            Exit Sub
        End If
        ' ==================================
        ' Traitement pour la liste Fraction
        If Variables.NumeroList = 5 And Panel5.Top <= -50 Then
            ' ==================================
            ' Remise à zéro de la variable de numéro de la liste
            Variables.NumeroList = 0
            ' ==================================
            ' Arrêt du timer
            Timer5.Enabled = False
            ' ==================================
            ' Arrêt du timer de déclenchement de la procédure de retrait
            Timer6.Enabled = False
            ' ==================================
            ' Vérification si l'apparition d'une liste a été demandée
            If Variables.NumeroListSuivant <> 0 Then
                ' ==================================
                ' Transfert du numéro de la liste suivante dans celui de la liste
                Variables.NumeroList = Variables.NumeroListSuivant
                ' ==================================
                ' Remise à zéro de la variable de numéro de la liste suivante
                Variables.NumeroListSuivant = 0
                ' ==================================
                ' Déclenchement du timer de la procédure d'apparition des listes
                Timer2.Enabled = True
            End If
            Exit Sub
        End If
        ' ==================================
        ' Traitement pour la liste Support
        If Variables.NumeroList = 6 And Panel6.Top <= -50 Then
            ' ==================================
            ' Remise à zéro de la variable de numéro de la liste
            Variables.NumeroList = 0
            ' ==================================
            ' Arrêt du timer
            Timer5.Enabled = False
            ' ==================================
            ' Arrêt du timer de déclenchement de la procédure de retrait
            Timer6.Enabled = False
            ' ==================================
            ' Vérification si l'apparition d'une liste a été demandée
            If Variables.NumeroListSuivant <> 0 Then
                ' ==================================
                ' Transfert du numéro de la liste suivante dans celui de la liste
                Variables.NumeroList = Variables.NumeroListSuivant
                ' ==================================
                ' Remise à zéro de la variable de numéro de la liste suivante
                Variables.NumeroListSuivant = 0
                ' ==================================
                ' Déclenchement du timer de la procédure d'apparition des listes
                Timer2.Enabled = True
            End If
            Exit Sub
        End If
        ' ==================================
        ' Traitement pour la liste Engin de Prélèvement
        If Variables.NumeroList = 7 And Panel7.Top <= -50 Then
            ' ==================================
            ' Remise à zéro de la variable de numéro de la liste
            Variables.NumeroList = 0
            ' ==================================
            ' Arrêt du timer
            Timer5.Enabled = False
            ' ==================================
            ' Arrêt du timer de déclenchement de la procédure de retrait
            Timer6.Enabled = False
            ' ==================================
            ' Vérification si l'apparition d'une liste a été demandée
            If Variables.NumeroListSuivant <> 0 Then
                ' ==================================
                ' Transfert du numéro de la liste suivante dans celui de la liste
                Variables.NumeroList = Variables.NumeroListSuivant
                ' ==================================
                ' Remise à zéro de la variable de numéro de la liste suivante
                Variables.NumeroListSuivant = 0
                ' ==================================
                ' Déclenchement du timer de la procédure d'apparition des listes
                Timer2.Enabled = True
            End If
            Exit Sub
        End If
        ' ==================================
        ' Traitement pour la liste Précision
        If Variables.NumeroList = 8 And Panel8.Top <= -50 Then
            ' ==================================
            ' Remise à zéro de la variable de numéro de la liste
            Variables.NumeroList = 0
            ' ==================================
            ' Arrêt du timer
            Timer5.Enabled = False
            ' ==================================
            ' Arrêt du timer de déclenchement de la procédure de retrait
            Timer6.Enabled = False
            ' ==================================
            ' Vérification si l'apparition d'une liste a été demandée
            If Variables.NumeroListSuivant <> 0 Then
                ' ==================================
                ' Transfert du numéro de la liste suivante dans celui de la liste
                Variables.NumeroList = Variables.NumeroListSuivant
                ' ==================================
                ' Remise à zéro de la variable de numéro de la liste suivante
                Variables.NumeroListSuivant = 0
                ' ==================================
                ' Déclenchement du timer de la procédure d'apparition des listes
                Timer2.Enabled = True
            End If
            Exit Sub
        End If
        ' ==================================
        ' Traitement pour la liste Type Précision
        If Variables.NumeroList = 9 And Panel9.Top <= -50 Then
            ' ==================================
            ' Remise à zéro de la variable de numéro de la liste
            Variables.NumeroList = 0
            ' ==================================
            ' Arrêt du timer
            Timer5.Enabled = False
            ' ==================================
            ' Arrêt du timer de déclenchement de la procédure de retrait
            Timer6.Enabled = False
            ' ==================================
            ' Vérification si l'apparition d'une liste a été demandée
            If Variables.NumeroListSuivant <> 0 Then
                ' ==================================
                ' Transfert du numéro de la liste suivante dans celui de la liste
                Variables.NumeroList = Variables.NumeroListSuivant
                ' ==================================
                ' Remise à zéro de la variable de numéro de la liste suivante
                Variables.NumeroListSuivant = 0
                ' ==================================
                ' Déclenchement du timer de la procédure d'apparition des listes
                Timer2.Enabled = True
            End If
            Exit Sub
        End If
        ' ==================================
        ' Traitement pour la liste Niveau de Saisie
        If Variables.NumeroList = 10 And Panel10.Top <= -50 Then
            ' ==================================
            ' Remise à zéro de la variable de numéro de la liste
            Variables.NumeroList = 0
            ' ==================================
            ' Arrêt du timer
            Timer5.Enabled = False
            ' ==================================
            ' Arrêt du timer de déclenchement de la procédure de retrait
            Timer6.Enabled = False
            ' ==================================
            ' Vérification si l'apparition d'une liste a été demandée
            If Variables.NumeroListSuivant <> 0 Then
                ' ==================================
                ' Transfert du numéro de la liste suivante dans celui de la liste
                Variables.NumeroList = Variables.NumeroListSuivant
                ' ==================================
                ' Remise à zéro de la variable de numéro de la liste suivante
                Variables.NumeroListSuivant = 0
                ' ==================================
                ' Déclenchement du timer de la procédure d'apparition des listes
                Timer2.Enabled = True
            End If
            Exit Sub
        End If
        ' ==================================
        ' Traitement pour la liste Unité
        If Variables.NumeroList = 11 And Panel11.Top <= -50 Then
            ' ==================================
            ' Remise à zéro de la variable de numéro de la liste
            Variables.NumeroList = 0
            ' ==================================
            ' Arrêt du timer
            Timer5.Enabled = False
            ' ==================================
            ' Arrêt du timer de déclenchement de la procédure de retrait
            Timer6.Enabled = False
            ' ==================================
            ' Vérification si l'apparition d'une liste a été demandée
            If Variables.NumeroListSuivant <> 0 Then
                ' ==================================
                ' Transfert du numéro de la liste suivante dans celui de la liste
                Variables.NumeroList = Variables.NumeroListSuivant
                ' ==================================
                ' Remise à zéro de la variable de numéro de la liste suivante
                Variables.NumeroListSuivant = 0
                ' ==================================
                ' Déclenchement du timer de la procédure d'apparition des listes
                Timer2.Enabled = True
            End If
            Exit Sub
        End If
        ' ==================================
        ' Traitement pour la liste Code Remarque
        If Variables.NumeroList = 12 And Panel13.Top <= -50 Then
            ' ==================================
            ' Remise à zéro de la variable de numéro de la liste
            Variables.NumeroList = 0
            ' ==================================
            ' Arrêt du timer
            Timer5.Enabled = False
            ' ==================================
            ' Arrêt du timer de déclenchement de la procédure de retrait
            Timer6.Enabled = False
            ' ==================================
            ' Vérification si l'apparition d'une liste a été demandée
            If Variables.NumeroListSuivant <> 0 Then
                ' ==================================
                ' Transfert du numéro de la liste suivante dans celui de la liste
                Variables.NumeroList = Variables.NumeroListSuivant
                ' ==================================
                ' Remise à zéro de la variable de numéro de la liste suivante
                Variables.NumeroListSuivant = 0
                ' ==================================
                ' Déclenchement du timer de la procédure d'apparition des listes
                Timer2.Enabled = True
            End If
            Exit Sub
        End If


    End Sub

    ' ==============================================================================================================================================
    ' Timer de déclenchement de la procédure de retrait de la liste affichée
    Private Sub Timer6_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer6.Tick
        ' ==================================
        ' Déclenchement du timer de la procédure de retrait
        Timer4.Enabled = True
        ' ==================================
        ' Arrêt du timer
        Timer6.Enabled = False
        ' ==================================
        ' Arrêt du timer de retrait automatique
        Timer7.Enabled = False
    End Sub

    ' ==============================================================================================================================================
    ' Timer de retrait automatique lorsque le curseur n'est pas sur la zone correspondante
    Private Sub Timer7_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer7.Tick

        '       TextBox21.Text = "X " & MousePosition.X - Me.Left
        '       TextBox11.Text = "Y " & MousePosition.Y - Me.Top

        ' ==================================
        ' Traitement pour la liste des Dénominations des Méthodes
        If Variables.NumeroList = 1 Then
            ' ==================================
            ' Vérification si le curseur est dans la zone correspondante
            If (MousePosition.X - Me.Left) > 167 And (MousePosition.X - Me.Left) < 848 And (MousePosition.Y - Me.Top) > 30 And (MousePosition.Y - Me.Top) < 575 Then    '   638 Then
                ' ==================================
                ' Arrêt du timer de déclenchement de la procédure de retrait
                Timer6.Enabled = False
                ' ==================================
                ' Sortie
                Exit Sub
            End If
        End If
        ' ==================================
        ' Traitement pour la liste des Dénominations des Analyseurs
        If Variables.NumeroList = 2 Then
            ' ==================================
            ' Vérification si le curseur est dans la zone correspondante
            If (MousePosition.X - Me.Left) > 110 And (MousePosition.X - Me.Left) < 951 And (MousePosition.Y - Me.Top) > 30 And (MousePosition.Y - Me.Top) < 575 Then
                ' ==================================
                ' Arrêt du timer de déclenchement de la procédure de retrait
                Timer6.Enabled = False
                ' ==================================
                ' Sortie
                Exit Sub
            End If
        End If
        ' ==================================
        ' Traitement pour la liste des Engin Analyse
        If Variables.NumeroList = 3 Then
            ' ==================================
            ' Vérification si le curseur est dans la zone correspondante
            If (MousePosition.X - Me.Left) > 397 And (MousePosition.X - Me.Left) < 663 And (MousePosition.Y - Me.Top) > 30 And (MousePosition.Y - Me.Top) < 351 Then
                ' ==================================
                ' Arrêt du timer de déclenchement de la procédure de retrait
                Timer6.Enabled = False
                ' ==================================
                ' Sortie
                Exit Sub
            End If
        End If
        ' ==================================
        ' Traitement pour la liste des Libellés des Analyseurs
        If Variables.NumeroList = 4 Then
            ' ==================================
            ' Vérification si le curseur est dans la zone correspondante
            If (MousePosition.X - Me.Left) > 430 And (MousePosition.X - Me.Left) < 631 And (MousePosition.Y - Me.Top) > 30 And (MousePosition.Y - Me.Top) < 575 Then
                ' ==================================
                ' Arrêt du timer de déclenchement de la procédure de retrait
                Timer6.Enabled = False
                ' ==================================
                ' Sortie
                Exit Sub
            End If
        End If
        ' ==================================
        ' Traitement pour la liste Fraction
        If Variables.NumeroList = 5 Then
            ' ==================================
            ' Vérification si le curseur est dans la zone correspondante
            If (MousePosition.X - Me.Left) > 398 And (MousePosition.X - Me.Left) < 663 And (MousePosition.Y - Me.Top) > 30 And (MousePosition.Y - Me.Top) < 351 Then
                ' ==================================
                ' Arrêt du timer de déclenchement de la procédure de retrait
                Timer6.Enabled = False
                ' ==================================
                ' Sortie
                Exit Sub
            End If
        End If
        ' ==================================
        ' Traitement pour la liste Support
        If Variables.NumeroList = 6 Then
            ' ==================================
            ' Vérification si le curseur est dans la zone correspondante
            If (MousePosition.X - Me.Left) > 398 And (MousePosition.X - Me.Left) < 663 And (MousePosition.Y - Me.Top) > 30 And (MousePosition.Y - Me.Top) < 351 Then
                ' ==================================
                ' Arrêt du timer de déclenchement de la procédure de retrait
                Timer6.Enabled = False
                ' ==================================
                ' Sortie
                Exit Sub
            End If
        End If
        ' ==================================
        ' Traitement pour la liste Engin de Prélèvement
        If Variables.NumeroList = 7 Then
            ' ==================================
            ' Vérification si le curseur est dans la zone correspondante
            If (MousePosition.X - Me.Left) > 398 And (MousePosition.X - Me.Left) < 663 And (MousePosition.Y - Me.Top) > 30 And (MousePosition.Y - Me.Top) < 351 Then
                ' ==================================
                ' Arrêt du timer de déclenchement de la procédure de retrait
                Timer6.Enabled = False
                ' ==================================
                ' Sortie
                Exit Sub
            End If
        End If
        ' ==================================
        ' Traitement pour la liste Précision
        If Variables.NumeroList = 8 Then
            ' ==================================
            ' Vérification si le curseur est dans la zone correspondante
            If (MousePosition.X - Me.Left) > 398 And (MousePosition.X - Me.Left) < 664 And (MousePosition.Y - Me.Top) > 30 And (MousePosition.Y - Me.Top) < 351 Then
                ' ==================================
                ' Arrêt du timer de déclenchement de la procédure de retrait
                Timer6.Enabled = False
                ' ==================================
                ' Sortie
                Exit Sub
            End If
        End If
        ' ==================================
        ' Traitement pour la liste Type Précision
        If Variables.NumeroList = 9 Then
            ' ==================================
            ' Vérification si le curseur est dans la zone correspondante
            If (MousePosition.X - Me.Left) > 398 And (MousePosition.X - Me.Left) < 664 And (MousePosition.Y - Me.Top) > 30 And (MousePosition.Y - Me.Top) < 351 Then
                ' ==================================
                ' Arrêt du timer de déclenchement de la procédure de retrait
                Timer6.Enabled = False
                ' ==================================
                ' Sortie
                Exit Sub
            End If
        End If
        ' ==================================
        ' Traitement pour la liste Niveau de Saisie
        If Variables.NumeroList = 10 Then
            ' ==================================
            ' Vérification si le curseur est dans la zone correspondante
            If (MousePosition.X - Me.Left) > 398 And (MousePosition.X - Me.Left) < 664 And (MousePosition.Y - Me.Top) > 30 And (MousePosition.Y - Me.Top) < 351 Then
                ' ==================================
                ' Arrêt du timer de déclenchement de la procédure de retrait
                Timer6.Enabled = False
                ' ==================================
                ' Sortie
                Exit Sub
            End If
        End If
        ' ==================================
        ' Traitement pour la liste Unité
        If Variables.NumeroList = 11 Then
            ' ==================================
            ' Vérification si le curseur est dans la zone correspondante
            If (MousePosition.X - Me.Left) > 398 And (MousePosition.X - Me.Left) < 664 And (MousePosition.Y - Me.Top) > 30 And (MousePosition.Y - Me.Top) < 351 Then
                ' ==================================
                ' Arrêt du timer de déclenchement de la procédure de retrait
                Timer6.Enabled = False
                ' ==================================
                ' Sortie
                Exit Sub
            End If
        End If
        ' ==================================
        ' Traitement pour la liste Code Remarque
        If Variables.NumeroList = 12 Then
            ' ==================================
            ' Vérification si le curseur est dans la zone correspondante
            If (MousePosition.X - Me.Left) > 398 And (MousePosition.X - Me.Left) < 664 And (MousePosition.Y - Me.Top) > 30 And (MousePosition.Y - Me.Top) < 351 Then
                ' ==================================
                ' Arrêt du timer de déclenchement de la procédure de retrait
                Timer6.Enabled = False
                ' ==================================
                ' Sortie
                Exit Sub
            End If
        End If



        ' ==================================
        ' Déclenchement du timer de déclenchement de la procédure de retrait
        Timer6.Enabled = True
    End Sub

    ' Permet de connaitre les côtes pour le retrait automatique
    '    Private Sub Propriétés_Param_MouseMove(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Me.MouseMove
    '       TextBox16.Text = "Ordonnée = " & e.Y + 3 & " Absisse = " & e.X + 3
    '    End Sub


    '=============================================================================================================================================
    ' PROCÉDURES DE SÉLECTION DANS LES LISTES
    '=============================================================================================================================================
    ' Sélection d'une dénomination de Méthodes
    Private Sub ListBox1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ListBox1.SelectedIndexChanged
        ' ==================================
        ' Sortie si sélection vide
        If ListBox1.SelectedItem = "" Then Exit Sub
        ' ==================================
        ' Arrêt du timer de temporisation de lecture pour éviter un conflit lorsque la liste se déroule et que l'on a cliqué sur une sélection
        Timer3.Enabled = False
        ' ==================================
        ' Transfert de la sélection
        If ListBox1.SelectedItem = " - Effacement du champ -" Then
            TextBox5.Text = ""
        Else
            TextBox5.Text = Strings.LTrim(ListBox1.SelectedItem)
        End If
        ' ==================================
        ' Déclenchement du retrait du sélecteur
        Timer4.Enabled = True
        ' ==================================
        ' Arrêt du timer de retrait automatique
        Timer7.Enabled = False
        ' ==================================
        ' Désélection 
        ListBox1.ClearSelected()
    End Sub

    '=============================================================================================================================================
    ' Sélection d'une dénomination d'Analyseur
    Private Sub ListBox2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ListBox2.SelectedIndexChanged
        ' ==================================
        ' Sortie si sélection vide
        If ListBox2.SelectedItem = "" Then Exit Sub
        ' ==================================
        Dim i As Integer
        Dim IndexTabInfo As Integer
        ' ==================================
        ' Arrêt du timer de temporisation de lecture pour éviter un conflit lorsque la liste se déroule et que l'on a cliqué sur une sélection
        Timer3.Enabled = False
        ' ==================================
        ' Transfert de la sélection
        If ListBox2.SelectedItem = " - Effacement du champ -" Then
            TextBox10.Text = ""
            TextBox24.Text = ""
        Else
            TextBox24.Text = Strings.LTrim(ListBox2.SelectedItem)
            ' ==================================
            ' Recherche du point d'entrée dans le tableau des informations pour les Analyseurs
            For i = 0 To 5000
                If Variables.TabRéfInfo(i, 0) = "ANALYSEUR" Then
                    IndexTabInfo = i + 1
                    Exit For
                End If
            Next
            TextBox10.Text = ""
            ' ==================================
            ' Récupération du libellé d'Analyseur
            For i = IndexTabInfo To 5000
                ' ==================================
                ' Sortie de la boucle si fin de tableau
                If Variables.TabRéfInfo(i, 0) = "" Then Exit For
                ' ==================================
                ' Rangement du libellé d'Analyseur
                If Variables.TabRéfInfo(i, 0) = TextBox24.Text Then
                    ' ==================================
                    ' Affectation de la désignation
                    TextBox10.Text = Variables.TabRéfInfo(i, 1)
                End If
            Next
        End If

        ' ==================================
        ' Déclenchement du retrait du sélecteur
        Timer4.Enabled = True
        ' ==================================
        ' Arrêt du timer de retrait automatique
        Timer7.Enabled = False
        ' ==================================
        ' Désélection 
        ListBox2.ClearSelected()
    End Sub

    '=============================================================================================================================================
    ' Sélection d'une dénomination Engin Analyse
    Private Sub ListBox3_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ListBox3.SelectedIndexChanged
        ' ==================================
        ' Sortie si sélection vide
        If ListBox3.SelectedItem = "" Then Exit Sub
        ' ==================================
        ' Arrêt du timer de temporisation de lecture pour éviter un conflit lorsque la liste se déroule et que l'on a cliqué sur une sélection
        Timer3.Enabled = False
        ' ==================================
        ' Transfert de la sélection
        If ListBox3.SelectedItem = " - Effacement du champ -" Then
            TextBox26.Text = ""
        Else
            TextBox26.Text = Strings.LTrim(ListBox3.SelectedItem)
        End If
        ' ==================================
        ' Déclenchement du retrait du sélecteur
        Timer4.Enabled = True
        ' ==================================
        ' Arrêt du timer de retrait automatique
        Timer7.Enabled = False
        ' ==================================
        ' Désélection 
        ListBox3.ClearSelected()
    End Sub

    '=============================================================================================================================================
    ' Sélection d'un libellé d'Analyseur
    Private Sub ListBox4_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ListBox4.SelectedIndexChanged
        ' ==================================
        ' Sortie si sélection vide
        If ListBox4.SelectedItem = "" Then Exit Sub
        ' ==================================
        Dim i As Integer
        Dim IndexTabInfo As Integer
        ' ==================================
        ' Arrêt du timer de temporisation de lecture pour éviter un conflit lorsque la liste se déroule et que l'on a cliqué sur une sélection
        Timer3.Enabled = False
        ' ==================================
        ' Transfert de la sélection
        If ListBox4.SelectedItem = " - Effacement du champ -" Then
            TextBox10.Text = ""
            TextBox24.Text = ""
        Else
            TextBox10.Text = Strings.LTrim(ListBox4.SelectedItem)
            ' ==================================
            ' Recherche du point d'entrée dans le tableau des informations pour les Analyseurs
            For i = 0 To 5000
                If Variables.TabRéfInfo(i, 0) = "ANALYSEUR" Then
                    IndexTabInfo = i + 1
                    Exit For
                End If
            Next
            TextBox24.Text = ""
            ' ==================================
            ' Récupération de la dénomination de l'Analyseur
            For i = IndexTabInfo To 5000
                ' ==================================
                ' Sortie de la boucle si fin de tableau
                If Variables.TabRéfInfo(i, 0) = "" Then Exit For
                ' ==================================
                ' Rangement du libellé d'Analyseur
                If Variables.TabRéfInfo(i, 1) = TextBox10.Text Then
                    ' ==================================
                    ' Affectation de la désignation
                    TextBox24.Text = Variables.TabRéfInfo(i, 0)
                End If
            Next
        End If
        ' ==================================
        ' Déclenchement du retrait du sélecteur
        Timer4.Enabled = True
        ' ==================================
        ' Arrêt du timer de retrait automatique
        Timer7.Enabled = False
        ' ==================================
        ' Désélection 
        ListBox4.ClearSelected()
    End Sub

    '=============================================================================================================================================
    ' Sélection d'une dénomination Fraction
    Private Sub ListBox5_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ListBox5.SelectedIndexChanged
        ' ==================================
        ' Sortie si sélection vide
        If ListBox5.SelectedItem = "" Then Exit Sub
        ' ==================================
        ' Arrêt du timer de temporisation de lecture pour éviter un conflit lorsque la liste se déroule et que l'on a cliqué sur une sélection
        Timer3.Enabled = False
        ' ==================================
        ' Transfert de la sélection
        If ListBox5.SelectedItem = " - Effacement du champ -" Then
            TextBox7.Text = ""
        Else
            TextBox7.Text = Strings.LTrim(ListBox5.SelectedItem)
        End If
        ' ==================================
        ' Déclenchement du retrait du sélecteur
        Timer4.Enabled = True
        ' ==================================
        ' Arrêt du timer de retrait automatique
        Timer7.Enabled = False
        ' ==================================
        ' Désélection 
        ListBox5.ClearSelected()
    End Sub

    '=============================================================================================================================================
    ' Sélection d'une dénomination Support
    Private Sub ListBox6_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ListBox6.SelectedIndexChanged
        ' ==================================
        ' Sortie si sélection vide
        If ListBox6.SelectedItem = "" Then Exit Sub
        ' ==================================
        ' Arrêt du timer de temporisation de lecture pour éviter un conflit lorsque la liste se déroule et que l'on a cliqué sur une sélection
        Timer3.Enabled = False
        ' ==================================
        ' Transfert de la sélection
        If ListBox6.SelectedItem = " - Effacement du champ -" Then
            TextBox9.Text = ""
        Else
            TextBox9.Text = Strings.LTrim(ListBox6.SelectedItem)
        End If
        ' ==================================
        ' Déclenchement du retrait du sélecteur
        Timer4.Enabled = True
        ' ==================================
        ' Arrêt du timer de retrait automatique
        Timer7.Enabled = False
        ' ==================================
        ' Désélection 
        ListBox6.ClearSelected()
        ' ==================================
        ' Renvoi du select sur le bouton caché sinon Bouton Fermer valider ?
        Button2.Select()
    End Sub

    '=============================================================================================================================================
    ' Sélection d'une dénomination Engin de Prélèvement
    Private Sub ListBox7_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ListBox7.SelectedIndexChanged
        ' ==================================
        ' Sortie si sélection vide
        If ListBox7.SelectedItem = "" Then Exit Sub
        ' ==================================
        ' Arrêt du timer de temporisation de lecture pour éviter un conflit lorsque la liste se déroule et que l'on a cliqué sur une sélection
        Timer3.Enabled = False
        ' ==================================
        ' Transfert de la sélection
        If ListBox7.SelectedItem = " - Effacement du champ -" Then
            TextBox15.Text = ""
        Else
            TextBox15.Text = Strings.LTrim(ListBox7.SelectedItem)
        End If
        ' ==================================
        ' Déclenchement du retrait du sélecteur
        Timer4.Enabled = True
        ' ==================================
        ' Arrêt du timer de retrait automatique
        Timer7.Enabled = False
        ' ==================================
        ' Désélection 
        ListBox7.ClearSelected()
        ' ==================================
        ' Renvoi du select sur le bouton caché sinon Bouton Fermer valider ?
        Button2.Select()
    End Sub

    '=============================================================================================================================================
    ' Sélection d'une dénomination Précision
    Private Sub ListBox8_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ListBox8.SelectedIndexChanged
        ' ==================================
        ' Sortie si sélection vide
        '        If ListBox8.SelectedItem = "" Then Exit Sub
        ' ==================================
        ' Arrêt du timer de temporisation de lecture pour éviter un conflit lorsque la liste se déroule et que l'on a cliqué sur une sélection
        '        Timer3.Enabled = False
        ' ==================================
        ' Transfert de la sélection
        '        If ListBox8.SelectedItem = " - Effacement du champ -" Then
        'TextBox18.Text = ""
        '        Else
        '        TextBox18.Text = Strings.LTrim(ListBox8.SelectedItem)
        '        End If
        ' ==================================
        ' Déclenchement du retrait du sélecteur
        '        Timer4.Enabled = True
        ' ==================================
        ' Arrêt du timer de retrait automatique
        '        Timer7.Enabled = False
        ' ==================================
        ' Désélection 
        '        ListBox8.ClearSelected()
        ' ==================================
        ' Renvoi du select sur le bouton caché sinon Bouton Fermer valider ?
        '        Button2.Select()
    End Sub

    '=============================================================================================================================================
    ' Sélection d'une dénomination Type Précision
    Private Sub ListBox9_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ListBox9.SelectedIndexChanged
        ' ==================================
        ' Sortie si sélection vide
        If ListBox9.SelectedItem = "" Then Exit Sub
        ' ==================================
        ' Arrêt du timer de temporisation de lecture pour éviter un conflit lorsque la liste se déroule et que l'on a cliqué sur une sélection
        Timer3.Enabled = False
        ' ==================================
        ' Transfert de la sélection
        If ListBox9.SelectedItem = " - Effacement du champ -" Then
            TextBox22.Text = ""
        Else
            TextBox22.Text = Strings.LTrim(ListBox9.SelectedItem)
        End If
        ' ==================================
        ' Déclenchement du retrait du sélecteur
        Timer4.Enabled = True
        ' ==================================
        ' Arrêt du timer de retrait automatique
        Timer7.Enabled = False
        ' ==================================
        ' Désélection 
        ListBox9.ClearSelected()
        ' ==================================
        ' Renvoi du select sur le bouton caché sinon Bouton Fermer valider ?
        Button2.Select()
    End Sub

    '=============================================================================================================================================
    ' Sélection d'une dénomination Niveau Saisie
    Private Sub ListBox10_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ListBox10.SelectedIndexChanged
        ' ==================================
        ' Sortie si sélection vide
        If ListBox10.SelectedItem = "" Then Exit Sub
        ' ==================================
        ' Arrêt du timer de temporisation de lecture pour éviter un conflit lorsque la liste se déroule et que l'on a cliqué sur une sélection
        Timer3.Enabled = False
        ' ==================================
        ' Transfert de la sélection
        If ListBox10.SelectedItem = " - Effacement du champ -" Then
            TextBox21.Text = ""
        Else
            TextBox21.Text = Strings.LTrim(ListBox10.SelectedItem)
        End If
        ' ==================================
        ' Déclenchement du retrait du sélecteur
        Timer4.Enabled = True
        ' ==================================
        ' Arrêt du timer de retrait automatique
        Timer7.Enabled = False
        ' ==================================
        ' Désélection 
        ListBox10.ClearSelected()
        ' ==================================
        ' Renvoi du select sur le bouton caché sinon Bouton Fermer valider ?
        Button2.Select()
    End Sub

    '=============================================================================================================================================
    ' Sélection d'une dénomination Unité
    Private Sub ListBox11_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ListBox11.SelectedIndexChanged
        ' ==================================
        ' Sortie si sélection vide
        If ListBox11.SelectedItem = "" Then Exit Sub
        ' ==================================
        ' Arrêt du timer de temporisation de lecture pour éviter un conflit lorsque la liste se déroule et que l'on a cliqué sur une sélection
        Timer3.Enabled = False
        ' ==================================
        ' Transfert de la sélection
        If ListBox11.SelectedItem = " - Effacement du champ -" Then
            TextBox12.Text = ""
        Else
            TextBox12.Text = Strings.LTrim(ListBox11.SelectedItem)
        End If
        ' ==================================
        ' Déclenchement du retrait du sélecteur
        Timer4.Enabled = True
        ' ==================================
        ' Arrêt du timer de retrait automatique
        Timer7.Enabled = False
        ' ==================================
        ' Désélection 
        ListBox11.ClearSelected()
        ' ==================================
        ' Renvoi du select sur le bouton caché sinon Bouton Fermer valider ?
        Button2.Select()
    End Sub

    '=============================================================================================================================================
    ' Sélection d'une dénomination Code Remarque
    Private Sub ListBox12_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ListBox12.SelectedIndexChanged
        ' ==================================
        ' Sortie si sélection vide
        If ListBox12.SelectedItem = "" Then Exit Sub
        ' ==================================
        ' Arrêt du timer de temporisation de lecture pour éviter un conflit lorsque la liste se déroule et que l'on a cliqué sur une sélection
        Timer3.Enabled = False
        ' ==================================
        ' Transfert de la sélection
        If ListBox12.SelectedItem = " - Effacement du champ -" Then
            TextBox25.Text = ""
        Else
            TextBox25.Text = Strings.LTrim(ListBox12.SelectedItem)
        End If
        ' ==================================
        ' Déclenchement du retrait du sélecteur
        Timer4.Enabled = True
        ' ==================================
        ' Arrêt du timer de retrait automatique
        Timer7.Enabled = False
        ' ==================================
        ' Désélection 
        ListBox12.ClearSelected()
        ' ==================================
        ' Renvoi du select sur le bouton caché sinon Bouton Fermer valider ?
        Button2.Select()
    End Sub



    '=============================================================================================================================================
    ' PROCÉDURES DE VALIDATION DES VARIABLES DE SAUVEGARDE
    '=============================================================================================================================================
    ' Champ Méthode
    Private Sub TextBox5_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox5.TextChanged
        '        Variables.SauveInfosParam = True
        ' ==================================
        ' Validation automatique - Col 6  : Dénomination méthode
        If TextBox5.Text = Variables.TabRéfPara(Variables.AdresseTabPara, 6) Then Exit Sub
        ' ==================================
        ' Sauvegarde du Paramètre
        Variables.TabRéfPara(Variables.AdresseTabPara, 6) = TextBox5.Text
        ' ==================================
        ' Proposition sauvegarde à la fermeture
        Variables.SauveFichierMasque = True
    End Sub

    '=============================================================================================================================================
    ' Champ Fraction
    Private Sub TextBox7_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox7.TextChanged
        '        Variables.SauveInfosParam = True
        ' ==================================
        ' Validation automatique - Col 8  : Dénomination fraction
        If TextBox7.Text = Variables.TabRéfPara(Variables.AdresseTabPara, 8) Then Exit Sub
        ' ==================================
        ' Sauvegarde du Paramètre
        Variables.TabRéfPara(Variables.AdresseTabPara, 8) = TextBox7.Text
        ' ==================================
        ' Proposition sauvegarde à la fermeture
        Variables.SauveFichierMasque = True
    End Sub

    '=============================================================================================================================================
    ' Champ Support
    Private Sub TextBox9_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox9.TextChanged
        '        Variables.SauveInfosParam = True
        ' ==================================
        ' Validation automatique - Col 9  : Libellé support
        If TextBox9.Text = Variables.TabRéfPara(Variables.AdresseTabPara, 9) Then Exit Sub
        ' ==================================
        ' Sauvegarde du Paramètre
        Variables.TabRéfPara(Variables.AdresseTabPara, 9) = TextBox9.Text
        ' ==================================
        ' Proposition sauvegarde à la fermeture
        Variables.SauveFichierMasque = True
    End Sub

    '=============================================================================================================================================
    ' Champ Résultat
    Private Sub TextBox11_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox11.TextChanged



        Dim i As Integer

        '        Variables.SauveInfosParam = True                1.2   1dgdg
        ' ==================================
        ' Validation automatique - Col 17 : Résultat
        If TextBox11.Text = Variables.TabRéfPara(Variables.AdresseTabPara, 17) Then Exit Sub



        ' Suite à la création du Code Résultat Qualitatif modification pour pouvoir remettre le résultat numérique sans valeur suite à une erreur d'inscription
        If TextBox11.Text = "" Then Variables.TabRéfPara(Variables.AdresseTabPara, 17) = ""



        ' Pour régler le problème de la non mise en surimpression de la valeur précédente lorsque le paramètre est renseigné la première fois
        If TextBox11.Text = Variables.ValeurPrécédente Then Exit Sub


        ' ==================================
        ' Traitement de la vérification numérique
        If Len(TextBox11.Text) <> 0 Then
            For i = 1 To Len(TextBox11.Text)
                If Strings.Mid(TextBox11.Text, i, 1) <> "1" And Strings.Mid(TextBox11.Text, i, 1) <> "2" And Strings.Mid(TextBox11.Text, i, 1) <> "3" And Strings.Mid(TextBox11.Text, i, 1) <> "4" And Strings.Mid(TextBox11.Text, i, 1) <> "5" And Strings.Mid(TextBox11.Text, i, 1) <> "6" And Strings.Mid(TextBox11.Text, i, 1) <> "7" And Strings.Mid(TextBox11.Text, i, 1) <> "8" And Strings.Mid(TextBox11.Text, i, 1) <> "9" And Strings.Mid(TextBox11.Text, i, 1) <> "0" And Strings.Mid(TextBox11.Text, i, 1) <> "," And Strings.Mid(TextBox11.Text, i, 1) <> "." Then
                    MsgBox("Le " & i & "° caractère n'est pas valide !", MsgBoxStyle.Critical, "QUADRISPEL - Erreur de Résultat")

                    '                 TextBox11.Text = Strings.Left(TextBox11.Text, i - 1)
                    '               TextBox11.Undo()
                    '                 SendKeys.Send("{END}")

                    Exit Sub
                End If
            Next
        End If


        ' ==================================
        ' Remplacement du point par la virgule
        TextBox11.Text = Replace(TextBox11.Text, ".", ",")
        SendKeys.Send("{END}")

        ' ==================================
        ' Sauvegarde du Paramètre
        Variables.TabRéfPara(Variables.AdresseTabPara, 17) = TextBox11.Text
        ' ==================================
        ' Proposition sauvegarde à la fermeture
        Variables.SauveFichierRésultat = True

    End Sub

    '=============================================================================================================================================
    ' Champ Unité
    Private Sub TextBox12_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox12.TextChanged
        '        Variables.SauveInfosParam = True
        ' ==================================
        ' Validation automatique - Col 18 : Unité
        If TextBox12.Text = Variables.TabRéfPara(Variables.AdresseTabPara, 18) Then Exit Sub
        ' ==================================
        ' Sauvegarde du Paramètre
        Variables.TabRéfPara(Variables.AdresseTabPara, 18) = TextBox12.Text
        ' ==================================
        ' Proposition sauvegarde à la fermeture
        Variables.SauveFichierMasque = True
    End Sub

    '=============================================================================================================================================
    ' Champ Libellé Résultat Qualitatif
    Private Sub TextBox14_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox14.TextChanged
        '        Variables.SauveInfosParam = True
        ' ==================================
        ' Validation automatique - Col 15 : Libellé Résultat qualitatif
        If TextBox14.Text = Variables.TabRéfPara(Variables.AdresseTabPara, 15) Then Exit Sub
        ' ==================================
        ' Sauvegarde du Paramètre
        Variables.TabRéfPara(Variables.AdresseTabPara, 15) = TextBox14.Text
        ' ==================================
        ' Proposition sauvegarde à la fermeture
        Variables.SauveFichierMasque = True
    End Sub

    '=============================================================================================================================================
    ' Champ Engin Prélèvement
    Private Sub TextBox15_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox15.TextChanged
        '        Variables.SauveInfosParam = True
        ' ==================================
        ' Validation automatique - Col 10 : Dénomination engin de prélèvement
        If TextBox15.Text = Variables.TabRéfPara(Variables.AdresseTabPara, 10) Then Exit Sub
        ' ==================================
        ' Sauvegarde du Paramètre
        Variables.TabRéfPara(Variables.AdresseTabPara, 10) = TextBox15.Text
        ' ==================================
        ' Proposition sauvegarde à la fermeture
        Variables.SauveFichierMasque = True
    End Sub

    '=============================================================================================================================================
    ' Champ Précision
    Private Sub TextBox18_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox18.TextChanged
        '        Variables.SauveInfosParam = True
        ' ==================================
        ' Changement couleur obligation champ
        If TextBox18.Text <> "" Then
            TextBox22.BackColor = Color.FromArgb(255, 255, 200)
        Else
            TextBox22.BackColor = Color.White
        End If
        ' ==================================
        ' Validation automatique - Col 11 : Précision
        If TextBox18.Text = Variables.TabRéfPara(Variables.AdresseTabPara, 11) Then Exit Sub
        ' ==================================
        ' Sauvegarde du Paramètre
        Variables.TabRéfPara(Variables.AdresseTabPara, 11) = TextBox18.Text
        ' ==================================
        ' Proposition sauvegarde à la fermeture
        Variables.SauveFichierMasque = True
    End Sub

    '=============================================================================================================================================
    ' Champ Seuil de Détection
    Private Sub TextBox20_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox20.TextChanged
        '        Variables.SauveInfosParam = True
        ' ==================================
        ' Validation automatique - Col 14 : Seuil de détection
        If TextBox20.Text = Variables.TabRéfPara(Variables.AdresseTabPara, 14) Then Exit Sub
        ' ==================================
        ' Sauvegarde du Paramètre
        Variables.TabRéfPara(Variables.AdresseTabPara, 14) = TextBox20.Text
        ' ==================================
        ' Proposition sauvegarde à la fermeture
        Variables.SauveFichierMasque = True
    End Sub

    '=============================================================================================================================================
    ' Champ Niveau de Saisie
    Private Sub TextBox21_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox21.TextChanged
        '        Variables.SauveInfosParam = True
        ' ==================================
        ' Validation automatique - Col 13 : Niveau de saisie
        If TextBox21.Text = Variables.TabRéfPara(Variables.AdresseTabPara, 13) Then Exit Sub
        ' ==================================
        ' Sauvegarde du Paramètre
        Variables.TabRéfPara(Variables.AdresseTabPara, 13) = TextBox21.Text
        ' ==================================
        ' Proposition sauvegarde à la fermeture
        Variables.SauveFichierMasque = True
    End Sub

    '=============================================================================================================================================
    ' Champ Type Précision
    Private Sub TextBox22_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox22.TextChanged
        '        Variables.SauveInfosParam = True
        ' ==================================
        ' Validation automatique - Col 12 : Type Précision
        If TextBox22.Text = Variables.TabRéfPara(Variables.AdresseTabPara, 12) Then Exit Sub
        ' ==================================
        ' Sauvegarde du Paramètre
        Variables.TabRéfPara(Variables.AdresseTabPara, 12) = TextBox22.Text
        ' ==================================
        ' Proposition sauvegarde à la fermeture
        Variables.SauveFichierMasque = True
    End Sub

    '=============================================================================================================================================
    ' Champ Dénomination Analyseur
    Private Sub TextBox24_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox24.TextChanged
        '       Variables.SauveInfosParam = True
        ' ==================================
        ' Validation automatique - Col 4  : Dénomination Analyseur
        If TextBox24.Text = Variables.TabRéfPara(Variables.AdresseTabPara, 4) Then Exit Sub
        ' ==================================
        ' Sauvegarde des Paramètres
        Variables.TabRéfPara(Variables.AdresseTabPara, 4) = TextBox24.Text
        ' ==================================
        ' Proposition sauvegarde à la fermeture
        Variables.SauveFichierMasque = True
    End Sub

    '=============================================================================================================================================
    ' Champ Libellé Analyseur
    Private Sub TextBox10_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox10.TextChanged
        '       Variables.SauveInfosParam = True
        ' ==================================
        ' Validation automatique - Col 5  : Libellé Analyseur
        If TextBox10.Text = Variables.TabRéfPara(Variables.AdresseTabPara, 5) Then Exit Sub
        ' ==================================
        ' Sauvegarde des Paramètres
        Variables.TabRéfPara(Variables.AdresseTabPara, 5) = TextBox10.Text
        ' ==================================
        ' Proposition sauvegarde à la fermeture
        Variables.SauveFichierMasque = True
    End Sub

    '=============================================================================================================================================
    ' Champ Remarque
    Private Sub TextBox25_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox25.TextChanged
        '        Variables.SauveInfosParam = True
        ' ==================================
        ' Validation automatique - Col 20 : Remarque
        If TextBox25.Text = Variables.TabRéfPara(Variables.AdresseTabPara, 20) Then Exit Sub
        ' ==================================
        ' Sauvegarde du Paramètre
        Variables.TabRéfPara(Variables.AdresseTabPara, 20) = TextBox25.Text

        ' ==================================
        ' Suite demande modification Marine, le changement de texte entraine une demande de sauvegarde Résultat et non Masque

        ' ==================================
        ' Proposition sauvegarde à la fermeture
        '       Variables.SauveFichierMasque = True

        ' ==================================
        ' Proposition sauvegarde à la fermeture
        Variables.SauveFichierRésultat = True


    End Sub

    '=============================================================================================================================================
    ' Champ Engin Analyse
    Private Sub TextBox26_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox26.TextChanged
        '        Variables.SauveInfosParam = True
        ' ==================================
        ' Validation automatique - Col 7  : Engin d'analyse
        If TextBox26.Text = Variables.TabRéfPara(Variables.AdresseTabPara, 7) Then Exit Sub
        ' ==================================
        ' Sauvegarde du Paramètre
        Variables.TabRéfPara(Variables.AdresseTabPara, 7) = TextBox26.Text
        ' ==================================
        ' Proposition sauvegarde à la fermeture
        Variables.SauveFichierMasque = True
    End Sub

    '=============================================================================================================================================
    ' Champ Nombre Individu
    Private Sub TextBox27_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox27.TextChanged
        '        Variables.SauveInfosParam = True
        ' ==================================
        ' Validation automatique - Col 16 : Numéro individu
        If TextBox27.Text = Variables.TabRéfPara(Variables.AdresseTabPara, 16) Then Exit Sub
        ' ==================================
        ' Sauvegarde du Paramètre
        Variables.TabRéfPara(Variables.AdresseTabPara, 16) = TextBox27.Text
        ' ==================================
        ' Proposition sauvegarde à la fermeture
        Variables.SauveFichierMasque = True
    End Sub


    '=============================================================================================================================================
    ' Champ Code Résultat Qualitatif
    Private Sub TextBox33_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox33.TextChanged
        '        Variables.SauveInfosParam = True
        ' ==================================
        ' Validation automatique - Col 33 : Code Résultat Qualitatif
        If TextBox33.Text = Variables.TabRéfPara(Variables.AdresseTabPara, 23) Then Exit Sub
        ' ==================================
        ' Sauvegarde du Paramètre
        Variables.TabRéfPara(Variables.AdresseTabPara, 23) = TextBox33.Text
        ' ==================================
        ' Proposition sauvegarde à la fermeture
        Variables.SauveFichierMasque = True
    End Sub

    '=============================================================================================================================================
    ' Champ Code Groupe Taxon Résultat
    Private Sub TextBox34_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox34.TextChanged
        '        Variables.SauveInfosParam = True
        ' ==================================
        ' Validation automatique - Col 34 : Code Groupe Taxon Résultat
        If TextBox34.Text = Variables.TabRéfPara(Variables.AdresseTabPara, 24) Then Exit Sub
        ' ==================================
        ' Sauvegarde du Paramètre
        Variables.TabRéfPara(Variables.AdresseTabPara, 24) = TextBox34.Text
        ' ==================================
        ' Proposition sauvegarde à la fermeture
        Variables.SauveFichierMasque = True
    End Sub

    '=============================================================================================================================================
    ' Champ Code Taxon Résultat
    Private Sub TextBox35_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox35.TextChanged
        '        Variables.SauveInfosParam = True
        ' ==================================
        ' Validation automatique - Col 35 : Code Taxon Résultat
        If TextBox35.Text = Variables.TabRéfPara(Variables.AdresseTabPara, 25) Then Exit Sub
        ' ==================================
        ' Sauvegarde du Paramètre
        Variables.TabRéfPara(Variables.AdresseTabPara, 25) = TextBox35.Text
        ' ==================================
        ' Proposition sauvegarde à la fermeture
        Variables.SauveFichierMasque = True
    End Sub



    '=============================================================================================================================================
    ' PROCÉDURES D'INFORMATION POUR LES CHAMPS
    '=============================================================================================================================================
    ' Passage en demande d'information
    Private Sub Button15_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Button15.MouseDown
        ' ==================================
        ' Passage en curseur d'aide
        Me.Cursor = Cursors.Help
    End Sub

    '=============================================================================================================================================
    ' Traitements des champs sélectionnés
    Private Sub Button15_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Button15.MouseUp
        ' ==================================
        ' Retour au curseur normal
        Me.Cursor = Cursors.Default
        ' ==================================
        ' Repérage
        'TextBox17.Text = " X = " & e.X & "    Y = " & e.Y
        '       Exit Sub
        ' ==================================
        ' Vérification si le bouton Information est sélectionné
        If e.X > 1 And e.X < 129 And e.Y > 1 And e.Y < 24 Then
            MsgBox("- Cliquer sur le bouton " & Chr(34) & "Information" & Chr(34) & "," & vbCrLf & "- Maintenir appuyer," & vbCrLf & "- Déplacer le " & Chr(34) & "?" & Chr(34) & " sur le champ désiré,    " & vbCrLf & "- Relacher.", MsgBoxStyle.Information, "QUADRISPEL - Procédure d'utilisation")
            ' ==================================
            ' Renvoi du select sur le bouton caché
            Button2.Select()
            Exit Sub
        End If
        ' ==================================
        ' Vérification si le champ Sélection de la Famille de Paramètres est sélectionné
        If e.X > 15 And e.X < 669 And e.Y < -464 And e.Y > -485 Then
            Informations.TextBox1.Height = 40
            Informations.Button1.Top = 60
            Informations.Height = 128
            Informations.Text = " Information du champ " & Chr(171) & " Sélection de la Famille de Paramètres " & Chr(187)
            Informations.TextBox1.Text = "Le champ fait apparaître les familles de paramètres dans l'ordre de leur sélection."
            Informations.ShowDialog()
            ' ==================================
            ' Renvoi du select sur le bouton caché
            Button2.Select()
            Exit Sub
        End If
        ' ==================================
        ' Vérification si le champ Sélection d'un Paramètre de la Famille est sélectionné
        If e.X > 15 And e.X < 669 And e.Y < -394 And e.Y > -415 Then
            Informations.TextBox1.Height = 40 + 128
            Informations.Button1.Top = 60 + 128
            Informations.Height = 128 + 128
            Informations.Text = " Information du champ " & Chr(171) & " Sélection d'un Paramètre de la Famille " & Chr(187)
            Informations.TextBox1.Text = "Un paramètre est une propriété du milieu ou d'un élément du milieu qui contribue à en apprécier les caractéristiques et/ou la qualité et/ou l'aptitude à des usages." & vbCrLf & "Le paramètre se décline en deux types : quantitatif et qualitatif." & vbCrLf & "Le type quantitatif se rapporte aux paramètres qui ont une infinité de résultats." & vbCrLf & "Le type qualitatif se rapporte aux paramètres qui ne prennent qu'un nombre limité de valeurs prédéfinies pour chacun d'eux." & vbCrLf & "Ces deux types sont mutuellement exclusifs." & vbCrLf & "Un paramètre peut être taxinomique ou pas : s'il est taxinomique, il ne sera saisissable qu'en l'associant à un taxon (résultats de " & Chr(171) & " dénombrement " & Chr(187) & ")." & vbCrLf & "S'il ne l'est pas, il est associé à des résultats de mesure."
            Informations.ShowDialog()
            ' ==================================
            ' Renvoi du select sur le bouton caché
            Button2.Select()
            Exit Sub
        End If
        ' ==================================
        ' Vérification si le champ Acronyme Paramètre est sélectionné
        If e.X > 30 And e.X < 164 And e.Y < -304 And e.Y > -325 Then
            Informations.TextBox1.Height = 40
            Informations.Button1.Top = 60
            Informations.Height = 128
            Informations.Text = " Information du champ " & Chr(171) & " Acronyme Paramètre " & Chr(187)
            Informations.TextBox1.Text = "Ce champ représente l'acronyme du paramètre sélectionné."
            Informations.ShowDialog()
            ' ==================================
            ' Renvoi du select sur le bouton caché
            Button2.Select()
            Exit Sub
        End If
        ' ==================================
        ' Vérification si le champ Libellé Analyste est sélectionné
        If e.X > 210 And e.X < 814 And e.Y < -304 And e.Y > -325 Then
            Informations.TextBox1.Height = 40
            Informations.Button1.Top = 60
            Informations.Height = 128
            Informations.Text = " Information du champ " & Chr(171) & " Libellé Analyste " & Chr(187)
            Informations.TextBox1.Text = "Ce champ représente le libellé de l'organisme ou du laboratoire qui a effectué l'analyse."
            Informations.ShowDialog()
            ' ==================================
            ' Renvoi du select sur le bouton caché
            Button2.Select()
            Exit Sub
        End If
        ' ==================================
        ' Vérification si le champ Acronyme Analyste est sélectionné
        If e.X > 860 And e.X < 994 And e.Y < -304 And e.Y > -325 Then
            Informations.TextBox1.Height = 40
            Informations.Button1.Top = 60
            Informations.Height = 128
            Informations.Text = " Information du champ " & Chr(171) & " Acronyme Analyste " & Chr(187)
            Informations.TextBox1.Text = "Ce champ représente l'acronyme de l'organisme ou du laboratoire qui a effectué l'analyse."
            Informations.ShowDialog()
            ' ==================================
            ' Renvoi du select sur le bouton caché
            Button2.Select()
            Exit Sub
        End If
        ' ==================================
        ' Vérification si le champ Dénomination Méthode est sélectionné
        If e.X > 30 And e.X < 754 And e.Y < -239 And e.Y > -260 Then
            Informations.TextBox1.Height = 40 + 336
            Informations.Button1.Top = 60 + 336
            Informations.Height = 128 + 336
            Informations.Text = " Information du champ " & Chr(171) & " Dénomination Méthode " & Chr(187)
            Informations.TextBox1.Text = "Les seules méthodes reconnues par le SANDRE sont les méthodes normalisées par L'AFNOR ou les méthodes largement reconnues comme celle du type " & Chr(171) & " Rodier " & Chr(187) & " ou du " & Chr(171) & " STANDARD METHOD " & Chr(187) & "." & vbCrLf & "Les méthodes Quadrige², qu'elles soient reconnues par le SANDRE ou non, sont rassemblées dans une liste qui couvre tous les domaines pour lesquels il existe un paramètre." & vbCrLf & "La liste des méthodes est générique et porte sur toutes les phases du processus de mesure des paramètres." & vbCrLf & "Chaque méthode n'est pas non plus systématiquement spécifique à l'une de ces phases ou à une nature particulière de paramètre." & vbCrLf & "En effet, une méthode peut couvrir tout le cycle du processus et/ou être utilisable pour une phase quelle que soit la nature du paramètre." & vbCrLf & "Les méthodes peuvent être référencées par les paramètres à différentes phases de leur processus de mesure que sont :" & vbCrLf & " * Pour les paramètres chimiques et physiques : " & vbCrLf & "      - Le prélèvement et l'échantillonnage." & vbCrLf & "      - La conservation et le transport." & vbCrLf & "      - Le fractionnement." & vbCrLf & "      - L'analyse." & vbCrLf & " * Pour les paramètres environnementaux : " & vbCrLf & "      - L'observation." & vbCrLf & " * Pour les paramètres hydrobiologiques : " & vbCrLf & "      - L'ensemble du processus." & vbCrLf & " * Pour les paramètres microbiologiques : " & vbCrLf & "      - Le prélèvement, la conservation et le transport." & vbCrLf & "      - La détermination."
            Informations.ShowDialog()
            ' ==================================
            ' Renvoi du select sur le bouton caché
            Button2.Select()
            Exit Sub
        End If
        ' ==================================
        ' Vérification si le champ Dénomination Engin Analyse est sélectionné
        If e.X > 800 And e.X < 994 And e.Y < -239 And e.Y > -260 Then
            Informations.TextBox1.Height = 40
            Informations.Button1.Top = 60
            Informations.Height = 128
            Informations.Text = " Information du champ " & Chr(171) & " Dénomination Engin Analyse " & Chr(187)
            Informations.TextBox1.Text = "C'est la dénomination de l'engin ayant réalisé l'analyse."
            Informations.ShowDialog()
            ' ==================================
            ' Renvoi du select sur le bouton caché
            Button2.Select()
            Exit Sub
        End If
        ' ==================================
        ' Vérification si le champ Dénomination Fraction est sélectionné
        If e.X > 30 And e.X < 154 And e.Y < -174 And e.Y > -195 Then
            Informations.TextBox1.Height = 40 + 208
            Informations.Button1.Top = 60 + 208
            Informations.Height = 128 + 208
            Informations.Text = " Information du champ " & Chr(171) & " Dénomination Fraction " & Chr(187)
            Informations.TextBox1.Text = "Une fraction analysée est un composant du support, sur laquelle porte l'analyse." & vbCrLf & "Trois grandes catégories de fractions analysées ont été définies dans le cadre des travaux sur le dictionnaire de données national : " & vbCrLf & " - Le support brut ou entier : par exemple la fraction analysée eau brutes provenant du support " & Chr(171) & " eau " & Chr(187) & "." & vbCrLf & " - Les fractions " & Chr(171) & " partielles " & Chr(187) & ", au sens d'une classification par partie d'un même support, ex : sédiments / particules < 2 mm, particules < 63 µm, particules < 20 µm ... ou eau filtrée du support " & Chr(171) & " eau " & Chr(187) & "." & vbCrLf & " - Les fractions " & Chr(171) & " organiques " & Chr(187) & ", au sens d'une classification par partie d'un même organisme, ex : poisson / foie, écaille, reins, ... ex : palétuvier / système radiculaire, racine flottante ..." & vbCrLf & "Les fractions dites " & Chr(171) & " systématiques " & Chr(187) & ", au sens d'une classification systématique (ex : poisson : Cyprinidae / Cyprinus / Cyprinus carpio ...) ne sont pas considérées comme des fractions au sens de l'entité, mais comme une précision apportée au support." & vbCrLf & "Représentées par l'entité " & Chr(171) & " Taxon support " & Chr(187) & ", elles ne font pas partie de la liste des fractions analysées." & vbCrLf & "La liste des fractions analysées est administrée par le SANDRE qui en a la responsabilité." & vbCrLf & "Étant une liste de référence, une procédure stricte pour la création de nouvelles fractions analysées a été mise en place (cf. procédure de création d'un code SANDRE)."
            Informations.ShowDialog()
            ' ==================================
            ' Renvoi du select sur le bouton caché
            Button2.Select()
            Exit Sub
        End If
        ' ==================================
        ' Vérification si le champ Dénomination Support est sélectionné
        If e.X > 200 And e.X < 349 And e.Y < -174 And e.Y > -195 Then
            Informations.TextBox1.Height = 40 + 16
            Informations.Button1.Top = 60 + 16
            Informations.Height = 128 + 16
            Informations.Text = " Information du champ " & Chr(171) & " Dénomination Support " & Chr(187)
            Informations.TextBox1.Text = "C'est l'un des matériaux constitutifs du prélèvement, sur lequel l'analyse ou le dénombrement va être fait." & vbCrLf & "Cette notion est habituelle surtout pour les analyses de type chimique, mais elle est élargie, ici, de façon formelle à la biologie."
            Informations.ShowDialog()
            ' ==================================
            ' Renvoi du select sur le bouton caché
            Button2.Select()
            Exit Sub
        End If
        ' ==================================
        ' Vérification si le champ Dénomination Engin de Prélèvement est sélectionné
        If e.X > 395 And e.X < 629 And e.Y < -174 And e.Y > -195 Then
            Informations.TextBox1.Height = 40
            Informations.Button1.Top = 60
            Informations.Height = 128
            Informations.Text = " Information du champ " & Chr(171) & " Dénomination Engin de Prélèvement " & Chr(187)
            Informations.TextBox1.Text = "Ce sont les outils utilisés pour prélever dans le milieu le matériel qui sera analysé." & vbCrLf & "Dans ce cas, ils sont aussi appelés parfois échantillonneurs."
            Informations.ShowDialog()
            ' ==================================
            ' Renvoi du select sur le bouton caché
            Button2.Select()
            Exit Sub
        End If
        ' ==================================
        ' Vérification si le champ Précision est sélectionné
        If e.X > 675 And e.X < 754 And e.Y < -174 And e.Y > -195 Then
            Informations.TextBox1.Height = 40 + 32
            Informations.Button1.Top = 60 + 32
            Informations.Height = 128 + 32
            Informations.Text = " Information du champ " & Chr(171) & " Précision " & Chr(187)
            Informations.TextBox1.Text = "De type numérique et non obligatoire, cette valeur représente l'intervalle de confiance de la mesure." & vbCrLf & "Elle peut être exprimée en pourcentage (ex : si 10, cela signifie +/- 10 % du résultat)." & vbCrLf & "Elle peut être exprimée en valeur de l'unité du résultat (ex : si 0,5, cela signifie +/- 0,5 °C si la valeur de la température est exprimée en °C)." & vbCrLf & "Cette information est intégrée dans Q² au niveau du champ [INCERTITUDE] associée au résultat."
            Informations.ShowDialog()
            ' ==================================
            ' Renvoi du select sur le bouton caché
            Button2.Select()
            Exit Sub
        End If
        ' ==================================
        ' Vérification si le champ Type Précision est sélectionné
        If e.X > 800 And e.X < 994 And e.Y < -174 And e.Y > -195 Then
            Informations.TextBox1.Height = 40
            Informations.Button1.Top = 60
            Informations.Height = 128
            Informations.Text = " Information du champ " & Chr(171) & " Type Précision " & Chr(187)
            Informations.TextBox1.Text = "Type de précision (Pourcentage (=1) ou unité de la valeur mesurée (=2)) : nomenclature 602 du SANDRE."
            Informations.ShowDialog()
            ' ==================================
            ' Renvoi du select sur le bouton caché
            Button2.Select()
            Exit Sub
        End If
        ' ==================================
        ' Vérification si le champ Niveau de Saisie est sélectionné
        If e.X > 30 And e.X < 134 And e.Y < -109 And e.Y > -130 Then
            Informations.TextBox1.Height = 40
            Informations.Button1.Top = 60
            Informations.Height = 128
            Informations.Text = " Information du champ " & Chr(171) & " Niveau de Saisie " & Chr(187)
            Informations.TextBox1.Text = "Les résultats peuvent être associés au niveau de passage, du prélèvement ou de l'échantillon." & vbCrLf & "Cette information est précisée dans la stratégie par le responsable du réseau."
            Informations.ShowDialog()
            ' ==================================
            ' Renvoi du select sur le bouton caché
            Button2.Select()
            Exit Sub
        End If
        ' ==================================
        ' Vérification si le champ Seuil de Détection est sélectionné
        If e.X > 180 And e.X < 264 And e.Y < -109 And e.Y > -130 Then
            Informations.TextBox1.Height = 40 + 16
            Informations.Button1.Top = 60 + 16
            Informations.Height = 128 + 16
            Informations.Text = " Information du champ " & Chr(171) & " Seuil de Détection  " & Chr(187)
            Informations.TextBox1.Text = "Seuil à partir duquel la détection est réalisée."
            Informations.ShowDialog()
            ' ==================================
            ' Renvoi du select sur le bouton caché
            Button2.Select()
            Exit Sub
        End If
        ' ==================================
        ' Vérification si le champ Résultat Qualitatif est sélectionné
        If e.X > 310 And e.X < 814 And e.Y < -109 And e.Y > -130 Then
            Informations.TextBox1.Height = 40
            Informations.Button1.Top = 60
            Informations.Height = 128
            Informations.Text = " Information du champ " & Chr(171) & " Résultat Qualitatif " & Chr(187)
            Informations.TextBox1.Text = "Exemple " & Chr(171) & " Présence " & Chr(187) & " / " & Chr(171) & " Absence " & Chr(187) & "."
            Informations.ShowDialog()
            ' ==================================
            ' Renvoi du select sur le bouton caché
            Button2.Select()
            Exit Sub
        End If
        ' ==================================
        ' Vérification si le champ Numéro Individu est sélectionné
        If e.X > 860 And e.X < 994 And e.Y < -109 And e.Y > -130 Then
            Informations.TextBox1.Height = 40
            Informations.Button1.Top = 60
            Informations.Height = 128
            Informations.Text = " Information du champ " & Chr(171) & " Numéro Individu " & Chr(187)
            Informations.TextBox1.Text = "En cas de résultats sur individus (nombre d'individus renseignés), ce champ devient obligatoire."
            Informations.ShowDialog()
            ' ==================================
            ' Renvoi du select sur le bouton caché
            Button2.Select()
            Exit Sub
        End If
        ' ==================================
        ' Vérification si le champ Commentaires Mesure est sélectionné
        If e.X > 30 And e.X < 514 And e.Y < -44 And e.Y > -65 Then
            Informations.TextBox1.Height = 40
            Informations.Button1.Top = 60
            Informations.Height = 128
            Informations.Text = " Information du champ " & Chr(171) & " Commentaires Mesure " & Chr(187)
            Informations.TextBox1.Text = "Texte libre d'une longueur maximale de 2000 caractères." & vbCrLf & "Un décompteur indique le nombre de caractères restants."
            Informations.ShowDialog()
            ' ==================================
            ' Renvoi du select sur le bouton caché
            Button2.Select()
            Exit Sub
        End If
        ' ==================================
        ' Vérification si le champ Code Remarque est sélectionné
        If e.X > 560 And e.X < 729 And e.Y < -44 And e.Y > -65 Then
            Informations.TextBox1.Height = 40 + 64
            Informations.Button1.Top = 60 + 64
            Informations.Height = 128 + 64
            Informations.Text = " Information du champ " & Chr(171) & " Code Remarque " & Chr(187)
            Informations.TextBox1.Text = "Dans le Référentiel SANDRE, il s'agit de préciser si la valeur du résultat :" & vbCrLf & " - se situe dans la gamme des valeurs valides." & vbCrLf & " - si elle est inférieure au seuil de quantification." & vbCrLf & " - si elle est inférieure au seuil de détection." & vbCrLf & "Il est nécessaire de renseigner ce champ, il ne doit pas rester vide." & vbCrLf & "Cette information, dans Q², figure dans le champ [PRÉCISION] associé au Résultat."
            Informations.ShowDialog()
            ' ==================================
            ' Renvoi du select sur le bouton caché
            Button2.Select()
            Exit Sub
        End If
        ' ==================================
        ' Vérification si le champ Unité est sélectionné
        If e.X > 775 And e.X < 869 And e.Y < -44 And e.Y > -65 Then
            Informations.TextBox1.Height = 40
            Informations.Button1.Top = 60
            Informations.Height = 128
            Informations.Text = " Information du champ " & Chr(171) & " Unité " & Chr(187)
            Informations.TextBox1.Text = "C'est l'unité dans laquelle est exprimé le résultat."
            Informations.ShowDialog()
            ' ==================================
            ' Renvoi du select sur le bouton caché
            Button2.Select()
            Exit Sub
        End If
        ' ==================================
        ' Vérification si le champ Résultat est sélectionné
        If e.X > 915 And e.X < 995 And e.Y < -44 And e.Y > -65 Then
            Informations.TextBox1.Height = 40 + 32
            Informations.Button1.Top = 60 + 32
            Informations.Height = 128 + 32
            Informations.Text = " Information du champ " & Chr(171) & " Résultat " & Chr(187)
            Informations.TextBox1.Text = "C'est ce que produit la mesure sur un passage / prélèvement / échantillon, d'un paramètre." & vbCrLf & "Ce résultat peut être qualitatif ou quantitatif." & vbCrLf & "Cette valeur est obligatoirement quantitative et le séparateur de décimale est la virgule" & vbCrLf & "L'application transforme automatiquement la virgule en point."
            Informations.ShowDialog()
            ' ==================================
            ' Renvoi du select sur le bouton caché
            Button2.Select()
            Exit Sub
        End If
    End Sub


End Class