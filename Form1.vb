﻿' Importation des systèmes utilisés
Imports System
Imports System.IO
Imports System.IO.Directory
Imports System.Text
Imports System.Security.Cryptography

Public Class Form1

    '=============================================================================================================================================
    ' Procédure de fermeture par la croix
    Private Sub Form1_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        ' ==================================
        ' Blocage de la procédure de fermeture automatique
        e.Cancel = True
        ' ==================================
        ' Lancement de la procédure de fermeture
        FermetureFeuille()
    End Sub

    ' ==============================================================================================================================================
    ' Procédure au chargement de la feuille
    Private Sub Form1_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ' ==================================
        ' Disparition de la feuille Parent
        Me.Visible = False
        ' ==================================
        ' Suppression de la barre supérieure de la feuille Parent
        Me.FormBorderStyle = Windows.Forms.FormBorderStyle.None
        ' ==================================
        ' Suppression de la barre des menus de la feuille Parent
        MenuStrip1.Visible = False
        ' ==================================
        ' Prise en compte de la surface écran utilisable
        Variables.FormHauteur = My.Computer.Screen.WorkingArea.Size.Height - 1
        Variables.FormLargeur = My.Computer.Screen.WorkingArea.Size.Width
        ' ==================================
        ' Récupération du chemin vers les fichiers à charger à partir du répertoire de l'application
        Variables.Repertoire = My.Computer.FileSystem.CurrentDirectory
        ' ==================================
        ' Lancement de l'ouverture des fichiers
        OuvertureFichiers()
        ' ==================================
        ' Lancement de la procédure d'ouverture de la feuille
        OuvertureFeuille()
    End Sub

    ' ==============================================================================================================================================
    ' Procédure de chargement des fichiers de travail
    Private Sub OuvertureFichiers()
        ' ==================================
        ' Définition des variables
        Dim NomFichier As StreamReader
        Dim LectureLigne As String
        Dim Aiguilleur As Integer
        Dim i As Integer
        Dim j As Integer
        Dim k As Integer
        Dim TabMin As Integer
        Dim TabMax As Integer
        Dim NomFamille As String
        Dim DoublePrésence As Integer
        Dim NumDépartement As String = ""

        Dim IndexTabInfo As Integer

        ' ==================================
        ' Déclarations encryptage
        Dim MotACoder As String = ""
        Dim Md5 As New System.Security.Cryptography.MD5CryptoServiceProvider()
        Dim BytesInfos() As Byte = System.Text.Encoding.ASCII.GetBytes(MotACoder)
        Dim Result() As Byte = Md5.ComputeHash(BytesInfos)
        Dim ContenuPourCrypter As String = ""
        Dim CodeMD5Lu As String = ""


        ' ==================================
        ' Renvoi pour le traitement des erreurs 
        On Error GoTo TraitErreur1
        ' ==================================
        ' Positionnement de l'aiguilleur pour le traitement des erreurs 
        Aiguilleur = 1
        ' ==================================
        ' Ouverture du fichier Référentiel Paramètres QuadriSPEL
        NomFichier = New StreamReader(Variables.Repertoire & "\Référentiel Paramètres QuadriSPEL.csv", System.Text.Encoding.Default)
        ' ==================================
        ' Pointeur du tableau des informations
        j = 0
        ' ==================================
        ' Suppression de la ligne de la version
        LectureLigne = NomFichier.ReadLine

        ContenuPourCrypter = ContenuPourCrypter & LectureLigne

        LectureLigne = NomFichier.ReadLine

        ContenuPourCrypter = ContenuPourCrypter & LectureLigne

        ' ==================================
        ' Lecture du fichier Référentiel Paramètres QuadriSPEL
        Do Until NomFichier.EndOfStream = True
            ' ==================================
            ' Lecture d'une ligne
            LectureLigne = NomFichier.ReadLine

            ContenuPourCrypter = ContenuPourCrypter & LectureLigne

            ' ==================================
            ' Positionnement du pointeur sur le premier caractère de la ligne
            TabMin = 1
            ' ==================================
            ' Boucle d'affectation des informations pour les 5 colonnes
            For i = 0 To 5
                ' ==================================
                ' Recherche de la prochaine position de la tabulation
                TabMax = Strings.InStr(TabMin, LectureLigne, Chr("9"))
                ' ==================================
                ' Chargement de la colonne définie avec ses informations
                Variables.TabRéfInfo(j, i) = Strings.Mid(LectureLigne, TabMin, TabMax - TabMin)
                ' ==================================
                ' Positionnement de la prochaine recherche de tabulation
                TabMin = TabMax + 1
            Next
            ' ==================================
            ' Chargement de la dernière colonne définie avec ses informations
            Variables.TabRéfInfo(j, 6) = Strings.Right(LectureLigne, Len(LectureLigne) - TabMax)
            ' ==================================
            ' Vérification du nom de groupe d'informations et absorption des 3 lignes qui suivent
            If Variables.TabRéfInfo(j, 0) = "LIEU DE SURVEILLANCE" Then LectureLigne = NomFichier.ReadLine : ContenuPourCrypter = ContenuPourCrypter & LectureLigne : LectureLigne = NomFichier.ReadLine : ContenuPourCrypter = ContenuPourCrypter & LectureLigne : LectureLigne = NomFichier.ReadLine : ContenuPourCrypter = ContenuPourCrypter & LectureLigne
            If Variables.TabRéfInfo(j, 0) = "RÉSEAU" Then LectureLigne = NomFichier.ReadLine : ContenuPourCrypter = ContenuPourCrypter & LectureLigne : LectureLigne = NomFichier.ReadLine : ContenuPourCrypter = ContenuPourCrypter & LectureLigne : LectureLigne = NomFichier.ReadLine : ContenuPourCrypter = ContenuPourCrypter & LectureLigne
            If Variables.TabRéfInfo(j, 0) = "SAISISSEUR" Then LectureLigne = NomFichier.ReadLine : ContenuPourCrypter = ContenuPourCrypter & LectureLigne : LectureLigne = NomFichier.ReadLine : ContenuPourCrypter = ContenuPourCrypter & LectureLigne : LectureLigne = NomFichier.ReadLine : ContenuPourCrypter = ContenuPourCrypter & LectureLigne
            If Variables.TabRéfInfo(j, 0) = "UNITÉ SONDE" Then LectureLigne = NomFichier.ReadLine : ContenuPourCrypter = ContenuPourCrypter & LectureLigne : LectureLigne = NomFichier.ReadLine : ContenuPourCrypter = ContenuPourCrypter & LectureLigne : LectureLigne = NomFichier.ReadLine : ContenuPourCrypter = ContenuPourCrypter & LectureLigne
            If Variables.TabRéfInfo(j, 0) = "POSITIONNEMENT PASSAGE" Then LectureLigne = NomFichier.ReadLine : ContenuPourCrypter = ContenuPourCrypter & LectureLigne : LectureLigne = NomFichier.ReadLine : ContenuPourCrypter = ContenuPourCrypter & LectureLigne : LectureLigne = NomFichier.ReadLine : ContenuPourCrypter = ContenuPourCrypter & LectureLigne
            If Variables.TabRéfInfo(j, 0) = "ENGIN PRÉLÈVEMENT" Then LectureLigne = NomFichier.ReadLine : ContenuPourCrypter = ContenuPourCrypter & LectureLigne : LectureLigne = NomFichier.ReadLine : ContenuPourCrypter = ContenuPourCrypter & LectureLigne : LectureLigne = NomFichier.ReadLine : ContenuPourCrypter = ContenuPourCrypter & LectureLigne
            If Variables.TabRéfInfo(j, 0) = "NIVEAU PRÉLÈVEMENT" Then LectureLigne = NomFichier.ReadLine : ContenuPourCrypter = ContenuPourCrypter & LectureLigne : LectureLigne = NomFichier.ReadLine : ContenuPourCrypter = ContenuPourCrypter & LectureLigne : LectureLigne = NomFichier.ReadLine : ContenuPourCrypter = ContenuPourCrypter & LectureLigne
            If Variables.TabRéfInfo(j, 0) = "PRÉLEVEUR" Then LectureLigne = NomFichier.ReadLine : ContenuPourCrypter = ContenuPourCrypter & LectureLigne : LectureLigne = NomFichier.ReadLine : ContenuPourCrypter = ContenuPourCrypter & LectureLigne : LectureLigne = NomFichier.ReadLine : ContenuPourCrypter = ContenuPourCrypter & LectureLigne
            If Variables.TabRéfInfo(j, 0) = "UNITÉ IMMERSION" Then LectureLigne = NomFichier.ReadLine : ContenuPourCrypter = ContenuPourCrypter & LectureLigne : LectureLigne = NomFichier.ReadLine : ContenuPourCrypter = ContenuPourCrypter & LectureLigne : LectureLigne = NomFichier.ReadLine : ContenuPourCrypter = ContenuPourCrypter & LectureLigne
            If Variables.TabRéfInfo(j, 0) = "UNITÉ TAILLE PRÉLÈVEMENT" Then LectureLigne = NomFichier.ReadLine : ContenuPourCrypter = ContenuPourCrypter & LectureLigne : LectureLigne = NomFichier.ReadLine : ContenuPourCrypter = ContenuPourCrypter & LectureLigne : LectureLigne = NomFichier.ReadLine : ContenuPourCrypter = ContenuPourCrypter & LectureLigne
            If Variables.TabRéfInfo(j, 0) = "POSITIONNEMENT PRÉLÈVEMENT" Then LectureLigne = NomFichier.ReadLine : ContenuPourCrypter = ContenuPourCrypter & LectureLigne : LectureLigne = NomFichier.ReadLine : ContenuPourCrypter = ContenuPourCrypter & LectureLigne : LectureLigne = NomFichier.ReadLine : ContenuPourCrypter = ContenuPourCrypter & LectureLigne
            If Variables.TabRéfInfo(j, 0) = "SUPPORT ÉCHANTILLON" Then LectureLigne = NomFichier.ReadLine : ContenuPourCrypter = ContenuPourCrypter & LectureLigne : LectureLigne = NomFichier.ReadLine : ContenuPourCrypter = ContenuPourCrypter & LectureLigne : LectureLigne = NomFichier.ReadLine : ContenuPourCrypter = ContenuPourCrypter & LectureLigne
            If Variables.TabRéfInfo(j, 0) = "GROUPE TAXON SUPPORT ÉCHANTILLON" Then LectureLigne = NomFichier.ReadLine : ContenuPourCrypter = ContenuPourCrypter & LectureLigne : LectureLigne = NomFichier.ReadLine : ContenuPourCrypter = ContenuPourCrypter & LectureLigne : LectureLigne = NomFichier.ReadLine : ContenuPourCrypter = ContenuPourCrypter & LectureLigne
            If Variables.TabRéfInfo(j, 0) = "UNITÉ TAILLE ÉCHANTILLON" Then LectureLigne = NomFichier.ReadLine : ContenuPourCrypter = ContenuPourCrypter & LectureLigne : LectureLigne = NomFichier.ReadLine : ContenuPourCrypter = ContenuPourCrypter & LectureLigne : LectureLigne = NomFichier.ReadLine : ContenuPourCrypter = ContenuPourCrypter & LectureLigne
            If Variables.TabRéfInfo(j, 0) = "SUPPORT" Then LectureLigne = NomFichier.ReadLine : ContenuPourCrypter = ContenuPourCrypter & LectureLigne : LectureLigne = NomFichier.ReadLine : ContenuPourCrypter = ContenuPourCrypter & LectureLigne : LectureLigne = NomFichier.ReadLine : ContenuPourCrypter = ContenuPourCrypter & LectureLigne
            If Variables.TabRéfInfo(j, 0) = "FRACTION" Then LectureLigne = NomFichier.ReadLine : ContenuPourCrypter = ContenuPourCrypter & LectureLigne : LectureLigne = NomFichier.ReadLine : ContenuPourCrypter = ContenuPourCrypter & LectureLigne : LectureLigne = NomFichier.ReadLine : ContenuPourCrypter = ContenuPourCrypter & LectureLigne
            If Variables.TabRéfInfo(j, 0) = "MÉTHODE" Then LectureLigne = NomFichier.ReadLine : ContenuPourCrypter = ContenuPourCrypter & LectureLigne : LectureLigne = NomFichier.ReadLine : ContenuPourCrypter = ContenuPourCrypter & LectureLigne : LectureLigne = NomFichier.ReadLine : ContenuPourCrypter = ContenuPourCrypter & LectureLigne
            If Variables.TabRéfInfo(j, 0) = "UNITÉ" Then LectureLigne = NomFichier.ReadLine : ContenuPourCrypter = ContenuPourCrypter & LectureLigne : LectureLigne = NomFichier.ReadLine : ContenuPourCrypter = ContenuPourCrypter & LectureLigne : LectureLigne = NomFichier.ReadLine : ContenuPourCrypter = ContenuPourCrypter & LectureLigne
            If Variables.TabRéfInfo(j, 0) = "ANALYSEUR" Then LectureLigne = NomFichier.ReadLine : ContenuPourCrypter = ContenuPourCrypter & LectureLigne : LectureLigne = NomFichier.ReadLine : ContenuPourCrypter = ContenuPourCrypter & LectureLigne : LectureLigne = NomFichier.ReadLine : ContenuPourCrypter = ContenuPourCrypter & LectureLigne
            If Variables.TabRéfInfo(j, 0) = "ENGIN ANALYSE" Then LectureLigne = NomFichier.ReadLine : ContenuPourCrypter = ContenuPourCrypter & LectureLigne : LectureLigne = NomFichier.ReadLine : ContenuPourCrypter = ContenuPourCrypter & LectureLigne : LectureLigne = NomFichier.ReadLine : ContenuPourCrypter = ContenuPourCrypter & LectureLigne
            If Variables.TabRéfInfo(j, 0) = "CODE REMARQUE" Then LectureLigne = NomFichier.ReadLine : ContenuPourCrypter = ContenuPourCrypter & LectureLigne : LectureLigne = NomFichier.ReadLine : ContenuPourCrypter = ContenuPourCrypter & LectureLigne : LectureLigne = NomFichier.ReadLine : ContenuPourCrypter = ContenuPourCrypter & LectureLigne
            '           If Variables.TabRéfInfo(j, 0) = "PRÉCISION" Then LectureLigne = NomFichier.ReadLine : LectureLigne = NomFichier.ReadLine : LectureLigne = NomFichier.ReadLine
            If Variables.TabRéfInfo(j, 0) = "TYPE PRÉCISION" Then LectureLigne = NomFichier.ReadLine : ContenuPourCrypter = ContenuPourCrypter & LectureLigne : LectureLigne = NomFichier.ReadLine : ContenuPourCrypter = ContenuPourCrypter & LectureLigne : LectureLigne = NomFichier.ReadLine : ContenuPourCrypter = ContenuPourCrypter & LectureLigne
            If Variables.TabRéfInfo(j, 0) = "NIVEAU SAISIE" Then LectureLigne = NomFichier.ReadLine : ContenuPourCrypter = ContenuPourCrypter & LectureLigne : LectureLigne = NomFichier.ReadLine : ContenuPourCrypter = ContenuPourCrypter & LectureLigne : LectureLigne = NomFichier.ReadLine : ContenuPourCrypter = ContenuPourCrypter & LectureLigne
            ' ==================================
            ' Si paramètres, traitement pour le tableau paramètres
            If Variables.TabRéfInfo(j, 0) = "PARAMÈTRE" Then
                Exit Do
            End If
            ' ==================================
            ' Incrémentation de la ligne du tableau des informations
            j = j + 1
        Loop



        ' ==================================
        ' Traitement pour le tableau paramètres
        LectureLigne = NomFichier.ReadLine

        ContenuPourCrypter = ContenuPourCrypter & LectureLigne

        LectureLigne = NomFichier.ReadLine

        ContenuPourCrypter = ContenuPourCrypter & LectureLigne

        LectureLigne = NomFichier.ReadLine

        ContenuPourCrypter = ContenuPourCrypter & LectureLigne

        ' ==================================
        ' Pointeur du tableau des paramètres
        j = 0
        ' ==================================
        ' Lecture du fichier Référentiel Paramètres QuadriSPEL
        Do Until NomFichier.EndOfStream = True
            ' ==================================
            ' Lecture d'une ligne
            LectureLigne = NomFichier.ReadLine



            If Strings.Left(LectureLigne, 8) = "Sicrypto" Then

                CodeMD5Lu = Strings.Right(LectureLigne, 32)

                Exit Do

            End If

            ContenuPourCrypter = ContenuPourCrypter & LectureLigne

            ' ==================================
            ' Positionnement du pointeur sur le premier caractère de la ligne
            TabMin = 1
            ' ==================================
            ' Boucle d'affectation des informations pour les 4 colonnes
            For i = 0 To 3
                ' ==================================
                ' Recherche de la prochaine position de la tabulation
                TabMax = Strings.InStr(TabMin, LectureLigne, Chr("9"))
                ' ==================================
                ' Chargement de la colonne définie avec ses informations
                Variables.TabRéfPara(j, i) = Strings.Mid(LectureLigne, TabMin, TabMax - TabMin)
                ' ==================================
                ' Positionnement de la prochaine recherche de tabulation
                TabMin = TabMax + 1
            Next
            ' ==================================
            ' Chargement de la dernière colonne définie avec ses informations
            '           Variables.TabRéfPara(j, 4) = Strings.Right(LectureLigne, Len(LectureLigne) - TabMax)
            ' ==================================
            ' Incrémentation de la ligne du tableau des paramètres
            j = j + 1
        Loop
        ' ==================================
        ' Fermeture du fichier Référentiel Paramètres QuadriSPEL
        NomFichier.Close()

        '       i = 0
        '       MsgBox(Variables.TabRéfInfo(i, 0) & vbCrLf & Variables.TabRéfInfo(i + 1, 0) & vbCrLf & Variables.TabRéfInfo(i + 2, 0) & vbCrLf & Variables.TabRéfInfo(i + 3, 0) & vbCrLf & Variables.TabRéfInfo(i + 4, 0) & vbCrLf & Variables.TabRéfInfo(i + 5, 0) & vbCrLf & Variables.TabRéfInfo(i + 6, 0) & vbCrLf & Variables.TabRéfInfo(i + 7, 0) & vbCrLf & Variables.TabRéfInfo(i + 8, 0) & vbCrLf & Variables.TabRéfInfo(i + 9, 0) & vbCrLf & Variables.TabRéfInfo(i + 10, 0) & vbCrLf & Variables.TabRéfInfo(i + 11, 0) & vbCrLf & Variables.TabRéfInfo(i + 12, 0))

        '       i = 0
        '       MsgBox(Variables.TabRéfPara(i, 0) & vbCrLf & Variables.TabRéfPara(i + 1, 0) & vbCrLf & Variables.TabRéfPara(i + 2, 0) & vbCrLf & Variables.TabRéfPara(i + 3, 0) & vbCrLf & Variables.TabRéfPara(i + 4, 0) & vbCrLf & Variables.TabRéfPara(i + 5, 0) & vbCrLf & Variables.TabRéfPara(i + 6, 0) & vbCrLf & Variables.TabRéfPara(i + 7, 0) & vbCrLf & Variables.TabRéfPara(i + 8, 0) & vbCrLf & Variables.TabRéfPara(i + 9, 0) & vbCrLf & Variables.TabRéfPara(i + 10, 0) & vbCrLf & Variables.TabRéfPara(i + 11, 0) & vbCrLf & Variables.TabRéfPara(i + 12, 0))

        '       i = 0
        '       MsgBox(Variables.TabRéfPara(i, 0) & vbCrLf & Variables.TabRéfPara(i, 1) & vbCrLf & Variables.TabRéfPara(i, 2) & vbCrLf & Variables.TabRéfPara(i, 3) & vbCrLf & Variables.TabRéfPara(i, 4))



        ' ==================================
        ' Vérification du code encryptage calculer et celui du fichier
        ' ==================================

        BytesInfos = System.Text.Encoding.ASCII.GetBytes(ContenuPourCrypter & "JB")
        Result = Md5.ComputeHash(BytesInfos)
        MotACoder = ""
        For Each b As Byte In Result
            MotACoder += b.ToString("X2")
        Next

        If MotACoder <> CodeMD5Lu Then

            MsgBox("Le fichier Référentiel est corrompu !", MsgBoxStyle.Critical, " QUADRISPEL : Erreur de signature")

            Me.Dispose()

        End If


        ' ==================================
        ' Vérification qu'il n'y a qu'un nom de paramètre et un seul
        ' ==================================

        For i = 0 To 499
            ' ==================================
            ' Si tableau terminé, sortir
            If Variables.TabRéfPara(i, 1) = "" Then Exit For
            ' ==================================
            ' Balayage du tableau
            For j = i + 1 To 500
                ' ==================================
                ' Si tableau terminé, sortir
                If Variables.TabRéfPara(j, 1) = "" Then Exit For
                ' ==================================
                If Variables.TabRéfPara(i, 1) = Variables.TabRéfPara(j, 1) Then
                    MsgBox("Le nom du paramètre, «" & Variables.TabRéfPara(i, 1) & "», est déjà utilisé !", MsgBoxStyle.Critical, " QUADRISPEL : Erreur du fichier Référentiel Paramètres")
                    Me.Dispose()
                End If
            Next
        Next


        ' ==================================
        ' Traitement SAISISSEUR
        ' ==================================
        ' Recherche du point d'entrée dans le tableau des informations pour les Saisisseurs
        For i = 0 To 5000
            If Variables.TabRéfInfo(i, 0) = "SAISISSEUR" Then
                IndexTabInfo = i + 1
                Exit For
            End If
        Next
        ' ==================================
        ' Chargement du libellé des Saisisseurs dans la liste 3 et de la dénomination des Saisisseurs dans la liste 1
        For i = IndexTabInfo To 5000
            ' ==================================
            ' Sortie de la boucle si fin de tableau
            If Variables.TabRéfInfo(i, 0) = "" Then Exit For
            ' ==================================
            ' Rangement des désignations des Saisisseurs dans la liste
            Passage.ListBox1.Items.Add(" " & Variables.TabRéfInfo(i, 0))
            ' ==================================
            ' Affectation dans la liste du libellé unique des Saisisseurs
            Passage.ListBox3.Items.Add(" " & Variables.TabRéfInfo(i, 1))
        Next

        ' ==================================
        ' Traitement LIEU DE SURVEILLANCE
        ' ==================================
        ' Recherche du point d'entrée dans le tableau des informations pour les Lieux de Surveillance
        For i = 0 To 5000
            If Variables.TabRéfInfo(i, 0) = "LIEU DE SURVEILLANCE" Then
                IndexTabInfo = i + 1
                Exit For
            End If
        Next
        DoublePrésence = IndexTabInfo - 1
        ' ==================================
        ' Chargement des dénomination des Lieux de Surveillance dans la liste 2 et du numéro de département des Lieux de Surveillance dans la liste 4
        For i = IndexTabInfo To 5000
            ' ==================================
            ' Sortie de la boucle si fin de tableau
            If Variables.TabRéfInfo(i, 0) = "" Then Exit For
            ' ==================================
            If Variables.TabRéfInfo(i, 0) <> Variables.TabRéfInfo(DoublePrésence, 0) Then
                ' ==================================
                ' Affectation dans la liste du numéro de département des Lieux de Surveillance
                Passage.ListBox4.Items.Add("                      " & Variables.TabRéfInfo(i, 0))
                DoublePrésence = i
            End If
            ' ==================================
            ' Rangement des dénomination des Lieux de Surveillance dans la liste
            Passage.ListBox2.Items.Add(" " & Variables.TabRéfInfo(i, 1))
        Next

        ' ==================================
        ' Traitement RÉSEAU
        ' ==================================
        ' Recherche du point d'entrée dans le tableau des informations pour les Réseaux
        For i = 0 To 5000
            If Variables.TabRéfInfo(i, 0) = "RÉSEAU" Then
                IndexTabInfo = i + 1
                Exit For
            End If
        Next
        ' ==================================
        ' Chargement des dénomination des Réseaux dans la liste 5 
        For i = IndexTabInfo To 5000
            ' ==================================
            ' Sortie de la boucle si fin de tableau
            If Variables.TabRéfInfo(i, 0) = "" Then Exit For
            ' ==================================
            ' Rangement des dénomination des Réseaux
            Passage.ListBox5.Items.Add(" " & Variables.TabRéfInfo(i, 0))
            ' ==================================
            ' Reformatage du Code sandre 118, 119 et autres, au cas où il manquerait les zéros avant
            If Variables.TabRéfInfo(i, 1) = "14" Then Variables.TabRéfInfo(i, 1) = "0000000014"
            If Variables.TabRéfInfo(i, 1) = "15" Then Variables.TabRéfInfo(i, 1) = "0000000015"
            If Variables.TabRéfInfo(i, 1) = "21" Then Variables.TabRéfInfo(i, 1) = "0000000021"
            If Variables.TabRéfInfo(i, 1) = "25" Then Variables.TabRéfInfo(i, 1) = "0000000025"
            If Variables.TabRéfInfo(i, 1) = "80" Then Variables.TabRéfInfo(i, 1) = "0000000080"
            If Variables.TabRéfInfo(i, 1) = "800000024" Then Variables.TabRéfInfo(i, 1) = "0800000024"
            If Variables.TabRéfInfo(i, 1) = "800000025" Then Variables.TabRéfInfo(i, 1) = "0800000025"
            If Variables.TabRéfInfo(i, 1) = "118" Then Variables.TabRéfInfo(i, 1) = "0000000118"
            If Variables.TabRéfInfo(i, 1) = "119" Then Variables.TabRéfInfo(i, 1) = "0000000119"
        Next

        ' ==================================
        ' Traitement UNITÉ SONDE
        ' ==================================
        ' Recherche du point d'entrée dans le tableau des informations pour Unité Sonde
        For i = 0 To 5000
            If Variables.TabRéfInfo(i, 0) = "UNITÉ SONDE" Then
                IndexTabInfo = i + 1
                Exit For
            End If
        Next
        ' ==================================
        ' Chargement des dénomination Unité Sonde dans la liste 6 
        For i = IndexTabInfo To 5000
            ' ==================================
            ' Sortie de la boucle si fin de tableau
            If Variables.TabRéfInfo(i, 0) = "" Then Exit For
            ' ==================================
            ' Rangement des dénomination Unité Sonde
            Passage.ListBox6.Items.Add(" " & Variables.TabRéfInfo(i, 0))
        Next

        ' ==================================
        ' Traitement POSITIONNEMENT PASSAGE
        ' ==================================
        ' Recherche du point d'entrée dans le tableau des informations pour Positionnement Passage
        For i = 0 To 5000
            If Variables.TabRéfInfo(i, 0) = "POSITIONNEMENT PASSAGE" Then
                IndexTabInfo = i + 1
                Exit For
            End If
        Next
        ' ==================================
        ' Chargement des dénomination Positionnement Passage dans la liste 7 
        For i = IndexTabInfo To 5000
            ' ==================================
            ' Sortie de la boucle si fin de tableau
            If Variables.TabRéfInfo(i, 0) = "" Then Exit For
            ' ==================================
            ' Rangement des dénomination Positionnement Passage
            Passage.ListBox7.Items.Add(" " & Variables.TabRéfInfo(i, 0))
        Next

        ' ==================================
        ' Traitement NIVEAU PRÉLÈVEMENT
        ' ==================================
        ' Recherche du point d'entrée dans le tableau des informations pour Niveau Prélèvement
        For i = 0 To 5000
            If Variables.TabRéfInfo(i, 0) = "NIVEAU PRÉLÈVEMENT" Then
                IndexTabInfo = i + 1
                Exit For
            End If
        Next
        ' ==================================
        ' Chargement des dénomination Niveau Prélèvement dans la liste 2 
        For i = IndexTabInfo To 5000
            ' ==================================
            ' Sortie de la boucle si fin de tableau
            If Variables.TabRéfInfo(i, 0) = "" Then Exit For
            ' ==================================
            ' Rangement des dénomination Niveau Prélèvement
            Prélèvement.ListBox2.Items.Add(" " & Variables.TabRéfInfo(i, 0))
        Next

        ' ==================================
        ' Traitement PRÉLEVEUR
        ' ==================================
        ' Recherche du point d'entrée dans le tableau des informations pour les Préleveurs
        For i = 0 To 5000
            If Variables.TabRéfInfo(i, 0) = "PRÉLEVEUR" Then
                IndexTabInfo = i + 1
                Exit For
            End If
        Next
        ' ==================================
        ' Chargement du libellé des Préleveurs dans la liste 3 et de la dénomination des Préleveurs dans la liste 1
        For i = IndexTabInfo To 5000
            ' ==================================
            ' Sortie de la boucle si fin de tableau
            If Variables.TabRéfInfo(i, 0) = "" Then Exit For
            ' ==================================
            ' Rangement des désignations des Préleveurs dans la liste 1
            Prélèvement.ListBox1.Items.Add(" " & Variables.TabRéfInfo(i, 0))
            ' ==================================
            ' Affectation dans la liste du libellé unique des Préleveurs dans la liste 3
            Prélèvement.ListBox3.Items.Add(" " & Variables.TabRéfInfo(i, 1))
        Next

        ' ==================================
        ' Traitement UNITÉ IMMERSION
        ' ==================================
        ' Recherche du point d'entrée dans le tableau des informations pour Unité Immersion
        For i = 0 To 5000
            If Variables.TabRéfInfo(i, 0) = "UNITÉ IMMERSION" Then
                IndexTabInfo = i + 1
                Exit For
            End If
        Next
        ' ==================================
        ' Chargement des dénomination Unité Immersion dans la liste 4 
        For i = IndexTabInfo To 5000
            ' ==================================
            ' Sortie de la boucle si fin de tableau
            If Variables.TabRéfInfo(i, 0) = "" Then Exit For
            ' ==================================
            ' Rangement des dénomination Unité Immersion
            Prélèvement.ListBox4.Items.Add(" " & Variables.TabRéfInfo(i, 0))
        Next

        ' ==================================
        ' Traitement UNITÉ TAILLE PRÉLÈVEMENT
        ' ==================================
        ' Recherche du point d'entrée dans le tableau des informations pour Unité Taille Prélèvement
        For i = 0 To 5000
            If Variables.TabRéfInfo(i, 0) = "UNITÉ TAILLE PRÉLÈVEMENT" Then
                IndexTabInfo = i + 1
                Exit For
            End If
        Next
        ' ==================================
        ' Chargement des dénomination Unité Taille Prélèvement dans la liste 5 
        For i = IndexTabInfo To 5000
            ' ==================================
            ' Sortie de la boucle si fin de tableau
            If Variables.TabRéfInfo(i, 0) = "" Then Exit For
            ' ==================================
            ' Rangement des dénomination Unité Taille Prélèvement
            Prélèvement.ListBox5.Items.Add(" " & Variables.TabRéfInfo(i, 0))
        Next

        ' ==================================
        ' Traitement POSITIONNEMENT PRÉLÈVEMENT
        ' ==================================
        ' Recherche du point d'entrée dans le tableau des informations pour Positionnement Prélèvement
        For i = 0 To 5000
            If Variables.TabRéfInfo(i, 0) = "POSITIONNEMENT PRÉLÈVEMENT" Then
                IndexTabInfo = i + 1
                Exit For
            End If
        Next
        ' ==================================
        ' Chargement des dénomination Positionnement Prélèvement dans la liste 6 
        For i = IndexTabInfo To 5000
            ' ==================================
            ' Sortie de la boucle si fin de tableau
            If Variables.TabRéfInfo(i, 0) = "" Then Exit For
            ' ==================================
            ' Rangement des dénomination Positionnement Prélèvement
            Prélèvement.ListBox6.Items.Add(" " & Variables.TabRéfInfo(i, 0))
        Next

        ' ==================================
        ' Traitement SUPPORT ÉCHANTILLON
        ' ==================================
        ' Recherche du point d'entrée dans le tableau des informations pour Support Échantillon
        For i = 0 To 5000
            If Variables.TabRéfInfo(i, 0) = "SUPPORT ÉCHANTILLON" Then
                IndexTabInfo = i + 1
                Exit For
            End If
        Next
        ' ==================================
        ' Chargement des dénomination Support Échantillon dans la liste 1
        For i = IndexTabInfo To 5000
            ' ==================================
            ' Sortie de la boucle si fin de tableau
            If Variables.TabRéfInfo(i, 0) = "" Then Exit For
            ' ==================================
            ' Rangement des dénomination Support Échantillon
            Échantillon.ListBox1.Items.Add(" " & Variables.TabRéfInfo(i, 0))
        Next

        ' ==================================
        ' Traitement GROUPE TAXON SUPPORT ÉCHANTILLON
        ' ==================================
        ' Recherche du point d'entrée dans le tableau des informations pour Groupe Taxon Support Échantillon
        For i = 0 To 5000
            If Variables.TabRéfInfo(i, 0) = "GROUPE TAXON SUPPORT ÉCHANTILLON" Then
                IndexTabInfo = i + 1
                Exit For
            End If
        Next
        ' ==================================
        ' Chargement des dénomination Groupe Taxon Support Échantillon dans la liste 2
        For i = IndexTabInfo To 5000
            ' ==================================
            ' Sortie de la boucle si fin de tableau
            If Variables.TabRéfInfo(i, 0) = "" Then Exit For
            ' ==================================
            ' Rangement des dénomination Groupe Taxon Support Échantillon
            Échantillon.ListBox2.Items.Add(" " & Variables.TabRéfInfo(i, 0))
        Next

        ' ==================================
        ' Traitement UNITÉ TAILLE ÉCHANTILLON
        ' ==================================
        ' Recherche du point d'entrée dans le tableau des informations pour Unité Taille Échantillon
        For i = 0 To 5000
            If Variables.TabRéfInfo(i, 0) = "UNITÉ TAILLE ÉCHANTILLON" Then
                IndexTabInfo = i + 1
                Exit For
            End If
        Next
        ' ==================================
        ' Chargement des dénomination Unité Taille Échantillon dans la liste 3
        For i = IndexTabInfo To 5000
            ' ==================================
            ' Sortie de la boucle si fin de tableau
            If Variables.TabRéfInfo(i, 0) = "" Then Exit For
            ' ==================================
            ' Rangement des dénomination Unité Taille Échantillon
            Échantillon.ListBox3.Items.Add(" " & Variables.TabRéfInfo(i, 0))
        Next

        ' ==================================
        ' Traitement MÉTHODE
        ' ==================================
        ' Recherche du point d'entrée dans le tableau des informations pour Méthode
        For i = 0 To 5000
            If Variables.TabRéfInfo(i, 0) = "MÉTHODE" Then
                IndexTabInfo = i + 1
                Exit For
            End If
        Next
        ' ==================================
        ' Chargement des dénomination Méthode dans la liste 1
        For i = IndexTabInfo To 5000
            ' ==================================
            ' Sortie de la boucle si fin de tableau
            If Variables.TabRéfInfo(i, 0) = "" Then Exit For
            ' ==================================
            ' Rangement des dénomination Méthode
            Propriétés_Param.ListBox1.Items.Add(" " & Variables.TabRéfInfo(i, 0))
        Next

        ' ==================================
        ' Traitement ANALYSEUR
        ' ==================================
        ' Recherche du point d'entrée dans le tableau des informations pour les Analyseurs
        For i = 0 To 5000
            If Variables.TabRéfInfo(i, 0) = "ANALYSEUR" Then
                IndexTabInfo = i + 1
                Exit For
            End If
        Next
        ' ==================================
        ' Chargement du libellé des Analyseurs dans la liste 4 et de la dénomination des Analyseurs dans la liste 2
        For i = IndexTabInfo To 5000
            ' ==================================
            ' Sortie de la boucle si fin de tableau
            If Variables.TabRéfInfo(i, 0) = "" Then Exit For
            ' ==================================
            ' Rangement des désignations des Analyseurs dans la liste 2
            Propriétés_Param.ListBox2.Items.Add(" " & Variables.TabRéfInfo(i, 0))
            ' ==================================
            ' Affectation dans la liste du libellé unique des Analyseurs dans la liste 4
            Propriétés_Param.ListBox4.Items.Add(" " & Variables.TabRéfInfo(i, 1))
        Next

        ' ==================================
        ' Traitement ENGIN ANALYSE
        ' ==================================
        ' Recherche du point d'entrée dans le tableau des informations pour Engin Analyse
        For i = 0 To 5000
            If Variables.TabRéfInfo(i, 0) = "ENGIN ANALYSE" Then
                IndexTabInfo = i + 1
                Exit For
            End If
        Next
        ' ==================================
        ' Chargement des dénomination Engin Analyse dans la liste 3
        For i = IndexTabInfo To 5000
            ' ==================================
            ' Sortie de la boucle si fin de tableau
            If Variables.TabRéfInfo(i, 0) = "" Then Exit For
            ' ==================================
            ' Rangement des dénomination Engin Analyse
            Propriétés_Param.ListBox3.Items.Add(" " & Variables.TabRéfInfo(i, 0))
        Next

        ' ==================================
        ' Traitement FRACTION
        ' ==================================
        ' Recherche du point d'entrée dans le tableau des informations pour Fraction
        For i = 0 To 5000
            If Variables.TabRéfInfo(i, 0) = "FRACTION" Then
                IndexTabInfo = i + 1
                Exit For
            End If
        Next
        ' ==================================
        ' Chargement des dénomination Fraction dans la liste 5
        For i = IndexTabInfo To 5000
            ' ==================================
            ' Sortie de la boucle si fin de tableau
            If Variables.TabRéfInfo(i, 0) = "" Then Exit For
            ' ==================================
            ' Rangement des dénomination Fraction
            Propriétés_Param.ListBox5.Items.Add(" " & Variables.TabRéfInfo(i, 0))
        Next

        ' ==================================
        ' Traitement SUPPORT
        ' ==================================
        ' Recherche du point d'entrée dans le tableau des informations pour Support
        For i = 0 To 5000
            If Variables.TabRéfInfo(i, 0) = "SUPPORT" Then
                IndexTabInfo = i + 1
                Exit For
            End If
        Next
        ' ==================================
        ' Chargement des dénomination Support dans la liste 6
        For i = IndexTabInfo To 5000
            ' ==================================
            ' Sortie de la boucle si fin de tableau
            If Variables.TabRéfInfo(i, 0) = "" Then Exit For
            ' ==================================
            ' Rangement des dénomination Support
            Propriétés_Param.ListBox6.Items.Add(" " & Variables.TabRéfInfo(i, 0))
        Next

        ' ==================================
        ' Traitement ENGIN DE PRÉLÈVEMENT
        ' ==================================
        ' Recherche du point d'entrée dans le tableau des informations pour Engin de Prélèvement
        For i = 0 To 5000
            If Variables.TabRéfInfo(i, 0) = "ENGIN PRÉLÈVEMENT" Then
                IndexTabInfo = i + 1
                Exit For
            End If
        Next
        ' ==================================
        ' Chargement des dénomination Engin de Prélèvement dans la liste 7
        For i = IndexTabInfo To 5000
            ' ==================================
            ' Sortie de la boucle si fin de tableau
            If Variables.TabRéfInfo(i, 0) = "" Then Exit For
            ' ==================================
            ' Rangement des dénomination Engin de Prélèvement
            Propriétés_Param.ListBox7.Items.Add(" " & Variables.TabRéfInfo(i, 0))
        Next

        ' ==================================
        ' Traitement CODE REMARQUE
        ' ==================================
        ' Recherche du point d'entrée dans le tableau des informations pour Code Remarque
        For i = 0 To 5000
            If Variables.TabRéfInfo(i, 0) = "CODE REMARQUE" Then
                IndexTabInfo = i + 1
                Exit For
            End If
        Next
        ' ==================================
        ' Chargement des dénomination Code Remarque dans la liste 12
        For i = IndexTabInfo To 5000
            ' ==================================
            ' Sortie de la boucle si fin de tableau
            If Variables.TabRéfInfo(i, 0) = "" Then Exit For
            ' ==================================
            ' Rangement des dénomination Code Remarque
            Propriétés_Param.ListBox12.Items.Add(" " & Variables.TabRéfInfo(i, 0))
        Next

        ' ==================================
        ' Traitement PRÉCISION
        ' ==================================
        ' Recherche du point d'entrée dans le tableau des informations pour Précision
        '        For i = 0 To 5000
        '        If Variables.TabRéfInfo(i, 0) = "PRÉCISION" Then
        '        IndexTabInfo = i + 1
        '        Exit For
        '        End If
        '        Next
        ' ==================================
        ' Chargement des dénomination Précision dans la liste 8
        '        For i = IndexTabInfo To 5000
        ' ==================================
        ' Sortie de la boucle si fin de tableau
        '        If Variables.TabRéfInfo(i, 0) = "" Then Exit For
        ' ==================================
        ' Rangement des dénomination Précision
        '        Propriétés_Param.ListBox8.Items.Add(" " & Variables.TabRéfInfo(i, 0))
        '        Next

        ' ==================================
        ' Traitement TYPE PRÉCISION
        ' ==================================
        ' Recherche du point d'entrée dans le tableau des informations pour Type Précision
        For i = 0 To 5000
            If Variables.TabRéfInfo(i, 0) = "TYPE PRÉCISION" Then
                IndexTabInfo = i + 1
                Exit For
            End If
        Next
        ' ==================================
        ' Chargement des dénomination Type Précision dans la liste 9
        For i = IndexTabInfo To 5000
            ' ==================================
            ' Sortie de la boucle si fin de tableau
            If Variables.TabRéfInfo(i, 0) = "" Then Exit For
            ' ==================================
            ' Rangement des dénomination Type Précision
            Propriétés_Param.ListBox9.Items.Add(" " & Variables.TabRéfInfo(i, 0))
        Next

        ' ==================================
        ' Traitement NIVEAU SAISIE
        ' ==================================
        ' Recherche du point d'entrée dans le tableau des informations pour Niveau Saisie
        For i = 0 To 5000
            If Variables.TabRéfInfo(i, 0) = "NIVEAU SAISIE" Then
                IndexTabInfo = i + 1
                Exit For
            End If
        Next
        ' ==================================
        ' Chargement des dénomination Niveau Saisie dans la liste 9
        For i = IndexTabInfo To 5000
            ' ==================================
            ' Sortie de la boucle si fin de tableau
            If Variables.TabRéfInfo(i, 0) = "" Then Exit For
            ' ==================================
            ' Rangement des dénomination Niveau Saisie
            Propriétés_Param.ListBox10.Items.Add(" " & Variables.TabRéfInfo(i, 0))
        Next

        ' ==================================
        ' Traitement UNITÉ
        ' ==================================
        ' Recherche du point d'entrée dans le tableau des informations pour Unité
        For i = 0 To 5000
            If Variables.TabRéfInfo(i, 0) = "UNITÉ" Then
                IndexTabInfo = i + 1
                Exit For
            End If
        Next
        ' ==================================
        ' Chargement des dénomination Unité dans la liste 9
        For i = IndexTabInfo To 5000
            ' ==================================
            ' Sortie de la boucle si fin de tableau
            If Variables.TabRéfInfo(i, 0) = "" Then Exit For
            ' ==================================
            ' Rangement des dénomination Unité
            Propriétés_Param.ListBox11.Items.Add(" " & Variables.TabRéfInfo(i, 0))
        Next


        ' ==================================
        ' Traitement PARAMÈTRE
        ' ==================================
        ' Chargement des Familles de Paramètres dans la liste 1
        DoublePrésence = 500
        ' ==================================
        For i = 0 To 500
            ' ==================================
            ' Vérification si le nom de Famille a déjà été transféré
            If Variables.TabRéfPara(i, 0) <> Variables.TabRéfPara(DoublePrésence, 0) And Variables.TabRéfPara(i, 0) <> "" Then
                ' ==================================
                ' Affectation dans la liste du numéro de la Famille de Paramètres
                Familles.ListBox1.Items.Add(" " & Variables.TabRéfPara(i, 0))
                DoublePrésence = i
            End If
        Next



        ' ==================================
        ' Positionnement de l'aiguilleur pour le traitement des erreurs 
        Aiguilleur = 2
        ' ==================================
        ' Ouverture du Fichier SPEL Configuration
        NomFichier = New StreamReader(Variables.Repertoire & "\Fichier SPEL - Configuration.cfg", System.Text.Encoding.Default)
        ' ==================================
        SPEL.ListBox2.Items.Clear()
        ' ==================================
        ' Lecture du fichier Référentiel Paramètres QuadriSPEL
        Do Until NomFichier.EndOfStream = True
            ' ==================================
            ' Lecture d'une ligne
            SPEL.ListBox2.Items.Add(NomFichier.ReadLine)
        Loop
        ' ==================================
        ' Fermeture du fichier
        NomFichier.Close()





        ' ====================================================================================
        ' Chargement des photos des correspondants 1 & 2
SuiteCorres1:
        Dim Photo As FileStream
        Aiguilleur = 601
        Photo = New FileStream(Variables.Repertoire & "\Correspondant 1.png", FileMode.Open)
        Correspondants.PictureBox15.Image = Image.FromStream(Photo)
        Photo.Close()
SuiteCorres2:
        Aiguilleur = 602
        Photo = New FileStream(Variables.Repertoire & "\Correspondant 2.png", FileMode.Open)
        Correspondants.PictureBox1.Image = Image.FromStream(Photo)
        Photo.Close()


        ' ====================================================================================
        ' Chargement des informations des correspondants 1 & 2
TexteCorrespondants:
        ' ==================================
        ' Positionnement de l'aiguilleur pour le traitement des erreurs 
        Aiguilleur = 603
        ' ==================================
        ' Ouverture du fichier Référentiel Paramètres QuadriSPEL
        NomFichier = New StreamReader(Variables.Repertoire & "\Correspondants.txt", System.Text.Encoding.Default)
        ' ==================================
        ' Lecture du fichier Correspondants.txt
        Do Until NomFichier.EndOfStream = True
            ' ==================================
            ' Lecture d'une ligne
            Correspondants.TextBox15.Text = NomFichier.ReadLine
            ' ==================================
            ' Lecture d'une ligne
            Correspondants.TextBox16.Text = NomFichier.ReadLine
            ' ==================================
            ' Lecture d'une ligne
            Correspondants.TextBox17.Text = NomFichier.ReadLine
            ' ==================================
            ' Lecture d'une ligne
            Correspondants.TextBox18.Text = NomFichier.ReadLine
            ' ==================================
            ' Lecture d'une ligne
            Correspondants.TextBox19.Text = NomFichier.ReadLine
            ' ==================================
            ' Lecture d'une ligne
            Correspondants.TextBox21.Text = NomFichier.ReadLine
            ' ==================================
            ' Lecture d'une ligne
            Correspondants.TextBox20.Text = NomFichier.ReadLine
            ' ==================================
            ' Lecture d'une ligne
            Correspondants.TextBox7.Text = NomFichier.ReadLine
            ' ==================================
            ' Lecture d'une ligne
            Correspondants.TextBox6.Text = NomFichier.ReadLine
            ' ==================================
            ' Lecture d'une ligne
            Correspondants.TextBox5.Text = NomFichier.ReadLine
            ' ==================================
            ' Lecture d'une ligne
            Correspondants.TextBox4.Text = NomFichier.ReadLine
            ' ==================================
            ' Lecture d'une ligne
            Correspondants.TextBox3.Text = NomFichier.ReadLine
            ' ==================================
            ' Lecture d'une ligne
            Correspondants.TextBox1.Text = NomFichier.ReadLine
            ' ==================================
            ' Lecture d'une ligne
            Correspondants.TextBox2.Text = NomFichier.ReadLine
            ' ==================================
            ' Lecture d'une ligne
            '        Variables.AdresseCourrielleur = NomFichier.ReadLine
        Loop
        ' ==================================
        ' Fermeture du fichier
        NomFichier.Close()



        Exit Sub

TraitErreur1:
        ' ==================================
        ' Traitement si l'erreur est l'absence du fichier Référentiel Paramètres
        If Err.Number = 53 And Aiguilleur = 1 Then
            MsgBox("Pas de fichier Référentiel Paramètres ! ", MsgBoxStyle.Critical, " QUADRISPEL - Erreur")
            FermetureFeuille()
            Exit Sub
        End If
        ' ==================================
        ' Traitement si l'erreur est l'utilisation du fichier Référentiel Paramètres par une autre application
        If Err.Number = 57 And Aiguilleur = 1 Then
            MsgBox("Le fichier Référentiel Paramètres est utilisé ! ", MsgBoxStyle.Critical, " QUADRISPEL - Erreur")
            FermetureFeuille()
            Exit Sub
        End If

        ' ==================================
        ' Traitement si l'erreur est l'absence du fichier Fichier SPEL - Configuration
        If Aiguilleur = 2 Then
            Resume SuiteCorres1
        End If

        ' ==================================
        ' Traitement si l'erreur est l'absence de la photo correspondant 1
        If Aiguilleur = 601 Then
            Resume SuiteCorres2
        End If

        ' ==================================
        ' Traitement si l'erreur est l'absence de la photo correspondant 2
        If Aiguilleur = 602 Then
            Resume TexteCorrespondants
        End If

        ' ==================================
        ' Traitement si l'erreur est l'absence du fichier texte des correspondants
        If Aiguilleur = 603 Then
            Exit Sub
        End If



        ' ==================================
        ' Traitement si l'erreur inconnue
        MsgBox("Numéro d'erreur =" & Err.Number & "    Signification = " & ErrorToString(), MsgBoxStyle.Critical, " QUADRISPEL - Erreur")
    End Sub

    ' ==============================================================================================================================================
    ' Procédure de fermeture de l'application
    Private Sub QuitterToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles QuitterToolStripMenuItem.Click
        ' ==================================
        ' Lancement de la procédure de fermeture
        FermetureFeuille()
    End Sub

    '=============================================================================================================================================
    ' Traitement du repositionnement automatique de la feuille quand on l'a déplacée
    Private Sub Form1_MouseEnter(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.MouseEnter
        Me.CenterToScreen()
    End Sub

    Private Sub DataGridView1_MouseEnter(ByVal sender As Object, ByVal e As System.EventArgs) Handles DataGridView1.MouseEnter
        Me.CenterToScreen()
    End Sub

    '=============================================================================================================================================
    ' APPARITION DES FEUILLES DU MENU
    '=============================================================================================================================================
    ' Création d'un nouveau masque
    Private Sub NouveauMasqueToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NouveauMasqueToolStripMenuItem.Click
        Dim i As Integer
        Dim j As Integer
        ' ==================================
        ' Vérification d'une demande de sauvegarde d'informations Masque
        If Variables.SauveFichierMasque = True And Variables.TypeFichierChargé = "" Then
            i = MsgBox(" Des informations de fichier Masque ont été réalisées ! " & vbCrLf & vbCrLf & "                        Sauvegarder ?", MsgBoxStyle.Exclamation + MsgBoxStyle.YesNo + MsgBoxStyle.DefaultButton1, " QUADRISPEL - Proposition de sauvegarde")
            If i = 6 Then
                ' ==================================
                ' Affichage de la Feuille
                Sauve_Masque.ShowDialog()
                '                Exit Sub
            End If
        End If
        ' ==================================
        ' Vérification d'une demande de sauvegarde d'informations Masque
        If Variables.SauveFichierMasque = True And Variables.TypeFichierChargé = "Masque" Then
            i = MsgBox(" Des informations du fichier Masque ont été modifiées ! " & vbCrLf & vbCrLf & "                        Sauvegarder ?", MsgBoxStyle.Exclamation + MsgBoxStyle.YesNo + MsgBoxStyle.DefaultButton1, " QUADRISPEL - Proposition de sauvegarde")
            If i = 6 Then
                ' ==================================
                ' Affichage de la Feuille
                Sauve_Masque.ShowDialog()
                '                Exit Sub
            End If
        End If
        ' ==================================
        ' Vérification d'une demande de sauvegarde d'informations Résultat
        If Variables.SauveFichierRésultat = True And Variables.TypeFichierChargé = "Masque" Then
            i = MsgBox(" Des informations Résultats sont à sauvegarder ! " & vbCrLf & vbCrLf & "                    Sauvegarder ?", MsgBoxStyle.Exclamation + MsgBoxStyle.YesNo + MsgBoxStyle.DefaultButton1, " QUADRISPEL - Proposition de sauvegarde")
            If i = 6 Then
                ' ==================================
                ' Affichage de la Feuille
                Sauve_Résultat.ShowDialog()
                '                Exit Sub
            End If
        End If
        ' ==================================
        ' Vérification d'une demande de sauvegarde d'informations Résultat
        If Variables.SauveFichierRésultat = True And Variables.TypeFichierChargé = "Résultat" Then
            i = MsgBox(" Des informations du fichier Résultats ont été modifiées ! " & vbCrLf & vbCrLf & "                         Sauvegarder ?", MsgBoxStyle.Exclamation + MsgBoxStyle.YesNo + MsgBoxStyle.DefaultButton1, " QUADRISPEL - Proposition de sauvegarde")
            If i = 6 Then
                ' ==================================
                ' Affichage de la Feuille
                Sauve_Résultat.ShowDialog()
                '                Exit Sub
            End If
        End If
        ' ==================================
        ' Vérification d'une demande de sauvegarde d'informations Masque
        If Variables.SauveFichierMasque = True And Variables.TypeFichierChargé = "Résultat" Then
            i = MsgBox(" Des informations Masques correspondantes au fichier Masque ont été modifiées ! " & vbCrLf & vbCrLf & "                                             Sauvegarder ?", MsgBoxStyle.Exclamation + MsgBoxStyle.YesNo + MsgBoxStyle.DefaultButton1, " QUADRISPEL - Proposition de sauvegarde")
            If i = 6 Then
                ' ==================================
                ' Affichage de la Feuille
                Sauve_Masque.ShowDialog()
                '                Exit Sub
            End If
        End If


        ' ==================================
        ' Effacement des informations Passage
        Passage.TextBox10.Text = ""
        Passage.TextBox24.Text = ""
        Passage.TextBox3.Text = ""
        Passage.TextBox2.Text = ""
        Passage.TextBox1.Text = ""
        Passage.TextBox5.Text = ""
        Passage.TextBox8.Text = ""
        Passage.TextBox23.Text = ""
        Passage.TextBox7.Text = ""
        Passage.TextBox9.Text = ""
        Passage.TextBox11.Text = ""
        Passage.TextBox12.Text = ""
        Passage.TextBox14.Text = ""
        Passage.TextBox15.Text = ""
        Passage.TextBox17.Text = ""
        Passage.TextBox18.Text = ""
        Passage.TextBox19.Text = ""
        Passage.TextBox20.Text = ""
        ' ==================================
        ' Effacement des informations Prélèvement
        Prélèvement.TextBox1.Text = ""
        Prélèvement.TextBox2.Text = ""
        Prélèvement.TextBox8.Text = ""
        Prélèvement.TextBox23.Text = ""
        Prélèvement.TextBox3.Text = ""
        Prélèvement.TextBox5.Text = ""
        Prélèvement.TextBox6.Text = ""
        Prélèvement.TextBox7.Text = ""
        Prélèvement.TextBox9.Text = ""
        Prélèvement.TextBox10.Text = ""
        Prélèvement.TextBox11.Text = ""
        Prélèvement.TextBox12.Text = ""
        Prélèvement.TextBox14.Text = ""
        Prélèvement.TextBox15.Text = ""
        Prélèvement.TextBox16.Text = ""
        Prélèvement.TextBox17.Text = ""
        ' ==================================
        ' Effacement des informations Échantillon
        Échantillon.TextBox1.Text = ""
        Échantillon.TextBox2.Text = ""
        Échantillon.TextBox3.Text = ""
        Échantillon.TextBox4.Text = ""
        Échantillon.TextBox5.Text = ""
        Échantillon.TextBox6.Text = ""
        Échantillon.TextBox7.Text = ""
        Échantillon.TextBox8.Text = ""
        Échantillon.TextBox9.Text = ""
        ' ==================================
        ' Effacement de la Liste 2 des familles sélectionnées
        Familles.ListBox2.Items.Clear()
        ' ==================================
        ' Effacement des informations des Paramètres du Tableau des Paramètres
        For i = 0 To 500
            For j = 4 To 20
                Variables.TabRéfPara(i, j) = ""
            Next
            Variables.TabRéfPara(i, 21) = 0

            ' Modifications pour V 1.7 Ifremer

            Variables.TabRéfPara(i, 22) = ""
            Variables.TabRéfPara(i, 23) = ""
            Variables.TabRéfPara(i, 24) = ""
            Variables.TabRéfPara(i, 25) = ""

        Next
        ' ==================================
        ' Effacement du nom du fichier de sélection chargé
        Charge_Masque.TextBox1.Text = ""
        ' ==================================
        ' Effacement du nom du fichier pour le nom du fichier Masque
        Sauve_Masque.TextBox1.Text = ""
        ' ==================================
        ' Effacement du nom du fichier de sélection chargé
        Charge_Résultat.TextBox1.Text = ""
        ' ==================================
        ' Effacement du nom du fichier pour le nom du fichier Résultat
        Sauve_Résultat.TextBox1.Text = ""
        ' ==================================
        ' Effacement du nom du fichier pour le nom du fichier Ifremer
        Sauve_Ifremer.TextBox1.Text = ""
        ' ==================================
        ' Effacement du nom du fichier de sélection chargé
        Variables.NomSelect = ""
        ' ==================================
        ' Invalidation sauvegarde à la fermeture
        Variables.SauveFichierMasque = False
        ' ==================================
        ' Invalidation sauvegarde à la fermeture
        Variables.SauveFichierRésultat = False

        ' ==================================
        ' Indication du type de fichier chargé
        Variables.TypeFichierChargé = ""

        ' ==================================
        ' Effacement des lignes du DataGridView
        DataGridView1.Rows.Clear()

    End Sub

    '=============================================================================================================================================
    ' Règles Structurelles
    Private Sub RèglesStructurellesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RèglesStructurellesToolStripMenuItem.Click
        ' ==================================
        ' Vérification des règles structurelles
        AnalyseRèglesStructurelles()

        ' ==================================
        ' Sortie si pas de paramètre à sauvegarder
        If Variables.PrésenceParamètre = False Then Exit Sub

        ' ==================================
        ' Message d'information de validation des Règles Structurelles
        If Variables.ArretSauve = False Then
            '            MsgBox(" Pas d'invalidation de Règles Structurelles !", MsgBoxStyle.Exclamation, " QUADRISPEL - Validation")
            '           Exit Sub
            ' ==================================
            ' Effacement des lignes du DataGridView et affectation de l'information de validation
            RèglesStructurelles.DataGridView1.Rows.Clear()
            RèglesStructurelles.DataGridView1.Rows.Add()
            RèglesStructurelles.DataGridView1.Item(0, 0).Value = "Pas d'invalidation de Règles Structurelles !"
            Variables.TabRèglesInv(0) = "Pas d'invalidation de Règles Structurelles !"
        End If
        ' ==================================
        ' Affichage de la fenêtre des Règles Structurelles non valides
        RèglesStructurelles.ShowDialog()
    End Sub

    '=============================================================================================================================================
    ' Fichier Sauvegarde QuadriSPEL
    Private Sub SauvegardeToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SauvegardeToolStripMenuItem.Click

        ' Dim ArretSauve As Boolean = False
        ' ==================================
        ' Colorisation en rose des cellules obligatoires non renseignées
        '        For ii = 0 To DataGridView1.RowCount - 1

        '        NomColonne = "ColRéseau" : If DataGridView1.Item(NomColonne, ii).Value = "" Then DataGridView1.Item(NomColonne, ii).Style.BackColor = Color.Pink
        '        NomColonne = "ColSaissi" : If DataGridView1.Item(NomColonne, ii).Value = "" Then DataGridView1.Item(NomColonne, ii).Style.BackColor = Color.Pink
        '        NomColonne = "ColDatePassage" : If DataGridView1.Item(NomColonne, ii).Value = "" Then DataGridView1.Item(NomColonne, ii).Style.BackColor = Color.Pink

        '        If DataGridView1.Item("ColLatitudePass", ii).Value <> "" Or DataGridView1.Item("ColLongitudePass", ii).Value <> "" Then
        '        NomColonne = "ColPosition" : If DataGridView1.Item(NomColonne, ii).Value = "" Then DataGridView1.Item(NomColonne, ii).Style.BackColor = Color.Pink
        '        End If

        '        NomColonne = "ColEnginPrlvmt" : If DataGridView1.Item(NomColonne, ii).Value = "" Then DataGridView1.Item(NomColonne, ii).Style.BackColor = Color.Pink
        '        NomColonne = "ColPréleveur" : If DataGridView1.Item(NomColonne, ii).Value = "" Then DataGridView1.Item(NomColonne, ii).Style.BackColor = Color.Pink

        '        If DataGridView1.Item("ColImmPrlvmt", ii).Value <> "" Then
        '        NomColonne = "ColImmMaxPrlvmt" : If DataGridView1.Item(NomColonne, ii).Value <> "" Then DataGridView1.Item(NomColonne, ii).Style.BackColor = Color.Pink
        '        NomColonne = "ColImmMinPrlvmt" : If DataGridView1.Item(NomColonne, ii).Value <> "" Then DataGridView1.Item(NomColonne, ii).Style.BackColor = Color.Pink
        '        NomColonne = "ColUnitéImm" : If DataGridView1.Item(NomColonne, ii).Value = "" Then DataGridView1.Item(NomColonne, ii).Style.BackColor = Color.Pink
        '        End If

        '        If DataGridView1.Item("ColImmMaxPrlvmt", ii).Value <> "" Then
        '        NomColonne = "ColImmMinPrlvmt" : If DataGridView1.Item(NomColonne, ii).Value = "" Then DataGridView1.Item(NomColonne, ii).Style.BackColor = Color.Pink
        '        NomColonne = "ColUnitéImm" : If DataGridView1.Item(NomColonne, ii).Value = "" Then DataGridView1.Item(NomColonne, ii).Style.BackColor = Color.Pink
        '        End If

        '        If DataGridView1.Item("ColImmMinPrlvmt", ii).Value <> "" Then
        '       NomColonne = "ColImmMaxPrlvmt" : If DataGridView1.Item(NomColonne, ii).Value = "" Then DataGridView1.Item(NomColonne, ii).Style.BackColor = Color.Pink
        '       NomColonne = "ColUnitéImm" : If DataGridView1.Item(NomColonne, ii).Value = "" Then DataGridView1.Item(NomColonne, ii).Style.BackColor = Color.Pink
        '        End If

        '        If DataGridView1.Item("ColTaillePrlvmt", ii).Value <> "" Then
        'NomColonne = "ColUnitéPrlvmt" : If DataGridView1.Item(NomColonne, ii).Value = "" Then DataGridView1.Item(NomColonne, ii).Style.BackColor = Color.Pink
        '        End If

        '       If DataGridView1.Item("ColLatitudePrlvmt", ii).Value <> "" Or DataGridView1.Item("ColLongitudePrlvmt", ii).Value <> "" Then
        'NomColonne = "ColPosPrlvmt" : If DataGridView1.Item(NomColonne, ii).Value = "" Then DataGridView1.Item(NomColonne, ii).Style.BackColor = Color.Pink
        '        End If

        '       NomColonne = "ColSupportÉchant" : If DataGridView1.Item(NomColonne, ii).Value = "" Then DataGridView1.Item(NomColonne, ii).Style.BackColor = Color.Pink

        '       If DataGridView1.Item("ColTaxonÉchant", ii).Value <> "" Then
        'NomColonne = "ColGrpTaxÉchant" : If DataGridView1.Item(NomColonne, ii).Value <> "" Then DataGridView1.Item(NomColonne, ii).Style.BackColor = Color.Pink
        '       End If

        '       If DataGridView1.Item("ColGrpTaxÉchant", ii).Value <> "" Then
        'NomColonne = "ColTaxonÉchant" : If DataGridView1.Item(NomColonne, ii).Value <> "" Then DataGridView1.Item(NomColonne, ii).Style.BackColor = Color.Pink
        '       End If

        '       NomColonne = "ColNumÉchant" : If DataGridView1.Item(NomColonne, ii).Value = "" Then DataGridView1.Item(NomColonne, ii).Style.BackColor = Color.Pink

        '        If DataGridView1.Item("ColTailleÉchant", ii).Value <> "" Then
        'NomColonne = "ColUnitéÉchant" : If DataGridView1.Item(NomColonne, ii).Value = "" Then DataGridView1.Item(NomColonne, ii).Style.BackColor = Color.Pink
        '        End If

        '        NomColonne = "ColNivRés" : If DataGridView1.Item(NomColonne, ii).Value = "" Then DataGridView1.Item(NomColonne, ii).Style.BackColor = Color.Pink
        '        NomColonne = "ColCodeParam" : If DataGridView1.Item(NomColonne, ii).Value = "" Then DataGridView1.Item(NomColonne, ii).Style.BackColor = Color.Pink
        '        NomColonne = "ColLibParam" : If DataGridView1.Item(NomColonne, ii).Value = "" Then DataGridView1.Item(NomColonne, ii).Style.BackColor = Color.Pink
        '        NomColonne = "ColCodeSupport" : If DataGridView1.Item(NomColonne, ii).Value = "" Then DataGridView1.Item(NomColonne, ii).Style.BackColor = Color.Pink
        '       NomColonne = "ColCodeFraction" : If DataGridView1.Item(NomColonne, ii).Value = "" Then DataGridView1.Item(NomColonne, ii).Style.BackColor = Color.Pink
        '       NomColonne = "ColCodeMéthode" : If DataGridView1.Item(NomColonne, ii).Value = "" Then DataGridView1.Item(NomColonne, ii).Style.BackColor = Color.Pink
        '       NomColonne = "ColRésultat" : If DataGridView1.Item(NomColonne, ii).Value = "" Then DataGridView1.Item(NomColonne, ii).Style.BackColor = Color.Pink

        '        NomColonne = "ColCodeUnité" : If DataGridView1.Item(NomColonne, ii).Value = "" Then DataGridView1.Item(NomColonne, ii).Style.BackColor = Color.Pink
        '        NomColonne = "ColAnalyste" : If DataGridView1.Item(NomColonne, ii).Value = "" Then DataGridView1.Item(NomColonne, ii).Style.BackColor = Color.Pink
        '        NomColonne = "ColCodeRemarque" : If DataGridView1.Item(NomColonne, ii).Value = "" Then DataGridView1.Item(NomColonne, ii).Style.BackColor = Color.Pink

        '        If DataGridView1.Item("ColCodePrécision", ii).Value <> "" Then
        'NomColonne = "ColCodeTypePrécision" : If DataGridView1.Item(NomColonne, ii).Value = "" Then DataGridView1.Item(NomColonne, ii).Style.BackColor = Color.Pink
        '        End If

        '        Next


        ' ==================================
        ' Vérification des règles structurelles
        AnalyseRèglesStructurelles()

        ' ==================================
        ' Sortie si pas de paramètre à sauvegarder
        If Variables.PrésenceParamètre = False Then Exit Sub

        ' ==================================
        ' Message de non possibilité de sauvegarde s'il y a lieu
        ' ==================================
        ' Traitement si au moins une erreur a été décelée
        If Variables.ArretSauve = True Then
            MsgBox(" Au moins une règle structurelle Quadrige² IFREMER n'a pas été respectée !", MsgBoxStyle.Exclamation, " QUADRISPEL - Erreur de règle structurelle")
            ' ==================================
            ' Affichage de la fenêtre des Règles Structurelles non valides
            RèglesStructurelles.ShowDialog()
            Exit Sub
        End If

        ' ==================================
        ' Affichage de la Feuille
        Sauve_Ifremer.ShowDialog()

    End Sub

    '=============================================================================================================================================
    ' Traitement de la vérification des règles structurelles et affichage
    Private Sub AnalyseRèglesStructurelles()

        ' ==================================
        ' Initialisation des variables
        '        Dim InfoFichier As FileInfo
        '        Dim NomFichier As StreamWriter
        Dim LigneParamètre As String

        ' ==================================
        ' Définition des variables
        '        Dim NuméroLigne As Integer = -1
        Dim NomColonne As String = ""
        Dim i As Integer
        Dim j As Integer = 0
        Dim k As Integer = 0
        Dim Sandre As Integer = 0
        Dim NombreIndex As Integer = 0
        Dim NombreParamètre As Integer = 0
        Dim PremierParam As Integer = 0
        Dim IndexTabInfo As Integer = 0
        Dim CodeSandreLieuSurveillance As String = ""
        Dim CodeSandreRéseau As String = ""
        Dim CodeSandreSaisisseur As String = ""
        Dim CodeSandreUnitéSonde As String = ""
        Dim CodeSandrePositionnementPassage As String = ""
        Dim CodeSandreNiveauPrélèvement As String = ""
        Dim CodeSandrePréleveur As String = ""
        Dim CodeSandreUnitéImmersion As String = ""
        Dim CodeSandreUnitéTaillePrélèvement As String = ""
        Dim CodeSandrePositionnementPrélèvement As String = ""
        Dim CodeSandreSupportÉchantillon As String = ""
        Dim CodeSandreGroupeTaxon As String = ""
        Dim CodeSandreUnitéTailleÉchantillon As String = ""
        Dim CodeSandre As String = ""
        Dim EnginPrélèvement As Integer = 0
        Dim EntréeSupport As Integer = 0
        Dim Fraction As Integer = 0
        Dim Méthode As Integer = 0
        Dim Unité As Integer = 0
        Dim Analyste As Integer = 0
        Dim EnginAnalyse As Integer = 0
        Dim CodeRemarque As Integer = 0
        Dim TypePrécision As Integer = 0
        Dim Information As String
        Dim PrésenceParamètre As Boolean = False
        Dim NiveauSaisie As Integer = 0

        Dim RègleStru1 As Boolean = False
        Dim RègleStru2 As Boolean = False
        Dim RègleStru3 As Boolean = False
        Dim RègleStru4 As Boolean = False
        Dim RègleStru5 As Boolean = False
        Dim RègleStru6 As Boolean = False
        Dim RègleStru7 As Boolean = False
        Dim RègleStru8 As Boolean = False
        Dim RègleStru9 As Boolean = False
        Dim RègleStru10 As Boolean = False
        Dim RègleStru11 As Boolean = False
        Dim RègleStru12 As Boolean = False
        Dim RègleStru13 As Boolean = False
        Dim RègleStru14 As Boolean = False
        Dim RègleStru15 As Boolean = False
        Dim RègleStru16 As Boolean = False
        Dim RègleStru17 As Boolean = False
        Dim RègleStru18 As Boolean = False
        Dim RègleStru19 As Boolean = False
        Dim RègleStru20 As Boolean = False
        Dim RègleStru21 As Boolean = False
        Dim RègleStru22 As Boolean = False
        Dim RègleStru23 As Boolean = False
        Dim RègleStru24 As Boolean = False
        Dim RègleStru25 As Boolean = False
        Dim RègleStru26 As Boolean = False
        Dim RègleStru27 As Boolean = False
        Dim RègleStru28 As Boolean = False
        Dim RègleStru29 As Boolean = False
        Dim RègleStru30 As Boolean = False
        Dim RègleStru31 As Boolean = False

        Dim IndexRègleStructurelle As Integer

        Variables.PrésenceParamètre = False
        Variables.ArretSauve = False

        Dim IndexTabRèglesInv As Integer

        ' ==================================
        ' Recherche du Code Sandre du LIEU DE SURVEILLANCE
        ' ==================================
        ' Recherche du point d'entrée dans le tableau des informations pour les Lieux de Surveillance
        For i = 0 To 5000
            If Variables.TabRéfInfo(i, 0) = "LIEU DE SURVEILLANCE" Then
                IndexTabInfo = i + 1
                Exit For
            End If
        Next
        ' ==================================
        ' Recherche du Code Sandre 
        For i = IndexTabInfo To 5000
            ' ==================================
            ' Sortie de la boucle si fin de tableau
            If Variables.TabRéfInfo(i, 0) = "" Then Exit For
            ' ==================================
            If Variables.TabRéfInfo(i, 1) = Passage.TextBox24.Text Then
                ' ==================================
                ' Récupération du Code Sandre
                CodeSandreLieuSurveillance = Variables.TabRéfInfo(i, 2)
                Exit For
            End If
        Next

        ' ==================================
        ' Recherche du Code Sandre du RÉSEAU
        ' ==================================
        ' Recherche du point d'entrée dans le tableau des informations pour les Lieux de Surveillance
        For i = 0 To 5000
            If Variables.TabRéfInfo(i, 0) = "RÉSEAU" Then
                IndexTabInfo = i + 1
                Exit For
            End If
        Next
        ' ==================================
        ' Recherche du Code Sandre 
        For i = IndexTabInfo To 5000
            ' ==================================
            ' Sortie de la boucle si fin de tableau
            If Variables.TabRéfInfo(i, 0) = "" Then Exit For
            ' ==================================
            If Variables.TabRéfInfo(i, 0) = Passage.TextBox3.Text Then
                ' ==================================
                ' Récupération du Code Sandre
                CodeSandreRéseau = Variables.TabRéfInfo(i, 1)
                Exit For
            End If
        Next

        ' ==================================
        ' Recherche du Code Sandre du SAISISSEUR
        ' ==================================
        ' Recherche du point d'entrée dans le tableau des informations pour les Lieux de Surveillance
        For i = 0 To 5000
            If Variables.TabRéfInfo(i, 0) = "SAISISSEUR" Then
                IndexTabInfo = i + 1
                Exit For
            End If
        Next
        ' ==================================
        ' Recherche du Code Sandre 
        For i = IndexTabInfo To 5000
            ' ==================================
            ' Sortie de la boucle si fin de tableau
            If Variables.TabRéfInfo(i, 0) = "" Then Exit For
            ' ==================================
            If Variables.TabRéfInfo(i, 0) = Passage.TextBox23.Text Then
                ' ==================================
                ' Récupération du Code Sandre
                CodeSandreSaisisseur = Variables.TabRéfInfo(i, 2)
                Exit For
            End If
        Next

        ' ==================================
        ' Recherche du Code Sandre UNITÉ SONDE
        ' ==================================
        ' Recherche du point d'entrée dans le tableau des informations pour les Lieux de Surveillance
        For i = 0 To 5000
            If Variables.TabRéfInfo(i, 0) = "UNITÉ SONDE" Then
                IndexTabInfo = i + 1
                Exit For
            End If
        Next
        ' ==================================
        ' Recherche du Code Sandre 
        For i = IndexTabInfo To 5000
            ' ==================================
            ' Sortie de la boucle si fin de tableau
            If Variables.TabRéfInfo(i, 0) = "" Then Exit For
            ' ==================================
            If Variables.TabRéfInfo(i, 0) = Passage.TextBox15.Text Then
                ' ==================================
                ' Récupération du Code Sandre
                CodeSandreUnitéSonde = Variables.TabRéfInfo(i, 1)
                Exit For
            End If
        Next

        ' ==================================
        ' Recherche du Code Sandre POSITIONNEMENT PASSAGE
        ' ==================================
        ' Recherche du point d'entrée dans le tableau des informations pour les Lieux de Surveillance
        For i = 0 To 5000
            If Variables.TabRéfInfo(i, 0) = "POSITIONNEMENT PASSAGE" Then
                IndexTabInfo = i + 1
                Exit For
            End If
        Next
        ' ==================================
        ' Recherche du Code Sandre 
        For i = IndexTabInfo To 5000
            ' ==================================
            ' Sortie de la boucle si fin de tableau
            If Variables.TabRéfInfo(i, 0) = "" Then Exit For
            ' ==================================
            If Variables.TabRéfInfo(i, 0) = Passage.TextBox19.Text Then
                ' ==================================
                ' Récupération du Code Sandre
                CodeSandrePositionnementPassage = Variables.TabRéfInfo(i, 1)
                Exit For
            End If
        Next

        ' ==================================
        ' Recherche du Code Sandre NIVEAU PRÉLÈVEMENT
        ' ==================================
        ' Recherche du point d'entrée dans le tableau des informations pour les Lieux de Surveillance
        For i = 0 To 5000
            If Variables.TabRéfInfo(i, 0) = "NIVEAU PRÉLÈVEMENT" Then
                IndexTabInfo = i + 1
                Exit For
            End If
        Next
        ' ==================================
        ' Recherche du Code Sandre 
        For i = IndexTabInfo To 5000
            ' ==================================
            ' Sortie de la boucle si fin de tableau
            If Variables.TabRéfInfo(i, 0) = "" Then Exit For
            ' ==================================
            If Variables.TabRéfInfo(i, 0) = Prélèvement.TextBox1.Text Then
                ' ==================================
                ' Récupération du Code Sandre
                CodeSandreNiveauPrélèvement = Variables.TabRéfInfo(i, 1)
                Exit For
            End If
        Next

        ' ==================================
        ' Recherche du Code Sandre PRÉLEVEUR
        ' ==================================
        ' Recherche du point d'entrée dans le tableau des informations pour les Lieux de Surveillance
        For i = 0 To 5000
            If Variables.TabRéfInfo(i, 0) = "PRÉLEVEUR" Then
                IndexTabInfo = i + 1
                Exit For
            End If
        Next
        ' ==================================
        ' Recherche du Code Sandre 
        For i = IndexTabInfo To 5000
            ' ==================================
            ' Sortie de la boucle si fin de tableau
            If Variables.TabRéfInfo(i, 0) = "" Then Exit For
            ' ==================================
            If Variables.TabRéfInfo(i, 0) = Prélèvement.TextBox23.Text Then
                ' ==================================
                ' Récupération du Code Sandre
                CodeSandrePréleveur = Variables.TabRéfInfo(i, 2)
                Exit For
            End If
        Next

        ' ==================================
        ' Recherche du Code Sandre UNITÉ IMMERSION
        ' ==================================
        ' Recherche du point d'entrée dans le tableau des informations pour les Lieux de Surveillance
        For i = 0 To 5000
            If Variables.TabRéfInfo(i, 0) = "UNITÉ IMMERSION" Then
                IndexTabInfo = i + 1
                Exit For
            End If
        Next
        ' ==================================
        ' Recherche du Code Sandre 
        For i = IndexTabInfo To 5000
            ' ==================================
            ' Sortie de la boucle si fin de tableau
            If Variables.TabRéfInfo(i, 0) = "" Then Exit For
            ' ==================================
            If Variables.TabRéfInfo(i, 0) = Prélèvement.TextBox7.Text Then
                ' ==================================
                ' Récupération du Code Sandre
                CodeSandreUnitéImmersion = Variables.TabRéfInfo(i, 1)
                Exit For
            End If
        Next

        ' ==================================
        ' Recherche du Code Sandre UNITÉ TAILLE PRÉLÈVEMENT
        ' ==================================
        ' Recherche du point d'entrée dans le tableau des informations pour les Lieux de Surveillance
        For i = 0 To 5000
            If Variables.TabRéfInfo(i, 0) = "UNITÉ TAILLE PRÉLÈVEMENT" Then
                IndexTabInfo = i + 1
                Exit For
            End If
        Next
        ' ==================================
        ' Recherche du Code Sandre 
        For i = IndexTabInfo To 5000
            ' ==================================
            ' Sortie de la boucle si fin de tableau
            If Variables.TabRéfInfo(i, 0) = "" Then Exit For
            ' ==================================
            If Variables.TabRéfInfo(i, 0) = Prélèvement.TextBox10.Text Then
                ' ==================================
                ' Récupération du Code Sandre
                CodeSandreUnitéTaillePrélèvement = Variables.TabRéfInfo(i, 1)
                Exit For
            End If
        Next

        ' ==================================
        ' Recherche du Code Sandre POSITIONNEMENT PRÉLÈVEMENT
        ' ==================================
        ' Recherche du point d'entrée dans le tableau des informations pour les Lieux de Surveillance
        For i = 0 To 5000
            If Variables.TabRéfInfo(i, 0) = "POSITIONNEMENT PRÉLÈVEMENT" Then
                IndexTabInfo = i + 1
                Exit For
            End If
        Next
        ' ==================================
        ' Recherche du Code Sandre 
        For i = IndexTabInfo To 5000
            ' ==================================
            ' Sortie de la boucle si fin de tableau
            If Variables.TabRéfInfo(i, 0) = "" Then Exit For
            ' ==================================
            If Variables.TabRéfInfo(i, 0) = Prélèvement.TextBox16.Text Then
                ' ==================================
                ' Récupération du Code Sandre
                CodeSandrePositionnementPrélèvement = Variables.TabRéfInfo(i, 1)
                Exit For
            End If
        Next

        ' ==================================
        ' Recherche du Code Sandre SUPPORT ÉCHANTILLON
        ' ==================================
        ' Recherche du point d'entrée dans le tableau des informations pour les Lieux de Surveillance
        For i = 0 To 5000
            If Variables.TabRéfInfo(i, 0) = "SUPPORT ÉCHANTILLON" Then
                IndexTabInfo = i + 1
                Exit For
            End If
        Next
        ' ==================================
        ' Recherche du Code Sandre 
        For i = IndexTabInfo To 5000
            ' ==================================
            ' Sortie de la boucle si fin de tableau
            If Variables.TabRéfInfo(i, 0) = "" Then Exit For
            ' ==================================
            If Variables.TabRéfInfo(i, 0) = Échantillon.TextBox2.Text Then
                ' ==================================
                ' Récupération du Code Sandre
                CodeSandreSupportÉchantillon = Variables.TabRéfInfo(i, 1)
                Exit For
            End If
        Next

        ' ==================================
        ' Recherche du Code Sandre GROUPE TAXON SUPPORT ÉCHANTILLON
        ' ==================================
        ' Recherche du point d'entrée dans le tableau des informations pour les Lieux de Surveillance
        For i = 0 To 5000
            If Variables.TabRéfInfo(i, 0) = "GROUPE TAXON SUPPORT ÉCHANTILLON" Then
                IndexTabInfo = i + 1
                Exit For
            End If
        Next
        ' ==================================
        ' Recherche du Code Sandre 
        For i = IndexTabInfo To 5000
            ' ==================================
            ' Sortie de la boucle si fin de tableau
            If Variables.TabRéfInfo(i, 0) = "" Then Exit For
            ' ==================================
            If Variables.TabRéfInfo(i, 0) = Échantillon.TextBox4.Text Then
                ' ==================================
                ' Récupération du Code Sandre
                CodeSandreGroupeTaxon = Variables.TabRéfInfo(i, 1)
                Exit For
            End If
        Next

        ' ==================================
        ' Recherche du Code Sandre UNITÉ TAILLE ÉCHANTILLON
        ' ==================================
        ' Recherche du point d'entrée dans le tableau des informations pour les Lieux de Surveillance
        For i = 0 To 5000
            If Variables.TabRéfInfo(i, 0) = "UNITÉ TAILLE ÉCHANTILLON" Then
                IndexTabInfo = i + 1
                Exit For
            End If
        Next
        ' ==================================
        ' Recherche du Code Sandre 
        For i = IndexTabInfo To 5000
            ' ==================================
            ' Sortie de la boucle si fin de tableau
            If Variables.TabRéfInfo(i, 0) = "" Then Exit For
            ' ==================================
            If Variables.TabRéfInfo(i, 0) = Échantillon.TextBox7.Text Then
                ' ==================================
                ' Récupération du Code Sandre
                CodeSandreUnitéTailleÉchantillon = Variables.TabRéfInfo(i, 1)
                Exit For
            End If
        Next

        ' ==================================
        ' Recherche du point d'entrée NIVEAU SAISIE dans le tableau des informations
        ' ==================================
        For i = 0 To 5000
            ' ==================================
            If Variables.TabRéfInfo(i, 0) = "NIVEAU SAISIE" Then
                NiveauSaisie = i + 1
                Exit For
            End If
        Next

        ' ==================================
        ' Recherche du point d'entrée ENGIN DE PRÉLÈVEMENT dans le tableau des informations
        ' ==================================
        For i = 0 To 5000
            ' ==================================
            If Variables.TabRéfInfo(i, 0) = "ENGIN PRÉLÈVEMENT" Then
                EnginPrélèvement = i + 1
                Exit For
            End If
        Next

        ' ==================================
        ' Recherche du point d'entrée SUPPORT dans le tableau des informations
        ' ==================================
        For i = 0 To 5000
            ' ==================================
            If Variables.TabRéfInfo(i, 0) = "SUPPORT" Then
                EntréeSupport = i + 1
                Exit For
            End If
        Next

        ' ==================================
        ' Recherche du point d'entrée FRACTION dans le tableau des informations
        ' ==================================
        For i = 0 To 5000
            ' ==================================
            If Variables.TabRéfInfo(i, 0) = "FRACTION" Then
                Fraction = i + 1
                Exit For
            End If
        Next

        ' ==================================
        ' Recherche du point d'entrée MÉTHODE dans le tableau des informations
        ' ==================================
        For i = 0 To 5000
            ' ==================================
            If Variables.TabRéfInfo(i, 0) = "MÉTHODE" Then
                Méthode = i + 1
                Exit For
            End If
        Next

        ' ==================================
        ' Recherche du point d'entrée UNITÉ dans le tableau des informations
        ' ==================================
        For i = 0 To 5000
            ' ==================================
            If Variables.TabRéfInfo(i, 0) = "UNITÉ" Then
                Unité = i + 1
                Exit For
            End If
        Next

        ' ==================================
        ' Recherche du point d'entrée ANALYSEUR dans le tableau des informations
        ' ==================================
        For i = 0 To 5000
            ' ==================================
            If Variables.TabRéfInfo(i, 0) = "ANALYSEUR" Then
                Analyste = i + 1
                Exit For
            End If
        Next

        ' ==================================
        ' Recherche du point d'entrée ENGIN ANALYSE dans le tableau des informations
        ' ==================================
        For i = 0 To 5000
            ' ==================================
            If Variables.TabRéfInfo(i, 0) = "ENGIN ANALYSE" Then
                EnginAnalyse = i + 1
                Exit For
            End If
        Next

        ' ==================================
        ' Recherche du point d'entrée PRÉCISION dans le tableau des informations
        ' ==================================
        For i = 0 To 5000
            ' ==================================
            If Variables.TabRéfInfo(i, 0) = "CODE REMARQUE" Then
                CodeRemarque = i + 1
                Exit For
            End If
        Next

        ' ==================================
        ' Recherche du point d'entrée PRÉCISION dans le tableau des informations
        ' ==================================
        '        For i = 0 To 5000
        ' ==================================
        '        If Variables.TabRéfInfo(i, 0) = "PRÉCISION" Then
        '        Précision = i + 1
        '        Exit For
        '        End If
        '        Next

        ' ==================================
        ' Recherche du point d'entrée TYPE PRÉCISION dans le tableau des informations
        ' ==================================
        For i = 0 To 5000
            ' ==================================
            If Variables.TabRéfInfo(i, 0) = "TYPE PRÉCISION" Then
                TypePrécision = i + 1
                Exit For
            End If
        Next



        ' ==================================
        ' Balayage des noms des Familles sélectionnées dans la feuille des Familles
        For i = 0 To Familles.ListBox2.Items.Count - 1
            ' ==================================
            ' Recherche de la première adresse de la Famille sélectionnée
            For j = 0 To 500
                If LTrim(Familles.ListBox2.Items(i)) = Variables.TabRéfPara(j, 0) Then
                    PremierParam = j
                    Exit For
                End If
            Next
            ' ==================================
            ' Initialisation des indicateurs
            NombreIndex = 0
            NombreParamètre = 0
            LigneParamètre = ""
            ' ==================================
            ' Recherche du nombre de Paramètres et du nombre de Paramètres indexés pour la Famille sélectionnée
            For j = PremierParam To 500
                ' ==================================
                ' Sortie si tous les Paramètres de la Famille sélectionnée ont été balayés
                If LTrim(Familles.ListBox2.Items(i)) <> Variables.TabRéfPara(j, 0) Then
                    Exit For
                End If
                ' ==================================
                ' Incrémentation de l'indicateur du nombre de Paramètres indexés pour la Famille sélectionnée
                If Val(Variables.TabRéfPara(j, 21)) <> 0 Then
                    NombreIndex = NombreIndex + 1
                    ' ==================================
                    ' Indicateur de la présence d'au moins un paramètre à sauvegarder
                    Variables.PrésenceParamètre = True
                End If
                ' ==================================
                ' Incrémentation de l'indicateur du nombre de Paramètres pour la Famille sélectionnée
                NombreParamètre = NombreParamètre + 1
            Next
            ' ==================================
            ' Balayage des Paramètres pour le transfert des informations du paramètre sélectionné de la famille sélectionnée dans la Feuille des Paramètres
            For j = PremierParam To PremierParam + NombreParamètre - 1
                ' ==================================
                ' Balayage des index pour le transfert des informations du paramètre sélectionné de la famille sélectionnée dans la Feuille des Paramètres
                For k = 1 To NombreIndex
                    ' ==================================
                    ' Recherche du Paramètre dont l'index correspond à l'index recherché
                    If Variables.TabRéfPara(j, 21) = k Then

                        ' ==================================
                        ' Code Sandre Lieu de Surveillance 2
                        ' Absence de la « Dénomination du Lieu de Surveillance ».
                        If CodeSandreLieuSurveillance = "" Then Variables.ArretSauve = True : RègleStru1 = True

                        ' ==================================
                        ' Code Sandre Réseau 3
                        ' Absence de la « Dénomination du Réseau ».
                        If CodeSandreRéseau = "" Then Variables.ArretSauve = True : RègleStru2 = True

                        ' ==================================
                        ' Code Sandre Saisisseur 4
                        ' Absence du « Libellé du Saisisseur ».
                        If CodeSandreSaisisseur = "" Then Variables.ArretSauve = True : RègleStru3 = True

                        ' ==================================
                        ' Date Passage 8
                        ' Absence de la « Date de Passage ».
                        If Passage.TextBox11.Text = "" Then Variables.ArretSauve = True : RègleStru4 = True

                        ' ==================================
                        ' Sonde 10 & Code Sandre Unité Sonde 11
                        ' Présence d’une « Valeur de Sonde » sans « Unité de Sonde ».
                        If Passage.TextBox14.Text <> "" And CodeSandreUnitéSonde = "" Then Variables.ArretSauve = True : RègleStru5 = True

                        ' ==================================
                        ' Latitude Passage 14 & Longitude Passage 15 & Code Sandre Positionnement Passage 16
                        ' Absence de « Positionnement Passage » avec présence de la « Latitude Passage » ou de la « Longitude Passage » ou des deux.
                        If (Passage.TextBox1.Text <> "" Or Passage.TextBox2.Text <> "") And CodeSandrePositionnementPassage = "" Then Variables.ArretSauve = True : RègleStru6 = True

                        ' ==================================
                        ' Code Sandre Engin Prélèvement 18
                        ' Absence de la « Dénomination de l’Engin de Prélèvement ».
                        ' ==================================
                        ' Recherche du Code Sandre correspondant
                        Information = ""
                        For Sandre = EnginPrélèvement To 5000
                            ' ==================================
                            ' Sortie de la boucle si fin de tableau
                            If Variables.TabRéfInfo(Sandre, 0) = "" Then Exit For
                            ' ==================================
                            If Variables.TabRéfInfo(Sandre, 0) = Variables.TabRéfPara(j, 10) Then
                                ' ==================================
                                ' Récupération du Code Sandre
                                Information = Variables.TabRéfInfo(Sandre, 1)
                                Exit For
                            End If
                        Next
                        If Information = "" Then Variables.ArretSauve = True : RègleStru7 = True

                        ' ==================================
                        ' Code Sandre Préleveur 20
                        ' Absence de la « Dénomination du Préleveur ».
                        If CodeSandrePréleveur = "" Then Variables.ArretSauve = True : RègleStru8 = True

                        ' ==================================
                        ' Immersion Prélèvement 22 &  Immersion Max Prélèvement 23 & Immersion Min Prélèvement 24
                        ' Présence de l’« Immersion de Prélèvement » avec l’« Immersion Max » ou avec l’« Immersion Min » ou sans l’« Unité de Prélèvement ».
                        If Prélèvement.TextBox3.Text <> "" Then
                            If Prélèvement.TextBox6.Text <> "" Then Variables.ArretSauve = True : RègleStru9 = True
                            If Prélèvement.TextBox5.Text <> "" Then Variables.ArretSauve = True : RègleStru9 = True
                            If CodeSandreUnitéImmersion = "" Then Variables.ArretSauve = True : RègleStru9 = True
                        End If

                        ' ==================================
                        ' Immersion Max Prélèvement 23
                        ' Présence de l’« Immersion Max de Prélèvement » sans l’« Immersion Min de Prélèvement » ou sans l’« Unité de Prélèvement ».
                        If Prélèvement.TextBox6.Text <> "" Then
                            If Prélèvement.TextBox5.Text = "" Then Variables.ArretSauve = True : RègleStru10 = True
                            If CodeSandreUnitéImmersion = "" Then Variables.ArretSauve = True : RègleStru10 = True
                        End If

                        ' ==================================
                        ' Immersion Min Prélèvement 24
                        ' Présence de l’« Immersion Min de Prélèvement » sans l’« Immersion Max de Prélèvement » ou sans l’« Unité de Prélèvement ».
                        If Prélèvement.TextBox5.Text <> "" Then
                            If Prélèvement.TextBox6.Text = "" Then Variables.ArretSauve = True : RègleStru11 = True
                            If CodeSandreUnitéImmersion = "" Then Variables.ArretSauve = True : RègleStru11 = True
                        End If

                        ' ==================================
                        ' Taille Prélèvement 26 & Code Sandre Unité Taille Prélèvement 27
                        ' Présence de la « Taille de Prélèvement » sans l’« Unité de Taille de Prélèvement ».
                        If Prélèvement.TextBox9.Text <> "" And CodeSandreUnitéTaillePrélèvement = "" Then Variables.ArretSauve = True : RègleStru12 = True

                        ' ==================================
                        ' Latitude Prélèvement 30 & Longitude Prélèvement 31 & Code Sandre Positionnement Prélèvement 32
                        ' Absence de « Positionnement de Prélèvement » avec présence de la « Latitude » ou de la « Longitude de Prélèvement » ou des deux.
                        If (Prélèvement.TextBox14.Text <> "" Or Prélèvement.TextBox15.Text <> "") And CodeSandrePositionnementPrélèvement = "" Then Variables.ArretSauve = True : RègleStru13 = True

                        ' ==================================
                        ' Code Sandre Support Échantillon 35
                        ' Absence de la « Dénomination du Support Échantillon ».
                        If CodeSandreSupportÉchantillon = "" Then Variables.ArretSauve = True : RègleStru14 = True

                        ' ==================================
                        ' Taxon Support Échantillon 36 & Code Sandre Groupe Taxon Support Échantillon 37
                        ' Présence du « Taxon Support Échantillon » et du « Groupe Taxon Support Échantillon ».
                        If Échantillon.TextBox3.Text <> "" And CodeSandreGroupeTaxon <> "" Then Variables.ArretSauve = True : RègleStru15 = True

                        ' ==================================
                        ' Numéro Échantillon 38
                        ' Absence du « Numéro d’Échantillon ».
                        If Échantillon.TextBox5.Text = "" Then Variables.ArretSauve = True : RègleStru16 = True

                        ' ==================================
                        ' Taille Échantillon 39 & Code Sandre Unité Taille Échantillon 40
                        ' Présence de la « Taille d’Échantillon » sans l’« Unité de Taille d’Échantillon ».
                        If Échantillon.TextBox6.Text <> "" And CodeSandreUnitéTailleÉchantillon = "" Then Variables.ArretSauve = True : RègleStru17 = True

                        ' ==================================
                        ' Niveau Saisie Résultat 43
                        ' Absence du « Niveau de Saisie Résultat ».
                        If Variables.TabRéfPara(j, 13) = "" Then Variables.ArretSauve = True : RègleStru18 = True

                        ' ==================================
                        ' Niveau Saisie Résultat 43
                        ' Absence du « Niveau de Saisie Résultat ».
                        ' ==================================
                        Information = ""
                        For Sandre = NiveauSaisie To 5000
                            ' ==================================
                            ' Sortie de la boucle si fin de tableau
                            If Variables.TabRéfInfo(Sandre, 0) = "" Then Exit For
                            ' ==================================
                            If Variables.TabRéfInfo(Sandre, 0) = Variables.TabRéfPara(j, 13) Then
                                ' ==================================
                                ' Récupération du Code Sandre
                                Information = Variables.TabRéfInfo(Sandre, 1)
                                Exit For
                            End If
                        Next
                        If Information = "" Then Variables.ArretSauve = True : RègleStru18 = True

                        ' ==================================
                        ' Code Sandre Paramètre 44
                        ' Absence du « Code Sandre Paramètre ».
                        If Variables.TabRéfPara(j, 3) = "" Then Variables.ArretSauve = True : RègleStru19 = True

                        ' ==================================
                        ' Libellé Paramètre 45
                        ' Absence de la « Dénomination du Paramètre ».
                        If Variables.TabRéfPara(j, 1) = "" Then Variables.ArretSauve = True : RègleStru20 = True

                        ' ==================================
                        ' Code Sandre Support 46
                        ' Absence de la « Dénomination du Support ».
                        ' ==================================
                        ' Recherche du Code Sandre correspondant
                        Information = ""
                        For Sandre = EntréeSupport To 5000
                            ' ==================================
                            ' Sortie de la boucle si fin de tableau
                            If Variables.TabRéfInfo(Sandre, 0) = "" Then Exit For
                            ' ==================================
                            If Variables.TabRéfInfo(Sandre, 0) = Variables.TabRéfPara(j, 9) Then
                                ' ==================================
                                ' Récupération du Code Sandre
                                Information = Variables.TabRéfInfo(Sandre, 1)
                                Exit For
                            End If
                        Next
                        If Information = "" Then Variables.ArretSauve = True : RègleStru21 = True

                        ' ==================================
                        ' Vérification égalité du "Code Sandre Support Échantillon" et du "Code Sandre Support"
                        ' Présence d’une « Dénomination Support Échantillon » différente d’une « Dénomination du Support ».
                        ' ==================================
                        If Information <> CodeSandreSupportÉchantillon Then Variables.ArretSauve = True : RègleStru22 = True

                        ' ==================================
                        ' Code Sandre Fraction 47
                        ' Absence de la « Dénomination de la Fraction ».
                        ' ==================================
                        ' Recherche du Code Sandre correspondant
                        Information = ""
                        For Sandre = Fraction To 5000
                            ' ==================================
                            ' Sortie de la boucle si fin de tableau
                            If Variables.TabRéfInfo(Sandre, 0) = "" Then Exit For
                            ' ==================================
                            If Variables.TabRéfInfo(Sandre, 0) = Variables.TabRéfPara(j, 8) Then
                                ' ==================================
                                ' Récupération du Code Sandre
                                Information = Variables.TabRéfInfo(Sandre, 1)
                                Exit For
                            End If
                        Next
                        If Information = "" Then Variables.ArretSauve = True : RègleStru23 = True

                        ' ==================================
                        ' Code Sandre Méthode 48
                        ' Absence de la « Dénomination de la Méthode ».
                        ' ==================================
                        ' Recherche du Code Sandre correspondant
                        Information = ""
                        For Sandre = Méthode To 5000
                            ' ==================================
                            ' Sortie de la boucle si fin de tableau
                            If Variables.TabRéfInfo(Sandre, 0) = "" Then Exit For
                            ' ==================================
                            If Variables.TabRéfInfo(Sandre, 0) = Variables.TabRéfPara(j, 6) Then
                                ' ==================================
                                ' Récupération du Code Sandre
                                Information = Variables.TabRéfInfo(Sandre, 1)
                                Exit For
                            End If
                        Next
                        If Information = "" Then Variables.ArretSauve = True : RègleStru24 = True




                        ' ==================================
                        ' Résultat Numérique 50

                        ' Absence du « Résultat Numérique ».
                        If Variables.TabRéfPara(j, 17) = "" And Variables.TabRéfPara(j, 15) = "" And Variables.TabRéfPara(j, 23) = "" Then Variables.ArretSauve = True : RègleStru25 = True

                        ' Présence du signe « < » dans la valeur du Résultat Numérique.
                        If Strings.InStr(Variables.TabRéfPara(j, 17), "<") <> 0 Then Variables.ArretSauve = True : RègleStru26 = True



                        ' ==================================
                        ' Résultat Numérique 

                        ' Absence du « Code Résultat Qualitatif ».
                        If Variables.TabRéfPara(j, 17) = "" And Variables.TabRéfPara(j, 23) = "" Then Variables.ArretSauve = True : RègleStru31 = True




                        ' ==================================
                        ' Code Sandre Unité 52
                        ' Absence de l’« Unité du Résultat ».
                        ' ==================================
                        ' Recherche du Code Sandre correspondant
                        Information = ""
                        For Sandre = Unité To 5000
                            ' ==================================
                            ' Sortie de la boucle si fin de tableau
                            If Variables.TabRéfInfo(Sandre, 0) = "" Then Exit For
                            ' ==================================
                            If Variables.TabRéfInfo(Sandre, 0) = Variables.TabRéfPara(j, 18) Then
                                ' ==================================
                                ' Récupération du Code Sandre
                                Information = Variables.TabRéfInfo(Sandre, 1)
                                Exit For
                            End If
                        Next
                        If Information = "" Then Variables.ArretSauve = True : RègleStru27 = True

                        ' ==================================
                        ' Code Sandre Analyste 53
                        ' Absence de la « Dénomination de l’Analyseur ».
                        ' ==================================
                        ' Recherche du Code Sandre correspondant
                        Information = ""
                        For Sandre = Analyste To 5000
                            ' ==================================
                            ' Sortie de la boucle si fin de tableau
                            If Variables.TabRéfInfo(Sandre, 0) = "" Then Exit For
                            ' ==================================
                            If Variables.TabRéfInfo(Sandre, 0) = Variables.TabRéfPara(j, 4) Then
                                ' ==================================
                                ' Récupération du Code Sandre
                                Information = Variables.TabRéfInfo(Sandre, 2)
                                Exit For
                            End If
                        Next
                        If Information = "" Then Variables.ArretSauve = True : RègleStru28 = True

                        ' ==================================
                        ' Code Remarque 55
                        ' Absence du « Code Remarque ».
                        ' ==================================
                        ' Recherche du Code Sandre correspondant
                        Information = ""
                        For Sandre = CodeRemarque To 5000
                            ' ==================================
                            ' Sortie de la boucle si fin de tableau
                            If Variables.TabRéfInfo(Sandre, 0) = "" Then Exit For
                            ' ==================================
                            If Variables.TabRéfInfo(Sandre, 0) = Variables.TabRéfPara(j, 20) Then
                                ' ==================================
                                ' Récupération du Code Sandre
                                Information = Variables.TabRéfInfo(Sandre, 1)
                                Exit For
                            End If
                        Next
                        If Information = "" Then Variables.ArretSauve = True : RègleStru29 = True

                        ' ==================================
                        ' Code Sandre Précision 56 & Code Sandre Type Précision 57
                        ' Présence d’une « Valeur de Précision » sans le « Type de Précision ».
                        ' ==================================
                        ' Recherche du Code Sandre correspondant
                        Information = ""
                        For Sandre = TypePrécision To 5000
                            ' ==================================
                            ' Sortie de la boucle si fin de tableau
                            If Variables.TabRéfInfo(Sandre, 0) = "" Then Exit For
                            ' ==================================
                            If Variables.TabRéfInfo(Sandre, 0) = Variables.TabRéfPara(j, 12) Then
                                ' ==================================
                                ' Récupération du Code Sandre
                                Information = Variables.TabRéfInfo(Sandre, 1)
                                Exit For
                            End If
                        Next
                        If Variables.TabRéfPara(j, 11) <> "" And Information = "" Then Variables.ArretSauve = True : RègleStru30 = True

                    End If
                Next
            Next
        Next

        ' ==================================

        IndexRègleStructurelle = -1
        NomColonne = "Column1"
        IndexTabRèglesInv = 0
        For i = 0 To 40
            Variables.TabRèglesInv(i) = ""
        Next

        ' ==================================
        ' Effacement des lignes du DataGridView
        RèglesStructurelles.DataGridView1.Rows.Clear()

        ' ==================================
        ' Absence de la « Dénomination du Lieu de Surveillance ».
        If RègleStru1 = True Then
            IndexRègleStructurelle = IndexRègleStructurelle + 1
            RèglesStructurelles.DataGridView1.Rows.Add()
            RèglesStructurelles.DataGridView1.Item(NomColonne, IndexRègleStructurelle).Value = "Absence de la " & Chr(171) & " Dénomination du Lieu de Surveillance " & Chr(187)
            Variables.TabRèglesInv(IndexTabRèglesInv) = "Absence de la " & Chr(171) & " Dénomination du Lieu de Surveillance " & Chr(187)
            IndexTabRèglesInv = IndexTabRèglesInv + 1
        End If
        ' ==================================
        ' Absence de la « Dénomination du Réseau ».
        If RègleStru2 = True Then
            IndexRègleStructurelle = IndexRègleStructurelle + 1
            RèglesStructurelles.DataGridView1.Rows.Add()
            RèglesStructurelles.DataGridView1.Item(NomColonne, IndexRègleStructurelle).Value = "Absence de la " & Chr(171) & " Dénomination du Réseau " & Chr(187)
            Variables.TabRèglesInv(IndexTabRèglesInv) = "Absence de la " & Chr(171) & " Dénomination du Réseau " & Chr(187)
            IndexTabRèglesInv = IndexTabRèglesInv + 1
        End If
        ' ==================================
        ' Absence du « Libellé du Saisisseur ».
        If RègleStru3 = True Then
            IndexRègleStructurelle = IndexRègleStructurelle + 1
            RèglesStructurelles.DataGridView1.Rows.Add()
            RèglesStructurelles.DataGridView1.Item(NomColonne, IndexRègleStructurelle).Value = "Absence du " & Chr(171) & " Libellé du Saisisseur " & Chr(187)
            Variables.TabRèglesInv(IndexTabRèglesInv) = "Absence du " & Chr(171) & " Libellé du Saisisseur " & Chr(187)
            IndexTabRèglesInv = IndexTabRèglesInv + 1
        End If
        ' ==================================
        ' Absence de la « Date de Passage ».
        If RègleStru4 = True Then
            IndexRègleStructurelle = IndexRègleStructurelle + 1
            RèglesStructurelles.DataGridView1.Rows.Add()
            RèglesStructurelles.DataGridView1.Item(NomColonne, IndexRègleStructurelle).Value = "Absence de la " & Chr(171) & " Date de Passage " & Chr(187)
            Variables.TabRèglesInv(IndexTabRèglesInv) = "Absence de la " & Chr(171) & " Date de Passage " & Chr(187)
            IndexTabRèglesInv = IndexTabRèglesInv + 1
        End If
        ' ==================================
        ' Présence d’une « Valeur de Sonde » sans « Unité de Sonde ».
        If RègleStru5 = True Then
            IndexRègleStructurelle = IndexRègleStructurelle + 1
            RèglesStructurelles.DataGridView1.Rows.Add()
            RèglesStructurelles.DataGridView1.Item(NomColonne, IndexRègleStructurelle).Value = "Présence d’une " & Chr(171) & " Valeur de Sonde " & Chr(187) & " sans " & Chr(171) & " Unité de Sonde " & Chr(187)
            Variables.TabRèglesInv(IndexTabRèglesInv) = "Présence d’une " & Chr(171) & " Valeur de Sonde " & Chr(187) & " sans " & Chr(171) & " Unité de Sonde " & Chr(187)
            IndexTabRèglesInv = IndexTabRèglesInv + 1
        End If
        ' ==================================
        ' Absence de « Positionnement Passage » avec présence de la « Latitude Passage » ou de la « Longitude Passage » ou des deux.
        If RègleStru6 = True Then
            IndexRègleStructurelle = IndexRègleStructurelle + 1
            RèglesStructurelles.DataGridView1.Rows.Add()
            RèglesStructurelles.DataGridView1.Item(NomColonne, IndexRègleStructurelle).Value = "Absence de " & Chr(171) & " Positionnement Passage " & Chr(187) & " avec présence de la " & Chr(171) & " Latitude Passage " & Chr(187) & " ou de la " & Chr(171) & " Longitude Passage " & Chr(187) & " ou des deux"
            Variables.TabRèglesInv(IndexTabRèglesInv) = "Absence de " & Chr(171) & " Positionnement Passage " & Chr(187) & " avec présence de la " & Chr(171) & " Latitude Passage " & Chr(187) & " ou de la " & Chr(171) & " Longitude Passage " & Chr(187) & " ou des deux"
            IndexTabRèglesInv = IndexTabRèglesInv + 1
        End If
        ' ==================================
        ' Absence de la « Dénomination de l’Engin de Prélèvement ».
        If RègleStru7 = True Then
            IndexRègleStructurelle = IndexRègleStructurelle + 1
            RèglesStructurelles.DataGridView1.Rows.Add()
            RèglesStructurelles.DataGridView1.Item(NomColonne, IndexRègleStructurelle).Value = "Absence de la " & Chr(171) & " Dénomination de l’Engin de Prélèvement " & Chr(187)
            Variables.TabRèglesInv(IndexTabRèglesInv) = "Absence de la " & Chr(171) & " Dénomination de l’Engin de Prélèvement " & Chr(187)
            IndexTabRèglesInv = IndexTabRèglesInv + 1
        End If
        ' ==================================
        ' Absence du « Libellé du Préleveur ».
        If RègleStru8 = True Then
            IndexRègleStructurelle = IndexRègleStructurelle + 1
            RèglesStructurelles.DataGridView1.Rows.Add()
            RèglesStructurelles.DataGridView1.Item(NomColonne, IndexRègleStructurelle).Value = "Absence du " & Chr(171) & " Libellé du Préleveur " & Chr(187)
            Variables.TabRèglesInv(IndexTabRèglesInv) = "Absence du " & Chr(171) & " Libellé du Préleveur " & Chr(187)
            IndexTabRèglesInv = IndexTabRèglesInv + 1
        End If
        ' ==================================
        ' Présence de l’« Immersion de Prélèvement » avec l’« Immersion Max » ou avec l’« Immersion Min » ou sans l’« Unité de Prélèvement ».
        If RègleStru9 = True Then
            IndexRègleStructurelle = IndexRègleStructurelle + 1
            RèglesStructurelles.DataGridView1.Rows.Add()
            RèglesStructurelles.DataGridView1.Item(NomColonne, IndexRègleStructurelle).Value = "Présence de l’ " & Chr(171) & " Immersion de Prélèvement " & Chr(187) & " avec l' " & Chr(171) & " Immersion Max " & Chr(187) & " ou avec l’ " & Chr(171) & " Immersion Min " & Chr(187) & " ou sans l’ " & Chr(171) & " Unité d'Immersion " & Chr(187)
            Variables.TabRèglesInv(IndexTabRèglesInv) = "Présence de l’ " & Chr(171) & " Immersion de Prélèvement " & Chr(187) & " avec l' " & Chr(171) & " Immersion Max " & Chr(187) & " ou avec l’ " & Chr(171) & " Immersion Min " & Chr(187) & " ou sans l’ " & Chr(171) & " Unité d'Immersion " & Chr(187)
            IndexTabRèglesInv = IndexTabRèglesInv + 1
        End If
        ' ==================================
        ' Présence de l’« Immersion Max de Prélèvement » sans l’« Immersion Min de Prélèvement » ou sans l’« Unité de Prélèvement ».
        If RègleStru10 = True Then
            IndexRègleStructurelle = IndexRègleStructurelle + 1
            RèglesStructurelles.DataGridView1.Rows.Add()
            RèglesStructurelles.DataGridView1.Item(NomColonne, IndexRègleStructurelle).Value = "Présence de l’ " & Chr(171) & " Immersion Max de Prélèvement " & Chr(187) & " sans l' " & Chr(171) & " Immersion Min de Prélèvement " & Chr(187) & " ou sans l’ " & Chr(171) & " Unité d'Immersion " & Chr(187)
            Variables.TabRèglesInv(IndexTabRèglesInv) = "Présence de l’ " & Chr(171) & " Immersion Max de Prélèvement " & Chr(187) & " sans l' " & Chr(171) & " Immersion Min de Prélèvement " & Chr(187) & " ou sans l’ " & Chr(171) & " Unité d'Immersion " & Chr(187)
            IndexTabRèglesInv = IndexTabRèglesInv + 1
        End If
        ' ==================================
        ' Présence de l’« Immersion Min de Prélèvement » sans l’« Immersion Max de Prélèvement » ou sans l’« Unité de Prélèvement ».
        If RègleStru11 = True Then
            IndexRègleStructurelle = IndexRègleStructurelle + 1
            RèglesStructurelles.DataGridView1.Rows.Add()
            RèglesStructurelles.DataGridView1.Item(NomColonne, IndexRègleStructurelle).Value = "Présence de l’ " & Chr(171) & " Immersion Min de Prélèvement " & Chr(187) & " sans l' " & Chr(171) & " Immersion Max de Prélèvement " & Chr(187) & " ou sans l’ " & Chr(171) & " Unité d'Immersion " & Chr(187)
            Variables.TabRèglesInv(IndexTabRèglesInv) = "Présence de l’ " & Chr(171) & " Immersion Min de Prélèvement " & Chr(187) & " sans l' " & Chr(171) & " Immersion Max de Prélèvement " & Chr(187) & " ou sans l’ " & Chr(171) & " Unité d'Immersion " & Chr(187)
            IndexTabRèglesInv = IndexTabRèglesInv + 1
        End If
        ' ==================================
        ' Présence de la « Taille de Prélèvement » sans l’« Unité de Taille de Prélèvement ».
        If RègleStru12 = True Then
            IndexRègleStructurelle = IndexRègleStructurelle + 1
            RèglesStructurelles.DataGridView1.Rows.Add()
            RèglesStructurelles.DataGridView1.Item(NomColonne, IndexRègleStructurelle).Value = "Présence de la " & Chr(171) & " Taille de Prélèvement " & Chr(187) & " sans l' " & Chr(171) & " Unité de Taille de Prélèvement " & Chr(187)
            Variables.TabRèglesInv(IndexTabRèglesInv) = "Présence de la " & Chr(171) & " Taille de Prélèvement " & Chr(187) & " sans l' " & Chr(171) & " Unité de Taille de Prélèvement " & Chr(187)
            IndexTabRèglesInv = IndexTabRèglesInv + 1
        End If
        ' ==================================
        ' Absence de « Positionnement Prélèvement » avec présence de la « Latitude » ou de la « Longitude de Prélèvement » ou des deux.
        If RègleStru13 = True Then
            IndexRègleStructurelle = IndexRègleStructurelle + 1
            RèglesStructurelles.DataGridView1.Rows.Add()
            RèglesStructurelles.DataGridView1.Item(NomColonne, IndexRègleStructurelle).Value = "Absence de " & Chr(171) & " Positionnement de Prélèvement " & Chr(187) & " avec présence de la " & Chr(171) & " Latitude " & Chr(187) & " ou de la " & Chr(171) & " Longitude de Prélèvement " & Chr(187) & " ou des deux"
            Variables.TabRèglesInv(IndexTabRèglesInv) = "Absence de " & Chr(171) & " Positionnement de Prélèvement " & Chr(187) & " avec présence de la " & Chr(171) & " Latitude " & Chr(187) & " ou de la " & Chr(171) & " Longitude de Prélèvement " & Chr(187) & " ou des deux"
            IndexTabRèglesInv = IndexTabRèglesInv + 1
        End If
        ' ==================================
        ' Absence de la « Dénomination du Support Échantillon ».
        If RègleStru14 = True Then
            IndexRègleStructurelle = IndexRègleStructurelle + 1
            RèglesStructurelles.DataGridView1.Rows.Add()
            RèglesStructurelles.DataGridView1.Item(NomColonne, IndexRègleStructurelle).Value = "Absence de la " & Chr(171) & " Dénomination du Support Échantillon " & Chr(187)
            Variables.TabRèglesInv(IndexTabRèglesInv) = "Absence de la " & Chr(171) & " Dénomination du Support Échantillon " & Chr(187)
            IndexTabRèglesInv = IndexTabRèglesInv + 1
        End If
        ' ==================================
        ' Présence du « Taxon Support Échantillon » et du « Groupe Taxon Support Échantillon ».
        If RègleStru15 = True Then
            IndexRègleStructurelle = IndexRègleStructurelle + 1
            RèglesStructurelles.DataGridView1.Rows.Add()
            RèglesStructurelles.DataGridView1.Item(NomColonne, IndexRègleStructurelle).Value = "Présence du " & Chr(171) & " Taxon Support Échantillon " & Chr(187) & " et du " & Chr(171) & " Groupe Taxon Support Échantillon " & Chr(187)
            Variables.TabRèglesInv(IndexTabRèglesInv) = "Présence du " & Chr(171) & " Taxon Support Échantillon " & Chr(187) & " et du " & Chr(171) & " Groupe Taxon Support Échantillon " & Chr(187)
            IndexTabRèglesInv = IndexTabRèglesInv + 1
        End If
        ' ==================================
        ' Absence du « Numéro d’Échantillon ».
        If RègleStru16 = True Then
            IndexRègleStructurelle = IndexRègleStructurelle + 1
            RèglesStructurelles.DataGridView1.Rows.Add()
            RèglesStructurelles.DataGridView1.Item(NomColonne, IndexRègleStructurelle).Value = "Absence du " & Chr(171) & " Numéro d’Échantillon " & Chr(187)
            Variables.TabRèglesInv(IndexTabRèglesInv) = "Absence du " & Chr(171) & " Numéro d’Échantillon " & Chr(187)
            IndexTabRèglesInv = IndexTabRèglesInv + 1
        End If
        ' ==================================
        ' Présence de la « Taille d’Échantillon » sans l’« Unité de Taille d’Échantillon ».
        If RègleStru17 = True Then
            IndexRègleStructurelle = IndexRègleStructurelle + 1
            RèglesStructurelles.DataGridView1.Rows.Add()
            RèglesStructurelles.DataGridView1.Item(NomColonne, IndexRègleStructurelle).Value = "Présence de la " & Chr(171) & " Taille d’Échantillon " & Chr(187) & " sans l' " & Chr(171) & " Unité de Taille d’Échantillon " & Chr(187)
            Variables.TabRèglesInv(IndexTabRèglesInv) = "Présence de la " & Chr(171) & " Taille d’Échantillon " & Chr(187) & " sans l' " & Chr(171) & " Unité de Taille d’Échantillon " & Chr(187)
            IndexTabRèglesInv = IndexTabRèglesInv + 1
        End If
        ' ==================================
        ' Absence du « Niveau de Saisie Résultat ».
        If RègleStru18 = True Then
            IndexRègleStructurelle = IndexRègleStructurelle + 1
            RèglesStructurelles.DataGridView1.Rows.Add()
            RèglesStructurelles.DataGridView1.Item(NomColonne, IndexRègleStructurelle).Value = "Absence du " & Chr(171) & " Niveau de Saisie Résultat " & Chr(187)
            Variables.TabRèglesInv(IndexTabRèglesInv) = "Absence du " & Chr(171) & " Niveau de Saisie Résultat " & Chr(187)
            IndexTabRèglesInv = IndexTabRèglesInv + 1
        End If
        ' ==================================
        ' Absence du « Code Sandre Paramètre ».
        If RègleStru19 = True Then
            IndexRègleStructurelle = IndexRègleStructurelle + 1
            RèglesStructurelles.DataGridView1.Rows.Add()
            RèglesStructurelles.DataGridView1.Item(NomColonne, IndexRègleStructurelle).Value = "Absence du " & Chr(171) & " Code Sandre Paramètre " & Chr(187)
            Variables.TabRèglesInv(IndexTabRèglesInv) = "Absence du " & Chr(171) & " Code Sandre Paramètre " & Chr(187)
            IndexTabRèglesInv = IndexTabRèglesInv + 1
        End If
        ' ==================================
        ' Absence de la « Dénomination du Paramètre ».
        If RègleStru20 = True Then
            IndexRègleStructurelle = IndexRègleStructurelle + 1
            RèglesStructurelles.DataGridView1.Rows.Add()
            RèglesStructurelles.DataGridView1.Item(NomColonne, IndexRègleStructurelle).Value = "Absence de la " & Chr(171) & " Dénomination du Paramètre " & Chr(187)
            Variables.TabRèglesInv(IndexTabRèglesInv) = "Absence de la " & Chr(171) & " Dénomination du Paramètre " & Chr(187)
            IndexTabRèglesInv = IndexTabRèglesInv + 1
        End If
        ' ==================================
        ' Absence de la « Dénomination du Support ».
        If RègleStru21 = True Then
            IndexRègleStructurelle = IndexRègleStructurelle + 1
            RèglesStructurelles.DataGridView1.Rows.Add()
            RèglesStructurelles.DataGridView1.Item(NomColonne, IndexRègleStructurelle).Value = "Absence de la " & Chr(171) & " Dénomination du Support " & Chr(187)
            Variables.TabRèglesInv(IndexTabRèglesInv) = "Absence de la " & Chr(171) & " Dénomination du Support " & Chr(187)
            IndexTabRèglesInv = IndexTabRèglesInv + 1
        End If
        ' ==================================
        ' Différence entre la « Dénomination Support Échantillon » et une « Dénomination du Support ».
        If RègleStru22 = True Then
            IndexRègleStructurelle = IndexRègleStructurelle + 1
            RèglesStructurelles.DataGridView1.Rows.Add()
            RèglesStructurelles.DataGridView1.Item(NomColonne, IndexRègleStructurelle).Value = "Différence entre la " & Chr(171) & " Dénomination Support Échantillon " & Chr(187) & " et une " & Chr(171) & " Dénomination du Support " & Chr(187)
            Variables.TabRèglesInv(IndexTabRèglesInv) = "Différence entre la " & Chr(171) & " Dénomination Support Échantillon " & Chr(187) & " et une " & Chr(171) & " Dénomination du Support " & Chr(187)
            IndexTabRèglesInv = IndexTabRèglesInv + 1
        End If
        ' ==================================
        ' Absence de la « Dénomination de la Fraction ».
        If RègleStru23 = True Then
            IndexRègleStructurelle = IndexRègleStructurelle + 1
            RèglesStructurelles.DataGridView1.Rows.Add()
            RèglesStructurelles.DataGridView1.Item(NomColonne, IndexRègleStructurelle).Value = "Absence de la " & Chr(171) & " Dénomination de la Fraction " & Chr(187)
            Variables.TabRèglesInv(IndexTabRèglesInv) = "Absence de la " & Chr(171) & " Dénomination de la Fraction " & Chr(187)
            IndexTabRèglesInv = IndexTabRèglesInv + 1
        End If
        ' ==================================
        ' Absence de la « Dénomination de la Méthode ».
        If RègleStru24 = True Then
            IndexRègleStructurelle = IndexRègleStructurelle + 1
            RèglesStructurelles.DataGridView1.Rows.Add()
            RèglesStructurelles.DataGridView1.Item(NomColonne, IndexRègleStructurelle).Value = "Absence de la " & Chr(171) & " Dénomination de la Méthode " & Chr(187)
            Variables.TabRèglesInv(IndexTabRèglesInv) = "Absence de la " & Chr(171) & " Dénomination de la Méthode " & Chr(187)
            IndexTabRèglesInv = IndexTabRèglesInv + 1
        End If




        ' ==================================
        ' Absence du « Résultat Numérique ».
        If RègleStru25 = True Then
            IndexRègleStructurelle = IndexRègleStructurelle + 1
            RèglesStructurelles.DataGridView1.Rows.Add()
            RèglesStructurelles.DataGridView1.Item(NomColonne, IndexRègleStructurelle).Value = "Absence du " & Chr(171) & " Résultat Numérique " & Chr(187)
            Variables.TabRèglesInv(IndexTabRèglesInv) = "Absence du " & Chr(171) & " Résultat Numérique " & Chr(187)
            IndexTabRèglesInv = IndexTabRèglesInv + 1
        End If



        ' ==================================
        ' Absence du « Code Résultat Qualitatif ».
        If RègleStru31 = True Then
            IndexRègleStructurelle = IndexRègleStructurelle + 1
            RèglesStructurelles.DataGridView1.Rows.Add()
            RèglesStructurelles.DataGridView1.Item(NomColonne, IndexRègleStructurelle).Value = "Absence du " & Chr(171) & " Code Résultat Qualitatif " & Chr(187)
            Variables.TabRèglesInv(IndexTabRèglesInv) = "Absence du " & Chr(171) & " Code Résultat Qualitatif " & Chr(187)
            IndexTabRèglesInv = IndexTabRèglesInv + 1
        End If



        ' ==================================
        ' Présence du signe « < » dans la valeur du Résultat.
        If RègleStru26 = True Then
            IndexRègleStructurelle = IndexRègleStructurelle + 1
            RèglesStructurelles.DataGridView1.Rows.Add()
            RèglesStructurelles.DataGridView1.Item(NomColonne, IndexRègleStructurelle).Value = "Présence du signe " & Chr(171) & " < " & Chr(187) & " dans la valeur du Résultat"
            Variables.TabRèglesInv(IndexTabRèglesInv) = "Présence du signe " & Chr(171) & " < " & Chr(187) & " dans la valeur du Résultat"
            IndexTabRèglesInv = IndexTabRèglesInv + 1
        End If
        ' ==================================
        ' Absence de l’« Unité du Résultat ».
        If RègleStru27 = True Then
            IndexRègleStructurelle = IndexRègleStructurelle + 1
            RèglesStructurelles.DataGridView1.Rows.Add()
            RèglesStructurelles.DataGridView1.Item(NomColonne, IndexRègleStructurelle).Value = "Absence de l’ " & Chr(171) & " Unité du Résultat " & Chr(187)
            Variables.TabRèglesInv(IndexTabRèglesInv) = "Absence de l’ " & Chr(171) & " Unité du Résultat " & Chr(187)
            IndexTabRèglesInv = IndexTabRèglesInv + 1
        End If
        ' ==================================
        ' Absence de la « Dénomination de l’Analyseur ».
        If RègleStru28 = True Then
            IndexRègleStructurelle = IndexRègleStructurelle + 1
            RèglesStructurelles.DataGridView1.Rows.Add()
            RèglesStructurelles.DataGridView1.Item(NomColonne, IndexRègleStructurelle).Value = "Absence du " & Chr(171) & " Libellé de l’Analyste " & Chr(187)
            Variables.TabRèglesInv(IndexTabRèglesInv) = "Absence du " & Chr(171) & " Libellé de l’Analyste " & Chr(187)
            IndexTabRèglesInv = IndexTabRèglesInv + 1
        End If
        ' ==================================
        ' Absence du « Code Remarque ».
        If RègleStru29 = True Then
            IndexRègleStructurelle = IndexRègleStructurelle + 1
            RèglesStructurelles.DataGridView1.Rows.Add()
            RèglesStructurelles.DataGridView1.Item(NomColonne, IndexRègleStructurelle).Value = "Absence du " & Chr(171) & " Code Remarque " & Chr(187)
            Variables.TabRèglesInv(IndexTabRèglesInv) = "Absence du " & Chr(171) & " Code Remarque " & Chr(187)
            IndexTabRèglesInv = IndexTabRèglesInv + 1
        End If
        ' ==================================
        ' Présence d’une « Valeur de Précision » sans le « Type de Précision ».
        If RègleStru30 = True Then
            IndexRègleStructurelle = IndexRègleStructurelle + 1
            RèglesStructurelles.DataGridView1.Rows.Add()
            RèglesStructurelles.DataGridView1.Item(NomColonne, IndexRègleStructurelle).Value = "Présence de la " & Chr(171) & " Valeur de Précision " & Chr(187) & " sans le " & Chr(171) & " Type de Précision " & Chr(187)
            Variables.TabRèglesInv(IndexTabRèglesInv) = "Présence de la " & Chr(171) & " Valeur de Précision " & Chr(187) & " sans le " & Chr(171) & " Type de Précision " & Chr(187)
            IndexTabRèglesInv = IndexTabRèglesInv + 1
        End If

        ' ==================================
        ' Sortie si pas de paramètre à sauvegarder
        If Variables.PrésenceParamètre = False Then
            MsgBox(" Pas d'informations pour la création du fichier Quadrige² IFREMER !", MsgBoxStyle.Exclamation, " QUADRISPEL - Erreur de création")
            Exit Sub
        End If

        ' ==================================
        ' Message de non possibilité de sauvegarde s'il y a lieu et affichage de la fenêtre des Règles Structurelles non valides
        ' ==================================
        ' Traitement si au moins une erreur a été décelée
        '     If Variables.ArretSauve = True Then
        '         MsgBox(" Au moins une règle structurelle QUADRIGE² IFREMER n'a pas été respectée !", MsgBoxStyle.Exclamation, " QUADRISPEL - Erreur de règle structurelle")
        '     End If


        ' ==================================
        ' Affichage de la fenêtre des Règles Structurelles non valides
        '       RèglesStructurelles.ShowDialog()

        Exit Sub

        ' ==================================
        ' Zone d'inscription de toutes les règles structurelles pour tester la sortie des messages d'information lorsque l'on clique dessus
        RèglesStructurelles.DataGridView1.Rows.Add()
        RèglesStructurelles.DataGridView1.Item(NomColonne, 0).Value = "Absence de la " & Chr(171) & " Dénomination du Lieu de Surveillance " & Chr(187)
        RèglesStructurelles.DataGridView1.Rows.Add()
        RèglesStructurelles.DataGridView1.Item(NomColonne, 1).Value = "Absence de la " & Chr(171) & " Dénomination du Réseau " & Chr(187)
        RèglesStructurelles.DataGridView1.Rows.Add()
        RèglesStructurelles.DataGridView1.Item(NomColonne, 2).Value = "Absence du " & Chr(171) & " Libellé du Saisisseur " & Chr(187)
        RèglesStructurelles.DataGridView1.Rows.Add()
        RèglesStructurelles.DataGridView1.Item(NomColonne, 3).Value = "Absence de la " & Chr(171) & " Date de Passage " & Chr(187)
        RèglesStructurelles.DataGridView1.Rows.Add()
        RèglesStructurelles.DataGridView1.Item(NomColonne, 4).Value = "Présence d’une " & Chr(171) & " Valeur de Sonde " & Chr(187) & " sans " & Chr(171) & " Unité de Sonde " & Chr(187)
        RèglesStructurelles.DataGridView1.Rows.Add()
        RèglesStructurelles.DataGridView1.Item(NomColonne, 5).Value = "Absence de " & Chr(171) & " Positionnement Passage " & Chr(187) & " avec présence de la " & Chr(171) & " Latitude Passage " & Chr(187) & " ou de la " & Chr(171) & " Longitude Passage " & Chr(187) & " ou des deux"
        RèglesStructurelles.DataGridView1.Rows.Add()
        RèglesStructurelles.DataGridView1.Item(NomColonne, 6).Value = "Absence de la " & Chr(171) & " Dénomination de l’Engin de Prélèvement " & Chr(187)
        RèglesStructurelles.DataGridView1.Rows.Add()
        RèglesStructurelles.DataGridView1.Item(NomColonne, 7).Value = "Absence du " & Chr(171) & " Libellé du Préleveur " & Chr(187)
        RèglesStructurelles.DataGridView1.Rows.Add()
        RèglesStructurelles.DataGridView1.Item(NomColonne, 8).Value = "Présence de l’ " & Chr(171) & " Immersion de Prélèvement " & Chr(187) & " avec l' " & Chr(171) & " Immersion Max " & Chr(187) & " ou avec l’ " & Chr(171) & " Immersion Min " & Chr(187) & " ou sans l’ " & Chr(171) & " Unité d'Immersion " & Chr(187)
        RèglesStructurelles.DataGridView1.Rows.Add()
        RèglesStructurelles.DataGridView1.Item(NomColonne, 9).Value = "Présence de l’ " & Chr(171) & " Immersion Max de Prélèvement " & Chr(187) & " sans l' " & Chr(171) & " Immersion Min de Prélèvement " & Chr(187) & " ou sans l’ " & Chr(171) & " Unité d'Immersion " & Chr(187)
        RèglesStructurelles.DataGridView1.Rows.Add()
        RèglesStructurelles.DataGridView1.Item(NomColonne, 10).Value = "Présence de l’ " & Chr(171) & " Immersion Min de Prélèvement " & Chr(187) & " sans l' " & Chr(171) & " Immersion Max de Prélèvement " & Chr(187) & " ou sans l’ " & Chr(171) & " Unité d'Immersion " & Chr(187)
        RèglesStructurelles.DataGridView1.Rows.Add()
        RèglesStructurelles.DataGridView1.Item(NomColonne, 11).Value = "Présence de la " & Chr(171) & " Taille de Prélèvement " & Chr(187) & " sans l' " & Chr(171) & " Unité de Taille de Prélèvement " & Chr(187)
        RèglesStructurelles.DataGridView1.Rows.Add()
        RèglesStructurelles.DataGridView1.Item(NomColonne, 12).Value = "Absence de " & Chr(171) & " Positionnement de Prélèvement " & Chr(187) & " avec présence de la " & Chr(171) & " Latitude " & Chr(187) & " ou de la " & Chr(171) & " Longitude de Prélèvement " & Chr(187) & " ou des deux"
        RèglesStructurelles.DataGridView1.Rows.Add()
        RèglesStructurelles.DataGridView1.Item(NomColonne, 13).Value = "Absence de la " & Chr(171) & " Dénomination du Support Échantillon " & Chr(187)
        RèglesStructurelles.DataGridView1.Rows.Add()
        RèglesStructurelles.DataGridView1.Item(NomColonne, 14).Value = "Présence du " & Chr(171) & " Taxon Support Échantillon " & Chr(187) & " et du " & Chr(171) & " Groupe Taxon Support Échantillon " & Chr(187)
        RèglesStructurelles.DataGridView1.Rows.Add()
        RèglesStructurelles.DataGridView1.Item(NomColonne, 15).Value = "Absence du " & Chr(171) & " Numéro d’Échantillon " & Chr(187)
        RèglesStructurelles.DataGridView1.Rows.Add()
        RèglesStructurelles.DataGridView1.Item(NomColonne, 16).Value = "Présence de la " & Chr(171) & " Taille d’Échantillon " & Chr(187) & " sans l' " & Chr(171) & " Unité de Taille d’Échantillon " & Chr(187)
        RèglesStructurelles.DataGridView1.Rows.Add()
        RèglesStructurelles.DataGridView1.Item(NomColonne, 17).Value = "Absence du " & Chr(171) & " Niveau de Saisie Résultat " & Chr(187)
        RèglesStructurelles.DataGridView1.Rows.Add()
        RèglesStructurelles.DataGridView1.Item(NomColonne, 18).Value = "Absence du " & Chr(171) & " Code Sandre Paramètre " & Chr(187)
        RèglesStructurelles.DataGridView1.Rows.Add()
        RèglesStructurelles.DataGridView1.Item(NomColonne, 19).Value = "Absence de la " & Chr(171) & " Dénomination du Paramètre " & Chr(187)
        RèglesStructurelles.DataGridView1.Rows.Add()
        RèglesStructurelles.DataGridView1.Item(NomColonne, 20).Value = "Absence de la " & Chr(171) & " Dénomination du Support " & Chr(187)
        RèglesStructurelles.DataGridView1.Rows.Add()
        RèglesStructurelles.DataGridView1.Item(NomColonne, 21).Value = "Différence entre la " & Chr(171) & " Dénomination Support Échantillon " & Chr(187) & " et une " & Chr(171) & " Dénomination du Support " & Chr(187)
        RèglesStructurelles.DataGridView1.Rows.Add()
        RèglesStructurelles.DataGridView1.Item(NomColonne, 22).Value = "Absence de la " & Chr(171) & " Dénomination de la Fraction " & Chr(187)
        RèglesStructurelles.DataGridView1.Rows.Add()
        RèglesStructurelles.DataGridView1.Item(NomColonne, 23).Value = "Absence de la " & Chr(171) & " Dénomination de la Méthode " & Chr(187)
        RèglesStructurelles.DataGridView1.Rows.Add()
        RèglesStructurelles.DataGridView1.Item(NomColonne, 24).Value = "Absence du " & Chr(171) & " Résultat " & Chr(187)
        RèglesStructurelles.DataGridView1.Rows.Add()
        RèglesStructurelles.DataGridView1.Item(NomColonne, 25).Value = "Présence du signe " & Chr(171) & " < " & Chr(187) & " dans la valeur du Résultat"
        RèglesStructurelles.DataGridView1.Rows.Add()
        RèglesStructurelles.DataGridView1.Item(NomColonne, 26).Value = "Absence de l’ " & Chr(171) & " Unité du Résultat " & Chr(187)
        RèglesStructurelles.DataGridView1.Rows.Add()
        RèglesStructurelles.DataGridView1.Item(NomColonne, 27).Value = "Absence du " & Chr(171) & " Libellé de l’Analyste " & Chr(187)
        RèglesStructurelles.DataGridView1.Rows.Add()
        RèglesStructurelles.DataGridView1.Item(NomColonne, 28).Value = "Absence du " & Chr(171) & " Code Remarque " & Chr(187)
        '       RèglesStructurelles.DataGridView1.Rows.Add()
        RèglesStructurelles.DataGridView1.Item(NomColonne, 29).Value = "Présence de la " & Chr(171) & " Valeur de Précision " & Chr(187) & " sans le " & Chr(171) & " Type de Précision " & Chr(187)


    End Sub

    '=============================================================================================================================================
    ' Visualisation Fichier Sauvegarde QuadriSPEL
    Private Sub VisualisationToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles VisualisationToolStripMenuItem.Click
        ' ==================================
        ' Définition des variables
        Dim NuméroLigne As Integer = -1
        Dim NomColonne As String = ""
        Dim i As Integer = 0
        Dim j As Integer = 0
        Dim k As Integer = 0
        Dim ii As Integer
        Dim Sandre As Integer = 0
        Dim NombreIndex As Integer = 0
        Dim NombreParamètre As Integer = 0
        Dim PremierParam As Integer = 0
        Dim IndexTabInfo As Integer = 0
        Dim CodeSandreLieuSurveillance As String = ""
        Dim CodeSandreRéseau As String = ""
        Dim CodeSandreSaisisseur As String = ""
        Dim CodeSandreUnitéSonde As String = ""
        Dim CodeSandrePositionnementPassage As String = ""
        Dim CodeSandreNiveauPrélèvement As String = ""
        Dim CodeSandrePréleveur As String = ""
        Dim CodeSandreUnitéImmersion As String = ""
        Dim CodeSandreUnitéTaillePrélèvement As String = ""
        Dim CodeSandrePositionnementPrélèvement As String = ""
        Dim CodeSandreSupportÉchantillon As String = ""
        Dim CodeSandreGroupeTaxon As String = ""
        Dim CodeSandreUnitéTailleÉchantillon As String = ""
        Dim CodeSandre As String = ""
        Dim EnginPrélèvement As Integer = 0
        Dim EntréeSupport As Integer = 0
        Dim Fraction As Integer = 0
        Dim Méthode As Integer = 0
        Dim Unité As Integer = 0
        Dim CodeRemarque As Integer = 0
        Dim Analyste As Integer = 0
        Dim EnginAnalyse As Integer = 0
        '        Dim Précision As Integer = 0
        Dim TypePrécision As Integer = 0
        Dim NiveauSaisie As Integer = 0

        ' ==================================
        ' Recherche du Code Sandre du LIEU DE SURVEILLANCE
        ' ==================================
        ' Recherche du point d'entrée dans le tableau des informations
        For i = 0 To 5000
            If Variables.TabRéfInfo(i, 0) = "LIEU DE SURVEILLANCE" Then
                IndexTabInfo = i + 1
                Exit For
            End If
        Next
        ' ==================================
        ' Recherche du Code Sandre 
        For i = IndexTabInfo To 5000
            ' ==================================
            ' Sortie de la boucle si fin de tableau
            If Variables.TabRéfInfo(i, 0) = "" Then Exit For
            ' ==================================
            If Variables.TabRéfInfo(i, 1) = Passage.TextBox24.Text Then
                ' ==================================
                ' Récupération du Code Sandre
                CodeSandreLieuSurveillance = Variables.TabRéfInfo(i, 2)
                Exit For
            End If
        Next

        ' ==================================
        ' Recherche du Code Sandre du RÉSEAU
        ' ==================================
        ' Recherche du point d'entrée dans le tableau des informations
        For i = 0 To 5000
            If Variables.TabRéfInfo(i, 0) = "RÉSEAU" Then
                IndexTabInfo = i + 1
                Exit For
            End If
        Next
        ' ==================================
        ' Recherche du Code Sandre 
        For i = IndexTabInfo To 5000
            ' ==================================
            ' Sortie de la boucle si fin de tableau
            If Variables.TabRéfInfo(i, 0) = "" Then Exit For
            ' ==================================
            If Variables.TabRéfInfo(i, 0) = Passage.TextBox3.Text Then
                ' ==================================
                ' Récupération du Code Sandre
                CodeSandreRéseau = Variables.TabRéfInfo(i, 1)
                Exit For
            End If
        Next

        ' ==================================
        ' Recherche du Code Sandre du SAISISSEUR
        ' ==================================
        ' Recherche du point d'entrée dans le tableau des informations
        For i = 0 To 5000
            If Variables.TabRéfInfo(i, 0) = "SAISISSEUR" Then
                IndexTabInfo = i + 1
                Exit For
            End If
        Next
        ' ==================================
        ' Recherche du Code Sandre 
        For i = IndexTabInfo To 5000
            ' ==================================
            ' Sortie de la boucle si fin de tableau
            If Variables.TabRéfInfo(i, 0) = "" Then Exit For
            ' ==================================
            If Variables.TabRéfInfo(i, 0) = Passage.TextBox23.Text Then
                ' ==================================
                ' Récupération du Code Sandre
                CodeSandreSaisisseur = Variables.TabRéfInfo(i, 2)
                Exit For
            End If
        Next

        ' ==================================
        ' Recherche du Code Sandre UNITÉ SONDE
        ' ==================================
        ' Recherche du point d'entrée dans le tableau des informations
        For i = 0 To 5000
            If Variables.TabRéfInfo(i, 0) = "UNITÉ SONDE" Then
                IndexTabInfo = i + 1
                Exit For
            End If
        Next
        ' ==================================
        ' Recherche du Code Sandre 
        For i = IndexTabInfo To 5000
            ' ==================================
            ' Sortie de la boucle si fin de tableau
            If Variables.TabRéfInfo(i, 0) = "" Then Exit For
            ' ==================================
            If Variables.TabRéfInfo(i, 0) = Passage.TextBox15.Text Then
                ' ==================================
                ' Récupération du Code Sandre
                CodeSandreUnitéSonde = Variables.TabRéfInfo(i, 1)
                Exit For
            End If
        Next

        ' ==================================
        ' Recherche du Code Sandre POSITIONNEMENT PASSAGE
        ' ==================================
        ' Recherche du point d'entrée dans le tableau des informations
        For i = 0 To 5000
            If Variables.TabRéfInfo(i, 0) = "POSITIONNEMENT PASSAGE" Then
                IndexTabInfo = i + 1
                Exit For
            End If
        Next
        ' ==================================
        ' Recherche du Code Sandre 
        For i = IndexTabInfo To 5000
            ' ==================================
            ' Sortie de la boucle si fin de tableau
            If Variables.TabRéfInfo(i, 0) = "" Then Exit For
            ' ==================================
            If Variables.TabRéfInfo(i, 0) = Passage.TextBox19.Text Then
                ' ==================================
                ' Récupération du Code Sandre
                CodeSandrePositionnementPassage = Variables.TabRéfInfo(i, 1)
                Exit For
            End If
        Next

        ' ==================================
        ' Recherche du Code Sandre NIVEAU PRÉLÈVEMENT
        ' ==================================
        ' Recherche du point d'entrée dans le tableau des informations
        For i = 0 To 5000
            If Variables.TabRéfInfo(i, 0) = "NIVEAU PRÉLÈVEMENT" Then
                IndexTabInfo = i + 1
                Exit For
            End If
        Next
        ' ==================================
        ' Recherche du Code Sandre 
        For i = IndexTabInfo To 5000
            ' ==================================
            ' Sortie de la boucle si fin de tableau
            If Variables.TabRéfInfo(i, 0) = "" Then Exit For
            ' ==================================
            If Variables.TabRéfInfo(i, 0) = Prélèvement.TextBox1.Text Then
                ' ==================================
                ' Récupération du Code Sandre
                CodeSandreNiveauPrélèvement = Variables.TabRéfInfo(i, 1)
                Exit For
            End If
        Next

        ' ==================================
        ' Recherche du Code Sandre PRÉLEVEUR
        ' ==================================
        ' Recherche du point d'entrée dans le tableau des informations
        For i = 0 To 5000
            If Variables.TabRéfInfo(i, 0) = "PRÉLEVEUR" Then
                IndexTabInfo = i + 1
                Exit For
            End If
        Next
        ' ==================================
        ' Recherche du Code Sandre 
        For i = IndexTabInfo To 5000
            ' ==================================
            ' Sortie de la boucle si fin de tableau
            If Variables.TabRéfInfo(i, 0) = "" Then Exit For
            ' ==================================
            If Variables.TabRéfInfo(i, 0) = Prélèvement.TextBox23.Text Then
                ' ==================================
                ' Récupération du Code Sandre
                CodeSandrePréleveur = Variables.TabRéfInfo(i, 2)
                Exit For
            End If
        Next

        ' ==================================
        ' Recherche du Code Sandre UNITÉ IMMERSION
        ' ==================================
        ' Recherche du point d'entrée dans le tableau des informations
        For i = 0 To 5000
            If Variables.TabRéfInfo(i, 0) = "UNITÉ IMMERSION" Then
                IndexTabInfo = i + 1
                Exit For
            End If
        Next
        ' ==================================
        ' Recherche du Code Sandre 
        For i = IndexTabInfo To 5000
            ' ==================================
            ' Sortie de la boucle si fin de tableau
            If Variables.TabRéfInfo(i, 0) = "" Then Exit For
            ' ==================================
            If Variables.TabRéfInfo(i, 0) = Prélèvement.TextBox7.Text Then
                ' ==================================
                ' Récupération du Code Sandre
                CodeSandreUnitéImmersion = Variables.TabRéfInfo(i, 1)
                Exit For
            End If
        Next

        ' ==================================
        ' Recherche du Code Sandre UNITÉ TAILLE PRÉLÈVEMENT
        ' ==================================
        ' Recherche du point d'entrée dans le tableau des informations
        For i = 0 To 5000
            If Variables.TabRéfInfo(i, 0) = "UNITÉ TAILLE PRÉLÈVEMENT" Then
                IndexTabInfo = i + 1
                Exit For
            End If
        Next
        ' ==================================
        ' Recherche du Code Sandre 
        For i = IndexTabInfo To 5000
            ' ==================================
            ' Sortie de la boucle si fin de tableau
            If Variables.TabRéfInfo(i, 0) = "" Then Exit For
            ' ==================================
            If Variables.TabRéfInfo(i, 0) = Prélèvement.TextBox10.Text Then
                ' ==================================
                ' Récupération du Code Sandre
                CodeSandreUnitéTaillePrélèvement = Variables.TabRéfInfo(i, 1)
                Exit For
            End If
        Next

        ' ==================================
        ' Recherche du Code Sandre POSITIONNEMENT PRÉLÈVEMENT
        ' ==================================
        ' Recherche du point d'entrée dans le tableau des informations
        For i = 0 To 5000
            If Variables.TabRéfInfo(i, 0) = "POSITIONNEMENT PRÉLÈVEMENT" Then
                IndexTabInfo = i + 1
                Exit For
            End If
        Next
        ' ==================================
        ' Recherche du Code Sandre 
        For i = IndexTabInfo To 5000
            ' ==================================
            ' Sortie de la boucle si fin de tableau
            If Variables.TabRéfInfo(i, 0) = "" Then Exit For
            ' ==================================
            If Variables.TabRéfInfo(i, 0) = Prélèvement.TextBox16.Text Then
                ' ==================================
                ' Récupération du Code Sandre
                CodeSandrePositionnementPrélèvement = Variables.TabRéfInfo(i, 1)
                Exit For
            End If
        Next

        ' ==================================
        ' Recherche du Code Sandre SUPPORT ÉCHANTILLON
        ' ==================================
        ' Recherche du point d'entrée dans le tableau des informations
        For i = 0 To 5000
            If Variables.TabRéfInfo(i, 0) = "SUPPORT ÉCHANTILLON" Then
                IndexTabInfo = i + 1
                Exit For
            End If
        Next
        ' ==================================
        ' Recherche du Code Sandre 
        For i = IndexTabInfo To 5000
            ' ==================================
            ' Sortie de la boucle si fin de tableau
            If Variables.TabRéfInfo(i, 0) = "" Then Exit For
            ' ==================================
            If Variables.TabRéfInfo(i, 0) = Échantillon.TextBox2.Text Then
                ' ==================================
                ' Récupération du Code Sandre
                CodeSandreSupportÉchantillon = Variables.TabRéfInfo(i, 1)
                Exit For
            End If
        Next

        ' ==================================
        ' Recherche du Code Sandre GROUPE TAXON SUPPORT ÉCHANTILLON
        ' ==================================
        ' Recherche du point d'entrée dans le tableau des informations
        For i = 0 To 5000
            If Variables.TabRéfInfo(i, 0) = "GROUPE TAXON SUPPORT ÉCHANTILLON" Then
                IndexTabInfo = i + 1
                Exit For
            End If
        Next
        ' ==================================
        ' Recherche du Code Sandre 
        For i = IndexTabInfo To 5000
            ' ==================================
            ' Sortie de la boucle si fin de tableau
            If Variables.TabRéfInfo(i, 0) = "" Then Exit For
            ' ==================================
            If Variables.TabRéfInfo(i, 0) = Échantillon.TextBox4.Text Then
                ' ==================================
                ' Récupération du Code Sandre
                CodeSandreGroupeTaxon = Variables.TabRéfInfo(i, 1)
                Exit For
            End If
        Next

        ' ==================================
        ' Recherche du Code Sandre UNITÉ TAILLE ÉCHANTILLON
        ' ==================================
        ' Recherche du point d'entrée dans le tableau des informations
        For i = 0 To 5000
            If Variables.TabRéfInfo(i, 0) = "UNITÉ TAILLE ÉCHANTILLON" Then
                IndexTabInfo = i + 1
                Exit For
            End If
        Next
        ' ==================================
        ' Recherche du Code Sandre 
        For i = IndexTabInfo To 5000
            ' ==================================
            ' Sortie de la boucle si fin de tableau
            If Variables.TabRéfInfo(i, 0) = "" Then Exit For
            ' ==================================
            If Variables.TabRéfInfo(i, 0) = Échantillon.TextBox7.Text Then
                ' ==================================
                ' Récupération du Code Sandre
                CodeSandreUnitéTailleÉchantillon = Variables.TabRéfInfo(i, 1)
                Exit For
            End If
        Next


        ' ==================================
        ' Recherche du point d'entrée NIVEAU SAISIE dans le tableau des informations
        ' ==================================
        For i = 0 To 5000
            ' ==================================
            If Variables.TabRéfInfo(i, 0) = "NIVEAU SAISIE" Then
                NiveauSaisie = i + 1
                Exit For
            End If
        Next

        ' ==================================
        ' Recherche du point d'entrée ENGIN DE PRÉLÈVEMENT dans le tableau des informations
        ' ==================================
        For i = 0 To 5000
            ' ==================================
            If Variables.TabRéfInfo(i, 0) = "ENGIN PRÉLÈVEMENT" Then
                EnginPrélèvement = i + 1
                Exit For
            End If
        Next

        ' ==================================
        ' Recherche du point d'entrée SUPPORT dans le tableau des informations
        ' ==================================
        For i = 0 To 5000
            ' ==================================
            If Variables.TabRéfInfo(i, 0) = "SUPPORT" Then
                EntréeSupport = i + 1
                Exit For
            End If
        Next

        ' ==================================
        ' Recherche du point d'entrée FRACTION dans le tableau des informations
        ' ==================================
        For i = 0 To 5000
            ' ==================================
            If Variables.TabRéfInfo(i, 0) = "FRACTION" Then
                Fraction = i + 1
                Exit For
            End If
        Next

        ' ==================================
        ' Recherche du point d'entrée MÉTHODE dans le tableau des informations
        ' ==================================
        For i = 0 To 5000
            ' ==================================
            If Variables.TabRéfInfo(i, 0) = "MÉTHODE" Then
                Méthode = i + 1
                Exit For
            End If
        Next

        ' ==================================
        ' Recherche du point d'entrée UNITÉ dans le tableau des informations
        ' ==================================
        For i = 0 To 5000
            ' ==================================
            If Variables.TabRéfInfo(i, 0) = "UNITÉ" Then
                Unité = i + 1
                Exit For
            End If
        Next

        ' ==================================
        ' Recherche du point d'entrée ANALYSEUR dans le tableau des informations
        ' ==================================
        For i = 0 To 5000
            ' ==================================
            If Variables.TabRéfInfo(i, 0) = "ANALYSEUR" Then
                Analyste = i + 1
                Exit For
            End If
        Next

        ' ==================================
        ' Recherche du point d'entrée ENGIN ANALYSE dans le tableau des informations
        ' ==================================
        For i = 0 To 5000
            ' ==================================
            If Variables.TabRéfInfo(i, 0) = "ENGIN ANALYSE" Then
                EnginAnalyse = i + 1
                Exit For
            End If
        Next

        ' ==================================
        ' Recherche du point d'entrée CODE REMARQUE dans le tableau des informations
        ' ==================================
        For i = 0 To 5000
            ' ==================================
            If Variables.TabRéfInfo(i, 0) = "CODE REMARQUE" Then
                CodeRemarque = i + 1
                Exit For
            End If
        Next

        ' ==================================
        ' Recherche du point d'entrée PRÉCISION dans le tableau des informations
        ' ==================================
        '        For i = 0 To 5000
        ' ==================================
        '        If Variables.TabRéfInfo(i, 0) = "PRÉCISION" Then
        '        Précision = i + 1
        '        Exit For
        '        End If
        '        Next

        ' ==================================
        ' Recherche du point d'entrée TYPE PRÉCISION dans le tableau des informations
        ' ==================================
        For i = 0 To 5000
            ' ==================================
            If Variables.TabRéfInfo(i, 0) = "TYPE PRÉCISION" Then
                TypePrécision = i + 1
                Exit For
            End If
        Next

        ' ==================================
        ' Traitement du DATAGRIDVIEW
        ' ==================================
        ' Effacement des lignes du DataGridView
        DataGridView1.Rows.Clear()

        ' ==================================
        ' Initialisation de la variable de l'ancienne couleur de la ligne
        Variables.AncienneLigne = 0

        ' ==================================
        ' Balayage des noms des Familles sélectionnées dans la feuille des Familles
        For i = 0 To Familles.ListBox2.Items.Count - 1
            ' ==================================
            ' Recherche de la première adresse de la Famille sélectionnée
            For j = 0 To 500
                If LTrim(Familles.ListBox2.Items(i)) = Variables.TabRéfPara(j, 0) Then
                    PremierParam = j
                    Exit For
                End If
            Next
            ' ==================================
            ' Initialisation des indicateurs
            NombreIndex = 0
            NombreParamètre = 0
            ' ==================================
            ' Recherche du nombre de Paramètres et du nombre de Paramètres indexés pour la Famille sélectionnée
            For j = PremierParam To 500
                ' ==================================
                ' Sortie si tous les Paramètres de la Famille sélectionnée ont été balayés
                If LTrim(Familles.ListBox2.Items(i)) <> Variables.TabRéfPara(j, 0) Then
                    Exit For
                End If
                ' ==================================
                ' Incrémentation de l'indicateur du nombre de Paramètres indexés pour la Famille sélectionnée
                If Val(Variables.TabRéfPara(j, 21)) <> 0 Then
                    NombreIndex = NombreIndex + 1
                End If
                ' ==================================
                ' Incrémentation de l'indicateur du nombre de Paramètres pour la Famille sélectionnée
                NombreParamètre = NombreParamètre + 1
            Next

            ' ==================================
            ' Balayage des Paramètres pour le transfert des informations du paramètre sélectionné de la famille sélectionnée dans la Feuille des Paramètres
            For k = 1 To NombreIndex
                ' ==================================
                ' Balayage des index pour le transfert des informations du paramètre sélectionné de la famille sélectionnée dans la Feuille des Paramètres
                For j = PremierParam To PremierParam + NombreParamètre - 1
                    ' ==================================
                    ' Recherche du Paramètre dont l'index correspond à l'index recherché
                    If Variables.TabRéfPara(j, 21) = k Then
                        ' ==================================
                        ' Incrémentation du DataGridView d'une ligne
                        DataGridView1.Rows.Add()
                        ' ==================================
                        ' Incrémentation du pointeur de ligne
                        NuméroLigne = NuméroLigne + 1
                        ' ==================================
                        ' Numéro Ligne 1
                        NomColonne = "ColNumLigne" : DataGridView1.Item(NomColonne, NuméroLigne).Value = NuméroLigne + 1
                        ' ==================================
                        ' Code Sandre Lieu de Surveillance 2
                        NomColonne = "ColLieuSurv" : DataGridView1.Item(NomColonne, NuméroLigne).Value = CodeSandreLieuSurveillance
                        ' ==================================
                        ' Code Sandre Réseau 3
                        NomColonne = "ColRéseau" : DataGridView1.Item(NomColonne, NuméroLigne).Value = CodeSandreRéseau
                        ' ==================================
                        ' Code Sandre Saisisseur 4
                        NomColonne = "ColSaissi" : DataGridView1.Item(NomColonne, NuméroLigne).Value = CodeSandreSaisisseur
                        ' ==================================
                        ' Zone Destination Dragage 5
                        NomColonne = "ColZoneDestDrag" : DataGridView1.Item(NomColonne, NuméroLigne).Value = Passage.TextBox5.Text
                        ' ==================================
                        ' Campagne 6
                        NomColonne = "ColCampagne" : DataGridView1.Item(NomColonne, NuméroLigne).Value = Passage.TextBox7.Text
                        ' ==================================
                        ' Sortie 7
                        NomColonne = "ColSortie" : DataGridView1.Item(NomColonne, NuméroLigne).Value = Passage.TextBox9.Text
                        ' ==================================
                        ' Date Passage 8
                        NomColonne = "ColDatePassage" : DataGridView1.Item(NomColonne, NuméroLigne).Value = Passage.TextBox11.Text
                        ' ==================================
                        ' Heure Passage 9
                        NomColonne = "ColHeurePassage" : DataGridView1.Item(NomColonne, NuméroLigne).Value = Passage.TextBox12.Text
                        ' ==================================
                        ' Sonde 10
                        NomColonne = "ColSonde" : DataGridView1.Item(NomColonne, NuméroLigne).Value = Passage.TextBox14.Text
                        ' ==================================
                        ' Code Sandre Unité Sonde 11
                        NomColonne = "ColUnitéSonde" : DataGridView1.Item(NomColonne, NuméroLigne).Value = CodeSandreUnitéSonde
                        ' ==================================
                        ' Mnémonique Passage 12
                        NomColonne = "ColMnémoPassage" : DataGridView1.Item(NomColonne, NuméroLigne).Value = Passage.TextBox17.Text
                        ' ==================================
                        ' Commentaires Passage 13
                        NomColonne = "ColCommentPassage" : DataGridView1.Item(NomColonne, NuméroLigne).Value = Passage.TextBox18.Text
                        ' ==================================
                        ' Latitude Passage 14
                        NomColonne = "ColLatitudePass" : DataGridView1.Item(NomColonne, NuméroLigne).Value = Passage.TextBox1.Text
                        ' ==================================
                        ' Longitude Passage 15
                        NomColonne = "ColLongitudePass" : DataGridView1.Item(NomColonne, NuméroLigne).Value = Passage.TextBox2.Text
                        ' ==================================
                        ' Code Sandre Positionnement Passage 16
                        NomColonne = "ColPosition" : DataGridView1.Item(NomColonne, NuméroLigne).Value = CodeSandrePositionnementPassage
                        ' ==================================
                        ' Nombre Individu Passage 17
                        NomColonne = "ColNombrePass" : DataGridView1.Item(NomColonne, NuméroLigne).Value = Passage.TextBox20.Text
                        ' ==================================
                        ' Code Sandre Engin Prélèvement 18
                        ' ==================================
                        ' Recherche du Code Sandre correspondant
                        For Sandre = EnginPrélèvement To 5000
                            ' ==================================
                            ' Sortie de la boucle si fin de tableau
                            If Variables.TabRéfInfo(Sandre, 0) = "" Then Exit For
                            ' ==================================
                            If Variables.TabRéfInfo(Sandre, 0) = Variables.TabRéfPara(j, 10) Then
                                ' ==================================
                                ' Récupération du Code Sandre
                                NomColonne = "ColEnginPrlvmt" : DataGridView1.Item(NomColonne, NuméroLigne).Value = Variables.TabRéfInfo(Sandre, 1)
                                Exit For
                            End If
                        Next
                        ' ==================================
                        ' Code Sandre Niveau Prélèvement 19
                        NomColonne = "ColNiveauPrlvmt" : DataGridView1.Item(NomColonne, NuméroLigne).Value = CodeSandreNiveauPrélèvement
                        ' ==================================
                        ' Code Sandre Préleveur 20
                        NomColonne = "ColPréleveur" : DataGridView1.Item(NomColonne, NuméroLigne).Value = CodeSandrePréleveur
                        ' ==================================
                        ' Mnémonique Prélèvement 21 
                        NomColonne = "ColMnémoPrlvmt" : DataGridView1.Item(NomColonne, NuméroLigne).Value = Prélèvement.TextBox2.Text
                        ' ==================================
                        ' Immersion Prélèvement 22
                        NomColonne = "ColImmPrlvmt" : DataGridView1.Item(NomColonne, NuméroLigne).Value = Prélèvement.TextBox3.Text
                        ' ==================================
                        ' Immersion Max Prélèvement 23
                        NomColonne = "ColImmMaxPrlvmt" : DataGridView1.Item(NomColonne, NuméroLigne).Value = Prélèvement.TextBox6.Text
                        ' ==================================
                        ' Immersion Min Prélèvement 24
                        NomColonne = "ColImmMinPrlvmt" : DataGridView1.Item(NomColonne, NuméroLigne).Value = Prélèvement.TextBox5.Text
                        ' ==================================
                        ' Code Sandre Unité Immersion 25
                        NomColonne = "ColUnitéImm" : DataGridView1.Item(NomColonne, NuméroLigne).Value = CodeSandreUnitéImmersion
                        ' ==================================
                        ' Taille Prélèvement 26
                        NomColonne = "ColTaillePrlvmt" : DataGridView1.Item(NomColonne, NuméroLigne).Value = Prélèvement.TextBox9.Text
                        ' ==================================
                        ' Code Sandre Unité Taille Prélèvement 27
                        NomColonne = "ColUnitéPrlvmt" : DataGridView1.Item(NomColonne, NuméroLigne).Value = CodeSandreUnitéTaillePrélèvement
                        ' ==================================
                        ' Heure Prélèvement 28
                        NomColonne = "ColHeurePrlvmt" : DataGridView1.Item(NomColonne, NuméroLigne).Value = Prélèvement.TextBox11.Text
                        ' ==================================
                        ' Commentaires Prélèvement 29
                        NomColonne = "ColCommPrlvmt" : DataGridView1.Item(NomColonne, NuméroLigne).Value = Prélèvement.TextBox12.Text
                        ' ==================================
                        ' Latitude Prélèvement 30
                        NomColonne = "ColLatitudePrlvmt" : DataGridView1.Item(NomColonne, NuméroLigne).Value = Prélèvement.TextBox14.Text
                        ' ==================================
                        ' Longitude Prélèvement 31
                        NomColonne = "ColLongitudePrlvmt" : DataGridView1.Item(NomColonne, NuméroLigne).Value = Prélèvement.TextBox15.Text
                        ' ==================================
                        ' Code Sandre Positionnement Prélèvement 32
                        NomColonne = "ColPosPrlvmt" : DataGridView1.Item(NomColonne, NuméroLigne).Value = CodeSandrePositionnementPrélèvement
                        ' ==================================
                        ' Nombre Individu Prélèvement 33
                        NomColonne = "ColNbrIndPré" : DataGridView1.Item(NomColonne, NuméroLigne).Value = Prélèvement.TextBox17.Text
                        ' ==================================
                        ' Lot Aquacole 34
                        NomColonne = "ColLotAquacole" : DataGridView1.Item(NomColonne, NuméroLigne).Value = Échantillon.TextBox1.Text
                        ' ==================================
                        ' Code Sandre Support Échantillon 35
                        NomColonne = "ColSupportÉchant" : DataGridView1.Item(NomColonne, NuméroLigne).Value = CodeSandreSupportÉchantillon
                        ' ==================================
                        ' Taxon Support Échantillon 36
                        NomColonne = "ColTaxonÉchant" : DataGridView1.Item(NomColonne, NuméroLigne).Value = Échantillon.TextBox3.Text
                        ' ==================================
                        ' Code Sandre Groupe Taxon Support Échantillon 37
                        NomColonne = "ColGrpTaxÉchant" : DataGridView1.Item(NomColonne, NuméroLigne).Value = CodeSandreGroupeTaxon
                        ' ==================================
                        ' Numéro Échantillon 38
                        NomColonne = "ColNumÉchant" : DataGridView1.Item(NomColonne, NuméroLigne).Value = Échantillon.TextBox5.Text
                        ' ==================================
                        ' Taille Échantillon 39
                        NomColonne = "ColTailleÉchant" : DataGridView1.Item(NomColonne, NuméroLigne).Value = Échantillon.TextBox6.Text
                        ' ==================================
                        ' Code Sandre Unité Taille Échantillon 40
                        NomColonne = "ColUnitéÉchant" : DataGridView1.Item(NomColonne, NuméroLigne).Value = CodeSandreUnitéTailleÉchantillon
                        ' ==================================
                        ' Commentaire Échantillon 41
                        NomColonne = "ColCommÉchant" : DataGridView1.Item(NomColonne, NuméroLigne).Value = Échantillon.TextBox9.Text
                        ' ==================================
                        ' Nombre Individu Échantillon 42 
                        NomColonne = "ColNombreÉchant" : DataGridView1.Item(NomColonne, NuméroLigne).Value = Échantillon.TextBox8.Text
                        ' ==================================
                        ' Niveau Saisie Résultat 43
                        ' ==================================
                        ' Recherche du Code Sandre correspondant
                        For Sandre = NiveauSaisie To 5000
                            ' ==================================
                            ' Sortie de la boucle si fin de tableau
                            If Variables.TabRéfInfo(Sandre, 0) = "" Then Exit For
                            ' ==================================
                            If Variables.TabRéfInfo(Sandre, 0) = Variables.TabRéfPara(j, 13) Then
                                ' ==================================
                                ' Récupération du Code Sandre
                                NomColonne = "ColNivRés" : DataGridView1.Item(NomColonne, NuméroLigne).Value = Variables.TabRéfInfo(Sandre, 1)
                                Exit For
                            End If
                        Next
                        ' ==================================
                        ' Code Sandre Paramètre 44
                        NomColonne = "ColCodeParam" : DataGridView1.Item(NomColonne, NuméroLigne).Value = Variables.TabRéfPara(j, 3)
                        ' ==================================
                        ' Libellé Paramètre 45
                        NomColonne = "ColLibParam" : DataGridView1.Item(NomColonne, NuméroLigne).Value = Variables.TabRéfPara(j, 1)
                        ' ==================================
                        ' Code Sandre Support 46
                        ' ==================================
                        ' Recherche du Code Sandre correspondant
                        For Sandre = EntréeSupport To 5000
                            ' ==================================
                            ' Sortie de la boucle si fin de tableau
                            If Variables.TabRéfInfo(Sandre, 0) = "" Then Exit For
                            ' ==================================
                            If Variables.TabRéfInfo(Sandre, 0) = Variables.TabRéfPara(j, 9) Then
                                ' ==================================
                                ' Récupération du Code Sandre
                                NomColonne = "ColCodeSupport" : DataGridView1.Item(NomColonne, NuméroLigne).Value = Variables.TabRéfInfo(Sandre, 1)
                                Exit For
                            End If
                        Next
                        ' ==================================
                        ' Code Sandre Fraction 47
                        ' ==================================
                        ' Recherche du Code Sandre correspondant
                        For Sandre = Fraction To 5000
                            ' ==================================
                            ' Sortie de la boucle si fin de tableau
                            If Variables.TabRéfInfo(Sandre, 0) = "" Then Exit For
                            ' ==================================
                            If Variables.TabRéfInfo(Sandre, 0) = Variables.TabRéfPara(j, 8) Then
                                ' ==================================
                                ' Récupération du Code Sandre
                                NomColonne = "ColCodeFraction" : DataGridView1.Item(NomColonne, NuméroLigne).Value = Variables.TabRéfInfo(Sandre, 1)
                                Exit For
                            End If
                        Next
                        ' ==================================
                        ' Code Sandre Méthode 48
                        ' ==================================
                        ' Recherche du Code Sandre correspondant
                        For Sandre = Méthode To 5000
                            ' ==================================
                            ' Sortie de la boucle si fin de tableau
                            If Variables.TabRéfInfo(Sandre, 0) = "" Then Exit For
                            ' ==================================
                            If Variables.TabRéfInfo(Sandre, 0) = Variables.TabRéfPara(j, 6) Then
                                ' ==================================
                                ' Récupération du Code Sandre
                                NomColonne = "ColCodeMéthode" : DataGridView1.Item(NomColonne, NuméroLigne).Value = Variables.TabRéfInfo(Sandre, 1)
                                Exit For
                            End If
                        Next
                        ' ==================================
                        ' Numéro individu 49
                        NomColonne = "ColNumIndividu" : DataGridView1.Item(NomColonne, NuméroLigne).Value = Variables.TabRéfPara(j, 16)
                        ' ==================================
                        ' Résultat Numérique 50
                        NomColonne = "ColRésultat" : DataGridView1.Item(NomColonne, NuméroLigne).Value = Variables.TabRéfPara(j, 17)



                        ' ==================================
                        ' Libellé Résultat Qualitatif 51
                        NomColonne = "ColRésQualitatif" : DataGridView1.Item(NomColonne, NuméroLigne).Value = Variables.TabRéfPara(j, 15)

                        ' ==================================
                        ' Code Sandre Résultat Qualitatif
                        NomColonne = "ColCodeResQua" : DataGridView1.Item(NomColonne, NuméroLigne).Value = Variables.TabRéfPara(j, 23)
                        ' ==================================
                        ' Code Sandre Groupe Taxon Résultat
                        NomColonne = "ColCodeGroTaxRes" : DataGridView1.Item(NomColonne, NuméroLigne).Value = Variables.TabRéfPara(j, 24)
                        ' ==================================
                        ' Code Sandre Taxon Résultat
                        NomColonne = "ColCodeTaxRes" : DataGridView1.Item(NomColonne, NuméroLigne).Value = Variables.TabRéfPara(j, 25)

        


                        ' ==================================
                        ' Code Sandre Unité 52
                        ' ==================================
                        ' Recherche du Code Sandre correspondant
                        For Sandre = Unité To 5000
                            ' ==================================
                            ' Sortie de la boucle si fin de tableau
                            If Variables.TabRéfInfo(Sandre, 0) = "" Then Exit For
                            ' ==================================
                            If Variables.TabRéfInfo(Sandre, 0) = Variables.TabRéfPara(j, 18) Then
                                ' ==================================
                                ' Récupération du Code Sandre
                                NomColonne = "ColCodeUnité" : DataGridView1.Item(NomColonne, NuméroLigne).Value = Variables.TabRéfInfo(Sandre, 1)
                                Exit For
                            End If
                        Next
                        ' ==================================
                        ' Code Sandre Analyste 53
                        ' ==================================
                        ' Recherche du Code Sandre correspondant
                        For Sandre = Analyste To 5000
                            ' ==================================
                            ' Sortie de la boucle si fin de tableau
                            If Variables.TabRéfInfo(Sandre, 0) = "" Then Exit For
                            ' ==================================
                            If Variables.TabRéfInfo(Sandre, 0) = Variables.TabRéfPara(j, 4) Then
                                ' ==================================
                                ' Récupération du Code Sandre
                                NomColonne = "ColAnalyste" : DataGridView1.Item(NomColonne, NuméroLigne).Value = Variables.TabRéfInfo(Sandre, 2)
                                Exit For
                            End If
                        Next
                        ' ==================================
                        ' Code Sandre Engin Analyse 54
                        ' ==================================
                        ' Recherche du Code Sandre correspondant
                        For Sandre = EnginAnalyse To 5000
                            ' ==================================
                            ' Sortie de la boucle si fin de tableau
                            If Variables.TabRéfInfo(Sandre, 0) = "" Then Exit For
                            ' ==================================
                            If Variables.TabRéfInfo(Sandre, 0) = Variables.TabRéfPara(j, 7) Then
                                ' ==================================
                                ' Récupération du Code Sandre
                                NomColonne = "ColCodeEngin" : DataGridView1.Item(NomColonne, NuméroLigne).Value = Variables.TabRéfInfo(Sandre, 1)
                                Exit For
                            End If
                        Next
                        ' ==================================
                        ' Code Sandre Remarque 55
                        For Sandre = CodeRemarque To 5000
                            ' ==================================
                            ' Sortie de la boucle si fin de tableau
                            If Variables.TabRéfInfo(Sandre, 0) = "" Then Exit For
                            ' ==================================
                            If Variables.TabRéfInfo(Sandre, 0) = Variables.TabRéfPara(j, 20) Then
                                ' ==================================
                                ' Récupération du Code Sandre
                                NomColonne = "ColCodeRemarque" : DataGridView1.Item(NomColonne, NuméroLigne).Value = Variables.TabRéfInfo(Sandre, 1)
                                Exit For
                            End If
                        Next

                        ' ==================================
                        ' Code Sandre Précision 56
                        ' ==================================
                        ' Recherche du Code Sandre correspondant
                        '                        For Sandre = Précision To 5000
                        ' ==================================
                        ' Sortie de la boucle si fin de tableau
                        '                        If Variables.TabRéfInfo(Sandre, 0) = "" Then Exit For
                        ' ==================================
                        '                        If Variables.TabRéfInfo(Sandre, 0) = Variables.TabRéfPara(j, 11) Then
                        ' ==================================
                        ' Récupération du Code Sandre
                        '                        Exit For
                        '                    End If
                        '                        Next
                        NomColonne = "ColCodePrécision" : DataGridView1.Item(NomColonne, NuméroLigne).Value = Variables.TabRéfPara(j, 11)

                        ' ==================================
                        ' Code Sandre Type Précision 57
                        ' ==================================
                        ' Recherche du Code Sandre correspondant
                        For Sandre = TypePrécision To 5000
                            ' ==================================
                            ' Sortie de la boucle si fin de tableau
                            If Variables.TabRéfInfo(Sandre, 0) = "" Then Exit For
                            ' ==================================
                            If Variables.TabRéfInfo(Sandre, 0) = Variables.TabRéfPara(j, 12) Then
                                ' ==================================
                                ' Récupération du Code Sandre
                                NomColonne = "ColCodeTypePrécision" : DataGridView1.Item(NomColonne, NuméroLigne).Value = Variables.TabRéfInfo(Sandre, 1)
                                Exit For
                            End If
                        Next
                        ' ==================================
                        ' Commentaire Mesure 58
                        NomColonne = "ColCommentaire" : DataGridView1.Item(NomColonne, NuméroLigne).Value = Variables.TabRéfPara(j, 19)

                        ' ==================================
                        ' Colorisation en rose des cellules obligatoires non renseignées
                        For ii = 0 To DataGridView1.RowCount - 1
                            ' Absence de la « Dénomination du Lieu de Surveillance ».
                            NomColonne = "ColLieuSurv" : If DataGridView1.Item(NomColonne, ii).Value = "" Then DataGridView1.Item(NomColonne, ii).Style.BackColor = Color.Pink
                            ' Absence de la « Dénomination du Réseau ».
                            NomColonne = "ColRéseau" : If DataGridView1.Item(NomColonne, ii).Value = "" Then DataGridView1.Item(NomColonne, ii).Style.BackColor = Color.Pink
                            ' Absence de la « Dénomination du Saisisseur ».
                            NomColonne = "ColSaissi" : If DataGridView1.Item(NomColonne, ii).Value = "" Then DataGridView1.Item(NomColonne, ii).Style.BackColor = Color.Pink
                            ' Absence de la « Date de Passage ».
                            NomColonne = "ColDatePassage" : If DataGridView1.Item(NomColonne, ii).Value = "" Then DataGridView1.Item(NomColonne, ii).Style.BackColor = Color.Pink
                            ' Présence d’une « Valeur de Sonde » sans « Unité de Sonde ».
                            If DataGridView1.Item("ColSonde", ii).Value <> "" Then
                                NomColonne = "ColUnitéSonde" : If DataGridView1.Item(NomColonne, ii).Value = "" Then DataGridView1.Item(NomColonne, ii).Style.BackColor = Color.Pink
                            End If
                            ' Absence de « Positionnement Passage » avec présence de la « Latitude Passage » ou de la « Longitude Passage » ou des deux.
                            If DataGridView1.Item("ColLatitudePass", ii).Value <> "" Or DataGridView1.Item("ColLongitudePass", ii).Value <> "" Then
                                NomColonne = "ColPosition" : If DataGridView1.Item(NomColonne, ii).Value = "" Then DataGridView1.Item(NomColonne, ii).Style.BackColor = Color.Pink
                            End If
                            ' Absence de la « Dénomination de l’Engin de Prélèvement ».
                            NomColonne = "ColEnginPrlvmt" : If DataGridView1.Item(NomColonne, ii).Value = "" Then DataGridView1.Item(NomColonne, ii).Style.BackColor = Color.Pink
                            ' Absence de la « Dénomination du Préleveur ».
                            NomColonne = "ColPréleveur" : If DataGridView1.Item(NomColonne, ii).Value = "" Then DataGridView1.Item(NomColonne, ii).Style.BackColor = Color.Pink
                            ' Présence de l’« Immersion de Prélèvement » avec l’« Immersion Max » ou avec l’« Immersion Min » ou sans l’« Unité de Prélèvement ».
                            If DataGridView1.Item("ColImmPrlvmt", ii).Value <> "" Then
                                NomColonne = "ColImmMaxPrlvmt" : If DataGridView1.Item(NomColonne, ii).Value <> "" Then DataGridView1.Item(NomColonne, ii).Style.BackColor = Color.Pink
                                NomColonne = "ColImmMinPrlvmt" : If DataGridView1.Item(NomColonne, ii).Value <> "" Then DataGridView1.Item(NomColonne, ii).Style.BackColor = Color.Pink
                                NomColonne = "ColUnitéImm" : If DataGridView1.Item(NomColonne, ii).Value = "" Then DataGridView1.Item(NomColonne, ii).Style.BackColor = Color.Pink
                            End If
                            ' Présence de l’« Immersion Max de Prélèvement » sans l’« Immersion Min de Prélèvement » ou sans l’« Unité de Prélèvement ».
                            If DataGridView1.Item("ColImmMaxPrlvmt", ii).Value <> "" Then
                                NomColonne = "ColImmMinPrlvmt" : If DataGridView1.Item(NomColonne, ii).Value = "" Then DataGridView1.Item(NomColonne, ii).Style.BackColor = Color.Pink
                                NomColonne = "ColUnitéImm" : If DataGridView1.Item(NomColonne, ii).Value = "" Then DataGridView1.Item(NomColonne, ii).Style.BackColor = Color.Pink
                            End If
                            ' Présence de l’« Immersion Min de Prélèvement » sans l’« Immersion Max de Prélèvement » ou sans l’« Unité de Prélèvement ».
                            If DataGridView1.Item("ColImmMinPrlvmt", ii).Value <> "" Then
                                NomColonne = "ColImmMaxPrlvmt" : If DataGridView1.Item(NomColonne, ii).Value = "" Then DataGridView1.Item(NomColonne, ii).Style.BackColor = Color.Pink
                                NomColonne = "ColUnitéImm" : If DataGridView1.Item(NomColonne, ii).Value = "" Then DataGridView1.Item(NomColonne, ii).Style.BackColor = Color.Pink
                            End If
                            ' Présence de la « Taille de Prélèvement » sans l’« Unité de Taille de Prélèvement ».
                            If DataGridView1.Item("ColTaillePrlvmt", ii).Value <> "" Then
                                NomColonne = "ColUnitéPrlvmt" : If DataGridView1.Item(NomColonne, ii).Value = "" Then DataGridView1.Item(NomColonne, ii).Style.BackColor = Color.Pink
                            End If
                            ' Absence de « Positionnement de Prélèvement » avec présence de la « Latitude » ou de la « Longitude de Prélèvement » ou des deux.
                            If DataGridView1.Item("ColLatitudePrlvmt", ii).Value <> "" Or DataGridView1.Item("ColLongitudePrlvmt", ii).Value <> "" Then
                                NomColonne = "ColPosPrlvmt" : If DataGridView1.Item(NomColonne, ii).Value = "" Then DataGridView1.Item(NomColonne, ii).Style.BackColor = Color.Pink
                            End If
                            ' Absence de la « Dénomination du Support Échantillon ».
                            NomColonne = "ColSupportÉchant" : If DataGridView1.Item(NomColonne, ii).Value = "" Then DataGridView1.Item(NomColonne, ii).Style.BackColor = Color.Pink
                            ' Présence du « Taxon Support Échantillon » et du « Groupe Taxon Support Échantillon ».
                            If DataGridView1.Item("ColTaxonÉchant", ii).Value <> "" Then
                                NomColonne = "ColGrpTaxÉchant" : If DataGridView1.Item(NomColonne, ii).Value <> "" Then DataGridView1.Item(NomColonne, ii).Style.BackColor = Color.Pink
                            End If
                            ' Présence du « Groupe Taxon Support Échantillon » et du « Taxon Support Échantillon ».
                            If DataGridView1.Item("ColGrpTaxÉchant", ii).Value <> "" Then
                                NomColonne = "ColTaxonÉchant" : If DataGridView1.Item(NomColonne, ii).Value <> "" Then DataGridView1.Item(NomColonne, ii).Style.BackColor = Color.Pink
                            End If
                            ' Absence du « Numéro d’Échantillon ».
                            NomColonne = "ColNumÉchant" : If DataGridView1.Item(NomColonne, ii).Value = "" Then DataGridView1.Item(NomColonne, ii).Style.BackColor = Color.Pink
                            ' Présence de la « Taille d’Échantillon » sans l’« Unité de Taille d’Échantillon ».
                            If DataGridView1.Item("ColTailleÉchant", ii).Value <> "" Then
                                NomColonne = "ColUnitéÉchant" : If DataGridView1.Item(NomColonne, ii).Value = "" Then DataGridView1.Item(NomColonne, ii).Style.BackColor = Color.Pink
                            End If
                            ' Absence du « Niveau de Saisie Résultat ».
                            NomColonne = "ColNivRés" : If DataGridView1.Item(NomColonne, ii).Value = "" Then DataGridView1.Item(NomColonne, ii).Style.BackColor = Color.Pink
                            ' Absence du « Code Sandre Paramètre ».
                            NomColonne = "ColCodeParam" : If DataGridView1.Item(NomColonne, ii).Value = "" Then DataGridView1.Item(NomColonne, ii).Style.BackColor = Color.Pink
                            ' Absence de la « Dénomination du Paramètre ».
                            NomColonne = "ColLibParam" : If DataGridView1.Item(NomColonne, ii).Value = "" Then DataGridView1.Item(NomColonne, ii).Style.BackColor = Color.Pink
                            ' Absence de la « Dénomination du Support ».
                            NomColonne = "ColCodeSupport" : If DataGridView1.Item(NomColonne, ii).Value = "" Then DataGridView1.Item(NomColonne, ii).Style.BackColor = Color.Pink
                            ' Absence de la « Dénomination de la Fraction ».
                            NomColonne = "ColCodeFraction" : If DataGridView1.Item(NomColonne, ii).Value = "" Then DataGridView1.Item(NomColonne, ii).Style.BackColor = Color.Pink
                            ' Absence de la « Dénomination de la Méthode ».
                            NomColonne = "ColCodeMéthode" : If DataGridView1.Item(NomColonne, ii).Value = "" Then DataGridView1.Item(NomColonne, ii).Style.BackColor = Color.Pink

                            ' Absence du « Résultat Numérique ».
                            NomColonne = "ColRésultat" : If DataGridView1.Item(NomColonne, ii).Value = "" And DataGridView1.Item("ColCodeResQua", ii).Value = "" And DataGridView1.Item("ColRésQualitatif", ii).Value = "" Then DataGridView1.Item(NomColonne, ii).Style.BackColor = Color.Pink


                            ' Absence du « Code Résultat Qualitatif ».
                            NomColonne = "ColCodeResQua" : If DataGridView1.Item(NomColonne, ii).Value = "" And DataGridView1.Item("ColRésultat", ii).Value = "" Then DataGridView1.Item(NomColonne, ii).Style.BackColor = Color.Pink


                            ' Présence du signe « < » dans la valeur du Résultat.
                            NomColonne = "ColRésultat" : If Strings.InStr(DataGridView1.Item(NomColonne, ii).Value, "<") <> 0 Then DataGridView1.Item(NomColonne, ii).Style.BackColor = Color.Pink
                            ' Absence de l’« Unité du Résultat ».
                            NomColonne = "ColCodeUnité" : If DataGridView1.Item(NomColonne, ii).Value = "" Then DataGridView1.Item(NomColonne, ii).Style.BackColor = Color.Pink
                            ' Absence de la « Dénomination de l’Analyseur ».
                            NomColonne = "ColAnalyste" : If DataGridView1.Item(NomColonne, ii).Value = "" Then DataGridView1.Item(NomColonne, ii).Style.BackColor = Color.Pink
                            ' Absence du « Code Remarque ».
                            NomColonne = "ColCodeRemarque" : If DataGridView1.Item(NomColonne, ii).Value = "" Then DataGridView1.Item(NomColonne, ii).Style.BackColor = Color.Pink
                            ' Présence d’une « Valeur de Précision » sans le « Type de Précision ».
                            If DataGridView1.Item("ColCodePrécision", ii).Value <> "" Then
                                NomColonne = "ColCodeTypePrécision" : If DataGridView1.Item(NomColonne, ii).Value = "" Then DataGridView1.Item(NomColonne, ii).Style.BackColor = Color.Pink
                            End If
                            ' Présence d’une « Dénomination Support Échantillon » différente d’une « Dénomination du Support ».
                            If DataGridView1.Item("ColSupportÉchant", ii).Value <> DataGridView1.Item("ColCodeSupport", ii).Value Then
                                NomColonne = "ColSupportÉchant" : DataGridView1.Item(NomColonne, ii).Style.BackColor = Color.Pink
                                NomColonne = "ColCodeSupport" : DataGridView1.Item(NomColonne, ii).Style.BackColor = Color.Pink
                            End If

                        Next
                    End If
                Next
            Next
        Next

        ' ==================================
        ' Vérification avant recadrage du DataGridView qu'au moins une ligne existe afin d'éviter une erreur
        If DataGridView1.RowCount = 0 Then
            ' ==================================
            ' Sortie si pas de paramètre à visualiser
            MsgBox("Pas d'informations pour la visualisation du fichier Quadrige² IFREMER !", MsgBoxStyle.Exclamation, " QUADRISPEL - Erreur de visualisation")
            Exit Sub
        End If

        ' ==================================
        ' Remise dans la même position du DataGridView avant une réinitilisation
        For i = 0 To 60
            ' ==================================
            ' Récupération du numéro de colonne de la première cellule visible à gauche
            If DataGridView1.Item(i, 0).Selected = True Then
                ' ==================================
                ' Sélection du numéro de colonne de la première cellule visible à gauche de la ligne précédemment sélectionnée
                DataGridView1.Item(i, Variables.AncienneLigne).Selected = True
                ' ==================================
                ' Si coloration en pink, conservation
                If DataGridView1.Item(i, Variables.AncienneLigne).Style.BackColor = Color.Pink Then
                    DataGridView1.Item(i, Variables.AncienneLigne).Style.SelectionBackColor = Color.Pink
                Else
                    ' ==================================
                    ' Si non, coloration en cyan
                    DataGridView1.Item(i, Variables.AncienneLigne).Style.SelectionBackColor = Color.Cyan
                End If
            End If
        Next
        ' ==================================
        ' Colorisation en cyan de toutes les cellules de la ligne ou conservation de la couleur pink
        For i = 0 To 60
            If DataGridView1.Item(i, Variables.AncienneLigne).Style.BackColor <> Color.Pink Then
                DataGridView1.Item(i, Variables.AncienneLigne).Style.BackColor = Color.Cyan
            End If
        Next

    End Sub

    '=============================================================================================================================================
    ' Traitement d'un clic sur une cellule ou le déplacement par le curseur du DataGridView de la visualisation Fichier Sauvegarde QuadriSPEL
    Private Sub DataGridView1_CellEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellEnter

        ' ==================================
        ' Sortie si clic sur le titre de colonne
        If e.RowIndex < 0 Then Exit Sub
        ' ==================================
        ' Définition des variables
        Dim i As Integer
        Dim NomColonne As String = ""
        Dim NuméroLigne As Integer = 0
        ' ==================================
        ' Colorisation en cyan de la cellule sélectionnée ou conservation de la couleur pink précédente
        If DataGridView1.Item(e.ColumnIndex, e.RowIndex).Style.BackColor = Color.Pink Then
            DataGridView1.Item(e.ColumnIndex, e.RowIndex).Style.SelectionBackColor = Color.Pink
        Else
            DataGridView1.Item(e.ColumnIndex, e.RowIndex).Style.SelectionBackColor = Color.Cyan
        End If
        ' ==================================
        ' Colorisation en cyan de toutes les cellules de la ligne de la cellule sélectionnée ou conservation de la couleur pink précédente
        For i = 0 To 57
            If DataGridView1.Item(i, e.RowIndex).Style.BackColor <> Color.Pink Then
                DataGridView1.Item(i, e.RowIndex).Style.BackColor = Color.Cyan
            End If
        Next


        ' ==================================
        ' Colorisation en rose des cellules obligatoires non renseignées
        '       For i = 0 To DataGridView1.RowCount - 1
        '       NomColonne = "ColLieuSurv" : If DataGridView1.Item(NomColonne, i).Value = "" Then DataGridView1.Item(NomColonne, i).Style.BackColor = Color.Pink
        '       NomColonne = "ColRéseau" : If DataGridView1.Item(NomColonne, i).Value = "" Then DataGridView1.Item(NomColonne, i).Style.BackColor = Color.Pink
        '       NomColonne = "ColSaissi" : If DataGridView1.Item(NomColonne, i).Value = "" Then DataGridView1.Item(NomColonne, i).Style.BackColor = Color.Pink
        '       NomColonne = "ColDatePassage" : If DataGridView1.Item(NomColonne, i).Value = "" Then DataGridView1.Item(NomColonne, i).Style.BackColor = Color.Pink
        '       NomColonne = "ColEnginPrlvmt" : If DataGridView1.Item(NomColonne, i).Value = "" Then DataGridView1.Item(NomColonne, i).Style.BackColor = Color.Pink
        '       NomColonne = "ColPréleveur" : If DataGridView1.Item(NomColonne, i).Value = "" Then DataGridView1.Item(NomColonne, i).Style.BackColor = Color.Pink
        '       NomColonne = "ColSupportÉchant" : If DataGridView1.Item(NomColonne, i).Value = "" Then DataGridView1.Item(NomColonne, i).Style.BackColor = Color.Pink
        '       NomColonne = "ColNumÉchant" : If DataGridView1.Item(NomColonne, i).Value = "" Then DataGridView1.Item(NomColonne, i).Style.BackColor = Color.Pink
        '       NomColonne = "ColNivRés" : If DataGridView1.Item(NomColonne, i).Value = "" Then DataGridView1.Item(NomColonne, i).Style.BackColor = Color.Pink
        '       NomColonne = "ColCodeParam" : If DataGridView1.Item(NomColonne, i).Value = "" Then DataGridView1.Item(NomColonne, i).Style.BackColor = Color.Pink
        '       NomColonne = "ColLibParam" : If DataGridView1.Item(NomColonne, i).Value = "" Then DataGridView1.Item(NomColonne, i).Style.BackColor = Color.Pink
        '       NomColonne = "ColCodeSupport" : If DataGridView1.Item(NomColonne, i).Value = "" Then DataGridView1.Item(NomColonne, i).Style.BackColor = Color.Pink
        '       NomColonne = "ColCodeFraction" : If DataGridView1.Item(NomColonne, i).Value = "" Then DataGridView1.Item(NomColonne, i).Style.BackColor = Color.Pink
        '       NomColonne = "ColCodeMéthode" : If DataGridView1.Item(NomColonne, i).Value = "" Then DataGridView1.Item(NomColonne, i).Style.BackColor = Color.Pink
        '       NomColonne = "ColRésultat" : If DataGridView1.Item(NomColonne, i).Value = "" Then DataGridView1.Item(NomColonne, i).Style.BackColor = Color.Pink
        '       NomColonne = "ColCodeUnité" : If DataGridView1.Item(NomColonne, i).Value = "" Then DataGridView1.Item(NomColonne, i).Style.BackColor = Color.Pink
        '        NomColonne = "ColAnalyste" : If DataGridView1.Item(NomColonne, i).Value = "" Then DataGridView1.Item(NomColonne, i).Style.BackColor = Color.Pink
        '        NomColonne = "ColCodeRemarque" : If DataGridView1.Item(NomColonne, i).Value = "" Then DataGridView1.Item(NomColonne, i).Style.BackColor = Color.Pink

        '        If DataGridView1.Item("ColLatitudePass", i).Value <> "" Or DataGridView1.Item("ColLongitudePass", i).Value <> "" Then
        '        NomColonne = "ColPosition" : If DataGridView1.Item(NomColonne, i).Value = "" Then DataGridView1.Item(NomColonne, i).Style.BackColor = Color.Pink
        'End If


        '        Next


        ' ==================================
        ' Vérification si une ligne quittée doit être en grisée ou en blanc
        If Variables.AncienneLigne <> e.RowIndex Then
            ' ==================================
            ' Vérification si une ligne quittée doit être en grisée ou en blanc en fonction de son numéro
            If Variables.AncienneLigne Mod 2 = 1 Then
                ' ==================================
                ' Ligne en grisé
                For i = 0 To 57
                    If DataGridView1.Item(i, Variables.AncienneLigne).Style.BackColor = Color.Cyan Then
                        DataGridView1.Item(i, Variables.AncienneLigne).Style.BackColor = Color.FromArgb(230, 230, 230)
                    End If
                Next
            Else
                ' ==================================
                ' Ligne en blanc
                For i = 0 To 57
                    If DataGridView1.Item(i, Variables.AncienneLigne).Style.BackColor = Color.Cyan Then
                        DataGridView1.Item(i, Variables.AncienneLigne).Style.BackColor = Color.FromArgb(255, 255, 255)
                    End If
                Next
            End If
        End If


        ' ==================================
        ' Colorisation en rose des cellules obligatoires non renseignées
        '       For i = 0 To DataGridView1.RowCount - 1
        '        NomColonne = "ColRéseau" : If DataGridView1.Item(NomColonne, i).Value = "" Then DataGridView1.Item(NomColonne, i).Style.BackColor = Color.Pink
        '        NomColonne = "ColSaissi" : If DataGridView1.Item(NomColonne, i).Value = "" Then DataGridView1.Item(NomColonne, i).Style.BackColor = Color.Pink
        '        NomColonne = "ColDatePassage" : If DataGridView1.Item(NomColonne, i).Value = "" Then DataGridView1.Item(NomColonne, i).Style.BackColor = Color.Pink
        '        If DataGridView1.Item("ColLatitudePass", i).Value <> "" Or DataGridView1.Item("ColLongitudePass", i).Value <> "" Then
        '       NomColonne = "ColPosition" : If DataGridView1.Item(NomColonne, i).Value = "" Then DataGridView1.Item(NomColonne, i).Style.BackColor = Color.Pink
        '       End If
        '        NomColonne = "ColEnginPrlvmt" : If DataGridView1.Item(NomColonne, i).Value = "" Then DataGridView1.Item(NomColonne, i).Style.BackColor = Color.Pink
        '        NomColonne = "ColPréleveur" : If DataGridView1.Item(NomColonne, i).Value = "" Then DataGridView1.Item(NomColonne, i).Style.BackColor = Color.Pink




        '        NomColonne = "ColSupportÉchant" : If DataGridView1.Item(NomColonne, i).Value = "" Then DataGridView1.Item(NomColonne, i).Style.BackColor = Color.Pink
        '        NomColonne = "ColNumÉchant" : If DataGridView1.Item(NomColonne, i).Value = "" Then DataGridView1.Item(NomColonne, i).Style.BackColor = Color.Pink
        '        NomColonne = "ColNivRés" : If DataGridView1.Item(NomColonne, i).Value = "" Then DataGridView1.Item(NomColonne, i).Style.BackColor = Color.Pink
        '        NomColonne = "ColCodeParam" : If DataGridView1.Item(NomColonne, i).Value = "" Then DataGridView1.Item(NomColonne, i).Style.BackColor = Color.Pink
        '        NomColonne = "ColLibParam" : If DataGridView1.Item(NomColonne, i).Value = "" Then DataGridView1.Item(NomColonne, i).Style.BackColor = Color.Pink
        '        NomColonne = "ColCodeSupport" : If DataGridView1.Item(NomColonne, i).Value = "" Then DataGridView1.Item(NomColonne, i).Style.BackColor = Color.Pink
        '        NomColonne = "ColCodeFraction" : If DataGridView1.Item(NomColonne, i).Value = "" Then DataGridView1.Item(NomColonne, i).Style.BackColor = Color.Pink
        '        NomColonne = "ColCodeMéthode" : If DataGridView1.Item(NomColonne, i).Value = "" Then DataGridView1.Item(NomColonne, i).Style.BackColor = Color.Pink
        '        NomColonne = "ColRésultat" : If DataGridView1.Item(NomColonne, i).Value = "" Then DataGridView1.Item(NomColonne, i).Style.BackColor = Color.Pink
        '        NomColonne = "ColCodeUnité" : If DataGridView1.Item(NomColonne, i).Value = "" Then DataGridView1.Item(NomColonne, i).Style.BackColor = Color.Pink
        '        NomColonne = "ColAnalyste" : If DataGridView1.Item(NomColonne, i).Value = "" Then DataGridView1.Item(NomColonne, i).Style.BackColor = Color.Pink
        '        NomColonne = "ColCodeRemarque" : If DataGridView1.Item(NomColonne, i).Value = "" Then DataGridView1.Item(NomColonne, i).Style.BackColor = Color.Pink


        '       Next


        ' ==================================
        ' Sauvegarde du numéro de la ligne sélectionnée
        Variables.AncienneLigne = e.RowIndex

    End Sub
 
    '=============================================================================================================================================
    ' Fichier Chargement Masque
    Private Sub MasquesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MasquesToolStripMenuItem.Click
        Dim i As Integer
        ' ==================================
        ' Vérification d'une demande de sauvegarde d'informations Masque
        If Variables.SauveFichierMasque = True And Variables.TypeFichierChargé = "" Then
            i = MsgBox(" Des informations de fichier Masque ont été réalisées ! " & vbCrLf & vbCrLf & "                        Sauvegarder ?", MsgBoxStyle.Exclamation + MsgBoxStyle.YesNo + MsgBoxStyle.DefaultButton1, " QUADRISPEL - Proposition de sauvegarde")
            If i = 6 Then
                ' ==================================
                ' Affichage de la Feuille
                Sauve_Masque.ShowDialog()
                '                Exit Sub
            End If
        End If
        ' ==================================
        ' Vérification d'une demande de sauvegarde d'informations Masque
        If Variables.SauveFichierMasque = True And Variables.TypeFichierChargé = "Masque" Then
            i = MsgBox(" Des informations du fichier Masque ont été modifiées ! " & vbCrLf & vbCrLf & "                        Sauvegarder ?", MsgBoxStyle.Exclamation + MsgBoxStyle.YesNo + MsgBoxStyle.DefaultButton1, " QUADRISPEL - Proposition de sauvegarde")
            If i = 6 Then
                ' ==================================
                ' Affichage de la Feuille
                Sauve_Masque.ShowDialog()
                '               Exit Sub
            End If
        End If
        ' ==================================
        ' Vérification d'une demande de sauvegarde d'informations Résultat
        If Variables.SauveFichierRésultat = True And Variables.TypeFichierChargé = "Masque" Then
            i = MsgBox(" Des informations Résultats sont à sauvegarder ! " & vbCrLf & vbCrLf & "                    Sauvegarder ?", MsgBoxStyle.Exclamation + MsgBoxStyle.YesNo + MsgBoxStyle.DefaultButton1, " QUADRISPEL - Proposition de sauvegarde")
            If i = 6 Then
                ' ==================================
                ' Affichage de la Feuille
                Sauve_Résultat.ShowDialog()
                '               Exit Sub
            End If
        End If
        ' ==================================
        ' Vérification d'une demande de sauvegarde d'informations Résultat
        If Variables.SauveFichierRésultat = True And Variables.TypeFichierChargé = "Résultat" Then
            i = MsgBox(" Des informations du fichier Résultats ont été modifiées ! " & vbCrLf & vbCrLf & "                         Sauvegarder ?", MsgBoxStyle.Exclamation + MsgBoxStyle.YesNo + MsgBoxStyle.DefaultButton1, " QUADRISPEL - Proposition de sauvegarde")
            If i = 6 Then
                ' ==================================
                ' Affichage de la Feuille
                Sauve_Résultat.ShowDialog()
                '                Exit Sub
            End If
        End If
        ' ==================================
        ' Vérification d'une demande de sauvegarde d'informations Masque
        If Variables.SauveFichierMasque = True And Variables.TypeFichierChargé = "Résultat" Then
            i = MsgBox(" Des informations Masques correspondantes au fichier Masque ont été modifiées ! " & vbCrLf & vbCrLf & "                                             Sauvegarder ?", MsgBoxStyle.Exclamation + MsgBoxStyle.YesNo + MsgBoxStyle.DefaultButton1, " QUADRISPEL - Proposition de sauvegarde")
            If i = 6 Then
                ' ==================================
                ' Affichage de la Feuille
                Sauve_Masque.ShowDialog()
                '                Exit Sub
            End If
        End If

        ' ==================================
        ' Affichage de la Feuille
        Charge_Masque.ShowDialog()
    End Sub

    '=============================================================================================================================================
    ' Fichier Sauvegarde Masque
    Private Sub MasqueToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MasqueToolStripMenuItem.Click
        ' ==================================
        ' Affichage de la Feuille
        Sauve_Masque.ShowDialog()
    End Sub

    '=============================================================================================================================================
    ' Fichier Chargement Résultat
    Private Sub RésultatsToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RésultatsToolStripMenuItem1.Click
        Dim i As Integer
        ' ==================================
        ' Vérification d'une demande de sauvegarde d'informations Masque
        If Variables.SauveFichierMasque = True And Variables.TypeFichierChargé = "" Then
            i = MsgBox(" Des informations de fichier Masque ont été réalisées ! " & vbCrLf & vbCrLf & "                        Sauvegarder ?", MsgBoxStyle.Exclamation + MsgBoxStyle.YesNo + MsgBoxStyle.DefaultButton1, " QUADRISPEL - Proposition de sauvegarde")
            If i = 6 Then
                ' ==================================
                ' Affichage de la Feuille
                Sauve_Masque.ShowDialog()
                '                Exit Sub
            End If
        End If
        ' ==================================
        ' Vérification d'une demande de sauvegarde d'informations Masque
        If Variables.SauveFichierMasque = True And Variables.TypeFichierChargé = "Masque" Then
            i = MsgBox(" Des informations du fichier Masque ont été modifiées ! " & vbCrLf & vbCrLf & "                        Sauvegarder ?", MsgBoxStyle.Exclamation + MsgBoxStyle.YesNo + MsgBoxStyle.DefaultButton1, " QUADRISPEL - Proposition de sauvegarde")
            If i = 6 Then
                ' ==================================
                ' Affichage de la Feuille
                Sauve_Masque.ShowDialog()
                '                Exit Sub
            End If
        End If
        ' ==================================
        ' Vérification d'une demande de sauvegarde d'informations Résultat
        If Variables.SauveFichierRésultat = True And Variables.TypeFichierChargé = "Masque" Then
            i = MsgBox(" Des informations Résultats sont à sauvegarder ! " & vbCrLf & vbCrLf & "                    Sauvegarder ?", MsgBoxStyle.Exclamation + MsgBoxStyle.YesNo + MsgBoxStyle.DefaultButton1, " QUADRISPEL - Proposition de sauvegarde")
            If i = 6 Then
                ' ==================================
                ' Affichage de la Feuille
                Sauve_Résultat.ShowDialog()
                '                Exit Sub
            End If
        End If
        ' ==================================
        ' Vérification d'une demande de sauvegarde d'informations Résultat
        If Variables.SauveFichierRésultat = True And Variables.TypeFichierChargé = "Résultat" Then
            i = MsgBox(" Des informations du fichier Résultats ont été modifiées ! " & vbCrLf & vbCrLf & "                         Sauvegarder ?", MsgBoxStyle.Exclamation + MsgBoxStyle.YesNo + MsgBoxStyle.DefaultButton1, " QUADRISPEL - Proposition de sauvegarde")
            If i = 6 Then
                ' ==================================
                ' Affichage de la Feuille
                Sauve_Résultat.ShowDialog()
                '               Exit Sub
            End If
        End If
        ' ==================================
        ' Vérification d'une demande de sauvegarde d'informations Masque
        If Variables.SauveFichierMasque = True And Variables.TypeFichierChargé = "Résultat" Then
            i = MsgBox(" Des informations Masques correspondantes au fichier Masque ont été modifiées ! " & vbCrLf & vbCrLf & "                                             Sauvegarder ?", MsgBoxStyle.Exclamation + MsgBoxStyle.YesNo + MsgBoxStyle.DefaultButton1, " QUADRISPEL - Proposition de sauvegarde")
            If i = 6 Then
                ' ==================================
                ' Affichage de la Feuille
                Sauve_Masque.ShowDialog()
                '                Exit Sub
            End If
        End If

        ' ==================================
        ' Affichage de la Feuille
        Charge_Résultat.ShowDialog()
    End Sub

    '=============================================================================================================================================
    ' Fichier Sauvegarde Résultat
    Private Sub RésultatToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RésultatToolStripMenuItem.Click
        ' ==================================
        ' Affichage de la Feuille
        Sauve_Résultat.ShowDialog()
    End Sub

    '=============================================================================================================================================
    ' Feuille Passage
    Private Sub PassageToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PassageToolStripMenuItem.Click
        ' ==================================
        ' Affichage de la Feuille
        Passage.ShowDialog()
    End Sub

    '=============================================================================================================================================
    ' Feuille Prélèvement
    Private Sub PrélèvementToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PrélèvementToolStripMenuItem.Click
        ' ==================================
        ' Affichage de la Feuille
        Prélèvement.ShowDialog()
    End Sub

    '=============================================================================================================================================
    ' Feuille Échantillon
    Private Sub ÉchantillonToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ÉchantillonToolStripMenuItem.Click
        ' ==================================
        ' Affichage de la Feuille
        Échantillon.ShowDialog()
    End Sub

    '=============================================================================================================================================
    ' Feuille Familles
    Private Sub SélectionFamillesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SélectionFamillesToolStripMenuItem.Click
        ' ==================================
        ' Affichage de la Feuille
        Familles.ShowDialog()
    End Sub

    '=============================================================================================================================================
    ' Feuille Paramètres
    Private Sub SélectionParamètresToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SélectionParamètresToolStripMenuItem.Click
        ' ==================================
        ' Définition des variables
        Dim NomFamille As String
        '        Dim i As Integer
        ' ==================================
        ' Vérification qu'une Famille a été sélectionnée
        If Familles.ListBox2.Items.Count < 1 Then
            MsgBox("Vous n'avez pas sélectionné de Familles de Paramètres ! ", MsgBoxStyle.Exclamation, " QUADRISPEL - Sélection")
            Exit Sub
        End If
        ' ==================================
        ' Récupération du premier nom de Famille des Paramètres
        NomFamille = LTrim(Familles.ListBox2.Items.Item(0))
        Paramètres.TextBox1.Text = NomFamille
        ' ==================================
        ' RAZ de la liste des noms des Paramètres
        Paramètres.ListBox1.Items.Clear()
        Paramètres.ListBox2.Items.Clear()
        ' ==================================
        ' Initialisation de l'aiguilleur de tableau
        Variables.AiguilleurTableau = 0
        ' ==================================
        ' Validation des boutons de sélections pour la feuille Paramètres et du texte
        Paramètres.TextBox1.Text = NomFamille
        Paramètres.Button8.Enabled = False
        Paramètres.Button9.Enabled = True
        If Familles.ListBox2.Items.Count < 2 Then
            Paramètres.Button9.Enabled = False
        End If
        ' ==================================
        ' Affichage de la Feuille
        Paramètres.ShowDialog()
    End Sub

    '=============================================================================================================================================
    ' Feuille Propriétés et Résultats Paramètres
    Private Sub PropriétésToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PropriétésToolStripMenuItem.Click
        ' ==================================
        ' Définition des variables
        Dim i As Integer
        Dim j As Integer
        Dim ParamValide As Integer
        ' ==================================
        ' Vérification qu'une Famille a été sélectionnée
        If Familles.ListBox2.Items.Count < 1 Then
            MsgBox("Vous n'avez pas sélectionné de Familles de Paramètres ! ", MsgBoxStyle.Exclamation, " QUADRISPEL - Sélection")
            Exit Sub
        End If
        ' ==================================
        ' Vérification qu'un Paramètre a été sélectionné pour chacune des familles sélectionnées
        For i = 0 To Familles.ListBox2.Items.Count - 1
            ParamValide = 0
            ' ==================================
            ' Recherche de l'adresse de la famille sélectionnée
            For j = 0 To 500
                ' ==================================
                ' Vérification si au moins 1 paramètre de chaque famille a été validé
                If LTrim(Familles.ListBox2.Items(i)) = Variables.TabRéfPara(j, 0) Then
                    If Val(Variables.TabRéfPara(j, 21)) <> 0 Then
                        ParamValide = ParamValide + 1
                    End If
                End If
            Next
            ' ==================================
            ' Sortie si une famille sélectionnée n'a pas de paramètre sélectionné
            If ParamValide = 0 Then Exit For
        Next
        ' ==================================
        ' Génération du message de non validation d'au moins une famille de paramètres
        If ParamValide = 0 Then
            MsgBox(" Au moins une famille sélectionnée n'a pas de paramètres sélectionnés ! ", MsgBoxStyle.Exclamation, " QUADRISPEL - Erreur de sélection")
            Exit Sub
        End If
        ' ==================================
        ' Initialisation de l'aiguilleur de tableau
        Variables.AiguilleurTableau = 0
        ' ==================================
        ' Validation des boutons de sélections pour la feuille Paramètres et du texte
        Propriétés_Param.TextBox1.Text = LTrim(Familles.ListBox2.Items.Item(0))
        Propriétés_Param.Button8.Enabled = False
        Propriétés_Param.Button9.Enabled = True
        If Familles.ListBox2.Items.Count < 2 Then
            Propriétés_Param.Button9.Enabled = False
        End If
        ' ==================================
        ' Affichage de la Feuille
        Propriétés_Param.ShowDialog()
    End Sub



    '=============================================================================================================================================
    ' PROCÉDURE SPEL
    '=============================================================================================================================================
    ' Configuration du fichier Personnel SPEL
    Private Sub ConfigurerToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ConfigurerToolStripMenuItem.Click
        ' ==================================
        ' Affichage de la Feuille
        SPEL.ShowDialog()
    End Sub

    '=============================================================================================================================================
    ' Copie du fichier Personnel SPEL dans le Presse Papier
    Private Sub PressePapierToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PressePapierToolStripMenuItem.Click
        Dim i As Integer
        Dim j As Integer
        Dim k As Integer
        Dim Sandre As Integer

        Dim LignePressePapier As Integer

        Dim PremierParam As Integer
        Dim NombreIndex As Integer
        Dim NombreParamètre As Integer
        Dim LigneParamètre As String

        Dim IndexTabInfo As Integer

        Dim DateduTransfert As String
        Dim Département As String
        Dim LieudeSurveillance As String
        Dim LieudeSurveillanceCodeSandre As String = ""
        Dim Réseau As String
        Dim RéseauCodeSandre As String = ""
        Dim Saisisseur As String
        Dim SaisisseurCodeSandre As String = ""
        Dim ZoneDestinationDragage As String
        Dim Campagne As String
        Dim Sortie As String
        Dim DatePassage As String
        Dim HeurePassage As String
        Dim Sonde As String
        Dim UnitéSonde As String
        Dim UnitéSondeCodeSandre As String = ""
        Dim MnémoniquePassage As String
        Dim CommentairesPassage As String
        Dim LatitudePassage As String
        Dim LongitudePassage As String
        Dim PositionnementPassage As String
        Dim PositionnementPassageCodeSandre As String = ""
        Dim NombreIndividuPassage As String
        Dim PointEntréeEnginPrélèvement As Integer
        Dim EnginPrélèvement As String
        Dim EnginPrélèvementCodeSandre As String = ""
        Dim NiveauPrélèvement As String
        Dim NiveauPrélèvementCodeSandre As String = ""
        Dim Préleveur As String
        Dim PréleveurCodeSandre As String = ""
        Dim MnémoniquePrélèvement As String
        Dim ImmersionPrélèvement As String
        Dim ImmersionMaxPrélèvement As String
        Dim ImmersionMinPrélèvement As String
        Dim UnitéImmersion As String
        Dim UnitéImmersionCodeSandre As String = ""
        Dim TaillePrélèvement As String
        Dim UnitéTaillePrélèvement As String
        Dim UnitéTaillePrélèvementCodeSandre As String = ""
        Dim HeurePrélèvement As String
        Dim CommentairesPrélèvement As String
        Dim LatitudePrélèvement As String
        Dim LongitudePrélèvement As String
        Dim PositionnementPrélèvement As String
        Dim PositionnementPrélèvementCodeSandre As String = ""
        Dim NombreIndividuPrélèvement As String
        Dim LotAquacole As String
        Dim SupportÉchantillon As String
        Dim SupportÉchantillonCodeSandre As String = ""
        Dim TaxonSupportÉchantillon As String
        Dim GroupeTaxonSupportÉchantillon As String
        Dim GroupeTaxonSupportÉchantillonCodeSandre As String = ""
        Dim NuméroÉchantillon As String
        Dim TailleÉchantillon As String
        Dim UnitéTailleÉchantillon As String
        Dim UnitéTailleÉchantillonCodeSandre As String = ""
        Dim CommentairesÉchantillon As String
        Dim NombreIndividuÉchantillon As String

        Dim NiveauSaisieRésultat As String
        Dim AcronymeNiveauSaisieRésultat As String = ""
        Dim PointEntréeNiveauSaisieRésultat As Integer
        Dim FamilleParamètre As String
        Dim Paramètre As String
        Dim AcronymeParamètre As String
        Dim ParamètreCodeSandre As String = ""
        Dim PointEntréeSupport As Integer
        Dim Support As String
        Dim SupportCodeSandre As String = ""
        Dim PointEntréeFraction As Integer
        Dim Fraction As String
        Dim FractionCodeSandre As String = ""
        Dim PointEntréeMéthode As Integer
        Dim Méthode As String
        Dim MéthodeCodeSandre As String = ""
        Dim NuméroIndividu As String
        Dim LibelléRésultatQualitatif As String
        Dim Résultat As String
        Dim PointEntréeUnité As Integer
        Dim UnitéRésultat As String
        Dim UnitéRésultatCodeSandre As String = ""
        Dim PointEntréeAnalyste As Integer
        Dim Analyste As String
        Dim AcronymeAnalyste As String
        Dim AnalysteCodeSandre As String = ""
        Dim PointEntréeEnginAnalyste As Integer
        Dim EnginAnalyse As String
        Dim EnginAnalyseCodeSandre As String = ""
        Dim SeuilAnalyse As String
        Dim PointEntréeRemarque As Integer
        Dim Remarque As String
        Dim RemarqueCodeSandre As String = ""
        Dim Précision As String
        Dim PointEntréeTypePrécision As Integer
        Dim TypePrécision As String
        Dim TypePrécisionCodeSandre As String = ""
        Dim CommentairesMesure As String


        Dim CodeRésultatQualitatif As String = ""
        Dim CodeTaxonRésultat As String = ""
        Dim CodeGroupeTaxonRésultat As String = ""

        ' ============================================================= 
        ' Remise à zéro du presse papier
        Clipboard.Clear()

        ' ============================================================= 
        ' TRANSFERT DES ENTÊTES SÉLECTIONNÉES DANS LE PRESSE PAPIER
        ' ============================================================= 

        For i = 0 To SPEL.ListBox2.Items.Count - 1
            Clipboard.SetText(Clipboard.GetText & LTrim(SPEL.ListBox2.Items.Item(i)) & Chr("9"))
        Next

        ' ============================================================= 
        ' COLLECTE DES INFORMATIONS COMMUNES AUX LIGNES DE PARAMÈTES
        ' ============================================================= 

        ' DATE DU TRANSFERT
        DateduTransfert = Today     '   (Format(Today, "dd-MM-yyyy"))
        ' ==================================
        ' DÉPARTEMENT
        Département = Passage.TextBox10.Text
        ' ==================================
        ' LIEU DE SURVEILLANCE
        LieudeSurveillance = Passage.TextBox24.Text
        ' ==================================
        ' LIEU DE SURVEILLANCE CODE SANDRE
        ' ==================================
        ' Recherche du point d'entrée dans le tableau des informations pour les Lieux de Surveillance
        For i = 0 To 5000
            If Variables.TabRéfInfo(i, 0) = "LIEU DE SURVEILLANCE" Then
                IndexTabInfo = i + 1
                Exit For
            End If
        Next
        ' ==================================
        ' Recherche du Code Sandre 
        For i = IndexTabInfo To 5000
            ' ==================================
            ' Sortie de la boucle si fin de tableau
            If Variables.TabRéfInfo(i, 0) = "" Then Exit For
            ' ==================================
            If Variables.TabRéfInfo(i, 1) = LieudeSurveillance Then
                ' ==================================
                ' Récupération du Code Sandre
                LieudeSurveillanceCodeSandre = Variables.TabRéfInfo(i, 2)
                Exit For
            End If
        Next
        ' ==================================
        ' RÉSEAU
        Réseau = Passage.TextBox3.Text
        ' ==================================
        ' RÉSEAU CODE SANDRE
        ' ==================================
        ' Recherche du point d'entrée dans le tableau des informations pour les Lieux de Surveillance
        For i = 0 To 5000
            If Variables.TabRéfInfo(i, 0) = "RÉSEAU" Then
                IndexTabInfo = i + 1
                Exit For
            End If
        Next
        ' ==================================
        ' Recherche du Code Sandre 
        For i = IndexTabInfo To 5000
            ' ==================================
            ' Sortie de la boucle si fin de tableau
            If Variables.TabRéfInfo(i, 0) = "" Then Exit For
            ' ==================================
            If Variables.TabRéfInfo(i, 0) = Réseau Then
                ' ==================================
                ' Récupération du Code Sandre
                RéseauCodeSandre = Variables.TabRéfInfo(i, 1)
                Exit For
            End If
        Next
        ' ==================================
        ' SAISISSEUR
        Saisisseur = Passage.TextBox23.Text
        ' ==================================
        ' SAISISSEUR CODE SANDRE
        ' ==================================
        ' Recherche du point d'entrée dans le tableau des informations pour les Lieux de Surveillance
        For i = 0 To 5000
            If Variables.TabRéfInfo(i, 0) = "SAISISSEUR" Then
                IndexTabInfo = i + 1
                Exit For
            End If
        Next
        ' ==================================
        ' Recherche du Code Sandre 
        For i = IndexTabInfo To 5000
            ' ==================================
            ' Sortie de la boucle si fin de tableau
            If Variables.TabRéfInfo(i, 0) = "" Then Exit For
            ' ==================================
            If Variables.TabRéfInfo(i, 0) = Saisisseur Then
                ' ==================================
                ' Récupération du Code Sandre
                SaisisseurCodeSandre = Variables.TabRéfInfo(i, 2)
                Exit For
            End If
        Next
        ' ==================================
        ' ZONE DESTINATION DRAGAGE
        ZoneDestinationDragage = Passage.TextBox5.Text
        ' ==================================
        ' CAMPAGNE
        Campagne = Passage.TextBox7.Text
        ' ==================================
        ' SORTIE
        Sortie = Passage.TextBox9.Text
        ' ==================================
        ' DATE PASSAGE
        DatePassage = Passage.TextBox11.Text
        ' ==================================
        ' HEURE PASSAGE
        HeurePassage = Passage.TextBox12.Text
        ' ==================================
        ' SONDE
        Sonde = Passage.TextBox14.Text
        ' ==================================
        ' UNITÉ SONDE
        UnitéSonde = Passage.TextBox15.Text
        ' ==================================
        ' UNITÉ SONDE CODE SANDRE
        ' ==================================
        ' Recherche du point d'entrée dans le tableau des informations pour les Lieux de Surveillance
        For i = 0 To 5000
            If Variables.TabRéfInfo(i, 0) = "UNITÉ SONDE" Then
                IndexTabInfo = i + 1
                Exit For
            End If
        Next
        ' ==================================
        ' Recherche du Code Sandre 
        For i = IndexTabInfo To 5000
            ' ==================================
            ' Sortie de la boucle si fin de tableau
            If Variables.TabRéfInfo(i, 0) = "" Then Exit For
            ' ==================================
            If Variables.TabRéfInfo(i, 0) = UnitéSonde Then
                ' ==================================
                ' Récupération du Code Sandre
                UnitéSondeCodeSandre = Variables.TabRéfInfo(i, 1)
                Exit For
            End If
        Next
        ' ==================================
        ' MNÉMONIQUE PASSAGE
        MnémoniquePassage = Passage.TextBox17.Text
        ' ==================================
        ' COMMENTAIRE PASSAGE
        CommentairesPassage = Passage.TextBox18.Text
        ' ==================================
        ' LATITUDE PASSAGE
        LatitudePassage = Passage.TextBox1.Text
        ' ==================================
        ' LONGITUDE PASSAGE
        LongitudePassage = Passage.TextBox2.Text
        ' ==================================
        ' POSITIONNEMENT PASSAGE
        PositionnementPassage = Passage.TextBox19.Text
        ' ==================================
        ' POSITIONNEMENT PASSAGE CODE SANDRE
        ' ==================================
        ' Recherche du point d'entrée dans le tableau des informations pour les Lieux de Surveillance
        For i = 0 To 5000
            If Variables.TabRéfInfo(i, 0) = "POSITIONNEMENT PASSAGE" Then
                IndexTabInfo = i + 1
                Exit For
            End If
        Next
        ' ==================================
        ' Recherche du Code Sandre 
        For i = IndexTabInfo To 5000
            ' ==================================
            ' Sortie de la boucle si fin de tableau
            If Variables.TabRéfInfo(i, 0) = "" Then Exit For
            ' ==================================
            If Variables.TabRéfInfo(i, 0) = PositionnementPassage Then
                ' ==================================
                ' Récupération du Code Sandre
                PositionnementPassageCodeSandre = Variables.TabRéfInfo(i, 1)
                Exit For
            End If
        Next
        ' ==================================
        ' NOMBRE INDIVIDU PASSAGE
        NombreIndividuPassage = Passage.TextBox20.Text
        ' ==================================
        ' ENGIN PRÉLÈVEMENT POINT ENTRÉE
        ' ==================================
        ' Recherche du point d'entrée ENGIN DE PRÉLÈVEMENT dans le tableau des informations
        For i = 0 To 5000
            ' ==================================
            If Variables.TabRéfInfo(i, 0) = "ENGIN PRÉLÈVEMENT" Then
                PointEntréeEnginPrélèvement = i + 1
                Exit For
            End If
        Next
        ' ==================================
        ' NIVEAU PRÉLÈVEMENT
        NiveauPrélèvement = Prélèvement.TextBox1.Text
        ' ==================================
        ' NIVEAU PRÉLÈVEMENT CODE SANDRE
        ' ==================================
        ' Recherche du point d'entrée dans le tableau des informations pour les Lieux de Surveillance
        For i = 0 To 5000
            If Variables.TabRéfInfo(i, 0) = "NIVEAU PRÉLÈVEMENT" Then
                IndexTabInfo = i + 1
                Exit For
            End If
        Next
        ' ==================================
        ' Recherche du Code Sandre 
        For i = IndexTabInfo To 5000
            ' ==================================
            ' Sortie de la boucle si fin de tableau
            If Variables.TabRéfInfo(i, 0) = "" Then Exit For
            ' ==================================
            If Variables.TabRéfInfo(i, 0) = NiveauPrélèvement Then
                ' ==================================
                ' Récupération du Code Sandre
                NiveauPrélèvementCodeSandre = Variables.TabRéfInfo(i, 1)
                Exit For
            End If
        Next
        ' ==================================
        ' PRÉLEVEUR
        Préleveur = Prélèvement.TextBox23.Text
        ' ==================================
        ' PRÉLEVEUR CODE SANDRE
        ' ==================================
        ' Recherche du point d'entrée dans le tableau des informations pour les Lieux de Surveillance
        For i = 0 To 5000
            If Variables.TabRéfInfo(i, 0) = "PRÉLEVEUR" Then
                IndexTabInfo = i + 1
                Exit For
            End If
        Next
        ' ==================================
        ' Recherche du Code Sandre 
        For i = IndexTabInfo To 5000
            ' ==================================
            ' Sortie de la boucle si fin de tableau
            If Variables.TabRéfInfo(i, 0) = "" Then Exit For
            ' ==================================
            If Variables.TabRéfInfo(i, 0) = Préleveur Then
                ' ==================================
                ' Récupération du Code Sandre
                PréleveurCodeSandre = Variables.TabRéfInfo(i, 2)
                Exit For
            End If
        Next
        ' ==================================
        ' MNÉMONIQUE PRÉLÈVEMENT
        MnémoniquePrélèvement = Prélèvement.TextBox2.Text
        ' ==================================
        ' IMMERSION PRÉLÈVEMENT
        ImmersionPrélèvement = Prélèvement.TextBox3.Text
        ' ==================================
        ' IMMERSION MAX PRÉLÈVEMENT
        ImmersionMaxPrélèvement = Prélèvement.TextBox6.Text
        ' ==================================
        ' IMMERSION MIN PRÉLÈVEMENT
        ImmersionMinPrélèvement = Prélèvement.TextBox5.Text
        ' ==================================
        ' UNITÉ IMMERSION
        UnitéImmersion = Prélèvement.TextBox7.Text
        ' ==================================
        ' UNITÉ IMMERSION CODE SANDRE
        ' ==================================
        ' Recherche du point d'entrée dans le tableau des informations pour les Lieux de Surveillance
        For i = 0 To 5000
            If Variables.TabRéfInfo(i, 0) = "UNITÉ IMMERSION" Then
                IndexTabInfo = i + 1
                Exit For
            End If
        Next
        ' ==================================
        ' Recherche du Code Sandre 
        For i = IndexTabInfo To 5000
            ' ==================================
            ' Sortie de la boucle si fin de tableau
            If Variables.TabRéfInfo(i, 0) = "" Then Exit For
            ' ==================================
            If Variables.TabRéfInfo(i, 0) = UnitéImmersion Then
                ' ==================================
                ' Récupération du Code Sandre
                UnitéImmersionCodeSandre = Variables.TabRéfInfo(i, 1)
                Exit For
            End If
        Next
        ' ==================================
        ' TAILLE PRÉLÈVEMENT
        TaillePrélèvement = Prélèvement.TextBox9.Text
        ' ==================================
        ' UNITÉ TAILLE PRÉLÈVEMENT
        UnitéTaillePrélèvement = Prélèvement.TextBox10.Text
        ' ==================================
        ' UNITÉ TAILLE PRÉLÈVEMENT CODE SANDRE
        ' ==================================
        ' Recherche du point d'entrée dans le tableau des informations pour les Lieux de Surveillance
        For i = 0 To 5000
            If Variables.TabRéfInfo(i, 0) = "UNITÉ TAILLE PRÉLÈVEMENT" Then
                IndexTabInfo = i + 1
                Exit For
            End If
        Next
        ' ==================================
        ' Recherche du Code Sandre 
        For i = IndexTabInfo To 5000
            ' ==================================
            ' Sortie de la boucle si fin de tableau
            If Variables.TabRéfInfo(i, 0) = "" Then Exit For
            ' ==================================
            If Variables.TabRéfInfo(i, 0) = UnitéTaillePrélèvement Then
                ' ==================================
                ' Récupération du Code Sandre
                UnitéTaillePrélèvementCodeSandre = Variables.TabRéfInfo(i, 1)
                Exit For
            End If
        Next
        ' ==================================
        ' HEURE PRÉLÈVEMENT
        HeurePrélèvement = Prélèvement.TextBox11.Text
        ' ==================================
        ' COMMENTAIRE PRÉLÈVEMENT
        CommentairesPrélèvement = Prélèvement.TextBox12.Text
        ' ==================================
        ' LATITUDE PRÉLÈVEMENT
        LatitudePrélèvement = Prélèvement.TextBox14.Text
        ' ==================================
        ' LONGITUDE PRÉLÈVEMENT
        LongitudePrélèvement = Prélèvement.TextBox15.Text
        ' ==================================
        ' POSITIONNEMENT PRÉLÈVEMENT
        PositionnementPrélèvement = Prélèvement.TextBox16.Text
        ' ==================================
        ' POSITIONNEMENT PRÉLÈVEMENT CODE SANDRE
        ' ==================================
        ' Recherche du point d'entrée dans le tableau des informations pour les Lieux de Surveillance
        For i = 0 To 5000
            If Variables.TabRéfInfo(i, 0) = "POSITIONNEMENT PRÉLÈVEMENT" Then
                IndexTabInfo = i + 1
                Exit For
            End If
        Next
        ' ==================================
        ' Recherche du Code Sandre 
        For i = IndexTabInfo To 5000
            ' ==================================
            ' Sortie de la boucle si fin de tableau
            If Variables.TabRéfInfo(i, 0) = "" Then Exit For
            ' ==================================
            If Variables.TabRéfInfo(i, 0) = PositionnementPrélèvement Then
                ' ==================================
                ' Récupération du Code Sandre
                PositionnementPrélèvementCodeSandre = Variables.TabRéfInfo(i, 1)
                Exit For
            End If
        Next
        ' ==================================
        ' NOMBRE INDIVIDU PRÉLÈVEMENT
        NombreIndividuPrélèvement = Prélèvement.TextBox17.Text
        ' ==================================
        ' LOT AQUACOLE
        LotAquacole = Échantillon.TextBox1.Text
        ' ==================================
        ' SUPPORT ÉCHANTILLON
        SupportÉchantillon = Échantillon.TextBox2.Text
        ' ==================================
        ' SUPPORT ÉCHANTILLON CODE SANDRE
        ' ==================================
        ' Recherche du point d'entrée dans le tableau des informations pour les Lieux de Surveillance
        For i = 0 To 5000
            If Variables.TabRéfInfo(i, 0) = "SUPPORT ÉCHANTILLON" Then
                IndexTabInfo = i + 1
                Exit For
            End If
        Next
        ' ==================================
        ' Recherche du Code Sandre 
        For i = IndexTabInfo To 5000
            ' ==================================
            ' Sortie de la boucle si fin de tableau
            If Variables.TabRéfInfo(i, 0) = "" Then Exit For
            ' ==================================
            If Variables.TabRéfInfo(i, 0) = SupportÉchantillon Then
                ' ==================================
                ' Récupération du Code Sandre
                SupportÉchantillonCodeSandre = Variables.TabRéfInfo(i, 1)
                Exit For
            End If
        Next
        ' ==================================
        ' TAXON SUPPORT ÉCHANTILLON
        TaxonSupportÉchantillon = Échantillon.TextBox3.Text
        ' ==================================
        ' GROUPE TAXON SUPPORT ÉCHANTILLON
        GroupeTaxonSupportÉchantillon = Échantillon.TextBox4.Text
        ' ==================================
        ' GROUPE TAXON SUPPORT ÉCHANTILLON CODE SANDRE
        ' ==================================
        ' Recherche du point d'entrée dans le tableau des informations pour les Lieux de Surveillance
        For i = 0 To 5000
            If Variables.TabRéfInfo(i, 0) = "GROUPE TAXON SUPPORT ÉCHANTILLON" Then
                IndexTabInfo = i + 1
                Exit For
            End If
        Next
        ' ==================================
        ' Recherche du Code Sandre 
        For i = IndexTabInfo To 5000
            ' ==================================
            ' Sortie de la boucle si fin de tableau
            If Variables.TabRéfInfo(i, 0) = "" Then Exit For
            ' ==================================
            If Variables.TabRéfInfo(i, 0) = GroupeTaxonSupportÉchantillon Then
                ' ==================================
                ' Récupération du Code Sandre
                GroupeTaxonSupportÉchantillonCodeSandre = Variables.TabRéfInfo(i, 1)
                Exit For
            End If
        Next
        ' ==================================
        ' NUMÉRO ÉCHANTILLON
        NuméroÉchantillon = Échantillon.TextBox5.Text
        ' ==================================
        ' TAILLE ÉCHANTILLON
        TailleÉchantillon = Échantillon.TextBox6.Text
        ' ==================================
        ' UNITÉ TAILLE ÉCHANTILLON
        UnitéTailleÉchantillon = Échantillon.TextBox7.Text
        ' ==================================
        ' UNITÉ TAILLE ÉCHANTILLON CODE SANDRE
        ' ==================================
        ' Recherche du point d'entrée dans le tableau des informations pour les Lieux de Surveillance
        For i = 0 To 5000
            If Variables.TabRéfInfo(i, 0) = "UNITÉ TAILLE ÉCHANTILLON" Then
                IndexTabInfo = i + 1
                Exit For
            End If
        Next
        ' ==================================
        ' Recherche du Code Sandre 
        For i = IndexTabInfo To 5000
            ' ==================================
            ' Sortie de la boucle si fin de tableau
            If Variables.TabRéfInfo(i, 0) = "" Then Exit For
            ' ==================================
            If Variables.TabRéfInfo(i, 0) = UnitéTailleÉchantillon Then
                ' ==================================
                ' Récupération du Code Sandre
                UnitéTailleÉchantillonCodeSandre = Variables.TabRéfInfo(i, 1)
                Exit For
            End If
        Next
        ' ==================================
        ' COMMENTAIRES ÉCHANTILLON
        CommentairesÉchantillon = Échantillon.TextBox9.Text
        ' ==================================
        ' NOMBRE INDIVIDU ÉCHANTILLON
        NombreIndividuÉchantillon = Échantillon.TextBox8.Text

        ' ==================================
        ' SUPPORT POINT ENTRÉE
        ' ==================================
        ' Recherche du point d'entrée SUPPORT dans le tableau des informations
        For i = 0 To 5000
            ' ==================================
            If Variables.TabRéfInfo(i, 0) = "SUPPORT" Then
                PointEntréeSupport = i + 1
                Exit For
            End If
        Next

        ' ==================================
        ' FRACTION POINT ENTRÉE
        ' ==================================
        ' Recherche du point d'entrée FRACTION dans le tableau des informations
        For i = 0 To 5000
            ' ==================================
            If Variables.TabRéfInfo(i, 0) = "FRACTION" Then
                PointEntréeFraction = i + 1
                Exit For
            End If
        Next

        ' ==================================
        ' MÉTHODE POINT ENTRÉE
        ' ==================================
        ' Recherche du point d'entrée MÉTHODE dans le tableau des informations
        For i = 0 To 5000
            ' ==================================
            If Variables.TabRéfInfo(i, 0) = "MÉTHODE" Then
                PointEntréeMéthode = i + 1
                Exit For
            End If
        Next

        ' ==================================
        ' UNITÉ POINT ENTRÉE
        ' ==================================
        ' Recherche du point d'entrée UNITÉ dans le tableau des informations
        For i = 0 To 5000
            ' ==================================
            If Variables.TabRéfInfo(i, 0) = "UNITÉ" Then
                PointEntréeUnité = i + 1
                Exit For
            End If
        Next

        ' ==================================
        ' ANALYSTE POINT ENTRÉE
        ' ==================================
        ' Recherche du point d'entrée ANALYSTE dans le tableau des informations
        For i = 0 To 5000
            ' ==================================
            If Variables.TabRéfInfo(i, 0) = "ANALYSEUR" Then
                PointEntréeAnalyste = i + 1
                Exit For
            End If
        Next

        ' ==================================
        ' ENGIN ANALYSE POINT ENTRÉE
        ' ==================================
        ' Recherche du point d'entrée ENGIN ANALYSE dans le tableau des informations
        For i = 0 To 5000
            ' ==================================
            If Variables.TabRéfInfo(i, 0) = "ENGIN ANALYSE" Then
                PointEntréeEnginAnalyste = i + 1
                Exit For
            End If
        Next

        ' ==================================
        ' REMARQUE POINT ENTRÉE
        ' ==================================
        ' Recherche du point d'entrée REMARQUE dans le tableau des informations
        For i = 0 To 5000
            ' ==================================
            If Variables.TabRéfInfo(i, 0) = "CODE REMARQUE" Then
                PointEntréeRemarque = i + 1
                Exit For
            End If
        Next

        ' ==================================
        ' TYPE PRÉCISION POINT ENTRÉE
        ' ==================================
        ' Recherche du point d'entrée TYPE PRÉCISION dans le tableau des informations
        For i = 0 To 5000
            ' ==================================
            If Variables.TabRéfInfo(i, 0) = "TYPE PRÉCISION" Then
                PointEntréeTypePrécision = i + 1
                Exit For
            End If
        Next

        ' ==================================
        ' NIVEAU SAISIE RÉSULTAT POINT ENTRÉE
        ' ==================================
        ' Recherche du point d'entrée NIVEAU SAISIE dans le tableau des informations
        For i = 0 To 5000
            ' ==================================
            If Variables.TabRéfInfo(i, 0) = "NIVEAU SAISIE" Then
                PointEntréeNiveauSaisieRésultat = i + 1
                Exit For
            End If
        Next


        ' ============================================================= 
        ' TRANSFERT DES INFORMATIONS PARAMÈTRES
        ' ============================================================= 

        ' ==================================
        ' Balayage des noms des Familles sélectionnées dans la feuille des Familles
        For i = 0 To Familles.ListBox2.Items.Count - 1
            ' ==================================
            ' Recherche de la première adresse de la Famille sélectionnée
            For j = 0 To 500
                If LTrim(Familles.ListBox2.Items(i)) = Variables.TabRéfPara(j, 0) Then
                    PremierParam = j
                    Exit For
                End If
            Next
            ' ==================================
            ' Initialisation des indicateurs
            NombreIndex = 0
            NombreParamètre = 0
            ' ==================================
            ' Recherche du nombre de Paramètres et du nombre de Paramètres indexés pour la Famille sélectionnée
            For j = PremierParam To 500
                ' ==================================
                ' Sortie si tous les Paramètres de la Famille sélectionnée ont été balayés
                If LTrim(Familles.ListBox2.Items(i)) <> Variables.TabRéfPara(j, 0) Then
                    Exit For
                End If
                ' ==================================
                ' Incrémentation de l'indicateur du nombre de Paramètres indexés pour la Famille sélectionnée
                If Val(Variables.TabRéfPara(j, 21)) <> 0 Then
                    NombreIndex = NombreIndex + 1
                End If
                ' ==================================
                ' Incrémentation de l'indicateur du nombre de Paramètres pour la Famille sélectionnée
                NombreParamètre = NombreParamètre + 1
            Next
            ' ==================================
            ' Balayage des Paramètres pour le transfert des informations du paramètre sélectionné de la famille sélectionnée dans le Presse Papier
            For k = 1 To NombreIndex


                NiveauSaisieRésultat = ""
                FamilleParamètre = ""                     ' 0
                Paramètre = ""                            ' 1
                AcronymeParamètre = ""                    ' 2
                ParamètreCodeSandre = ""                  ' 3
                Analyste = ""                             ' 4
                AnalysteCodeSandre = ""
                AcronymeAnalyste = ""                     ' 5
                Méthode = ""                              ' 6
                MéthodeCodeSandre = ""
                EnginAnalyse = ""                         ' 7
                EnginAnalyseCodeSandre = ""
                Fraction = ""                             ' 8
                FractionCodeSandre = ""
                Support = ""                              ' 9
                SupportCodeSandre = ""
                EnginPrélèvement = ""                     ' 10
                EnginPrélèvementCodeSandre = ""
                Précision = ""                            ' 11
                TypePrécision = ""                        ' 12
                TypePrécisionCodeSandre = ""
                NiveauSaisieRésultat = ""                 ' 13
                AcronymeNiveauSaisieRésultat = ""
                SeuilAnalyse = ""                         ' 14
                LibelléRésultatQualitatif = ""                   ' 15
                NuméroIndividu = ""                       ' 16
                Résultat = ""                             ' 17
                UnitéRésultat = ""                        ' 18
                UnitéRésultatCodeSandre = ""
                CommentairesMesure = ""                   ' 19
                Remarque = ""                             ' 20
                RemarqueCodeSandre = ""

                CodeRésultatQualitatif = ""                     ' 23
                CodeGroupeTaxonRésultat = ""                    ' 24
                CodeTaxonRésultat = ""                          ' 25


                ' ==================================
                ' Balayage des index pour le transfert des informations du paramètre sélectionné de la famille sélectionnée dans le Presse Papier
                For j = PremierParam To PremierParam + NombreParamètre - 1
                    ' ==================================
                    ' Recherche du Paramètre dont l'index correspond à l'index recherché
                    If Variables.TabRéfPara(j, 21) = k Then


                        ' ==================================
                        ' Initialisation de l'indicateur
                        LigneParamètre = ""

                        ' ==================================
                        ' Création des lignes de paramètre
                        For LignePressePapier = 0 To SPEL.ListBox2.Items.Count - 1


                            ' ==================================
                            ' Vérification si la ligne sélectionnée correspond à "DateduTransfert"
                            If SPEL.ListBox2.Items.Item(LignePressePapier) = " Date du Transfert" Then
                                LigneParamètre = LigneParamètre & DateduTransfert
                                '                               MsgBox(k & "   " & LignePressePapier)
                            End If
                            ' ==================================
                            ' Vérification si la ligne sélectionnée correspond à "Département"
                            If SPEL.ListBox2.Items.Item(LignePressePapier) = " Département" Then
                                LigneParamètre = LigneParamètre & Département
                            End If
                            ' ==================================
                            ' Vérification si la ligne sélectionnée correspond à "LieudeSurveillance"
                            If SPEL.ListBox2.Items.Item(LignePressePapier) = " Lieu de Surveillance" Then
                                LigneParamètre = LigneParamètre & LieudeSurveillance
                            End If
                            ' ==================================
                            ' Vérification si la ligne sélectionnée correspond à "LieudeSurveillanceCodeSandre"
                            If SPEL.ListBox2.Items.Item(LignePressePapier) = " Lieu de Surveillance Code Sandre" Then
                                LigneParamètre = LigneParamètre & LieudeSurveillanceCodeSandre
                            End If
                            ' ==================================
                            ' Vérification si la ligne sélectionnée correspond à "Réseau"
                            If SPEL.ListBox2.Items.Item(LignePressePapier) = " Réseau" Then
                                LigneParamètre = LigneParamètre & Réseau
                            End If
                            ' ==================================
                            ' Vérification si la ligne sélectionnée correspond à "RéseauCodeSandre"
                            If SPEL.ListBox2.Items.Item(LignePressePapier) = " Réseau Code Sandre" Then
                                LigneParamètre = LigneParamètre & RéseauCodeSandre
                            End If
                            ' ==================================
                            ' Vérification si la ligne sélectionnée correspond à "Saisisseur"
                            If SPEL.ListBox2.Items.Item(LignePressePapier) = " Saisisseur" Then
                                LigneParamètre = LigneParamètre & Saisisseur
                            End If
                            ' ==================================
                            ' Vérification si la ligne sélectionnée correspond à "SaisisseurCodeSandre"
                            If SPEL.ListBox2.Items.Item(LignePressePapier) = " Saisisseur Code Sandre" Then
                                LigneParamètre = LigneParamètre & SaisisseurCodeSandre
                            End If
                            ' ==================================
                            ' Vérification si la ligne sélectionnée correspond à "ZoneDestinationDragage"
                            If SPEL.ListBox2.Items.Item(LignePressePapier) = " Zone Destination Dragage" Then
                                LigneParamètre = LigneParamètre & ZoneDestinationDragage
                            End If
                            ' ==================================
                            ' Vérification si la ligne sélectionnée correspond à "Campagne"
                            If SPEL.ListBox2.Items.Item(LignePressePapier) = " Campagne" Then
                                LigneParamètre = LigneParamètre & Campagne
                            End If
                            ' ==================================
                            ' Vérification si la ligne sélectionnée correspond à "Sortie"
                            If SPEL.ListBox2.Items.Item(LignePressePapier) = " Sortie" Then
                                LigneParamètre = LigneParamètre & Sortie
                            End If
                            ' ==================================
                            ' Vérification si la ligne sélectionnée correspond à "DatePassage"
                            If SPEL.ListBox2.Items.Item(LignePressePapier) = " Date Passage" Then
                                LigneParamètre = LigneParamètre & DatePassage
                            End If
                            ' ==================================
                            ' Vérification si la ligne sélectionnée correspond à "HeurePassage"
                            If SPEL.ListBox2.Items.Item(LignePressePapier) = " Heure Passage" Then
                                LigneParamètre = LigneParamètre & HeurePassage
                            End If
                            ' ==================================
                            ' Vérification si la ligne sélectionnée correspond à "Sonde"
                            If SPEL.ListBox2.Items.Item(LignePressePapier) = " Sonde" Then
                                LigneParamètre = LigneParamètre & Sonde
                            End If
                            ' ==================================
                            ' Vérification si la ligne sélectionnée correspond à "UnitéSonde"
                            If SPEL.ListBox2.Items.Item(LignePressePapier) = " Unité Sonde" Then
                                LigneParamètre = LigneParamètre & UnitéSonde
                            End If
                            ' ==================================
                            ' Vérification si la ligne sélectionnée correspond à "UnitéSondeCodeSandre"
                            If SPEL.ListBox2.Items.Item(LignePressePapier) = " Unité Sonde Code Sandre" Then
                                LigneParamètre = LigneParamètre & UnitéSondeCodeSandre
                            End If
                            ' ==================================
                            ' Vérification si la ligne sélectionnée correspond à "MnémoniquePassage"
                            If SPEL.ListBox2.Items.Item(LignePressePapier) = " Mnémonique Passage" Then
                                LigneParamètre = LigneParamètre & MnémoniquePassage
                            End If
                            ' ==================================
                            ' Vérification si la ligne sélectionnée correspond à "CommentairesPassage"
                            If SPEL.ListBox2.Items.Item(LignePressePapier) = " Commentaires Passage" Then
                                LigneParamètre = LigneParamètre & CommentairesPassage
                            End If
                            ' ==================================
                            ' Vérification si la ligne sélectionnée correspond à "LatitudePassage"
                            If SPEL.ListBox2.Items.Item(LignePressePapier) = " Latitude Passage" Then
                                LigneParamètre = LigneParamètre & LatitudePassage
                            End If
                            ' ==================================
                            ' Vérification si la ligne sélectionnée correspond à "LongitudePassage"
                            If SPEL.ListBox2.Items.Item(LignePressePapier) = " Longitude Passage" Then
                                LigneParamètre = LigneParamètre & LongitudePassage
                            End If
                            ' ==================================
                            ' Vérification si la ligne sélectionnée correspond à "PositionnementPassage"
                            If SPEL.ListBox2.Items.Item(LignePressePapier) = " Positionnement Passage" Then
                                LigneParamètre = LigneParamètre & PositionnementPassage
                            End If
                            ' ==================================
                            ' Vérification si la ligne sélectionnée correspond à "PositionnementPassageCodeSandre"
                            If SPEL.ListBox2.Items.Item(LignePressePapier) = " Positionnement Passage Code Sandre" Then
                                LigneParamètre = LigneParamètre & PositionnementPassageCodeSandre
                            End If
                            ' ==================================
                            ' Vérification si la ligne sélectionnée correspond à "NombreIndividuPassage"
                            If SPEL.ListBox2.Items.Item(LignePressePapier) = " Nombre Individu Passage" Then
                                LigneParamètre = LigneParamètre & NombreIndividuPassage
                            End If
                            ' ==================================
                            ' Vérification si la ligne sélectionnée correspond à "EnginPrélèvement"
                            If SPEL.ListBox2.Items.Item(LignePressePapier) = " Engin Prélèvement" Then
                                EnginPrélèvement = Variables.TabRéfPara(j, 10)
                                LigneParamètre = LigneParamètre & EnginPrélèvement
                            End If
                            ' ==================================
                            ' Vérification si la ligne sélectionnée correspond à "EnginPrélèvementCodeSandre"
                            If SPEL.ListBox2.Items.Item(LignePressePapier) = " Engin Prélèvement Code Sandre" Then
                                ' ==================================
                                ' Recherche du Code Sandre correspondant
                                For Sandre = PointEntréeEnginPrélèvement To 5000
                                    ' ==================================
                                    ' Sortie de la boucle si fin de tableau
                                    If Variables.TabRéfInfo(Sandre, 0) = "" Then Exit For
                                    ' ==================================
                                    If Variables.TabRéfInfo(Sandre, 0) = Variables.TabRéfPara(j, 10) Then
                                        ' ==================================
                                        ' Récupération du Code Sandre
                                        EnginPrélèvementCodeSandre = Variables.TabRéfInfo(Sandre, 1)
                                        Exit For
                                    End If
                                Next
                                LigneParamètre = LigneParamètre & EnginPrélèvementCodeSandre
                            End If
                            ' ==================================
                            ' Vérification si la ligne sélectionnée correspond à "NiveauPrélèvement"
                            If SPEL.ListBox2.Items.Item(LignePressePapier) = " Niveau Prélèvement" Then
                                LigneParamètre = LigneParamètre & NiveauPrélèvement
                            End If
                            ' ==================================
                            ' Vérification si la ligne sélectionnée correspond à "NiveauPrélèvementCodeSandre"
                            If SPEL.ListBox2.Items.Item(LignePressePapier) = " Niveau Prélèvement Code Sandre" Then
                                LigneParamètre = LigneParamètre & NiveauPrélèvementCodeSandre
                            End If
                            ' ==================================
                            ' Vérification si la ligne sélectionnée correspond à "Préleveur"
                            If SPEL.ListBox2.Items.Item(LignePressePapier) = " Préleveur" Then
                                LigneParamètre = LigneParamètre & Préleveur
                            End If
                            ' ==================================
                            ' Vérification si la ligne sélectionnée correspond à "PréleveurCodeSandre"
                            If SPEL.ListBox2.Items.Item(LignePressePapier) = " Préleveur Code Sandre" Then
                                LigneParamètre = LigneParamètre & PréleveurCodeSandre
                            End If
                            ' ==================================
                            ' Vérification si la ligne sélectionnée correspond à "MnémoniquePrélèvement"
                            If SPEL.ListBox2.Items.Item(LignePressePapier) = " Mnémonique Prélèvement" Then
                                LigneParamètre = LigneParamètre & MnémoniquePrélèvement
                            End If
                            ' ==================================
                            ' Vérification si la ligne sélectionnée correspond à "ImmersionPrélèvement"
                            If SPEL.ListBox2.Items.Item(LignePressePapier) = " Immersion Prélèvement" Then
                                LigneParamètre = LigneParamètre & ImmersionPrélèvement
                            End If
                            ' ==================================
                            ' Vérification si la ligne sélectionnée correspond à "ImmersionMaxPrélèvement"
                            If SPEL.ListBox2.Items.Item(LignePressePapier) = " Immersion Max Prélèvement" Then
                                LigneParamètre = LigneParamètre & ImmersionMaxPrélèvement
                            End If
                            ' ==================================
                            ' Vérification si la ligne sélectionnée correspond à "ImmersionMinPrélèvement"
                            If SPEL.ListBox2.Items.Item(LignePressePapier) = " Immersion Min Prélèvement" Then
                                LigneParamètre = LigneParamètre & ImmersionMinPrélèvement
                            End If
                            ' ==================================
                            ' Vérification si la ligne sélectionnée correspond à "UnitéImmersion"
                            If SPEL.ListBox2.Items.Item(LignePressePapier) = " Unité Immersion" Then
                                LigneParamètre = LigneParamètre & UnitéImmersion
                            End If
                            ' ==================================
                            ' Vérification si la ligne sélectionnée correspond à "UnitéImmersionCodeSandre"
                            If SPEL.ListBox2.Items.Item(LignePressePapier) = " Unité Immersion Code Sandre" Then
                                LigneParamètre = LigneParamètre & UnitéImmersionCodeSandre
                            End If
                            ' ==================================
                            ' Vérification si la ligne sélectionnée correspond à "TaillePrélèvement"
                            If SPEL.ListBox2.Items.Item(LignePressePapier) = " Taille Prélèvement" Then
                                LigneParamètre = LigneParamètre & TaillePrélèvement
                            End If
                            ' ==================================
                            ' Vérification si la ligne sélectionnée correspond à "UnitéTaillePrélèvement"
                            If SPEL.ListBox2.Items.Item(LignePressePapier) = " Unité Taille Prélèvement" Then
                                LigneParamètre = LigneParamètre & UnitéTaillePrélèvement
                            End If
                            ' ==================================
                            ' Vérification si la ligne sélectionnée correspond à "UnitéTaillePrélèvementCodeSandre"
                            If SPEL.ListBox2.Items.Item(LignePressePapier) = " Unité Taille Prélèvement Code Sandre" Then
                                LigneParamètre = LigneParamètre & UnitéTaillePrélèvementCodeSandre
                            End If
                            ' ==================================
                            ' Vérification si la ligne sélectionnée correspond à "HeurePrélèvement"
                            If SPEL.ListBox2.Items.Item(LignePressePapier) = " Heure Prélèvement" Then
                                LigneParamètre = LigneParamètre & HeurePrélèvement
                            End If
                            ' ==================================
                            ' Vérification si la ligne sélectionnée correspond à "CommentairesPrélèvement"
                            If SPEL.ListBox2.Items.Item(LignePressePapier) = " Commentaires Prélèvement" Then
                                LigneParamètre = LigneParamètre & CommentairesPrélèvement
                            End If
                            ' ==================================
                            ' Vérification si la ligne sélectionnée correspond à "LatitudePrélèvement"
                            If SPEL.ListBox2.Items.Item(LignePressePapier) = " Latitude Prélèvement" Then
                                LigneParamètre = LigneParamètre & LatitudePrélèvement
                            End If
                            ' ==================================
                            ' Vérification si la ligne sélectionnée correspond à "LongitudePrélèvement"
                            If SPEL.ListBox2.Items.Item(LignePressePapier) = " Longitude Prélèvement" Then
                                LigneParamètre = LigneParamètre & LongitudePrélèvement
                            End If
                            ' ==================================
                            ' Vérification si la ligne sélectionnée correspond à "PositionnementPrélèvement"
                            If SPEL.ListBox2.Items.Item(LignePressePapier) = " Positionnement Prélèvement" Then
                                LigneParamètre = LigneParamètre & PositionnementPrélèvement
                            End If
                            ' ==================================
                            ' Vérification si la ligne sélectionnée correspond à "PositionnementPrélèvementCodeSandre"
                            If SPEL.ListBox2.Items.Item(LignePressePapier) = " Positionnement Prélèvement Code Sandre" Then
                                LigneParamètre = LigneParamètre & PositionnementPrélèvementCodeSandre
                            End If
                            ' ==================================
                            ' Vérification si la ligne sélectionnée correspond à "NombreIndividuPrélèvement"
                            If SPEL.ListBox2.Items.Item(LignePressePapier) = " Nombre Individu Prélèvement" Then
                                LigneParamètre = LigneParamètre & NombreIndividuPrélèvement
                            End If
                            ' ==================================
                            ' Vérification si la ligne sélectionnée correspond à "LotAquacole"
                            If SPEL.ListBox2.Items.Item(LignePressePapier) = " Lot Aquacole" Then
                                LigneParamètre = LigneParamètre & LotAquacole
                            End If
                            ' ==================================
                            ' Vérification si la ligne sélectionnée correspond à "SupportÉchantillon"
                            If SPEL.ListBox2.Items.Item(LignePressePapier) = " Support Échantillon" Then
                                LigneParamètre = LigneParamètre & SupportÉchantillon
                            End If
                            ' ==================================
                            ' Vérification si la ligne sélectionnée correspond à "SupportÉchantillonCodeSandre"
                            If SPEL.ListBox2.Items.Item(LignePressePapier) = " Support Échantillon Code Sandre" Then
                                LigneParamètre = LigneParamètre & SupportÉchantillonCodeSandre
                            End If
                            ' ==================================
                            ' Vérification si la ligne sélectionnée correspond à "TaxonSupportÉchantillon"
                            If SPEL.ListBox2.Items.Item(LignePressePapier) = " Taxon Support Échantillon" Then
                                LigneParamètre = LigneParamètre & TaxonSupportÉchantillon
                            End If
                            ' ==================================
                            ' Vérification si la ligne sélectionnée correspond à "GroupeTaxonSupportÉchantillon"
                            If SPEL.ListBox2.Items.Item(LignePressePapier) = " Groupe Taxon Support Échantillon" Then
                                LigneParamètre = LigneParamètre & GroupeTaxonSupportÉchantillon
                            End If
                            ' ==================================
                            ' Vérification si la ligne sélectionnée correspond à "GroupeTaxonSupportÉchantillonCodeSandre"
                            If SPEL.ListBox2.Items.Item(LignePressePapier) = " Groupe Taxon Support Échantillon Code Sandre" Then
                                LigneParamètre = LigneParamètre & GroupeTaxonSupportÉchantillonCodeSandre
                            End If
                            ' ==================================
                            ' Vérification si la ligne sélectionnée correspond à "NuméroÉchantillon"
                            If SPEL.ListBox2.Items.Item(LignePressePapier) = " Numéro Échantillon" Then
                                LigneParamètre = LigneParamètre & NuméroÉchantillon
                            End If
                            ' ==================================
                            ' Vérification si la ligne sélectionnée correspond à "TailleÉchantillon"
                            If SPEL.ListBox2.Items.Item(LignePressePapier) = " Taille Échantillon" Then
                                LigneParamètre = LigneParamètre & TailleÉchantillon
                            End If
                            ' ==================================
                            ' Vérification si la ligne sélectionnée correspond à "UnitéTailleÉchantillon"
                            If SPEL.ListBox2.Items.Item(LignePressePapier) = " Unité Taille Échantillon" Then
                                LigneParamètre = LigneParamètre & UnitéTailleÉchantillon
                            End If
                            ' ==================================
                            ' Vérification si la ligne sélectionnée correspond à "UnitéTailleÉchantillonCodeSandre"
                            If SPEL.ListBox2.Items.Item(LignePressePapier) = " Unité Taille Échantillon Code Sandre" Then
                                LigneParamètre = LigneParamètre & UnitéTailleÉchantillonCodeSandre
                            End If
                            ' ==================================
                            ' Vérification si la ligne sélectionnée correspond à "CommentairesÉchantillon"
                            If SPEL.ListBox2.Items.Item(LignePressePapier) = " Commentaires Échantillon" Then
                                LigneParamètre = LigneParamètre & CommentairesÉchantillon
                            End If
                            ' ==================================
                            ' Vérification si la ligne sélectionnée correspond à "NombreIndividuÉchantillon"
                            If SPEL.ListBox2.Items.Item(LignePressePapier) = " Nombre Individu Échantillon" Then
                                LigneParamètre = LigneParamètre & NombreIndividuÉchantillon
                            End If

                            ' ==================================
                            ' Vérification si la ligne sélectionnée correspond à "NiveauSaisieRésultat"
                            If SPEL.ListBox2.Items.Item(LignePressePapier) = " Niveau Saisie Résultat" Then
                                NiveauSaisieRésultat = Variables.TabRéfPara(j, 13)
                                LigneParamètre = LigneParamètre & NiveauSaisieRésultat
                            End If
                            ' ==================================
                            ' Vérification si la ligne sélectionnée correspond à "AcronymeNiveauSaisieRésultat"
                            If SPEL.ListBox2.Items.Item(LignePressePapier) = " Acronyme Niveau Saisie Résultat" Then
                                ' ==================================
                                ' Recherche du Code Sandre correspondant
                                For Sandre = PointEntréeNiveauSaisieRésultat To 5000
                                    ' ==================================
                                    ' Sortie de la boucle si fin de tableau
                                    If Variables.TabRéfInfo(Sandre, 0) = "" Then Exit For
                                    ' ==================================
                                    If Variables.TabRéfInfo(Sandre, 0) = Variables.TabRéfPara(j, 13) Then
                                        ' ==================================
                                        ' Récupération du Code Sandre
                                        AcronymeNiveauSaisieRésultat = Variables.TabRéfInfo(Sandre, 1)
                                        Exit For
                                    End If
                                Next
                                LigneParamètre = LigneParamètre & AcronymeNiveauSaisieRésultat
                            End If

                            ' ==================================
                            ' Vérification si la ligne sélectionnée correspond à "FamilleParamètre"
                            If SPEL.ListBox2.Items.Item(LignePressePapier) = " Famille Paramètre" Then
                                FamilleParamètre = Variables.TabRéfPara(j, 0)
                                LigneParamètre = LigneParamètre & FamilleParamètre
                            End If
                            ' ==================================
                            ' Vérification si la ligne sélectionnée correspond à "Paramètre"
                            If SPEL.ListBox2.Items.Item(LignePressePapier) = " Paramètre" Then
                                Paramètre = Variables.TabRéfPara(j, 1)
                                LigneParamètre = LigneParamètre & Paramètre
                            End If
                            ' ==================================
                            ' Vérification si la ligne sélectionnée correspond à "AcronymeParamètre"
                            If SPEL.ListBox2.Items.Item(LignePressePapier) = " Acronyme Paramètre" Then
                                AcronymeParamètre = Variables.TabRéfPara(j, 2)
                                LigneParamètre = LigneParamètre & AcronymeParamètre
                            End If
                            ' ==================================
                            ' Vérification si la ligne sélectionnée correspond à "ParamètreCodeSandre"
                            If SPEL.ListBox2.Items.Item(LignePressePapier) = " Paramètre Code Sandre" Then
                                ParamètreCodeSandre = Variables.TabRéfPara(j, 3)
                                LigneParamètre = LigneParamètre & ParamètreCodeSandre
                            End If

                            ' ==================================
                            ' Vérification si la ligne sélectionnée correspond à "Support"
                            If SPEL.ListBox2.Items.Item(LignePressePapier) = " Support" Then
                                Support = Variables.TabRéfPara(j, 9)
                                LigneParamètre = LigneParamètre & Support
                            End If
                            ' ==================================
                            ' Vérification si la ligne sélectionnée correspond à "SupportCodeSandre"
                            If SPEL.ListBox2.Items.Item(LignePressePapier) = " Support Code Sandre" Then
                                ' ==================================
                                ' Recherche du Code Sandre correspondant
                                For Sandre = PointEntréeSupport To 5000
                                    ' ==================================
                                    ' Sortie de la boucle si fin de tableau
                                    If Variables.TabRéfInfo(Sandre, 0) = "" Then Exit For
                                    ' ==================================
                                    If Variables.TabRéfInfo(Sandre, 0) = Variables.TabRéfPara(j, 9) Then
                                        ' ==================================
                                        ' Récupération du Code Sandre
                                        SupportCodeSandre = Variables.TabRéfInfo(Sandre, 1)
                                        Exit For
                                    End If
                                Next
                                LigneParamètre = LigneParamètre & SupportCodeSandre
                            End If

                            ' ==================================
                            ' Vérification si la ligne sélectionnée correspond à "Fraction"
                            If SPEL.ListBox2.Items.Item(LignePressePapier) = " Fraction" Then
                                Fraction = Variables.TabRéfPara(j, 8)
                                LigneParamètre = LigneParamètre & Fraction
                            End If
                            ' ==================================
                            ' Vérification si la ligne sélectionnée correspond à "FractionCodeSandre"
                            If SPEL.ListBox2.Items.Item(LignePressePapier) = " Fraction Code Sandre" Then
                                ' ==================================
                                ' Recherche du Code Sandre correspondant
                                For Sandre = PointEntréeFraction To 5000
                                    ' ==================================
                                    ' Sortie de la boucle si fin de tableau
                                    If Variables.TabRéfInfo(Sandre, 0) = "" Then Exit For
                                    ' ==================================
                                    If Variables.TabRéfInfo(Sandre, 0) = Variables.TabRéfPara(j, 8) Then
                                        ' ==================================
                                        ' Récupération du Code Sandre
                                        FractionCodeSandre = Variables.TabRéfInfo(Sandre, 1)
                                        Exit For
                                    End If
                                Next
                                LigneParamètre = LigneParamètre & FractionCodeSandre
                            End If

                            ' ==================================
                            ' Vérification si la ligne sélectionnée correspond à "Méthode"
                            If SPEL.ListBox2.Items.Item(LignePressePapier) = " Méthode" Then
                                Méthode = Variables.TabRéfPara(j, 6)
                                LigneParamètre = LigneParamètre & Méthode
                            End If
                            ' ==================================
                            ' Vérification si la ligne sélectionnée correspond à "MéthodeCodeSandre"
                            If SPEL.ListBox2.Items.Item(LignePressePapier) = " Méthode Code Sandre" Then
                                ' ==================================
                                ' Recherche du Code Sandre correspondant
                                For Sandre = PointEntréeMéthode To 5000
                                    ' ==================================
                                    ' Sortie de la boucle si fin de tableau
                                    If Variables.TabRéfInfo(Sandre, 0) = "" Then Exit For
                                    ' ==================================
                                    If Variables.TabRéfInfo(Sandre, 0) = Variables.TabRéfPara(j, 6) Then
                                        ' ==================================
                                        ' Récupération du Code Sandre
                                        MéthodeCodeSandre = Variables.TabRéfInfo(Sandre, 1)
                                        Exit For
                                    End If
                                Next
                                LigneParamètre = LigneParamètre & MéthodeCodeSandre
                            End If

                            ' ==================================
                            ' Vérification si la ligne sélectionnée correspond à "NuméroIndividu"
                            If SPEL.ListBox2.Items.Item(LignePressePapier) = " Numéro Individu" Then
                                NuméroIndividu = Variables.TabRéfPara(j, 16)
                                LigneParamètre = LigneParamètre & NuméroIndividu
                            End If
                            ' ==================================
                            ' Vérification si la ligne sélectionnée correspond à "Libellé RésultatQualitatif"
                            If SPEL.ListBox2.Items.Item(LignePressePapier) = " Libellé Résultat Qualitatif" Then
                                LibelléRésultatQualitatif = Variables.TabRéfPara(j, 15)
                                LigneParamètre = LigneParamètre & LibelléRésultatQualitatif
                            End If


                            ' ==================================
                            ' Vérification si la ligne sélectionnée correspond à "Code Résultat Qualitatif"
                            If SPEL.ListBox2.Items.Item(LignePressePapier) = " Code Résultat Qualitatif" Then
                                LibelléRésultatQualitatif = Variables.TabRéfPara(j, 23)
                                LigneParamètre = LigneParamètre & LibelléRésultatQualitatif
                            End If
                            ' ==================================
                            ' Vérification si la ligne sélectionnée correspond à "Code Groupe Taxon Résultat"
                            If SPEL.ListBox2.Items.Item(LignePressePapier) = " Code Groupe Taxon Résultat" Then
                                CodeGroupeTaxonRésultat = Variables.TabRéfPara(j, 24)
                                LigneParamètre = LigneParamètre & CodeGroupeTaxonRésultat
                            End If
                            ' ==================================
                            ' Vérification si la ligne sélectionnée correspond à "Code Taxon Résultat"
                            If SPEL.ListBox2.Items.Item(LignePressePapier) = " Code Taxon Résultat" Then
                                CodeTaxonRésultat = Variables.TabRéfPara(j, 25)
                                LigneParamètre = LigneParamètre & CodeTaxonRésultat
                            End If


                            ' ==================================
                            ' Vérification si la ligne sélectionnée correspond à "Résultat"
                            If SPEL.ListBox2.Items.Item(LignePressePapier) = " Résultat" Then
                                Résultat = Variables.TabRéfPara(j, 17)
                                LigneParamètre = LigneParamètre & Résultat
                            End If

                            ' ==================================
                            ' Vérification si la ligne sélectionnée correspond à "Unité Résultat"
                            If SPEL.ListBox2.Items.Item(LignePressePapier) = " Unité Résultat" Then
                                UnitéRésultat = Variables.TabRéfPara(j, 18)
                                LigneParamètre = LigneParamètre & UnitéRésultat
                            End If
                            ' ==================================
                            ' Vérification si la ligne sélectionnée correspond à "UnitéRésultatCodeSandre"
                            If SPEL.ListBox2.Items.Item(LignePressePapier) = " Unité Résultat Code Sandre" Then
                                ' ==================================
                                ' Recherche du Code Sandre correspondant
                                For Sandre = PointEntréeUnité To 5000
                                    ' ==================================
                                    ' Sortie de la boucle si fin de tableau
                                    If Variables.TabRéfInfo(Sandre, 0) = "" Then Exit For
                                    ' ==================================
                                    If Variables.TabRéfInfo(Sandre, 0) = Variables.TabRéfPara(j, 18) Then
                                        ' ==================================
                                        ' Récupération du Code Sandre
                                        UnitéRésultatCodeSandre = Variables.TabRéfInfo(Sandre, 1)
                                        Exit For
                                    End If
                                Next
                                LigneParamètre = LigneParamètre & UnitéRésultatCodeSandre
                            End If

                            ' ==================================
                            ' Vérification si la ligne sélectionnée correspond à "Analyste"
                            If SPEL.ListBox2.Items.Item(LignePressePapier) = " Analyste" Then
                                Analyste = Variables.TabRéfPara(j, 4)
                                LigneParamètre = LigneParamètre & Analyste
                            End If
                            ' ==================================
                            ' Vérification si la ligne sélectionnée correspond à "AcronymeAnalyste"
                            If SPEL.ListBox2.Items.Item(LignePressePapier) = " Acronyme Analyste" Then
                                AcronymeAnalyste = Variables.TabRéfPara(j, 5)
                                LigneParamètre = LigneParamètre & AcronymeAnalyste
                            End If
                            ' ==================================
                            ' Vérification si la ligne sélectionnée correspond à "AnalysteCodeSandre"
                            If SPEL.ListBox2.Items.Item(LignePressePapier) = " Analyste Code Sandre" Then
                                ' ==================================
                                ' Recherche du Code Sandre correspondant
                                For Sandre = PointEntréeAnalyste To 5000
                                    ' ==================================
                                    ' Sortie de la boucle si fin de tableau
                                    If Variables.TabRéfInfo(Sandre, 0) = "" Then Exit For
                                    ' ==================================
                                    If Variables.TabRéfInfo(Sandre, 0) = Variables.TabRéfPara(j, 4) Then
                                        ' ==================================
                                        ' Récupération du Code Sandre
                                        AnalysteCodeSandre = Variables.TabRéfInfo(Sandre, 2)
                                        Exit For
                                    End If
                                Next
                                LigneParamètre = LigneParamètre & AnalysteCodeSandre
                            End If

                            ' ==================================
                            ' Vérification si la ligne sélectionnée correspond à "EnginAnalyse"
                            If SPEL.ListBox2.Items.Item(LignePressePapier) = " Engin Analyse" Then
                                EnginAnalyse = Variables.TabRéfPara(j, 7)
                                LigneParamètre = LigneParamètre & EnginAnalyse
                            End If
                            ' ==================================
                            ' Vérification si la ligne sélectionnée correspond à "EnginAnalyseCodeSandre"
                            If SPEL.ListBox2.Items.Item(LignePressePapier) = " Engin Analyse Code Sandre" Then
                                ' ==================================
                                ' Recherche du Code Sandre correspondant
                                For Sandre = PointEntréeEnginAnalyste To 5000
                                    ' ==================================
                                    ' Sortie de la boucle si fin de tableau
                                    If Variables.TabRéfInfo(Sandre, 0) = "" Then Exit For
                                    ' ==================================
                                    If Variables.TabRéfInfo(Sandre, 0) = Variables.TabRéfPara(j, 7) Then
                                        ' ==================================
                                        ' Récupération du Code Sandre
                                        EnginAnalyseCodeSandre = Variables.TabRéfInfo(Sandre, 1)
                                        Exit For
                                    End If
                                Next
                                LigneParamètre = LigneParamètre & EnginAnalyseCodeSandre
                            End If

                            ' ==================================
                            ' Vérification si la ligne sélectionnée correspond à "SeuilAnalyse"
                            If SPEL.ListBox2.Items.Item(LignePressePapier) = " Seuil Analyse" Then
                                SeuilAnalyse = Variables.TabRéfPara(j, 14)
                                LigneParamètre = LigneParamètre & SeuilAnalyse
                            End If

                            ' ==================================
                            ' Vérification si la ligne sélectionnée correspond à "Remarque"
                            If SPEL.ListBox2.Items.Item(LignePressePapier) = " Remarque" Then
                                Remarque = Variables.TabRéfPara(j, 20)
                                LigneParamètre = LigneParamètre & Remarque
                            End If
                            ' ==================================
                            ' Vérification si la ligne sélectionnée correspond à "RemarqueCodeSandre"
                            If SPEL.ListBox2.Items.Item(LignePressePapier) = " Remarque Code Sandre" Then
                                ' ==================================
                                ' Recherche du Code Sandre correspondant
                                For Sandre = PointEntréeRemarque To 5000
                                    ' ==================================
                                    ' Sortie de la boucle si fin de tableau
                                    If Variables.TabRéfInfo(Sandre, 0) = "" Then Exit For
                                    ' ==================================
                                    If Variables.TabRéfInfo(Sandre, 0) = Variables.TabRéfPara(j, 20) Then
                                        ' ==================================
                                        ' Récupération du Code Sandre
                                        RemarqueCodeSandre = Variables.TabRéfInfo(Sandre, 1)
                                        Exit For
                                    End If
                                Next
                                LigneParamètre = LigneParamètre & RemarqueCodeSandre
                            End If

                            ' ==================================
                            ' Vérification si la ligne sélectionnée correspond à "Précision"
                            If SPEL.ListBox2.Items.Item(LignePressePapier) = " Précision" Then
                                Précision = Variables.TabRéfPara(j, 11)
                                LigneParamètre = LigneParamètre & Précision
                            End If

                            ' ==================================
                            ' Vérification si la ligne sélectionnée correspond à "TypePrécision"
                            If SPEL.ListBox2.Items.Item(LignePressePapier) = " Type Précision" Then
                                TypePrécision = Variables.TabRéfPara(j, 12)
                                LigneParamètre = LigneParamètre & TypePrécision
                            End If
                            ' ==================================
                            ' Vérification si la ligne sélectionnée correspond à "TypePrécision"
                            If SPEL.ListBox2.Items.Item(LignePressePapier) = " Type Précision Code Sandre" Then
                                ' ==================================
                                ' Recherche du Code Sandre correspondant
                                For Sandre = PointEntréeTypePrécision To 5000
                                    ' ==================================
                                    ' Sortie de la boucle si fin de tableau
                                    If Variables.TabRéfInfo(Sandre, 0) = "" Then Exit For
                                    ' ==================================
                                    If Variables.TabRéfInfo(Sandre, 0) = Variables.TabRéfPara(j, 12) Then
                                        ' ==================================
                                        ' Récupération du Code Sandre
                                        TypePrécisionCodeSandre = Variables.TabRéfInfo(Sandre, 1)
                                        Exit For
                                    End If
                                Next
                                LigneParamètre = LigneParamètre & TypePrécisionCodeSandre
                            End If

                            ' ==================================
                            ' Vérification si la ligne sélectionnée correspond à "CommentairesMesure"
                            If SPEL.ListBox2.Items.Item(LignePressePapier) = " Commentaires Mesure" Then
                                CommentairesMesure = Variables.TabRéfPara(j, 19)
                                LigneParamètre = LigneParamètre & CommentairesMesure
                            End If

                            ' ==================================
                            ' Passage à la colonne suivante
                            LigneParamètre = LigneParamètre & Chr(9)

                        Next


                        ' ==================================
                        ' TRANSFERT DANS LE PRESSE PAPIER
                        ' ==================================
                        Clipboard.SetText(Clipboard.GetText & Chr(13) & LigneParamètre)


                    End If
                Next
            Next
        Next


        ' ============================================================= 
        ' Message de confirmation de transfert
        MsgBox("Les informations SPEL ont été transférées !", MsgBoxStyle.Information, " QUADRISPEL - Confirmation de Transfert")

    End Sub


    '=============================================================================================================================================
    ' Feuille Correspondants
    Private Sub CorrespondantsToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CorrespondantsToolStripMenuItem.Click
        ' ==================================
        ' Affichage de la Feuille
        Correspondants.ShowDialog()
    End Sub


    '=============================================================================================================================================
    ' À propos ...
    Private Sub ÀProposToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ÀProposToolStripMenuItem.Click
        MsgBox("           Logiciel  QUADRISPEL  Version  06-06-2017" & vbCr & vbLf & vbLf & "                             Jacques  BROVELLI" & vbCr & vbLf & vbLf & "    Développeur  Informatique  CEREMA/DTecEMF/SDL     ", , " QUADRISPEL - Version")
    End Sub

    '=============================================================================================================================================
    ' Demande de la Notice
    Private Sub NoticeToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NoticeToolStripMenuItem1.Click
        '      MsgBox(" La notice est en cours d'élaboration. ", MsgBoxStyle.Information, " QUADRISPEL - Information")
        '      Exit Sub

        On Error GoTo TraitementErreur
        Process.Start(Application.StartupPath & "\Notice QUADRISPEL.pdf")
        Exit Sub

TraitementErreur:
        MsgBox("Je n'ai pas trouvé la notice ! ", MsgBoxStyle.Information, " QUADRISPEL - Information")
    End Sub


    '=============================================================================================================================================
    ' PROCÉDURE D'OUVERTURE DE LA FEUILLE PARENT ET LA FERMETURE DU MASQUE AVEC APPARITION DU MOT "BONJOUR"
    '=============================================================================================================================================
    ' Procédure d'ouverture de la feuille
    Private Sub OuvertureFeuille()
        ' ==================================
        ' Affectation des variables largeur et hauteur
        Variables.Largeur = 1
        Variables.Hauteur = 1
        ' ==================================
        ' Affectation du pas de la largeur et de la hauteur
        Variables.PasLargeur = Variables.FormLargeur / 100 : Variables.PasHauteur = Variables.FormHauteur / 100
        ' ==================================
        ' Initialiation des variables couleurs
        Variables.CoulRouge = 0 : Variables.CoulVert = 0 : Variables.CoulBleu = 0
        ' ==================================
        ' Initialisation des variables d'incrémentation de pas avec la couleur à obtenir
        Variables.PasRouge = 230 / 100 : Variables.PasVert = 230 / 100 : Variables.PasBleu = 230 / 100
        ' ==================================
        ' Lancement de la procédure
        Timer11.Enabled = True
    End Sub

    ' ==============================================================================================================================================
    ' Procédure d'apparition du masque pour la feuille parent
    Private Sub Timer11_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer11.Tick
        ' ==================================
        ' Incrementation des variables largeur et la hauteur avec le pas défini
        Variables.Largeur = Variables.Largeur + Variables.PasLargeur
        Variables.Hauteur = Variables.Hauteur + Variables.PasHauteur
        ' ==================================
        ' Affectation des variables de dimension à la feuille
        Me.Size = New System.Drawing.Size(Variables.Largeur, Variables.Hauteur)
        ' ==================================
        ' Positionnement de la feuille au centre de l'écran
        Me.Location = New Point((My.Computer.Screen.Bounds.Width / 2) - (Me.Size.Width / 2), (My.Computer.Screen.Bounds.Height / 2) - (Me.Size.Height / 2))
        ' ==================================
        ' Incrémentation des couleurs
        Variables.CoulRouge = Variables.CoulRouge + Variables.PasRouge ': If Variables.CoulRouge > 255 Then Variables.CoulRouge = 255
        Variables.CoulVert = Variables.CoulVert + Variables.PasVert ': If Variables.CoulVert > 255 Then Variables.CoulVert = 255
        Variables.CoulBleu = Variables.CoulBleu + Variables.PasBleu ': If Variables.CoulBleu > 255 Then Variables.CoulBleu = 255
        ' ==================================
        ' affectation des couleurs à la zone backcolor du masque et de la feuille
        Me.BackColor = Color.FromArgb(Variables.CoulRouge, Variables.CoulVert, Variables.CoulBleu)
        ' ==================================
        ' Apparition de la feuille et du masque
        Me.Visible = True
        ' ==================================
        ' Vérification de l'expension complète du masque
        If Variables.Largeur >= Variables.FormLargeur Then
            ' ==================================
            ' Arrêt de la procédure de fermeture de la feuille
            Timer11.Enabled = False
            ' ==================================
            ' Apparition de la barre supérieure de la feuille
            Me.FormBorderStyle = Windows.Forms.FormBorderStyle.FixedDialog
            ' ==================================
            ' Apparition de la barre des menus
            MenuStrip1.Visible = True
            ' ==================================
            ' Affectation des dimensions à la feuille
            Me.Size = New System.Drawing.Size(Variables.FormLargeur, Variables.FormHauteur)
            ' ==================================
            ' Positionnement de la feuille
            Me.Top = 0
            Me.Left = 0
            ' ==================================
            ' Localisation du message Bonjour
            Label1.Location = New Point(((Me.Size.Width - Label1.Width) / 2), ((Me.Size.Height - Label1.Height) / 2))
            ' ==================================
            ' Apparition du mot "Bonjour"
            Label1.Visible = True
            ' ==================================
            ' Déclenchement du Timer
            Timer12.Enabled = True
        End If
    End Sub

    ' ==============================================================================================================================================
    ' Procédure de disparition du masque
    Private Sub Timer12_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer12.Tick
        ' ==================================
        ' Arrêt du Timer
        Timer12.Enabled = False
        ' ==================================
        ' Disparition du mot "Bonjour"
        Label1.Visible = False
        ' ==================================
        ' Adaptation du DataGridView 1
        DataGridView1.Height = My.Computer.Screen.WorkingArea.Size.Height - 56
        DataGridView1.Width = My.Computer.Screen.WorkingArea.Size.Width - 5
        ' ==================================
        ' Adaptation du DataGridView 2
        '        DataGridView2.Height = My.Computer.Screen.WorkingArea.Size.Height - 56
        '        DataGridView2.Width = My.Computer.Screen.WorkingArea.Size.Width - 5
        ' ==================================
        ' Mise en visibilité du DataGridView1
        DataGridView1.Visible = True
        ' ==================================
        ' Mise en cache du DataGridview 2
        '        DataGridView2.Top = 25 - DataGridView2.Height
        ' ==================================
        ' Mise en visibilité du DataGridView2
        '        DataGridView2.Visible = True
        ' ==================================
        ' Affichage état de l'application
        '        Variables.EtatAppli = "Base non chargée"
        ' ==================================
        ' Chargement de la base
        '        Timer2.Enabled = True
    End Sub


    '=============================================================================================================================================
    ' PROCÉDURE DE FERMETURE DE LA FEUILLE PARENT
    '=============================================================================================================================================
    ' Déclenchement de fermeture de la feuille
    Private Sub FermetureFeuille()
        ' ==================================
        ' Curseur de la souris en flèche
        Cursor = Cursors.Default

        ' ==================================
        Dim i As Integer
        ' ==================================
        ' Vérification d'une demande de sauvegarde d'informations Masque
        If Variables.SauveFichierMasque = True And Variables.TypeFichierChargé = "" Then
            i = MsgBox(" Des informations de fichier Masque ont été réalisées ! " & vbCrLf & vbCrLf & "                        Sauvegarder ?", MsgBoxStyle.Exclamation + MsgBoxStyle.YesNo + MsgBoxStyle.DefaultButton1, " QUADRISPEL - Proposition de sauvegarde")
            If i = 6 Then
                ' ==================================
                ' Affichage de la Feuille
                Sauve_Masque.ShowDialog()
                Exit Sub
            End If
        End If

        ' ==================================
        ' Vérification d'une demande de sauvegarde d'informations Résultat
        If Variables.SauveFichierRésultat = True And Variables.TypeFichierChargé = "" Then
            i = MsgBox(" Des informations de fichier Résultat ont été réalisées ! " & vbCrLf & vbCrLf & "                        Sauvegarder ?", MsgBoxStyle.Exclamation + MsgBoxStyle.YesNo + MsgBoxStyle.DefaultButton1, " QUADRISPEL - Proposition de sauvegarde")
            If i = 6 Then
                ' ==================================
                ' Affichage de la Feuille
                Sauve_Résultat.ShowDialog()
                Exit Sub
            End If
        End If


        ' ==================================
        ' Vérification d'une demande de sauvegarde d'informations Masque
        If Variables.SauveFichierMasque = True And Variables.TypeFichierChargé = "Masque" Then
            i = MsgBox(" Des informations du fichier Masque ont été modifiées ! " & vbCrLf & vbCrLf & "                        Sauvegarder ?", MsgBoxStyle.Exclamation + MsgBoxStyle.YesNo + MsgBoxStyle.DefaultButton1, " QUADRISPEL - Proposition de sauvegarde")
            If i = 6 Then
                ' ==================================
                ' Affichage de la Feuille
                Sauve_Masque.ShowDialog()
                Exit Sub
            End If
        End If


        ' ==================================
        ' Vérification d'une demande de sauvegarde d'informations Résultat
        If Variables.SauveFichierRésultat = True And Variables.TypeFichierChargé = "Masque" Then
            i = MsgBox(" Des informations Résultats sont à sauvegarder ! " & vbCrLf & vbCrLf & "                    Sauvegarder ?", MsgBoxStyle.Exclamation + MsgBoxStyle.YesNo + MsgBoxStyle.DefaultButton1, " QUADRISPEL - Proposition de sauvegarde")
            If i = 6 Then
                ' ==================================
                ' Affichage de la Feuille
                Sauve_Résultat.ShowDialog()
                Exit Sub
            End If
        End If


        ' ==================================
        ' Vérification d'une demande de sauvegarde d'informations Résultat
        '       If (Variables.SauveFichierRésultat = True Or Variables.SauveFichierMasque = True) And Variables.TypeFichierChargé = "Résultat" Then
        If Variables.SauveFichierRésultat = True And Variables.TypeFichierChargé = "Résultat" Then
            i = MsgBox(" Des informations du fichier Résultats ont été modifiées ! " & vbCrLf & vbCrLf & "                         Sauvegarder ?", MsgBoxStyle.Exclamation + MsgBoxStyle.YesNo + MsgBoxStyle.DefaultButton1, " QUADRISPEL - Proposition de sauvegarde")
            If i = 6 Then
                ' ==================================
                ' Affichage de la Feuille
                Sauve_Résultat.ShowDialog()
                Exit Sub
            End If
        End If


        ' ==================================
        ' Vérification d'une demande de sauvegarde d'informations Masque
        If Variables.SauveFichierMasque = True And Variables.TypeFichierChargé = "Résultat" Then
            i = MsgBox(" Des informations Masques correspondantes au fichier Masque ont été modifiées ! " & vbCrLf & vbCrLf & "                                             Sauvegarder ?", MsgBoxStyle.Exclamation + MsgBoxStyle.YesNo + MsgBoxStyle.DefaultButton1, " QUADRISPEL - Proposition de sauvegarde")
            If i = 6 Then
                ' ==================================
                ' Affichage de la Feuille
                Sauve_Masque.ShowDialog()
                Exit Sub
            End If
        End If

        ' ==================================
        ' Déclenchement de fermeture de la feuille
        Timer15.Enabled = True
    End Sub

    ' ==============================================================================================================================================
    ' Procédure d'apparition du masque et de réduction de la feuille
    Private Sub Timer13_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer13.Tick
        ' ==================================
        ' Décrémentation de la largeur et la hauteur de la feuille
        Variables.Largeur = Variables.Largeur - Variables.PasLargeur
        Variables.Hauteur = Variables.Hauteur - Variables.PasHauteur
        ' ==================================
        ' Affectation des variables de dimension à la feuille
        Me.Size = New System.Drawing.Size(Variables.Largeur, Variables.Hauteur)
        ' ==================================
        ' Positionnement de la feuille au centre de l'écran
        Me.Location = New Point((My.Computer.Screen.Bounds.Width / 2) - (Me.Size.Width / 2), (My.Computer.Screen.Bounds.Height / 2) - (Me.Size.Height / 2))
        ' ==================================
        ' Décrémentation des couleurs
        Variables.CoulRouge = Variables.CoulRouge - Variables.PasRouge : If Variables.CoulRouge < 0 Then Variables.CoulRouge = 0
        Variables.CoulVert = Variables.CoulVert - Variables.PasVert : If Variables.CoulVert < 0 Then Variables.CoulVert = 0
        Variables.CoulBleu = Variables.CoulBleu - Variables.PasBleu : If Variables.CoulBleu < 0 Then Variables.CoulBleu = 0
        ' ==================================
        ' affectation des couleurs à la zone backcolor du masque et de la feuille
        Me.BackColor = Color.FromArgb(Variables.CoulRouge, Variables.CoulVert, Variables.CoulBleu)
        ' ==================================
        ' Vérification de la réduction de la feuille avec suppression de la feuille à la fin
        If Variables.Largeur < 210 Then
            ' ==================================
            ' Arrêt de la procédure de réduction
            Timer13.Enabled = False
            ' ==================================
            ' Affectation de la hauteur et la largeur à la feuille
            Me.Width = Label1.Width
            Me.Height = Label1.Height
            ' ==================================
            ' Positionnement du libellé "Au revoir"
            Label1.Location = New Point(0, 0)
            ' ==================================
            ' Apparition du libellé
            Label1.Visible = True
            ' ==================================
            ' Procédure de fermeture de la feuille après vision du libellé "Au revoir"
            Timer14.Enabled = True
        End If
    End Sub

    ' ==============================================================================================================================================
    ' Procédure de disparition de la feuille
    Private Sub Timer14_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer14.Tick
        ' ==================================
        ' Arrêt du Timer
        Timer14.Enabled = False
        ' ==================================
        ' Fin de la procédure
        Me.Dispose()
    End Sub

    ' ==============================================================================================================================================
    ' Procédure de préparation de réduction de la feuille
    Private Sub Timer15_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer15.Tick
        ' ==================================
        ' Arrêt du Timer
        Timer15.Enabled = False
        ' ==================================
        ' Suppression du mot "Bonjour" en cas de fermeture avant l'ouverture totale
        Label1.Visible = False
        ' ==================================
        ' Blocage de certains timers de la feuille Parent afin d'éviter des collisions ouverture/fermeture
        Timer11.Enabled = False
        Timer12.Enabled = False
        ' ==================================
        ' Initialistion des variables couleurs
        Variables.CoulRouge = 230 : Variables.CoulVert = 230 : Variables.CoulBleu = 230
        ' ==================================
        ' Initialisation des variables d'incrémentation de pas avec la couleur à obtenir
        Variables.PasRouge = 230 / 100 : Variables.PasVert = 230 / 100 : Variables.PasBleu = 230 / 100
        ' ==================================
        ' Disparition des listes éventuellement affichées pour permettre une fermeture rapide
        DataGridView1.Visible = False
        ' ==================================
        ' Suppression de la barre supérieure de la feuille
        Me.FormBorderStyle = Windows.Forms.FormBorderStyle.None
        ' ==================================
        ' Suppression de la barre des menus
        MenuStrip1.Visible = False
        ' ==================================
        ' Disparition du dataGridView1
        DataGridView1.Visible = False
        ' ==================================
        ' Disparition des listes éventuellement affichées pour permettre une fermeture rapide
        '       DataGridView2.Visible = False
        ' ==================================
        ' Affectation des variables de dimension
        Variables.Largeur = Variables.FormLargeur
        Variables.Hauteur = Variables.FormHauteur
        ' ==================================
        ' Affectation des variables de pas
        Variables.PasLargeur = (Variables.FormLargeur - Label1.Width) / 100   '   250
        Variables.PasHauteur = (Variables.FormHauteur - Label1.Height) / 100   '   230
        ' ==================================
        ' Mise en place du mot
        Label1.Text = " AU REVOIR ... "
        ' ==================================
        ' Lancement de la procédure
        Timer13.Enabled = True
    End Sub


    '=============================================================================================================================================
    ' PROCÉDURE DU TUTORIEL VIDÉO
    '=============================================================================================================================================

    Private Sub FichierChargementMasqueToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles FichierChargementMasqueToolStripMenuItem.Click
        On Error GoTo TraitementErreur
        Process.Start(Application.StartupPath & "\Tutoriel\Chargement Fichier Masque.htm")
        Exit Sub
TraitementErreur:
        MsgBox("Je n'ai pas trouvé la vidéo ! ", MsgBoxStyle.Information, " QUADRISPEL - Information")
    End Sub

    Private Sub FichierChargementRésultatToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles FichierChargementRésultatToolStripMenuItem.Click
        On Error GoTo TraitementErreur
        Process.Start(Application.StartupPath & "\Tutoriel\Chargement Fichier Résultat.htm")
        Exit Sub
TraitementErreur:
        MsgBox("Je n'ai pas trouvé la vidéo ! ", MsgBoxStyle.Information, " QUADRISPEL - Information")
    End Sub

    Private Sub FichierSauvegardeMasqueToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles FichierSauvegardeMasqueToolStripMenuItem.Click
        On Error GoTo TraitementErreur
        Process.Start(Application.StartupPath & "\Tutoriel\Sauvegarde Fichier Masque.htm")
        Exit Sub
TraitementErreur:
        MsgBox("Je n'ai pas trouvé la vidéo ! ", MsgBoxStyle.Information, " QUADRISPEL - Information")
    End Sub

    Private Sub FichierSauvegardeRésultatToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles FichierSauvegardeRésultatToolStripMenuItem.Click
        On Error GoTo TraitementErreur
        Process.Start(Application.StartupPath & "\Tutoriel\Sauvegarde Fichier Résultat.htm")
        Exit Sub
TraitementErreur:
        MsgBox("Je n'ai pas trouvé la vidéo ! ", MsgBoxStyle.Information, " QUADRISPEL - Information")
    End Sub

    Private Sub InfosCommunesPassageToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles InfosCommunesPassageToolStripMenuItem.Click
        On Error GoTo TraitementErreur
        Process.Start(Application.StartupPath & "\Tutoriel\Infos Communes Passage.htm")
        Exit Sub
TraitementErreur:
        MsgBox("Je n'ai pas trouvé la vidéo ! ", MsgBoxStyle.Information, " QUADRISPEL - Information")
    End Sub

    Private Sub InfosCommunesPrélèvementToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles InfosCommunesPrélèvementToolStripMenuItem.Click
        On Error GoTo TraitementErreur
        Process.Start(Application.StartupPath & "\Tutoriel\Infos Communes Prélèvement.htm")
        Exit Sub
TraitementErreur:
        MsgBox("Je n'ai pas trouvé la vidéo ! ", MsgBoxStyle.Information, " QUADRISPEL - Information")
    End Sub

    Private Sub InfosCommunesÉchantillonToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles InfosCommunesÉchantillonToolStripMenuItem.Click
        On Error GoTo TraitementErreur
        Process.Start(Application.StartupPath & "\Tutoriel\Infos Communes Échantillon.htm")
        Exit Sub
TraitementErreur:
        MsgBox("Je n'ai pas trouvé la vidéo ! ", MsgBoxStyle.Information, " QUADRISPEL - Information")
    End Sub

    Private Sub InfosParamètresFamillesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles InfosParamètresFamillesToolStripMenuItem.Click
        On Error GoTo TraitementErreur
        Process.Start(Application.StartupPath & "\Tutoriel\Infos Paramètres Familles.htm")
        Exit Sub
TraitementErreur:
        MsgBox("Je n'ai pas trouvé la vidéo ! ", MsgBoxStyle.Information, " QUADRISPEL - Information")
    End Sub

    Private Sub InfosParamètresParamètresToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles InfosParamètresParamètresToolStripMenuItem.Click
        On Error GoTo TraitementErreur
        Process.Start(Application.StartupPath & "\Tutoriel\Infos Paramètres Paramètres.htm")
        Exit Sub
TraitementErreur:
        MsgBox("Je n'ai pas trouvé la vidéo ! ", MsgBoxStyle.Information, " QUADRISPEL - Information")
    End Sub

    Private Sub InfosParamètresPropriétésEtRésultatsToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles InfosParamètresPropriétésEtRésultatsToolStripMenuItem.Click
        On Error GoTo TraitementErreur
        Process.Start(Application.StartupPath & "\Tutoriel\Infos Paramètres Propriétés et Résultats.htm")
        Exit Sub
TraitementErreur:
        MsgBox("Je n'ai pas trouvé la vidéo ! ", MsgBoxStyle.Information, " QUADRISPEL - Information")
    End Sub

    Private Sub QuadrigeRèglesStructurellesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles QuadrigeRèglesStructurellesToolStripMenuItem.Click
        On Error GoTo TraitementErreur
        Process.Start(Application.StartupPath & "\Tutoriel\Quadrige Règles Structurelles.htm")
        Exit Sub
TraitementErreur:
        MsgBox("Je n'ai pas trouvé la vidéo ! ", MsgBoxStyle.Information, " QUADRISPEL - Information")
    End Sub

    Private Sub QuadrigeSauvegardeToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles QuadrigeSauvegardeToolStripMenuItem.Click
        On Error GoTo TraitementErreur
        Process.Start(Application.StartupPath & "\Tutoriel\Quadrige Sauvegarde.htm")
        Exit Sub
TraitementErreur:
        MsgBox("Je n'ai pas trouvé la vidéo ! ", MsgBoxStyle.Information, " QUADRISPEL - Information")
    End Sub

    Private Sub QuadrigeVisualisationToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles QuadrigeVisualisationToolStripMenuItem.Click
        On Error GoTo TraitementErreur
        Process.Start(Application.StartupPath & "\Tutoriel\Quadrige Visualisation.htm")
        Exit Sub
TraitementErreur:
        MsgBox("Je n'ai pas trouvé la vidéo ! ", MsgBoxStyle.Information, " QUADRISPEL - Information")
    End Sub


    Private Sub Timer2_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub Timer3_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub Timer4_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub



End Class
