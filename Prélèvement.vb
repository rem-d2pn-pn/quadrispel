﻿Public Class Prélèvement

    '=============================================================================================================================================
    ' Procédure lors de la fermeture de la feuille par la croix
    Private Sub Prélèvement_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        '==============================================
        ' Blocage de la procédure de fermeture automatique
        e.Cancel = True
        '==============================================
        ' Demande de fermeture par Fondu
        Fermeture()
    End Sub

    ' ==============================================================================================================================================
    ' Procédure au chargement de la page
    Private Sub Prélèvement_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button2.Select()
        '==============================================
        ' Demande d'ouverture par Fondu
        Me.Opacity = 0
        Timer1.Enabled = True
        ' ==================================
        ' Effacement du nombre de caractères autorisé
        GroupBox2.Text = " Mnémonique Prélèvement "
        ' ==================================
        ' Effacement du nombre de caractères autorisé
        GroupBox11.Text = " Commentaires Prélèvement "
    End Sub


    '=============================================================================================================================================
    ' PROCÉDURES DIVERSES
    '=============================================================================================================================================
    ' Traitement du repositionnement automatique de la feuille quand on l'a déplacée
    Private Sub Prélèvement_MouseEnter(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.MouseEnter
        Me.CenterToScreen()
    End Sub

    '=============================================================================================================================================
    ' Traitement de réduction des fenêtres
    Private Sub Pélèvement_Move(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Move
        ' ============================================
        ' Passage de la feuille principale transparente quand on réduit cette feuille
        If Me.WindowState = 1 Then
            Form1.Opacity = 0
        End If
        ' ============================================
        ' Passage de la feuille principale opaque quand on rétablit cette feuille
        If Me.WindowState = 0 Then
            Form1.Opacity = 1
        End If
    End Sub

    ' ==============================================================================================================================================
    ' Bouton de fermeture de l'application
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button2.Select()
        ' ==================================
        ' Demande de fermeture par Fondu
        Fermeture()
    End Sub


    '=============================================================================================================================================
    ' PROCÉDURE D'OUVERTURE ET DE FERMETURE
    '=============================================================================================================================================
    ' Procédure d'ouverture par Fondu
    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        Me.Opacity = Me.Opacity + 0.06
        If Me.Opacity >= 0.95 Then
            Timer1.Enabled = False
            Me.Opacity = 1
        End If
    End Sub

    Private Sub Fermeture()
        ' ============================================
        ' Essai pour éviter qu'une application ne passe devant la feuille ayant demandé celle-ci
        Form1.Activate()
        ' ============================================
        Me.Visible = False
    End Sub


    '=============================================================================================================================================
    ' PROCÉDURE POUR ÉVITER LE CURSEUR CLIGNOTANT DANS LES CHAMPS
    '=============================================================================================================================================

    Private Sub TextBox1_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox1.GotFocus
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button2.Select()
    End Sub

    Private Sub TextBox7_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox7.GotFocus
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button2.Select()
    End Sub

    Private Sub TextBox8_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox8.GotFocus
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button2.Select()
    End Sub

    Private Sub TextBox10_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox10.GotFocus
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button2.Select()
    End Sub

    Private Sub TextBox16_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox16.GotFocus
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button2.Select()
    End Sub

    Private Sub TextBox23_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox23.GotFocus
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button2.Select()
    End Sub


    '=============================================================================================================================================
    ' PROCÉDURE POUR LE NOMBRE DE CARACTÈRES DANS LES CHAMPS
    '=============================================================================================================================================
    ' Affichage du nombre de caractères autorisé
    Private Sub TextBox2_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox2.GotFocus
        ' ==================================
        ' Affichage du nombre de caractères autorisé
        GroupBox2.Text = " Mnémonique Prélèvement (" & 50 - Len(TextBox2.Text) & ") "
    End Sub

    '=============================================================================================================================================
    ' Affichage du nombre de caractères autorisé
    Private Sub TextBox2_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox2.TextChanged
        ' ==================================
        ' Validation sauvegarde à la fermeture
        Variables.SauveFichierMasque = True
        ' ==================================
        ' Affichage du nombre de caractères autorisé
        GroupBox2.Text = " Mnémonique Prélèvement (" & 50 - Len(TextBox2.Text) & ") "
    End Sub

    '=============================================================================================================================================
    ' Effacement du nombre de caractères autorisé
    Private Sub TextBox2_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox2.LostFocus
        ' ==================================
        ' Effacement du nombre de caractères autorisé
        GroupBox2.Text = " Mnémonique Prélèvement "
    End Sub

    '=============================================================================================================================================
    ' Affichage du nombre de caractères autorisé
    Private Sub TextBox12_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox12.GotFocus
        ' ==================================
        ' Affichage du nombre de caractères autorisé
        GroupBox11.Text = " Commentaires Prélèvement (" & 2000 - Len(TextBox12.Text) & ") "
    End Sub

    '=============================================================================================================================================
    ' Affichage du nombre de caractères autorisé
    Private Sub TextBox12_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox12.TextChanged
        ' ==================================
        ' Validation sauvegarde à la fermeture
        Variables.SauveFichierMasque = True
        ' ==================================
        ' Affichage du nombre de caractères autorisé
        GroupBox11.Text = " Commentaires Prélèvement (" & 2000 - Len(TextBox12.Text) & ") "
    End Sub

    '=============================================================================================================================================
    ' Effacement du nombre de caractères autorisé
    Private Sub TextBox12_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox12.LostFocus
        ' ==================================
        ' Effacement du nombre de caractères autorisé
        GroupBox11.Text = " Commentaires Prélèvement "
    End Sub


    '=============================================================================================================================================
    ' PROCÉDURES D'APPARITION DES SÉLECTEURS
    '=============================================================================================================================================
    ' Sélection de la liste Niveau de Prélèvement
    Private Sub TextBox1_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox1.MouseClick
        ' ==================================
        ' Traitement pour la liste des sujets si une autre liste est en cours
        If Variables.NumeroList <> 0 Then
            Variables.NumeroListSuivant = 2
            ' ==================================
            ' Arrêt du timer de la procédure d'apparition des listes
            Timer2.Enabled = False
            ' Arrêt du timer de la procédure d'apparition des listes
            Timer3.Enabled = False
            ' ==================================
            ' Déclenchement du retrait du sélecteur
            Timer4.Enabled = True
            Exit Sub
        End If
        ' ==================================
        ' Variable d'affichage du Listbox de la liste des sujets
        Variables.NumeroList = 2
        ' ==================================
        ' Déclenchement du timer de la procédure d'apparition des listes
        Timer2.Enabled = True
    End Sub

    '=============================================================================================================================================
    ' Sélection de la liste Unité Immersion
    Private Sub TextBox7_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox7.MouseClick
        ' ==================================
        ' Traitement pour la liste des sujets si une autre liste est en cours
        If Variables.NumeroList <> 0 Then
            Variables.NumeroListSuivant = 4
            ' ==================================
            ' Arrêt du timer de la procédure d'apparition des listes
            Timer2.Enabled = False
            ' Arrêt du timer de la procédure d'apparition des listes
            Timer3.Enabled = False
            ' ==================================
            ' Déclenchement du retrait du sélecteur
            Timer4.Enabled = True
            Exit Sub
        End If
        ' ==================================
        ' Variable d'affichage du Listbox de la liste des sujets
        Variables.NumeroList = 4
        ' ==================================
        ' Déclenchement du timer de la procédure d'apparition des listes
        Timer2.Enabled = True
    End Sub

    '=============================================================================================================================================
    ' Sélection de la liste des Libellés des Préleveurs
    Private Sub TextBox8_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox8.MouseClick
        ' ==================================
        ' Traitement pour la liste des sujets si une autre liste est en cours
        If Variables.NumeroList <> 0 Then
            Variables.NumeroListSuivant = 3
            ' ==================================
            ' Arrêt du timer de la procédure d'apparition des listes
            Timer2.Enabled = False
            ' Arrêt du timer de la procédure d'apparition des listes
            Timer3.Enabled = False
            ' ==================================
            ' Déclenchement du retrait du sélecteur
            Timer4.Enabled = True
            Exit Sub
        End If
        ' ==================================
        ' Variable d'affichage du Listbox de la liste des sujets
        Variables.NumeroList = 3
        ' ==================================
        ' Déclenchement du timer de la procédure d'apparition des listes
        Timer2.Enabled = True
    End Sub

    '=============================================================================================================================================
    ' Sélection de la liste Unité Taille Prélèvement
    Private Sub TextBox10_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox10.MouseClick
        ' ==================================
        ' Traitement pour la liste des sujets si une autre liste est en cours
        If Variables.NumeroList <> 0 Then
            Variables.NumeroListSuivant = 5
            ' ==================================
            ' Arrêt du timer de la procédure d'apparition des listes
            Timer2.Enabled = False
            ' Arrêt du timer de la procédure d'apparition des listes
            Timer3.Enabled = False
            ' ==================================
            ' Déclenchement du retrait du sélecteur
            Timer4.Enabled = True
            Exit Sub
        End If
        ' ==================================
        ' Variable d'affichage du Listbox de la liste des sujets
        Variables.NumeroList = 5
        ' ==================================
        ' Déclenchement du timer de la procédure d'apparition des listes
        Timer2.Enabled = True
    End Sub

    '=============================================================================================================================================
    ' Sélection de la liste Positionnement Prélèvement
    Private Sub TextBox16_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox16.MouseClick
        ' ==================================
        ' Traitement pour la liste des sujets si une autre liste est en cours
        If Variables.NumeroList <> 0 Then
            Variables.NumeroListSuivant = 6
            ' ==================================
            ' Arrêt du timer de la procédure d'apparition des listes
            Timer2.Enabled = False
            ' Arrêt du timer de la procédure d'apparition des listes
            Timer3.Enabled = False
            ' ==================================
            ' Déclenchement du retrait du sélecteur
            Timer4.Enabled = True
            Exit Sub
        End If
        ' ==================================
        ' Variable d'affichage du Listbox de la liste des sujets
        Variables.NumeroList = 6
        ' ==================================
        ' Déclenchement du timer de la procédure d'apparition des listes
        Timer2.Enabled = True
    End Sub

    '=============================================================================================================================================
    ' Sélection de la liste des Dénominations des Préleveurs
    Private Sub TextBox23_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox23.MouseClick
        ' ==================================
        ' Vérification si le texte du Libellé est vide
        '       If TextBox8.Text = "" Then
        ' ==================================
        ' Traitement pour la liste des sujets si une autre liste est en cours
        If Variables.NumeroList <> 0 Then
            Variables.NumeroListSuivant = 1
            ' ==================================
            ' Arrêt du timer de la procédure d'apparition des listes
            Timer2.Enabled = False
            ' Arrêt du timer de la procédure d'apparition des listes
            Timer3.Enabled = False
            ' ==================================
            ' Déclenchement du retrait du sélecteur
            Timer4.Enabled = True
            Exit Sub
        End If
        ' ==================================
        ' Variable d'affichage du Listbox de la liste des sujets
        Variables.NumeroList = 1
        ' ==================================
        ' Déclenchement du timer de la procédure d'apparition des listes
        Timer2.Enabled = True



        '        Else

        ' ==================================
        ' Affectation de la Dénomination, éventuellement suivante, en fonction du Libellé


        ' Vérification du nombre de Dénominations dans le tableau





        '       End If
    End Sub

    '=============================================================================================================================================
    ' Déclenche la sortie d'un sélecteur
    Private Sub Timer2_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer2.Tick
        ' ==================================
        ' Procédure lorsque le sélecteur de la liste des Dénominations des Préleveurs est arrivé à sa position
        If Variables.NumeroList = 1 And Panel1.Top >= 0 Then
            ' ==================================
            ' Arrêt du timer
            Timer2.Enabled = False
            ' ==================================
            ' Déclenchement du timer de la temporisation de lecture
            Timer3.Enabled = True
            ' ==================================
            ' Sortie
            Exit Sub
        End If
        ' ==================================
        ' Procédure lorsque le sélecteur de la liste Niveau de Prélèvement est arrivé à sa position
        If Variables.NumeroList = 2 And Panel2.Top >= 0 Then
            ' ==================================
            ' Arrêt du timer
            Timer2.Enabled = False
            ' ==================================
            ' Déclenchement du timer de la temporisation de lecture
            Timer3.Enabled = True
            ' ==================================
            ' Sortie
            Exit Sub
        End If
        ' ==================================
        ' Procédure lorsque le sélecteur de la liste des Libellés des Préleveurs est arrivé à sa position
        If Variables.NumeroList = 3 And Panel3.Top >= 0 Then
            ' ==================================
            ' Arrêt du timer
            Timer2.Enabled = False
            ' ==================================
            ' Déclenchement du timer de la temporisation de lecture
            Timer3.Enabled = True
            ' ==================================
            ' Sortie
            Exit Sub
        End If
        ' ==================================
        ' Procédure lorsque le sélecteur de la liste Unité Immersion est arrivé à sa position
        If Variables.NumeroList = 4 And Panel4.Top >= 0 Then
            ' ==================================
            ' Arrêt du timer
            Timer2.Enabled = False
            ' ==================================
            ' Déclenchement du timer de la temporisation de lecture
            Timer3.Enabled = True
            ' ==================================
            ' Sortie
            Exit Sub
        End If
        ' ==================================
        ' Procédure lorsque le sélecteur de la liste Unité Taille Prélèvement est arrivé à sa position
        If Variables.NumeroList = 5 And Panel5.Top >= 0 Then
            ' ==================================
            ' Arrêt du timer
            Timer2.Enabled = False
            ' ==================================
            ' Déclenchement du timer de la temporisation de lecture
            Timer3.Enabled = True
            ' ==================================
            ' Sortie
            Exit Sub
        End If
        ' ==================================
        ' Procédure lorsque le sélecteur de la liste Positionnement Prélèvement est arrivé à sa position
        If Variables.NumeroList = 6 And Panel6.Top >= 0 Then
            ' ==================================
            ' Arrêt du timer
            Timer2.Enabled = False
            ' ==================================
            ' Déclenchement du timer de la temporisation de lecture
            Timer3.Enabled = True
            ' ==================================
            ' Sortie
            Exit Sub
        End If


        ' ==================================
        ' Incrémentation du déplacement du sélecteur de la liste des Dénominations des Préleveurs
        If Variables.NumeroList = 1 Then Panel1.Top = Panel1.Top + 2
        ' ==================================
        ' Incrémentation du déplacement du sélecteur de la liste des Dénominations des Analystes
        If Variables.NumeroList = 2 Then Panel2.Top = Panel2.Top + 2
        ' ==================================
        ' Incrémentation du déplacement du sélecteur de la liste des Libellés des Préleveurs
        If Variables.NumeroList = 3 Then Panel3.Top = Panel3.Top + 2
        ' ==================================
        ' Incrémentation du déplacement du sélecteur de la liste Unité Immersion
        If Variables.NumeroList = 4 Then Panel4.Top = Panel4.Top + 2
        ' ==================================
        ' Incrémentation du déplacement du sélecteur de la liste Unité Taille Prélèvement
        If Variables.NumeroList = 5 Then Panel5.Top = Panel5.Top + 2
        ' ==================================
        ' Incrémentation du déplacement du sélecteur de la liste Positionnement Prélèvement
        If Variables.NumeroList = 6 Then Panel6.Top = Panel6.Top + 2

    End Sub

    ' ==============================================================================================================================================
    ' Déclenche l'attente de lecture et l'affichage de la liste
    Private Sub Timer3_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer3.Tick
        ' ==================================
        ' Incrémentation du déplacement du sélecteur de la liste des Dénominations des Préleveurs
        If Variables.NumeroList = 1 Then
            Panel1.Height = Panel1.Height + 16
            ' ==================================
            ' Apparition de la liste correspondante
            ListBox1.Visible = True
        End If
        ' ==================================
        ' Incrémentation du déplacement du sélecteur de la liste Niveau de Prélèvement
        If Variables.NumeroList = 2 Then
            Panel2.Height = Panel2.Height + 16
            ' ==================================
            ' Apparition de la liste correspondante
            ListBox2.Visible = True
        End If
        ' ==================================
        ' Incrémentation du déplacement du sélecteur de la liste des Libellés des Préleveurs
        If Variables.NumeroList = 3 Then
            Panel3.Height = Panel3.Height + 16
            ' ==================================
            ' Apparition de la liste correspondante
            ListBox3.Visible = True
        End If
        ' ==================================
        ' Incrémentation du déplacement du sélecteur de la liste Unité Immersion
        If Variables.NumeroList = 4 Then
            Panel4.Height = Panel4.Height + 16
            ' ==================================
            ' Apparition de la liste correspondante
            ListBox4.Visible = True
        End If
        ' ==================================
        ' Incrémentation du déplacement du sélecteur de la liste Unité Taille Prélèvement
        If Variables.NumeroList = 5 Then
            Panel5.Height = Panel5.Height + 16
            ' ==================================
            ' Apparition de la liste correspondante
            ListBox5.Visible = True
        End If
        ' ==================================
        ' Incrémentation du déplacement du sélecteur de la liste Positionnement Prélèvement 
        If Variables.NumeroList = 6 Then
            Panel6.Height = Panel6.Height + 16
            ' ==================================
            ' Apparition de la liste correspondante
            ListBox6.Visible = True
        End If


        ' ==================================
        ' Vérification de la fin du déplacement du sélecteur de la liste des Dénominations des Préleveurs
        If Variables.NumeroList = 1 And Panel1.Height >= 370 Then
            ' ==================================
            ' Arrête du timer de temporisation de lecture
            Timer3.Enabled = False
            ' ==================================
            ' Déclenchement du timer de déclenchement de la procédure de retrait
            Timer6.Enabled = True
            ' ==================================
            ' Déclenchement du timer de retrait automatique
            Timer7.Enabled = True
        End If
        ' ==================================
        ' Vérification de la fin du déplacement du sélecteur de la liste Niveau de Prélèvement
        If Variables.NumeroList = 2 And Panel2.Height >= 289 Then
            ' ==================================
            ' Arrête du timer de temporisation de lecture
            Timer3.Enabled = False
            ' ==================================
            ' Déclenchement du timer de déclenchement de la procédure de retrait
            Timer6.Enabled = True
            ' ==================================
            ' Déclenchement du timer de retrait automatique
            Timer7.Enabled = True
        End If
        ' ==================================
        ' Vérification de la fin du déplacement du sélecteur de la liste des Libellés des Préleveurs
        If Variables.NumeroList = 3 And Panel3.Height >= 370 Then
            ' ==================================
            ' Arrête du timer de temporisation de lecture
            Timer3.Enabled = False
            ' ==================================
            ' Déclenchement du timer de déclenchement de la procédure de retrait
            Timer6.Enabled = True
            ' ==================================
            ' Déclenchement du timer de retrait automatique
            Timer7.Enabled = True
        End If
        ' ==================================
        ' Vérification de la fin du déplacement du sélecteur de la liste Unité Immersion
        If Variables.NumeroList = 4 And Panel4.Height >= 289 Then
            ' ==================================
            ' Arrête du timer de temporisation de lecture
            Timer3.Enabled = False
            ' ==================================
            ' Déclenchement du timer de déclenchement de la procédure de retrait
            Timer6.Enabled = True
            ' ==================================
            ' Déclenchement du timer de retrait automatique
            Timer7.Enabled = True
        End If
        ' ==================================
        ' Vérification de la fin du déplacement du sélecteur de la liste Unité Taille Prélèvement
        If Variables.NumeroList = 5 And Panel5.Height >= 289 Then
            ' ==================================
            ' Arrête du timer de temporisation de lecture
            Timer3.Enabled = False
            ' ==================================
            ' Déclenchement du timer de déclenchement de la procédure de retrait
            Timer6.Enabled = True
            ' ==================================
            ' Déclenchement du timer de retrait automatique
            Timer7.Enabled = True
        End If
        ' ==================================
        ' Vérification de la fin du déplacement du sélecteur de la liste Positionnement Prélèvement 
        If Variables.NumeroList = 6 And Panel6.Height >= 289 Then
            ' ==================================
            ' Arrête du timer de temporisation de lecture
            Timer3.Enabled = False
            ' ==================================
            ' Déclenchement du timer de déclenchement de la procédure de retrait
            Timer6.Enabled = True
            ' ==================================
            ' Déclenchement du timer de retrait automatique
            Timer7.Enabled = True
        End If


    End Sub


    '=============================================================================================================================================
    ' PROCÉDURES DES DISPARITION DES SÉLECTEURS
    '=============================================================================================================================================
    ' Déclenche la disparition du sélecteur
    Private Sub Timer4_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer4.Tick
        ' ==================================
        ' Décrémentation du déplacement du sélecteur de la liste des Dénominations des Préleveurs
        If Variables.NumeroList = 1 And Panel1.Height > 50 Then
            Panel1.Height = Panel1.Height - 16
        End If
        ' ==================================
        ' Décrémentation du déplacement du sélecteur de la liste Niveau de Prélèvement
        If Variables.NumeroList = 2 And Panel2.Height > 50 Then
            Panel2.Height = Panel2.Height - 16
        End If

        ' ==================================
        ' Décrémentation du déplacement du sélecteur de la liste des Libellés des Préleveurs
        If Variables.NumeroList = 3 And Panel3.Height > 50 Then
            Panel3.Height = Panel3.Height - 16
        End If
        ' ==================================
        ' Décrémentation du déplacement du sélecteur de la liste Unité Immersion
        If Variables.NumeroList = 4 And Panel4.Height > 50 Then
            Panel4.Height = Panel4.Height - 16
        End If
        ' ==================================
        ' Décrémentation du déplacement du sélecteur de la liste Unité Immersion
        If Variables.NumeroList = 5 And Panel5.Height > 50 Then
            Panel5.Height = Panel5.Height - 16
        End If
        ' ==================================
        ' Décrémentation du déplacement du sélecteur de la liste Positionnement Prélèvement 
        If Variables.NumeroList = 6 And Panel6.Height > 50 Then
            Panel6.Height = Panel6.Height - 16
        End If

        ' ==================================
        ' Vérification de la fin du déplacement du sélecteur de la liste des Dénominations des Préleveurs
        If Variables.NumeroList = 1 And Panel1.Height <= 50 Then
            ' ==================================
            ' Retrait de la liste correspondante
            ListBox1.Visible = False
            ' ==================================
            ' Arrêt du timer
            Timer4.Enabled = False
            ' Déclenchement du timer de retrait du conteneur de la liste correspondante et lancement éventuel d'une liste demandée
            Timer5.Enabled = True
        End If
        ' ==================================
        ' Vérification de la fin du déplacement du sélecteur de la liste Niveau de Prélèvement
        If Variables.NumeroList = 2 And Panel2.Height <= 50 Then
            ' ==================================
            ' Retrait de la liste correspondante
            ListBox2.Visible = False
            ' ==================================
            ' Arrêt du timer
            Timer4.Enabled = False
            ' Déclenchement du timer de retrait du conteneur de la liste correspondante et lancement éventuel d'une liste demandée
            Timer5.Enabled = True
        End If

        ' ==================================
        ' Vérification de la fin du déplacement du sélecteur de la liste des Libellés des Préleveurs
        If Variables.NumeroList = 3 And Panel3.Height <= 50 Then
            ' ==================================
            ' Retrait de la liste correspondante
            ListBox3.Visible = False
            ' ==================================
            ' Arrêt du timer
            Timer4.Enabled = False
            ' Déclenchement du timer de retrait du conteneur de la liste correspondante et lancement éventuel d'une liste demandée
            Timer5.Enabled = True
        End If
        ' ==================================
        ' Vérification de la fin du déplacement du sélecteur de la liste Unité Immersion
        If Variables.NumeroList = 4 And Panel4.Height <= 50 Then
            ' ==================================
            ' Retrait de la liste correspondante
            ListBox4.Visible = False
            ' ==================================
            ' Arrêt du timer
            Timer4.Enabled = False
            ' Déclenchement du timer de retrait du conteneur de la liste correspondante et lancement éventuel d'une liste demandée
            Timer5.Enabled = True
        End If
        ' ==================================
        ' Vérification de la fin du déplacement du sélecteur de la liste Unité Taille Prélèvement
        If Variables.NumeroList = 5 And Panel5.Height <= 50 Then
            ' ==================================
            ' Retrait de la liste correspondante
            ListBox5.Visible = False
            ' ==================================
            ' Arrêt du timer
            Timer4.Enabled = False
            ' Déclenchement du timer de retrait du conteneur de la liste correspondante et lancement éventuel d'une liste demandée
            Timer5.Enabled = True
        End If
        ' ==================================
        ' Vérification de la fin du déplacement du sélecteur de la liste Positionnement Prélèvement 
        If Variables.NumeroList = 6 And Panel6.Height <= 50 Then
            ' ==================================
            ' Retrait de la liste correspondante
            ListBox6.Visible = False
            ' ==================================
            ' Arrêt du timer
            Timer4.Enabled = False
            ' Déclenchement du timer de retrait du conteneur de la liste correspondante et lancement éventuel d'une liste demandée
            Timer5.Enabled = True
        End If
    End Sub

    ' ==============================================================================================================================================
    ' Timer de retrait du conteneur de la liste correspondante et lancement éventuel d'une liste demandée
    Private Sub Timer5_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer5.Tick
        ' ==================================
        ' Décrémentation du déplacement du sélecteur de la liste des Dénominations des Préleveurs
        If Variables.NumeroList = 1 Then Panel1.Top = Panel1.Top - 2
        ' ==================================
        ' Décrémentation du déplacement du sélecteur de la liste Niveau de Prélèvement
        If Variables.NumeroList = 2 Then Panel2.Top = Panel2.Top - 2
        ' ==================================
        ' Décrémentation du déplacement du sélecteur de la liste des Libellés des Préleveurs
        If Variables.NumeroList = 3 Then Panel3.Top = Panel3.Top - 2
        ' ==================================
        ' Décrémentation du déplacement du sélecteur de la liste Unité Immersion
        If Variables.NumeroList = 4 Then Panel4.Top = Panel4.Top - 2
        ' ==================================
        ' Décrémentation du déplacement du sélecteur de la liste Unité Taille Prélèvement
        If Variables.NumeroList = 5 Then Panel5.Top = Panel5.Top - 2
        ' ==================================
        ' Décrémentation du déplacement du sélecteur de la liste Positionnement Prélèvement 
        If Variables.NumeroList = 6 Then Panel6.Top = Panel6.Top - 2


        ' ==================================
        ' Traitement pour la liste des Dénominations des Préleveurs
        If Variables.NumeroList = 1 And Panel1.Top <= -50 Then
            ' ==================================
            ' Remise à zéro de la variable de numéro de la liste
            Variables.NumeroList = 0
            ' ==================================
            ' Arrêt du timer
            Timer5.Enabled = False
            ' ==================================
            ' Arrêt du timer de déclenchement de la procédure de retrait
            Timer6.Enabled = False
            ' ==================================
            ' Vérification si l'apparition d'une liste a été demandée
            If Variables.NumeroListSuivant <> 0 Then
                ' ==================================
                ' Transfert du numéro de la liste suivante dans celui de la liste
                Variables.NumeroList = Variables.NumeroListSuivant
                ' ==================================
                ' Remise à zéro de la variable de numéro de la liste suivante
                Variables.NumeroListSuivant = 0
                ' ==================================
                ' Déclenchement du timer de la procédure d'apparition des listes
                Timer2.Enabled = True
            End If
            Exit Sub
        End If
        ' ==================================
        ' Traitement pour la liste Niveau de Prélèvement
        If Variables.NumeroList = 2 And Panel2.Top <= -50 Then
            ' ==================================
            ' Remise à zéro de la variable de numéro de la liste
            Variables.NumeroList = 0
            ' ==================================
            ' Arrêt du timer
            Timer5.Enabled = False
            ' ==================================
            ' Arrêt du timer de déclenchement de la procédure de retrait
            Timer6.Enabled = False
            ' ==================================
            ' Vérification si l'apparition d'une liste a été demandée
            If Variables.NumeroListSuivant <> 0 Then
                ' ==================================
                ' Transfert du numéro de la liste suivante dans celui de la liste
                Variables.NumeroList = Variables.NumeroListSuivant
                ' ==================================
                ' Remise à zéro de la variable de numéro de la liste suivante
                Variables.NumeroListSuivant = 0
                ' ==================================
                ' Déclenchement du timer de la procédure d'apparition des listes
                Timer2.Enabled = True
            End If
            Exit Sub
        End If
        ' ==================================
        ' Traitement pour la liste des Libellés des Préleveurs
        If Variables.NumeroList = 3 And Panel3.Top <= -50 Then
            ' ==================================
            ' Remise à zéro de la variable de numéro de la liste
            Variables.NumeroList = 0
            ' ==================================
            ' Arrêt du timer
            Timer5.Enabled = False
            ' ==================================
            ' Arrêt du timer de déclenchement de la procédure de retrait
            Timer6.Enabled = False
            ' ==================================
            ' Vérification si l'apparition d'une liste a été demandée
            If Variables.NumeroListSuivant <> 0 Then
                ' ==================================
                ' Transfert du numéro de la liste suivante dans celui de la liste
                Variables.NumeroList = Variables.NumeroListSuivant
                ' ==================================
                ' Remise à zéro de la variable de numéro de la liste suivante
                Variables.NumeroListSuivant = 0
                ' ==================================
                ' Déclenchement du timer de la procédure d'apparition des listes
                Timer2.Enabled = True
            End If
            Exit Sub
        End If
        ' ==================================
        ' Traitement pour la liste Unité Immersion
        If Variables.NumeroList = 4 And Panel4.Top <= -50 Then
            ' ==================================
            ' Remise à zéro de la variable de numéro de la liste
            Variables.NumeroList = 0
            ' ==================================
            ' Arrêt du timer
            Timer5.Enabled = False
            ' ==================================
            ' Arrêt du timer de déclenchement de la procédure de retrait
            Timer6.Enabled = False
            ' ==================================
            ' Vérification si l'apparition d'une liste a été demandée
            If Variables.NumeroListSuivant <> 0 Then
                ' ==================================
                ' Transfert du numéro de la liste suivante dans celui de la liste
                Variables.NumeroList = Variables.NumeroListSuivant
                ' ==================================
                ' Remise à zéro de la variable de numéro de la liste suivante
                Variables.NumeroListSuivant = 0
                ' ==================================
                ' Déclenchement du timer de la procédure d'apparition des listes
                Timer2.Enabled = True
            End If
            Exit Sub
        End If
        ' ==================================
        ' Traitement pour la liste Unité Taille Prélèvement
        If Variables.NumeroList = 5 And Panel5.Top <= -50 Then
            ' ==================================
            ' Remise à zéro de la variable de numéro de la liste
            Variables.NumeroList = 0
            ' ==================================
            ' Arrêt du timer
            Timer5.Enabled = False
            ' ==================================
            ' Arrêt du timer de déclenchement de la procédure de retrait
            Timer6.Enabled = False
            ' ==================================
            ' Vérification si l'apparition d'une liste a été demandée
            If Variables.NumeroListSuivant <> 0 Then
                ' ==================================
                ' Transfert du numéro de la liste suivante dans celui de la liste
                Variables.NumeroList = Variables.NumeroListSuivant
                ' ==================================
                ' Remise à zéro de la variable de numéro de la liste suivante
                Variables.NumeroListSuivant = 0
                ' ==================================
                ' Déclenchement du timer de la procédure d'apparition des listes
                Timer2.Enabled = True
            End If
            Exit Sub
        End If
        ' ==================================
        ' Traitement pour la liste Positionnement Prélèvement 
        If Variables.NumeroList = 6 And Panel6.Top <= -50 Then
            ' ==================================
            ' Remise à zéro de la variable de numéro de la liste
            Variables.NumeroList = 0
            ' ==================================
            ' Arrêt du timer
            Timer5.Enabled = False
            ' ==================================
            ' Arrêt du timer de déclenchement de la procédure de retrait
            Timer6.Enabled = False
            ' ==================================
            ' Vérification si l'apparition d'une liste a été demandée
            If Variables.NumeroListSuivant <> 0 Then
                ' ==================================
                ' Transfert du numéro de la liste suivante dans celui de la liste
                Variables.NumeroList = Variables.NumeroListSuivant
                ' ==================================
                ' Remise à zéro de la variable de numéro de la liste suivante
                Variables.NumeroListSuivant = 0
                ' ==================================
                ' Déclenchement du timer de la procédure d'apparition des listes
                Timer2.Enabled = True
            End If
            Exit Sub
        End If

    End Sub

    ' ==============================================================================================================================================
    ' Timer de déclenchement de la procédure de retrait de la liste affichée
    Private Sub Timer6_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer6.Tick
        ' ==================================
        ' Déclenchement du timer de la procédure de retrait
        Timer4.Enabled = True
        ' ==================================
        ' Arrêt du timer
        Timer6.Enabled = False
        ' ==================================
        ' Arrêt du timer de retrait automatique
        Timer7.Enabled = False
    End Sub

    ' ==============================================================================================================================================
    ' Timer de retrait automatique lorsque le curseur n'est pas sur la zone correspondante
    Private Sub Timer7_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer7.Tick

        '       TextBox1.Text = "X " & MousePosition.X - Me.Left
        '       TextBox8.Text = "Y " & MousePosition.Y - Me.Top

        ' ==================================
        ' Traitement pour la liste des Dénominations des Préleveurs
        If Variables.NumeroList = 1 Then
            ' ==================================
            ' Vérification si le curseur est dans la zone correspondante
            If (MousePosition.X - Me.Left) > 152 And (MousePosition.X - Me.Left) < 695 And (MousePosition.Y - Me.Top) > 30 And (MousePosition.Y - Me.Top) < 399 Then
                ' ==================================
                ' Arrêt du timer de déclenchement de la procédure de retrait
                Timer6.Enabled = False
                ' ==================================
                ' Sortie
                Exit Sub
            End If
        End If
        ' ==================================
        ' Traitement pour la liste Niveau de Prélèvement
        If Variables.NumeroList = 2 Then
            ' ==================================
            ' Vérification si le curseur est dans la zone correspondante
            If (MousePosition.X - Me.Left) > 292 And (MousePosition.X - Me.Left) < 553 And (MousePosition.Y - Me.Top) > 30 And (MousePosition.Y - Me.Top) < 319 Then
                ' ==================================
                ' Arrêt du timer de déclenchement de la procédure de retrait
                Timer6.Enabled = False
                ' ==================================
                ' Sortie
                Exit Sub
            End If
        End If
        ' ==================================
        ' Traitement pour la liste des Libellés des Préleveurs
        If Variables.NumeroList = 3 Then
            ' ==================================
            ' Vérification si le curseur est dans la zone correspondante
            If (MousePosition.X - Me.Left) > 292 And (MousePosition.X - Me.Left) < 553 And (MousePosition.Y - Me.Top) > 30 And (MousePosition.Y - Me.Top) < 399 Then
                ' ==================================
                ' Arrêt du timer de déclenchement de la procédure de retrait
                Timer6.Enabled = False
                ' ==================================
                ' Sortie
                Exit Sub
            End If
        End If
        ' ==================================
        ' Traitement pour la liste Unité Immersion
        If Variables.NumeroList = 4 Then
            ' ==================================
            ' Vérification si le curseur est dans la zone correspondante
            If (MousePosition.X - Me.Left) > 292 And (MousePosition.X - Me.Left) < 553 And (MousePosition.Y - Me.Top) > 30 And (MousePosition.Y - Me.Top) < 319 Then
                ' ==================================
                ' Arrêt du timer de déclenchement de la procédure de retrait
                Timer6.Enabled = False
                ' ==================================
                ' Sortie
                Exit Sub
            End If
        End If
        ' ==================================
        ' Traitement pour la liste Unité Taille Prélèvement
        If Variables.NumeroList = 5 Then
            ' ==================================
            ' Vérification si le curseur est dans la zone correspondante
            If (MousePosition.X - Me.Left) > 322 And (MousePosition.X - Me.Left) < 523 And (MousePosition.Y - Me.Top) > 30 And (MousePosition.Y - Me.Top) < 319 Then
                ' ==================================
                ' Arrêt du timer de déclenchement de la procédure de retrait
                Timer6.Enabled = False
                ' ==================================
                ' Sortie
                Exit Sub
            End If
        End If
        ' ==================================
        ' Traitement pour la liste Positionnement Prélèvement 
        If Variables.NumeroList = 6 Then
            ' ==================================
            ' Vérification si le curseur est dans la zone correspondante
            If (MousePosition.X - Me.Left) > 152 And (MousePosition.X - Me.Left) < 695 And (MousePosition.Y - Me.Top) > 30 And (MousePosition.Y - Me.Top) < 319 Then
                ' ==================================
                ' Arrêt du timer de déclenchement de la procédure de retrait
                Timer6.Enabled = False
                ' ==================================
                ' Sortie
                Exit Sub
            End If
        End If

        ' ==================================
        ' Déclenchement du timer de déclenchement de la procédure de retrait
        Timer6.Enabled = True
    End Sub

    '=============================================================================================================================================
    ' PROCÉDURES DE SÉLECTION DANS LES LISTES
    '=============================================================================================================================================
    ' Sélection d'une dénomination de Préleveur
    Private Sub ListBox1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ListBox1.SelectedIndexChanged
        ' ==================================
        ' Sortie si sélection vide
        If ListBox1.SelectedItem = "" Then Exit Sub
        ' ==================================
        Dim IndexTabInfo As Integer
        ' ==================================
        ' Arrêt du timer de temporisation de lecture pour éviter un conflit lorsque la liste se déroule et que l'on a cliqué sur une sélection et que le DataGridView1 se déroule en arrière
        Timer3.Enabled = False
        ' ==================================
        ' Transfert de la sélection
        If ListBox1.SelectedItem = " - Effacement du champ -" Then
            TextBox8.Text = ""
            TextBox23.Text = ""
        Else
            TextBox23.Text = Strings.LTrim(ListBox1.SelectedItem)
            ' ==================================
            ' Recherche du point d'entrée dans le tableau des informations pour les Préleveurs
            For i = 0 To 5000
                If Variables.TabRéfInfo(i, 0) = "PRÉLEVEUR" Then
                    IndexTabInfo = i + 1
                    Exit For
                End If
            Next
            TextBox8.Text = ""
            ' ==================================
            ' Récupération du libellé du Préleveur
            For i = IndexTabInfo To 5000
                ' ==================================
                ' Sortie de la boucle si fin de tableau
                If Variables.TabRéfInfo(i, 0) = "" Then Exit For
                ' ==================================
                ' Rangement du libellé  du Préleveur
                If Variables.TabRéfInfo(i, 0) = TextBox23.Text Then
                    ' ==================================
                    ' Affectation de la désignation
                    TextBox8.Text = Variables.TabRéfInfo(i, 1)
                End If
            Next
        End If
        ' ==================================
        ' Déclenchement du retrait du sélecteur
        Timer4.Enabled = True
        ' ==================================
        ' Arrêt du timer de retrait automatique
        Timer7.Enabled = False
        ' ==================================
        ' Désélection 
        ListBox1.ClearSelected()
    End Sub

    '=============================================================================================================================================
    ' Sélection d'une dénomination d'un Niveau de Prélèvement
    Private Sub ListBox2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ListBox2.SelectedIndexChanged
        ' ==================================
        ' Sortie si sélection vide
        If ListBox2.SelectedItem = "" Then Exit Sub
        ' ==================================
        ' Arrêt du timer de temporisation de lecture pour éviter un conflit lorsque la liste se déroule et que l'on a cliqué sur une sélection et que le DataGridView1 se déroule en arrière
        Timer3.Enabled = False
        ' ==================================
        ' Transfert de la sélection
        If ListBox2.SelectedItem = " - Effacement du champ -" Then
            TextBox1.Text = ""
        Else
            TextBox1.Text = Strings.LTrim(ListBox2.SelectedItem)
        End If
        ' ==================================
        ' Déclenchement du retrait du sélecteur
        Timer4.Enabled = True
        ' ==================================
        ' Arrêt du timer de retrait automatique
        Timer7.Enabled = False
        ' ==================================
        ' Désélection 
        ListBox2.ClearSelected()
    End Sub

    '=============================================================================================================================================
    ' Sélection d'un libellé de Préleveur
    Private Sub ListBox3_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ListBox3.SelectedIndexChanged
        ' ==================================
        ' Sortie si sélection vide
        If ListBox3.SelectedItem = "" Then Exit Sub
        ' ==================================
        Dim IndexTabInfo As Integer
        ' ==================================
        ' Arrêt du timer de temporisation de lecture pour éviter un conflit lorsque la liste se déroule et que l'on a cliqué sur une sélection et que le DataGridView1 se déroule en arrière
        Timer3.Enabled = False
        ' ==================================
        ' Transfert de la sélection
        If ListBox3.SelectedItem = " - Effacement du champ -" Then
            TextBox8.Text = ""
            TextBox23.Text = ""
        Else
            TextBox8.Text = Strings.LTrim(ListBox3.SelectedItem)
            ' ==================================
            ' Recherche du point d'entrée dans le tableau des informations pour les Préleveurs
            For i = 0 To 5000
                If Variables.TabRéfInfo(i, 0) = "PRÉLEVEUR" Then
                    IndexTabInfo = i + 1
                    Exit For
                End If
            Next
            TextBox23.Text = ""
            ' ==================================
            ' Récupération de la dénomination du Préleveur
            For i = IndexTabInfo To 5000
                ' ==================================
                ' Sortie de la boucle si fin de tableau
                If Variables.TabRéfInfo(i, 0) = "" Then Exit For
                ' ==================================
                ' Rangement de la dénomination du Préleveur
                If Variables.TabRéfInfo(i, 1) = TextBox8.Text Then
                    ' ==================================
                    ' Affectation de la désignation
                    TextBox23.Text = Variables.TabRéfInfo(i, 0)
                End If
            Next
        End If
        ' ==================================
        ' Déclenchement du retrait du sélecteur
        Timer4.Enabled = True
        ' ==================================
        ' Arrêt du timer de retrait automatique
        Timer7.Enabled = False
        ' ==================================
        ' Désélection 
        ListBox3.ClearSelected()
    End Sub

    '=============================================================================================================================================
    ' Sélection d'une dénomination Unité Immersion
    Private Sub ListBox4_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ListBox4.SelectedIndexChanged
        ' ==================================
        ' Sortie si sélection vide
        If ListBox4.SelectedItem = "" Then Exit Sub
        ' ==================================
        ' Arrêt du timer de temporisation de lecture pour éviter un conflit lorsque la liste se déroule et que l'on a cliqué sur une sélection et que le DataGridView1 se déroule en arrière
        Timer3.Enabled = False
        ' ==================================
        ' Transfert de la sélection
        If ListBox4.SelectedItem = " - Effacement du champ -" Then
            TextBox7.Text = ""
        Else
            TextBox7.Text = Strings.LTrim(ListBox4.SelectedItem)
        End If
        ' ==================================
        ' Déclenchement du retrait du sélecteur
        Timer4.Enabled = True
        ' ==================================
        ' Arrêt du timer de retrait automatique
        Timer7.Enabled = False
        ' ==================================
        ' Désélection 
        ListBox4.ClearSelected()
    End Sub

    '=============================================================================================================================================
    ' Sélection d'une dénomination Unité Taille Prélèvement
    Private Sub ListBox5_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ListBox5.SelectedIndexChanged
        ' ==================================
        ' Sortie si sélection vide
        If ListBox5.SelectedItem = "" Then Exit Sub
        ' ==================================
        ' Arrêt du timer de temporisation de lecture pour éviter un conflit lorsque la liste se déroule et que l'on a cliqué sur une sélection et que le DataGridView1 se déroule en arrière
        Timer3.Enabled = False
        ' ==================================
        ' Transfert de la sélection
        If ListBox5.SelectedItem = " - Effacement du champ -" Then
            TextBox10.Text = ""
        Else
            TextBox10.Text = Strings.LTrim(ListBox5.SelectedItem)
        End If
        ' ==================================
        ' Déclenchement du retrait du sélecteur
        Timer4.Enabled = True
        ' ==================================
        ' Arrêt du timer de retrait automatique
        Timer7.Enabled = False
        ' ==================================
        ' Désélection 
        ListBox5.ClearSelected()
    End Sub

    '=============================================================================================================================================
    ' Sélection d'une dénomination Positionnement Prélèvement 
    Private Sub ListBox6_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ListBox6.SelectedIndexChanged
        ' ==================================
        ' Sortie si sélection vide
        If ListBox6.SelectedItem = "" Then Exit Sub
        ' ==================================
        ' Arrêt du timer de temporisation de lecture pour éviter un conflit lorsque la liste se déroule et que l'on a cliqué sur une sélection et que le DataGridView1 se déroule en arrière
        Timer3.Enabled = False
        ' ==================================
        ' Transfert de la sélection
        If ListBox6.SelectedItem = " - Effacement du champ -" Then
            TextBox16.Text = ""
        Else
            TextBox16.Text = Strings.LTrim(ListBox6.SelectedItem)
        End If
        ' ==================================
        ' Déclenchement du retrait du sélecteur
        Timer4.Enabled = True
        ' ==================================
        ' Arrêt du timer de retrait automatique
        Timer7.Enabled = False
        ' ==================================
        ' Désélection 
        ListBox6.ClearSelected()
    End Sub


    '=============================================================================================================================================
    ' PROCÉDURES DE FORMATAGES
    '=============================================================================================================================================
    ' Vérification du formatage de l'Heure de Passage
    Private Sub TextBox11_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox11.LostFocus
        If TextBox11.Text = "" Then Exit Sub

        If Len(TextBox11.Text) <> 5 Then
            MsgBox("Le format est hh:mm ! ", MsgBoxStyle.Critical, " QUADRISPEL - Erreur")
            TextBox11.Select()
            Exit Sub
        End If

        If Val(Strings.Left(TextBox11.Text, 2)) < 0 Or Val(Strings.Left(TextBox11.Text, 2)) > 23 Then
            MsgBox("La valeur de l'heure est erronée ! ", MsgBoxStyle.Critical, " QUADRISPEL - Erreur")
            TextBox11.Select()
            Exit Sub
        End If

        If Val(Strings.Right(TextBox11.Text, 2)) < 0 Or Val(Strings.Right(TextBox11.Text, 2)) > 59 Then
            MsgBox("La valeur des minutes est erronée ! ", MsgBoxStyle.Critical, " QUADRISPEL - Erreur")
            TextBox11.Select()
            Exit Sub
        End If

        TextBox11.Text = Strings.Left(TextBox11.Text, 2) & ":" & Strings.Right(TextBox11.Text, 2)

    End Sub


    '=============================================================================================================================================
    ' Demande de sauvegarde du masque lors de la fermeture si oubli

    Private Sub TextBox1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox1.TextChanged
        ' ==================================
        ' Validation sauvegarde à la fermeture
        Variables.SauveFichierMasque = True
    End Sub

    Private Sub TextBox8_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox8.TextChanged
        ' ==================================
        ' Validation sauvegarde à la fermeture
        Variables.SauveFichierMasque = True
    End Sub

    Private Sub TextBox23_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox23.TextChanged
        ' ==================================
        ' Validation sauvegarde à la fermeture
        Variables.SauveFichierMasque = True
    End Sub

    Private Sub TextBox3_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox3.TextChanged
        ' ==================================
        ' Changement couleur obligation champ
        If TextBox3.Text <> "" Then
            TextBox7.BackColor = Color.FromArgb(255, 255, 200)
        Else
            If TextBox5.Text = "" And TextBox6.Text = "" Then
                TextBox7.BackColor = Color.White
            End If
        End If
            ' ==================================
            ' Validation sauvegarde à la fermeture
            Variables.SauveFichierMasque = True
    End Sub

    Private Sub TextBox6_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox6.TextChanged
        ' ==================================
        ' Changement couleur obligation champ
        If TextBox6.Text <> "" Then
            TextBox5.BackColor = Color.FromArgb(255, 255, 200)
            TextBox7.BackColor = Color.FromArgb(255, 255, 200)
        Else
            If TextBox5.Text = "" Then
                TextBox5.BackColor = Color.White
                TextBox6.BackColor = Color.White
            End If

            If TextBox3.Text = "" And TextBox5.Text = "" Then
                TextBox7.BackColor = Color.White
            End If
        End If
            ' ==================================
            ' Validation sauvegarde à la fermeture
            Variables.SauveFichierMasque = True
    End Sub

    Private Sub TextBox5_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox5.TextChanged
        ' ==================================
        ' Changement couleur obligation champ
        If TextBox5.Text <> "" Then
            TextBox6.BackColor = Color.FromArgb(255, 255, 200)
            TextBox7.BackColor = Color.FromArgb(255, 255, 200)
        Else
            If TextBox6.Text = "" Then
                TextBox5.BackColor = Color.White
                TextBox6.BackColor = Color.White
            End If
            If TextBox3.Text = "" And TextBox6.Text = "" Then
                TextBox7.BackColor = Color.White
            End If
        End If
            ' ==================================
            ' Validation sauvegarde à la fermeture
            Variables.SauveFichierMasque = True
    End Sub

    Private Sub TextBox7_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox7.TextChanged
        ' ==================================
        ' Validation sauvegarde à la fermeture
        Variables.SauveFichierMasque = True
    End Sub

    Private Sub TextBox9_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox9.TextChanged
        ' ==================================
        ' Changement couleur obligation champ
        If TextBox9.Text <> "" Then
            TextBox10.BackColor = Color.FromArgb(255, 255, 200)
        Else
            TextBox10.BackColor = Color.White
        End If
        ' ==================================
        ' Validation sauvegarde à la fermeture
        Variables.SauveFichierMasque = True
    End Sub

    Private Sub TextBox10_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox10.TextChanged
        ' ==================================
        ' Validation sauvegarde à la fermeture
        Variables.SauveFichierMasque = True
    End Sub

    Private Sub TextBox11_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox11.TextChanged
        ' ==================================
        ' Validation sauvegarde à la fermeture
        Variables.SauveFichierMasque = True
    End Sub

    Private Sub TextBox14_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox14.TextChanged
        ' ==================================
        ' Changement couleur obligation champ
        If TextBox14.Text <> "" Or TextBox15.Text <> "" Then
            TextBox16.BackColor = Color.FromArgb(255, 255, 200)
        Else
            TextBox16.BackColor = Color.White
        End If
        ' ==================================
        ' Validation sauvegarde à la fermeture
        Variables.SauveFichierMasque = True
    End Sub

    Private Sub TextBox15_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox15.TextChanged
        ' ==================================
        ' Changement couleur obligation champ
        If TextBox15.Text <> "" Or TextBox14.Text <> "" Then
            TextBox16.BackColor = Color.FromArgb(255, 255, 200)
        Else
            TextBox16.BackColor = Color.White
        End If
        ' ==================================
        ' Validation sauvegarde à la fermeture
        Variables.SauveFichierMasque = True
    End Sub

    Private Sub TextBox16_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox16.TextChanged
        ' ==================================
        ' Validation sauvegarde à la fermeture
        Variables.SauveFichierMasque = True
    End Sub

    Private Sub TextBox17_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox17.TextChanged
        ' ==================================
        ' Validation sauvegarde à la fermeture
        Variables.SauveFichierMasque = True
    End Sub

    '=============================================================================================================================================
    ' PROCÉDURES D'INFORMATION POUR LES CHAMPS
    '=============================================================================================================================================
    ' Passage en demande d'information
    Private Sub Button3_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Button3.MouseDown
        ' ==================================
        ' Passage en curseur d'aide
        Me.Cursor = Cursors.Help
    End Sub

    '=============================================================================================================================================
    ' Traitements des champs sélectionnés
    Private Sub Button3_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Button3.MouseUp
        ' ==================================
        ' Retour au curseur normal
        Me.Cursor = Cursors.Default
        ' ==================================
        ' Repérage
        '        TextBox14.Text = " X = " & e.X & "    Y = " & e.Y
        '        Exit Sub
        ' ==================================
        ' Vérification si le bouton Information est sélectionné
        If e.X > 1 And e.X < 114 And e.Y > 1 And e.Y < 24 Then
            MsgBox("- Cliquer sur le bouton " & Chr(34) & "Information" & Chr(34) & "," & vbCrLf & "- Maintenir appuyer," & vbCrLf & "- Déplacer le " & Chr(34) & "?" & Chr(34) & " sur le champ désiré,    " & vbCrLf & "- Relacher.", MsgBoxStyle.Information, "QUADRISPEL - Procédure d'utilisation")
            ' ==================================
            ' Renvoi du select sur le bouton caché
            Button2.Select()
            Exit Sub
        End If
        ' ==================================
        ' Vérification si le champ Dénomination Niveau Prélèvement est sélectionné
        If e.X > -175 And e.X < 29 And e.Y < -289 And e.Y > -310 Then
            Informations.TextBox1.Height = 40 + 80
            Informations.Button1.Top = 60 + 80
            Informations.Height = 128 + 80
            Informations.Text = " Information du champ " & Chr(171) & " Dénomination Niveau Prélèvement " & Chr(187)
            Informations.TextBox1.Text = "Codage particulier de la profondeur d'immersion : le matériau ramené par l'engin de prélèvement peut consister en un ensemble de parties individualisées ou individualisables." & vbCrLf & "Par exemple une palanquée va être composée de plusieurs bouteilles à des immersions différentes mais déclenchées (quasiment) au même moment, une carotte de sédiment va être découpée en tranches d'épaisseur variables." & vbCrLf & "On utilise un code arbitraire " & Chr(171) & " niveau " & Chr(187) & " pour caractériser ces parties, car lors de l'exploitation, le code niveau est souvent plus simple à utiliser que l'immersion (ou le couple épaisseur/immersion)."
            Informations.ShowDialog()
            ' ==================================
            ' Renvoi du select sur le bouton caché
            Button2.Select()
            Exit Sub
        End If
        ' ==================================
        ' Vérification si le champ Nombre Individu Prélèvement est sélectionné
        If e.X > 75 And e.X < 209 And e.Y < -289 And e.Y > -310 Then
            Informations.TextBox1.Height = 40
            Informations.Button1.Top = 60
            Informations.Height = 128
            Informations.Text = " Information du champ " & Chr(171) & " Nombre Individu Prélèvement " & Chr(187)
            Informations.TextBox1.Text = "Nombre d'individus constituant précisément le prélèvement."
            Informations.ShowDialog()
            ' ==================================
            ' Renvoi du select sur le bouton caché
            Button2.Select()
            Exit Sub
        End If
        ' ==================================
        ' Vérification si le champ Mnémonique Prélèvement est sélectionné
        If e.X > 255 And e.X < 604 And e.Y < -289 And e.Y > -310 Then
            Informations.TextBox1.Height = 40
            Informations.Button1.Top = 60
            Informations.Height = 128
            Informations.Text = " Information du champ " & Chr(171) & " Mnémonique Prélèvement " & Chr(187)
            Informations.TextBox1.Text = "Texte libre d'une longueur maximale de 50 caractères." & vbCrLf & "Un décompteur indique le nombre de caractères restants."
            Informations.ShowDialog()
            ' ==================================
            ' Renvoi du select sur le bouton caché
            Button2.Select()
            Exit Sub
        End If
        ' ==================================
        ' Vérification si le champ Acronyme Préleveur est sélectionné
        If e.X > -175 And e.X < 9 And e.Y < -224 And e.Y > -245 Then
            Informations.TextBox1.Height = 40
            Informations.Button1.Top = 60
            Informations.Height = 128
            Informations.Text = " Information du champ " & Chr(171) & " Acronyme Préleveur " & Chr(187)
            Informations.TextBox1.Text = "C'est l'acronyme de l'organisme ou du service préleveur." & vbCrLf & "La sélection d'un Acronyme Préleveur renseigne automatiquement le champ du Libellé Préleveur."
            Informations.ShowDialog()
            ' ==================================
            ' Renvoi du select sur le bouton caché
            Button2.Select()
            Exit Sub
        End If
        ' ==================================
        ' Vérification si le champ Libellé Préleveur est sélectionné
        If e.X > 55 And e.X < 604 And e.Y < -224 And e.Y > -245 Then
            Informations.TextBox1.Height = 40
            Informations.Button1.Top = 60
            Informations.Height = 128
            Informations.Text = " Information du champ " & Chr(171) & " Libellé Préleveur " & Chr(187)
            Informations.TextBox1.Text = "C'est le libellé de l'organisme ou du service préleveur." & vbCrLf & "La sélection d'un Libellé Préleveur renseigne automatiquement le champ de l'Acronyme Préleveur."
            Informations.ShowDialog()
            ' ==================================
            ' Renvoi du select sur le bouton caché
            Button2.Select()
            Exit Sub
        End If
        ' ==================================
        ' Vérification si le champ Immersion Prélèvement est sélectionné
        If e.X > -175 And e.X < -21 And e.Y < -159 And e.Y > -180 Then
            Informations.TextBox1.Height = 40 + 16
            Informations.Button1.Top = 60 + 16
            Informations.Height = 128 + 16
            Informations.Text = " Information du champ " & Chr(171) & " Immersion Prélèvement " & Chr(187)
            Informations.TextBox1.Text = "Profondeur exacte à laquelle a été faite le prélèvement." & vbCrLf & "Si l'immersion est renseignée les immersions min et max ne le sont pas." & vbCrLf & "Si l'immersion est renseignée l'unité de l'immersion est obligatoire."
            Informations.ShowDialog()
            ' ==================================
            ' Renvoi du select sur le bouton caché
            Button2.Select()
            Exit Sub
        End If
        ' ==================================
        ' Vérification si le champ Immersion Max Prélèvement est sélectionné
        If e.X > 25 And e.X < 179 And e.Y < -159 And e.Y > -180 Then
            Informations.TextBox1.Height = 40 + 16
            Informations.Button1.Top = 60 + 16
            Informations.Height = 128 + 16
            Informations.Text = " Information du champ " & Chr(171) & " Immersion Max Prélèvement " & Chr(187)
            Informations.TextBox1.Text = "Profondeur max à laquelle a été faite le prélèvement." & vbCrLf & "Si l'immersion max est renseignée l'immersion min est obligatoire." & vbCrLf & "Si l'immersion max est renseignée l'unité de l'immersion est obligatoire."
            Informations.ShowDialog()
            ' ==================================
            ' Renvoi du select sur le bouton caché
            Button2.Select()
            Exit Sub
        End If
        ' ==================================
        ' Vérification si le champ Immersion Min Prélèvement est sélectionné
        If e.X > 225 And e.X < 379 And e.Y < -159 And e.Y > -180 Then
            Informations.TextBox1.Height = 40 + 16
            Informations.Button1.Top = 60 + 16
            Informations.Height = 128 + 16
            Informations.Text = " Information du champ " & Chr(171) & " Immersion Min Prélèvement " & Chr(187)
            Informations.TextBox1.Text = "Profondeur min à laquelle a été faite le prélèvement." & vbCrLf & "Si l'immersion min est renseignée l'immersion max est obligatoire." & vbCrLf & "Si l'immersion min est renseignée l'unité de l'immersion est obligatoire."
            Informations.ShowDialog()
            ' ==================================
            ' Renvoi du select sur le bouton caché
            Button2.Select()
            Exit Sub
        End If
        ' ==================================
        ' Vérification si le champ Unité Immersion Prélèvement est sélectionné
        If e.X > 425 And e.X < 604 And e.Y < -159 And e.Y > -180 Then
            Informations.TextBox1.Height = 40 + 16
            Informations.Button1.Top = 60 + 16
            Informations.Height = 128 + 16
            Informations.Text = " Information du champ " & Chr(171) & " Unité Immersion Prélèvement " & Chr(187)
            Informations.TextBox1.Text = "Si l'immersion est renseignée ce champ est obligatoire." & vbCrLf & "Si l'immersion max est renseignée ce champ est obligatoire." & vbCrLf & "Si l'immersion min est renseignée ce champ est obligatoire."
            Informations.ShowDialog()
            ' ==================================
            ' Renvoi du select sur le bouton caché
            Button2.Select()
            Exit Sub
        End If
        ' ==================================
        ' Vérification si le champ Taille Prélèvement est sélectionné
        If e.X > -175 And e.X < -91 And e.Y < -94 And e.Y > -115 Then
            Informations.TextBox1.Height = 40
            Informations.Button1.Top = 60
            Informations.Height = 128
            Informations.Text = " Information du champ " & Chr(171) & " Taille Prélèvement " & Chr(187)
            Informations.TextBox1.Text = "Cette taille peut être exprimée soit en longueur, surface, volume ou poids."
            Informations.ShowDialog()
            ' ==================================
            ' Renvoi du select sur le bouton caché
            Button2.Select()
            Exit Sub
        End If
        ' ==================================
        ' Vérification si le champ Unité Taille Prélèvement est sélectionné
        If e.X > -45 And e.X < 69 And e.Y < -94 And e.Y > -115 Then
            Informations.TextBox1.Height = 40
            Informations.Button1.Top = 60
            Informations.Height = 128
            Informations.Text = " Information du champ " & Chr(171) & " Unité Taille Prélèvement " & Chr(187)
            Informations.TextBox1.Text = "Ce champ est obligatoire lorsque la taille du prélèvement est renseignée."
            Informations.ShowDialog()
            ' ==================================
            ' Renvoi du select sur le bouton caché
            Button2.Select()
            Exit Sub
        End If
        ' ==================================
        ' Vérification si le champ Heure Prélèvement est sélectionné
        If e.X > 115 And e.X < 204 And e.Y < -94 And e.Y > -115 Then
            Informations.TextBox1.Height = 40
            Informations.Button1.Top = 60
            Informations.Height = 128
            Informations.Text = " Information du champ " & Chr(171) & " Heure Prélèvement " & Chr(187)
            Informations.TextBox1.Text = "L'heure de passage est au format hh/mm."
            Informations.ShowDialog()
            ' ==================================
            ' Renvoi du select sur le bouton caché
            Button2.Select()
            Exit Sub
        End If
        ' ==================================
        ' Vérification si le champ Commentaires Prélèvement est sélectionné
        If e.X > 250 And e.X < 604 And e.Y < -94 And e.Y > -115 Then
            Informations.TextBox1.Height = 40
            Informations.Button1.Top = 60
            Informations.Height = 128
            Informations.Text = " Information du champ " & Chr(171) & " Commentaires Prélèvement " & Chr(187)
            Informations.TextBox1.Text = "Texte libre d'une longueur maximale de 2000 caractères." & vbCrLf & "Un décompteur indique le nombre de caractères restants."
            Informations.ShowDialog()
            ' ==================================
            ' Renvoi du select sur le bouton caché
            Button2.Select()
            Exit Sub
        End If
        ' ==================================
        ' Vérification si le champ Latitude Prélèvement est sélectionné
        If e.X > -175 And e.X < -16 And e.Y < -29 And e.Y > -50 Then
            Informations.TextBox1.Height = 40 + 32
            Informations.Button1.Top = 60 + 32
            Informations.Height = 128 + 32
            Informations.Text = " Information du champ " & Chr(171) & " Latitude Prélèvement " & Chr(187)
            Informations.TextBox1.Text = "Les prélèvements peuvent être réalisés à proximité du lieu de surveillance mais pas exactement sur celui-ci." & vbCrLf & "Si on la connaît il est possible de renseigner cette latitude au format WGS84 en degrés décimaux." & vbCrLf & "À ne renseigner que si elles sont différentes de celles du passage. Sinon, par défaut, le prélèvement a les mêmes coordonnées que le passage."
            Informations.ShowDialog()
            ' ==================================
            ' Renvoi du select sur le bouton caché
            Button2.Select()
            Exit Sub
        End If
        ' ==================================
        ' Vérification si le champ Longitude Prélèvement est sélectionné
        If e.X > 30 And e.X < 189 And e.Y < -29 And e.Y > -50 Then
            Informations.TextBox1.Height = 40 + 32
            Informations.Button1.Top = 60 + 32
            Informations.Height = 128 + 32
            Informations.Text = " Information du champ " & Chr(171) & " Longitude Prélèvement " & Chr(187)
            Informations.TextBox1.Text = "Les prélèvements peuvent être réalisés à proximité du lieu de surveillance mais pas exactement sur celui-ci." & vbCrLf & "Si on la connaît il est possible de renseigner cette latitude au format WGS84 en degrés décimaux." & vbCrLf & "À ne renseigner que si elles sont différentes de celles du passage. Sinon, par défaut, le prélèvement a les mêmes coordonnées que le passage."
            Informations.ShowDialog()
            ' ==================================
            ' Renvoi du select sur le bouton caché
            Button2.Select()
            Exit Sub
        End If
        ' ==================================
        ' Vérification si le champ Positionnement du Prélèvement est sélectionné
        If e.X > 235 And e.X < 604 And e.Y < -29 And e.Y > -50 Then
            Informations.TextBox1.Height = 40 + 80
            Informations.Button1.Top = 60 + 80
            Informations.Height = 128 + 80
            Informations.Text = " Information du champ " & Chr(171) & " Positionnement du Prélèvement " & Chr(187)
            Informations.TextBox1.Text = "Le positionnement s'intègre dans les métadonnées de la norme ISO 19115." & vbCrLf & "Il correspond à la méthodologie employée pour localiser des entités géographiques." & vbCrLf & "Il se base sur un engin de positionnement (GPS,Ortholittorale, image SPOT ...), et défini la façon dont cet engin a été utilisé pour positionner l'entité." & vbCrLf & "Il définit notamment la précision des données positionnées." & vbCrLf & vbCrLf & "Ce champ est obligatoire si la Latitude et la Longitude du Prélèvement sont renseignées."
            Informations.ShowDialog()
            ' ==================================
            ' Renvoi du select sur le bouton caché
            Button2.Select()
            Exit Sub
        End If
    End Sub

End Class