﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class SPEL
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(SPEL))
        Me.Button1 = New System.Windows.Forms.Button
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.Button9 = New System.Windows.Forms.Button
        Me.Button6 = New System.Windows.Forms.Button
        Me.Button5 = New System.Windows.Forms.Button
        Me.Button4 = New System.Windows.Forms.Button
        Me.ListBox2 = New System.Windows.Forms.ListBox
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.Button7 = New System.Windows.Forms.Button
        Me.Button3 = New System.Windows.Forms.Button
        Me.ListBox1 = New System.Windows.Forms.ListBox
        Me.Button2 = New System.Windows.Forms.Button
        Me.Button8 = New System.Windows.Forms.Button
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.Timer2 = New System.Windows.Forms.Timer(Me.components)
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(650, 530)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(120, 25)
        Me.Button1.TabIndex = 7
        Me.Button1.Text = "Fermer"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.Button9)
        Me.GroupBox2.Controls.Add(Me.Button6)
        Me.GroupBox2.Controls.Add(Me.Button5)
        Me.GroupBox2.Controls.Add(Me.Button4)
        Me.GroupBox2.Controls.Add(Me.ListBox2)
        Me.GroupBox2.ForeColor = System.Drawing.Color.Black
        Me.GroupBox2.Location = New System.Drawing.Point(400, 15)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(370, 500)
        Me.GroupBox2.TabIndex = 6
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = " Colonnes Sélectionnées "
        '
        'Button9
        '
        Me.Button9.Enabled = False
        Me.Button9.Location = New System.Drawing.Point(95, 460)
        Me.Button9.Name = "Button9"
        Me.Button9.Size = New System.Drawing.Size(100, 25)
        Me.Button9.TabIndex = 7
        Me.Button9.Text = "Tout Supprimer"
        Me.Button9.UseVisualStyleBackColor = True
        '
        'Button6
        '
        Me.Button6.Enabled = False
        Me.Button6.Location = New System.Drawing.Point(15, 460)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(70, 25)
        Me.Button6.TabIndex = 6
        Me.Button6.Text = "Supprimer"
        Me.Button6.UseVisualStyleBackColor = True
        '
        'Button5
        '
        Me.Button5.Enabled = False
        Me.Button5.Location = New System.Drawing.Point(285, 460)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(70, 25)
        Me.Button5.TabIndex = 5
        Me.Button5.Text = "Descendre"
        Me.Button5.UseVisualStyleBackColor = True
        '
        'Button4
        '
        Me.Button4.Enabled = False
        Me.Button4.Location = New System.Drawing.Point(205, 460)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(70, 25)
        Me.Button4.TabIndex = 4
        Me.Button4.Text = "Monter"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'ListBox2
        '
        Me.ListBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ListBox2.FormattingEnabled = True
        Me.ListBox2.ItemHeight = 16
        Me.ListBox2.Location = New System.Drawing.Point(15, 25)
        Me.ListBox2.Name = "ListBox2"
        Me.ListBox2.Size = New System.Drawing.Size(340, 420)
        Me.ListBox2.TabIndex = 0
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Button7)
        Me.GroupBox1.Controls.Add(Me.Button3)
        Me.GroupBox1.Controls.Add(Me.ListBox1)
        Me.GroupBox1.ForeColor = System.Drawing.Color.Black
        Me.GroupBox1.Location = New System.Drawing.Point(15, 15)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(370, 500)
        Me.GroupBox1.TabIndex = 5
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = " Colonnes Disponibles "
        '
        'Button7
        '
        Me.Button7.Location = New System.Drawing.Point(70, 460)
        Me.Button7.Name = "Button7"
        Me.Button7.Size = New System.Drawing.Size(70, 25)
        Me.Button7.TabIndex = 5
        Me.Button7.Text = "Toutes"
        Me.Button7.UseVisualStyleBackColor = True
        '
        'Button3
        '
        Me.Button3.Location = New System.Drawing.Point(225, 460)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(70, 25)
        Me.Button3.TabIndex = 4
        Me.Button3.Text = "Transférer"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'ListBox1
        '
        Me.ListBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ListBox1.FormattingEnabled = True
        Me.ListBox1.ItemHeight = 16
        Me.ListBox1.Items.AddRange(New Object() {" Date du Transfert", " Département", " Lieu de Surveillance", " Lieu de Surveillance Code Sandre", " Réseau", " Réseau Code Sandre", " Saisisseur", " Saisisseur Code Sandre", " Zone Destination Dragage", " Campagne", " Sortie", " Date Passage", " Heure Passage", " Sonde", " Unité Sonde", " Unité Sonde Code Sandre", " Mnémonique Passage", " Commentaires Passage", " Latitude Passage", " Longitude Passage", " Positionnement Passage", " Positionnement Passage Code Sandre", " Nombre Individu Passage", " Engin Prélèvement", " Engin Prélèvement Code Sandre", " Niveau Prélèvement", " Niveau Prélèvement Code Sandre", " Préleveur", " Préleveur Code Sandre", " Mnémonique Prélèvement", " Immersion Prélèvement", " Immersion Max Prélèvement", " Immersion Min Prélèvement", " Unité Immersion", " Unité Immersion Code Sandre", " Taille Prélèvement", " Unité Taille Prélèvement", " Unité Taille Prélèvement Code Sandre", " Heure Prélèvement", " Commentaires Prélèvement", " Latitude Prélèvement", " Longitude Prélèvement", " Positionnement Prélèvement", " Positionnement Prélèvement Code Sandre", " Nombre Individu Prélèvement", " Lot Aquacole", " Support Échantillon", " Support Échantillon Code Sandre", " Taxon Support Échantillon", " Groupe Taxon Support Échantillon", " Groupe Taxon Support Échantillon Code Sandre", " Numéro Échantillon", " Taille Échantillon", " Unité Taille Échantillon", " Unité Taille Échantillon Code Sandre", " Commentaires Échantillon", " Nombre Individu Échantillon", " Niveau Saisie Résultat", " Acronyme Niveau Saisie Résultat", " Famille Paramètre", " Paramètre", " Acronyme Paramètre", " Paramètre Code Sandre", " Support", " Support Code Sandre", " Fraction", " Fraction Code Sandre", " Méthode", " Méthode Code Sandre", " Numéro Individu", " Code Taxon Résultat", " Code Groupe Taxon Résultat", " Libellé Résultat Qualitatif", " Code Résultat Qualitatif", " Résultat", " Unité Résultat", " Unité Résultat Code Sandre", " Analyste", " Acronyme Analyste", " Analyste Code Sandre", " Engin Analyse", " Engin Analyse Code Sandre", " Seuil Analyse", " Remarque", " Remarque Code Sandre", " Précision", " Type Précision", " Type Précision Code Sandre", " Commentaires Mesure"})
        Me.ListBox1.Location = New System.Drawing.Point(15, 25)
        Me.ListBox1.Name = "ListBox1"
        Me.ListBox1.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple
        Me.ListBox1.Size = New System.Drawing.Size(340, 420)
        Me.ListBox1.TabIndex = 0
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(665, 535)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(15, 15)
        Me.Button2.TabIndex = 8
        Me.Button2.Text = "Button2"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Button8
        '
        Me.Button8.Location = New System.Drawing.Point(400, 530)
        Me.Button8.Name = "Button8"
        Me.Button8.Size = New System.Drawing.Size(120, 25)
        Me.Button8.TabIndex = 9
        Me.Button8.Text = "Sauvegarder"
        Me.Button8.UseVisualStyleBackColor = True
        '
        'Timer1
        '
        Me.Timer1.Interval = 1
        '
        'Timer2
        '
        Me.Timer2.Interval = 300
        '
        'SPEL
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(779, 566)
        Me.Controls.Add(Me.Button8)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Button2)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "SPEL"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = " QUADRISPEL - Configuration du fichier SPEL"
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents ListBox2 As System.Windows.Forms.ListBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Button7 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents ListBox1 As System.Windows.Forms.ListBox
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button8 As System.Windows.Forms.Button
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents Timer2 As System.Windows.Forms.Timer
    Friend WithEvents Button9 As System.Windows.Forms.Button
End Class
