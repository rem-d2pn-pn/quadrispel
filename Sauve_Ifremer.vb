﻿
' Importation des systèmes utilisés
Imports System
Imports System.IO
Imports System.IO.Directory
Imports System.Text


Public Class Sauve_Ifremer

    '=============================================================================================================================================
    ' Procédure lors de la fermeture de la feuille par la croix
    Private Sub Sauve_Ifremer_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        ' Blocage de la procédure de fermeture automatique
        e.Cancel = True
        '==============================================
        Fermeture()
    End Sub

    ' ==============================================================================================================================================
    ' Procédure au chargement de la feuille
    Private Sub Sauve_Ifremer_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        '==============================================
        ' Changement visuel de sélection de bouton
        Button3.Select()
        '==============================================
        ' Demande d'ouverture par Fondu
        Me.Opacity = 0
        Timer1.Enabled = True
    End Sub

    ' ==============================================================================================================================================
    ' Procédure de fermeture de la feuille
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        '==============================================
        ' Changement visuel de sélection de bouton
        Button3.Select()
        '==============================================
        Fermeture()
    End Sub

    ' ==============================================================================================================================================
    ' Procédure de validation
    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        '==============================================
        ' Changement visuel de sélection de bouton
        Button3.Select()
        ' ==================================
        ' Initialisation des variables
        Dim InfoFichier As FileInfo
        Dim NomFichier As StreamWriter
        Dim LigneParamètre As String
        Dim i As Integer
        ' ==================================
        ' Définition des variables
        Dim NuméroLigne As Integer = -1
        Dim NomColonne As String = ""
        Dim j As Integer = 0
        Dim k As Integer = 0
        Dim Sandre As Integer = 0
        Dim NombreIndex As Integer = 0
        Dim NombreParamètre As Integer = 0
        Dim PremierParam As Integer = 0
        Dim IndexTabInfo As Integer = 0
        Dim CodeSandreLieuSurveillance As String = ""
        Dim CodeSandreRéseau As String = ""
        Dim CodeSandreSaisisseur As String = ""
        Dim CodeSandreUnitéSonde As String = ""
        Dim CodeSandrePositionnementPassage As String = ""
        Dim CodeSandreNiveauPrélèvement As String = ""
        Dim CodeSandrePréleveur As String = ""
        Dim CodeSandreUnitéImmersion As String = ""
        Dim CodeSandreUnitéTaillePrélèvement As String = ""
        Dim CodeSandrePositionnementPrélèvement As String = ""
        Dim CodeSandreSupportÉchantillon As String = ""
        Dim CodeSandreGroupeTaxon As String = ""
        Dim CodeSandreUnitéTailleÉchantillon As String = ""
        Dim CodeSandre As String = ""
        Dim EnginPrélèvement As Integer = 0
        Dim EntréeSupport As Integer = 0
        Dim Fraction As Integer = 0
        Dim Méthode As Integer = 0
        Dim Unité As Integer = 0
        Dim Analyste As Integer = 0
        Dim EnginAnalyse As Integer = 0
        Dim CodeRemarque As Integer = 0
        '        Dim Précision As Integer = 0
        Dim TypePrécision As Integer = 0
        Dim Information As String
        Dim NiveauSaisie As Integer = 0

        ' ==================================
        ' Traitement des erreurs
        On Error GoTo TraitErreur1
        ' ==================================
        ' Vérification si la sélection d'un nom de fichier a été réalisée
        If TextBox1.Text = "" Then
            MsgBox("Vous n'avez pas défini un nom de fichier !", MsgBoxStyle.Exclamation, " Absence de sélection")
            Exit Sub
        End If
        ' ==================================
        ' Vérification si le fichier existe déjà
        ' ==================================
        ' Récupération du nom du fichier
        InfoFichier = New FileInfo(Variables.Repertoire & "\Ifremer\" & TextBox1.Text & ".csv")
        ' ==================================
        ' Vérification de l'existence du fichier
        If InfoFichier.Exists = True Then
            i = MsgBox("          Le fichier existe déjà !" & vbCrLf & vbCrLf & "  Confirmez-vous la sauvegarde ? ", MsgBoxStyle.Exclamation + MsgBoxStyle.YesNo + MsgBoxStyle.DefaultButton2, " QUADRISPEL - Demande de confirmation")
            If i = 7 Then
                Exit Sub
            End If
        End If

        ' ==================================
        ' Recherche du Code Sandre du LIEU DE SURVEILLANCE
        ' ==================================
        ' Recherche du point d'entrée dans le tableau des informations pour les Lieux de Surveillance
        For i = 0 To 5000
            If Variables.TabRéfInfo(i, 0) = "LIEU DE SURVEILLANCE" Then
                IndexTabInfo = i + 1
                Exit For
            End If
        Next
        ' ==================================
        ' Recherche du Code Sandre 
        For i = IndexTabInfo To 5000
            ' ==================================
            ' Sortie de la boucle si fin de tableau
            If Variables.TabRéfInfo(i, 0) = "" Then Exit For
            ' ==================================
            If Variables.TabRéfInfo(i, 1) = Passage.TextBox24.Text Then
                ' ==================================
                ' Récupération du Code Sandre
                CodeSandreLieuSurveillance = Variables.TabRéfInfo(i, 2)
                Exit For
            End If
        Next

        ' ==================================
        ' Recherche du Code Sandre du RÉSEAU
        ' ==================================
        ' Recherche du point d'entrée dans le tableau des informations pour les Lieux de Surveillance
        For i = 0 To 5000
            If Variables.TabRéfInfo(i, 0) = "RÉSEAU" Then
                IndexTabInfo = i + 1
                Exit For
            End If
        Next
        ' ==================================
        ' Recherche du Code Sandre 
        For i = IndexTabInfo To 5000
            ' ==================================
            ' Sortie de la boucle si fin de tableau
            If Variables.TabRéfInfo(i, 0) = "" Then Exit For
            ' ==================================
            If Variables.TabRéfInfo(i, 0) = Passage.TextBox3.Text Then
                ' ==================================
                ' Récupération du Code Sandre
                CodeSandreRéseau = Variables.TabRéfInfo(i, 1)
                Exit For
            End If
        Next

        ' ==================================
        ' Recherche du Code Sandre du SAISISSEUR
        ' ==================================
        ' Recherche du point d'entrée dans le tableau des informations pour les Lieux de Surveillance
        For i = 0 To 5000
            If Variables.TabRéfInfo(i, 0) = "SAISISSEUR" Then
                IndexTabInfo = i + 1
                Exit For
            End If
        Next
        ' ==================================
        ' Recherche du Code Sandre 
        For i = IndexTabInfo To 5000
            ' ==================================
            ' Sortie de la boucle si fin de tableau
            If Variables.TabRéfInfo(i, 0) = "" Then Exit For
            ' ==================================
            If Variables.TabRéfInfo(i, 0) = Passage.TextBox23.Text Then
                ' ==================================
                ' Récupération du Code Sandre
                CodeSandreSaisisseur = Variables.TabRéfInfo(i, 2)
                Exit For
            End If
        Next

        ' ==================================
        ' Recherche du Code Sandre UNITÉ SONDE
        ' ==================================
        ' Recherche du point d'entrée dans le tableau des informations pour les Lieux de Surveillance
        For i = 0 To 5000
            If Variables.TabRéfInfo(i, 0) = "UNITÉ SONDE" Then
                IndexTabInfo = i + 1
                Exit For
            End If
        Next
        ' ==================================
        ' Recherche du Code Sandre 
        For i = IndexTabInfo To 5000
            ' ==================================
            ' Sortie de la boucle si fin de tableau
            If Variables.TabRéfInfo(i, 0) = "" Then Exit For
            ' ==================================
            If Variables.TabRéfInfo(i, 0) = Passage.TextBox15.Text Then
                ' ==================================
                ' Récupération du Code Sandre
                CodeSandreUnitéSonde = Variables.TabRéfInfo(i, 1)
                Exit For
            End If
        Next

        ' ==================================
        ' Recherche du Code Sandre POSITIONNEMENT PASSAGE
        ' ==================================
        ' Recherche du point d'entrée dans le tableau des informations pour les Lieux de Surveillance
        For i = 0 To 5000
            If Variables.TabRéfInfo(i, 0) = "POSITIONNEMENT PASSAGE" Then
                IndexTabInfo = i + 1
                Exit For
            End If
        Next
        ' ==================================
        ' Recherche du Code Sandre 
        For i = IndexTabInfo To 5000
            ' ==================================
            ' Sortie de la boucle si fin de tableau
            If Variables.TabRéfInfo(i, 0) = "" Then Exit For
            ' ==================================
            If Variables.TabRéfInfo(i, 0) = Passage.TextBox19.Text Then
                ' ==================================
                ' Récupération du Code Sandre
                CodeSandrePositionnementPassage = Variables.TabRéfInfo(i, 1)
                Exit For
            End If
        Next

        ' ==================================
        ' Recherche du Code Sandre NIVEAU PRÉLÈVEMENT
        ' ==================================
        ' Recherche du point d'entrée dans le tableau des informations pour les Lieux de Surveillance
        For i = 0 To 5000
            If Variables.TabRéfInfo(i, 0) = "NIVEAU PRÉLÈVEMENT" Then
                IndexTabInfo = i + 1
                Exit For
            End If
        Next
        ' ==================================
        ' Recherche du Code Sandre 
        For i = IndexTabInfo To 5000
            ' ==================================
            ' Sortie de la boucle si fin de tableau
            If Variables.TabRéfInfo(i, 0) = "" Then Exit For
            ' ==================================
            If Variables.TabRéfInfo(i, 0) = Prélèvement.TextBox1.Text Then
                ' ==================================
                ' Récupération du Code Sandre
                CodeSandreNiveauPrélèvement = Variables.TabRéfInfo(i, 1)
                Exit For
            End If
        Next

        ' ==================================
        ' Recherche du Code Sandre PRÉLEVEUR
        ' ==================================
        ' Recherche du point d'entrée dans le tableau des informations pour les Lieux de Surveillance
        For i = 0 To 5000
            If Variables.TabRéfInfo(i, 0) = "PRÉLEVEUR" Then
                IndexTabInfo = i + 1
                Exit For
            End If
        Next
        ' ==================================
        ' Recherche du Code Sandre 
        For i = IndexTabInfo To 5000
            ' ==================================
            ' Sortie de la boucle si fin de tableau
            If Variables.TabRéfInfo(i, 0) = "" Then Exit For
            ' ==================================
            If Variables.TabRéfInfo(i, 0) = Prélèvement.TextBox23.Text Then
                ' ==================================
                ' Récupération du Code Sandre
                CodeSandrePréleveur = Variables.TabRéfInfo(i, 2)
                Exit For
            End If
        Next

        ' ==================================
        ' Recherche du Code Sandre UNITÉ IMMERSION
        ' ==================================
        ' Recherche du point d'entrée dans le tableau des informations pour les Lieux de Surveillance
        For i = 0 To 5000
            If Variables.TabRéfInfo(i, 0) = "UNITÉ IMMERSION" Then
                IndexTabInfo = i + 1
                Exit For
            End If
        Next
        ' ==================================
        ' Recherche du Code Sandre 
        For i = IndexTabInfo To 5000
            ' ==================================
            ' Sortie de la boucle si fin de tableau
            If Variables.TabRéfInfo(i, 0) = "" Then Exit For
            ' ==================================
            If Variables.TabRéfInfo(i, 0) = Prélèvement.TextBox7.Text Then
                ' ==================================
                ' Récupération du Code Sandre
                CodeSandreUnitéImmersion = Variables.TabRéfInfo(i, 1)
                Exit For
            End If
        Next

        ' ==================================
        ' Recherche du Code Sandre UNITÉ TAILLE PRÉLÈVEMENT
        ' ==================================
        ' Recherche du point d'entrée dans le tableau des informations pour les Lieux de Surveillance
        For i = 0 To 5000
            If Variables.TabRéfInfo(i, 0) = "UNITÉ TAILLE PRÉLÈVEMENT" Then
                IndexTabInfo = i + 1
                Exit For
            End If
        Next
        ' ==================================
        ' Recherche du Code Sandre 
        For i = IndexTabInfo To 5000
            ' ==================================
            ' Sortie de la boucle si fin de tableau
            If Variables.TabRéfInfo(i, 0) = "" Then Exit For
            ' ==================================
            If Variables.TabRéfInfo(i, 0) = Prélèvement.TextBox10.Text Then
                ' ==================================
                ' Récupération du Code Sandre
                CodeSandreUnitéTaillePrélèvement = Variables.TabRéfInfo(i, 1)
                Exit For
            End If
        Next

        ' ==================================
        ' Recherche du Code Sandre POSITIONNEMENT PRÉLÈVEMENT
        ' ==================================
        ' Recherche du point d'entrée dans le tableau des informations pour les Lieux de Surveillance
        For i = 0 To 5000
            If Variables.TabRéfInfo(i, 0) = "POSITIONNEMENT PRÉLÈVEMENT" Then
                IndexTabInfo = i + 1
                Exit For
            End If
        Next
        ' ==================================
        ' Recherche du Code Sandre 
        For i = IndexTabInfo To 5000
            ' ==================================
            ' Sortie de la boucle si fin de tableau
            If Variables.TabRéfInfo(i, 0) = "" Then Exit For
            ' ==================================
            If Variables.TabRéfInfo(i, 0) = Prélèvement.TextBox16.Text Then
                ' ==================================
                ' Récupération du Code Sandre
                CodeSandrePositionnementPrélèvement = Variables.TabRéfInfo(i, 1)
                Exit For
            End If
        Next

        ' ==================================
        ' Recherche du Code Sandre SUPPORT ÉCHANTILLON
        ' ==================================
        ' Recherche du point d'entrée dans le tableau des informations pour les Lieux de Surveillance
        For i = 0 To 5000
            If Variables.TabRéfInfo(i, 0) = "SUPPORT ÉCHANTILLON" Then
                IndexTabInfo = i + 1
                Exit For
            End If
        Next
        ' ==================================
        ' Recherche du Code Sandre 
        For i = IndexTabInfo To 5000
            ' ==================================
            ' Sortie de la boucle si fin de tableau
            If Variables.TabRéfInfo(i, 0) = "" Then Exit For
            ' ==================================
            If Variables.TabRéfInfo(i, 0) = Échantillon.TextBox2.Text Then
                ' ==================================
                ' Récupération du Code Sandre
                CodeSandreSupportÉchantillon = Variables.TabRéfInfo(i, 1)
                Exit For
            End If
        Next

        ' ==================================
        ' Recherche du Code Sandre GROUPE TAXON SUPPORT ÉCHANTILLON
        ' ==================================
        ' Recherche du point d'entrée dans le tableau des informations pour les Lieux de Surveillance
        For i = 0 To 5000
            If Variables.TabRéfInfo(i, 0) = "GROUPE TAXON SUPPORT ÉCHANTILLON" Then
                IndexTabInfo = i + 1
                Exit For
            End If
        Next
        ' ==================================
        ' Recherche du Code Sandre 
        For i = IndexTabInfo To 5000
            ' ==================================
            ' Sortie de la boucle si fin de tableau
            If Variables.TabRéfInfo(i, 0) = "" Then Exit For
            ' ==================================
            If Variables.TabRéfInfo(i, 0) = Échantillon.TextBox4.Text Then
                ' ==================================
                ' Récupération du Code Sandre
                CodeSandreGroupeTaxon = Variables.TabRéfInfo(i, 1)
                Exit For
            End If
        Next

        ' ==================================
        ' Recherche du Code Sandre UNITÉ TAILLE ÉCHANTILLON
        ' ==================================
        ' Recherche du point d'entrée dans le tableau des informations pour les Lieux de Surveillance
        For i = 0 To 5000
            If Variables.TabRéfInfo(i, 0) = "UNITÉ TAILLE ÉCHANTILLON" Then
                IndexTabInfo = i + 1
                Exit For
            End If
        Next
        ' ==================================
        ' Recherche du Code Sandre 
        For i = IndexTabInfo To 5000
            ' ==================================
            ' Sortie de la boucle si fin de tableau
            If Variables.TabRéfInfo(i, 0) = "" Then Exit For
            ' ==================================
            If Variables.TabRéfInfo(i, 0) = Échantillon.TextBox7.Text Then
                ' ==================================
                ' Récupération du Code Sandre
                CodeSandreUnitéTailleÉchantillon = Variables.TabRéfInfo(i, 1)
                Exit For
            End If
        Next

        ' ==================================
        ' Recherche du point d'entrée NIVEAU SAISIE dans le tableau des informations
        ' ==================================
        For i = 0 To 5000
            ' ==================================
            If Variables.TabRéfInfo(i, 0) = "NIVEAU SAISIE" Then
                NiveauSaisie = i + 1
                Exit For
            End If
        Next

        ' ==================================
        ' Recherche du point d'entrée ENGIN DE PRÉLÈVEMENT dans le tableau des informations
        ' ==================================
        For i = 0 To 5000
            ' ==================================
            If Variables.TabRéfInfo(i, 0) = "ENGIN PRÉLÈVEMENT" Then
                EnginPrélèvement = i + 1
                Exit For
            End If
        Next

        ' ==================================
        ' Recherche du point d'entrée SUPPORT dans le tableau des informations
        ' ==================================
        For i = 0 To 5000
            ' ==================================
            If Variables.TabRéfInfo(i, 0) = "SUPPORT" Then
                EntréeSupport = i + 1
                Exit For
            End If
        Next

        ' ==================================
        ' Recherche du point d'entrée FRACTION dans le tableau des informations
        ' ==================================
        For i = 0 To 5000
            ' ==================================
            If Variables.TabRéfInfo(i, 0) = "FRACTION" Then
                Fraction = i + 1
                Exit For
            End If
        Next

        ' ==================================
        ' Recherche du point d'entrée MÉTHODE dans le tableau des informations
        ' ==================================
        For i = 0 To 5000
            ' ==================================
            If Variables.TabRéfInfo(i, 0) = "MÉTHODE" Then
                Méthode = i + 1
                Exit For
            End If
        Next

        ' ==================================
        ' Recherche du point d'entrée UNITÉ dans le tableau des informations
        ' ==================================
        For i = 0 To 5000
            ' ==================================
            If Variables.TabRéfInfo(i, 0) = "UNITÉ" Then
                Unité = i + 1
                Exit For
            End If
        Next

        ' ==================================
        ' Recherche du point d'entrée ANALYSEUR dans le tableau des informations
        ' ==================================
        For i = 0 To 5000
            ' ==================================
            If Variables.TabRéfInfo(i, 0) = "ANALYSEUR" Then
                Analyste = i + 1
                Exit For
            End If
        Next

        ' ==================================
        ' Recherche du point d'entrée ENGIN ANALYSE dans le tableau des informations
        ' ==================================
        For i = 0 To 5000
            ' ==================================
            If Variables.TabRéfInfo(i, 0) = "ENGIN ANALYSE" Then
                EnginAnalyse = i + 1
                Exit For
            End If
        Next

        ' ==================================
        ' Recherche du point d'entrée PRÉCISION dans le tableau des informations
        ' ==================================
        For i = 0 To 5000
            ' ==================================
            If Variables.TabRéfInfo(i, 0) = "CODE REMARQUE" Then
                CodeRemarque = i + 1
                Exit For
            End If
        Next

        ' ==================================
        ' Recherche du point d'entrée PRÉCISION dans le tableau des informations
        ' ==================================
        '        For i = 0 To 5000
        ' ==================================
        '        If Variables.TabRéfInfo(i, 0) = "PRÉCISION" Then
        '        Précision = i + 1
        '        Exit For
        '        End If
        '        Next

        ' ==================================
        ' Recherche du point d'entrée TYPE PRÉCISION dans le tableau des informations
        ' ==================================
        For i = 0 To 5000
            ' ==================================
            If Variables.TabRéfInfo(i, 0) = "TYPE PRÉCISION" Then
                TypePrécision = i + 1
                Exit For
            End If
        Next



        ' ==================================
        ' Ouverture du fichier de sauvegarde
        ' ==================================
        NomFichier = New StreamWriter(Variables.Repertoire & "\Ifremer\" & TextBox1.Text & ".csv", False, System.Text.Encoding.UTF8)
        ' ==================================
        ' Écriture de la ligne des titres
        '        NomFichier.WriteLine("NUMERO_LIGNE" & Chr(9) & "CODE LIEU DE SURVEILLANCE" & Chr(9) & "CODE RESEAU" & Chr(9) & "CODE SANDRE SAISISSEUR" & Chr(9) & "ZONE DESTINATION DRAGAGE" & Chr(9) & "CAMPAGNE" & Chr(9) & "SORTIE" & Chr(9) & "DATE PRLVMT" & Chr(9) & "HEURE PASSAGE" & Chr(9) & "SONDE" & Chr(9) & "UNITE SONDE" & Chr(9) & "MNEMONIQUE PASSAGE" & Chr(9) & "COMMENTAIRES PASSAGE" & Chr(9) & "LATITUDE PASSAGE" & Chr(9) & "LONGITUDE PASSAGE" & Chr(9) & "POSITIONNEMENT PASSAGE" & Chr(9) & "NOMBRE INDIVIDU PASSAGE" & Chr(9) & "ENGIN PRLVMT" & Chr(9) & "NIVEAU PRLVMT" & Chr(9) & "CODE SANDRE PRELEVEUR" & Chr(9) & "MNEMONIQUE PRLVMT" & Chr(9) & "IMMERSION PRLVMT" & Chr(9) & "IMMERSION MAX PRLVMT" & Chr(9) & "IMMERSION MIN PRLVMT" & Chr(9) & "UNITE IMMERSION" & Chr(9) & "TAILLE PRLVMT" & Chr(9) & "UNITE TAILLE PRLVMT" & Chr(9) & "HEURE PRLVMT" & Chr(9) & "COMMENTAIRES_PRELEVEMENT" & Chr(9) & "LATITUDE_PRELEVEMENT" & Chr(9) & "LONGITUDE_PRELEVEMENT" & Chr(9) & "POSITIONNEMENT_PRELEVEMENT" & Chr(9) & "NOMBRE_INDIVIDU_PRLVMT" & Chr(9) & "LOT AQUACOLE" & Chr(9) & "CODE SANDRE SUPPORT_ECHANTILLON" & Chr(9) & "TAXON_SUPPORT_ECHANTILLON" & Chr(9) & "GROUPE_TAXON_SUPPORT_ECHANTILLON" & Chr(9) & "NUM ECHANTI" & Chr(9) & "TAILLE_ECHANTILLON" & Chr(9) & "UNITE_TAILLE_ECHANTILLON" & Chr(9) & "COMMENTAIRE_ECHANTILLON" & Chr(9) & "NOMBRE_INDIVIDU_ECHANTILLON" & Chr(9) & "NIVEAU_SAISIE_RESULTAT" & Chr(9) & "CODE SANDRE PARAMETRE" & Chr(9) & "LIBELLE SANDRE" & Chr(9) & "CODE SANDRE SUPPORT" & Chr(9) & "CODE SANDRE FRACTION" & Chr(9) & "CODE SANDRE METHOD" & Chr(9) & "NUMERO_INDIVIDU" & Chr(9) & "RESULTAT" & Chr(9) & "RESULTAT_QUALITATIF" & Chr(9) & "CODE SANDRE UNITE" & Chr(9) & "CODE SANDRE ANALYSTE" & Chr(9) & "ENGIN_ANALYSE" & Chr(9) & "CODE REMARQUE" & Chr(9) & "PRECISION" & Chr(9) & "TYPE_PRECISION" & Chr(9) & "COMMENTAIRE")
        '       NomFichier.WriteLine("NUMERO_LIGNE;CODE LIEU DE SURVEILLANCE;CODE RESEAU;CODE SANDRE SAISISSEUR;ZONE DESTINATION DRAGAGE;CAMPAGNE;SORTIE;DATE PRLVMT;HEURE PASSAGE;SONDE;UNITE SONDE;MNEMONIQUE PASSAGE;COMMENTAIRES PASSAGE;LATITUDE PASSAGE;LONGITUDE PASSAGE;POSITIONNEMENT PASSAGE;NOMBRE INDIVIDU PASSAGE;ENGIN PRLVMT;NIVEAU PRLVMT;CODE SANDRE PRELEVEUR;MNEMONIQUE PRLVMT;IMMERSION PRLVMT;IMMERSION MAX PRLVMT;IMMERSION MIN PRLVMT;UNITE IMMERSION;TAILLE PRLVMT;UNITE TAILLE PRLVMT;HEURE PRLVMT;COMMENTAIRES_PRELEVEMENT;LATITUDE_PRELEVEMENT;LONGITUDE_PRELEVEMENT;POSITIONNEMENT_PRELEVEMENT;NOMBRE_INDIVIDU_PRLVMT;LOT AQUACOLE;CODE SANDRE SUPPORT_ECHANTILLON;TAXON_SUPPORT_ECHANTILLON;GROUPE_TAXON_SUPPORT_ECHANTILLON;NUM ECHANTI;TAILLE_ECHANTILLON;UNITE_TAILLE_ECHANTILLON;COMMENTAIRE_ECHANTILLON;NOMBRE_INDIVIDU_ECHANTILLON;NIVEAU_SAISIE_RESULTAT;CODE SANDRE PARAMETRE;LIBELLE SANDRE;CODE SANDRE SUPPORT;CODE SANDRE FRACTION;CODE SANDRE METHOD;NUMERO_INDIVIDU;RESULTAT;RESULTAT_QUALITATIF;CODE SANDRE UNITE;CODE SANDRE ANALYSTE;ENGIN_ANALYSE;CODE REMARQUE;PRECISION;TYPE_PRECISION;COMMENTAIRE")

        ' Modification 1.6 Ifremer
        ' NomFichier.WriteLine("NUMERO_LIGNE;CODE LIEU DE SURVEILLANCE;CODE RESEAU;CODE SANDRE SAISISSEUR;ZONE DESTINATION DRAGAGE;CAMPAGNE;SORTIE;DATE PRLVMT;HEURE PASSAGE;SONDE;UNITE SONDE;MNEMONIQUE PASSAGE;COMMENTAIRES PASSAGE;LATITUDE PASSAGE;LONGITUDE PASSAGE;POSITIONNEMENT PASSAGE;NOMBRE INDIVIDU PASSAGE;ENGIN PRLVMT;NIVEAU PRLVMT;CODE SANDRE PRELEVEUR;MNEMONIQUE PRLVMT;IMMERSION PRLVMT;IMMERSION MAX PRLVMT;IMMERSION MIN PRLVMT;UNITE IMMERSION;TAILLE PRLVMT;UNITE TAILLE PRLVMT;HEURE PRLVMT;COMMENTAIRES_PRELEVEMENT;LATITUDE_PRELEVEMENT;LONGITUDE_PRELEVEMENT;POSITIONNEMENT_PRELEVEMENT;NOMBRE_INDIVIDU_PRLVMT;LOT AQUACOLE;CODE SANDRE SUPPORT_ECHANTILLON;TAXON_SUPPORT_ECHANTILLON;GROUPE_TAXON_SUPPORT_ECHANTILLON;NUM ECHANTI;TAILLE_ECHANTILLON;UNITE_TAILLE_ECHANTILLON;COMMENTAIRE_ECHANTILLON;NOMBRE_INDIVIDU_ECHANTILLON;NIVEAU_SAISIE_RESULTAT;CODE SANDRE PARAMETRE;LIBELLE SANDRE;CODE SANDRE SUPPORT;CODE SANDRE FRACTION;CODE SANDRE METHOD;NUMERO_INDIVIDU;         RESULTAT Numérique ; libellé RESULTAT_QUALITATIF;     Code Sandre Résultat Qualitatif ; Code Groupe taxon Résultat  ;  Code Taxon Résultat  ;             CODE SANDRE UNITE;CODE SANDRE ANALYSTE;ENGIN_ANALYSE;CODE REMARQUE;PRECISION;TYPE_PRECISION;COMMENTAIRE")
        NomFichier.WriteLine("NUMERO_LIGNE;CODE_LIEU_SURVEILLANCE;CODE_PROGRAMME;CODE_SANDRE_SAISISSEUR;ZONE_DESTINATION_DRAGAGE;CAMPAGNE;SORTIE;DATE_PASSAGE;HEURE_PASSAGE;SONDE;UNITE_SONDE;MNEMONIQUE_PASSAGE;COMMENTAIRES_PASSAGE;LATITUDE_PASSAGE;LONGITUDE_PASSAGE;POSITIONNEMENT_PASSAGE;NOMBRE_INDIVIDU_PASSAGE;CODE_SANDRE_ENGIN_PRELEVEMENT;CODE_SANDRE_NIVEAU_PRELEVEMENT;CODE_SANDRE_PRELEVEUR;MNEMONIQUE_PRELEVEMENT;IMMERSION_PRELEVEMENT;IMMERSION_MAX_PRELEVEMENT;IMMERSION_MIN_PRELEVEMENT;CODE_SANDRE_UNITE_IMMERSION;TAILLE_PRELEVEMENT;CODE_SANDRE_UNITE_TAILLE_PRELEVEMENT;HEURE_PRELEVEMENT;COMMENTAIRES_PRELEVEMENT;LATITUDE_PRELEVEMENT;LONGITUDE_PRELEVEMENT;POSITIONNEMENT_PRELEVEMENT;NOMBRE_INDIVIDU_PRELEVEMENT;LOT_AQUACOLE;CODE_SANDRE_SUPPORT_ECHANTILLON;CODE_SANDRE_TAXON_SUPPORT_ECHANTILLON;CODE_SANDRE_GROUPE_TAXON_SUPPORT_ECHANTILLON;MNEMONIQUE_ECHANTILLON;TAILLE_ECHANTILLON;CODE_SANDRE_UNITE_TAILLE_ECHANTILLON;COMMENTAIRES_ECHANTILLON;NOMBRE_INDIVIDU_ECHANTILLON;NIVEAU_SAISIE_RESULTAT;CODE_SANDRE_PARAMETRE;LIBELLE_SANDRE_PARAMETRE;CODE_SANDRE_SUPPORT;CODE_SANDRE_FRACTION;CODE_SANDRE_METHODE;NUMERO_INDIVIDU;CODE_SANDRE_TAXON_RESULTAT;CODE_SANDRE_GROUPE_TAXON_RESULTAT;RESULTAT_NUMERIQUE;RESULTAT_QUALITATIF_CODE_SANDRE;RESULTAT_QUALITATIF_LIBELLE_SANDRE;CODE_SANDRE_UNITE;CODE_SANDRE_ANALYSTE;CODE_SANDRE_ENGIN_ANALYSE;CODE_SANDRE_REMARQUE;PRECISION;TYPE_PRECISION;COMMENTAIRES_RESULTAT")


        ' ==================================
        ' Balayage des noms des Familles sélectionnées dans la feuille des Familles
        For i = 0 To Familles.ListBox2.Items.Count - 1
            ' ==================================
            ' Recherche de la première adresse de la Famille sélectionnée
            For j = 0 To 500
                If LTrim(Familles.ListBox2.Items(i)) = Variables.TabRéfPara(j, 0) Then
                    PremierParam = j
                    Exit For
                End If
            Next
            ' ==================================
            ' Initialisation des indicateurs
            NombreIndex = 0
            NombreParamètre = 0
            LigneParamètre = ""
            ' ==================================
            ' Recherche du nombre de Paramètres et du nombre de Paramètres indexés pour la Famille sélectionnée
            For j = PremierParam To 500
                ' ==================================
                ' Sortie si tous les Paramètres de la Famille sélectionnée ont été balayés
                If LTrim(Familles.ListBox2.Items(i)) <> Variables.TabRéfPara(j, 0) Then
                    Exit For
                End If
                ' ==================================
                ' Incrémentation de l'indicateur du nombre de Paramètres indexés pour la Famille sélectionnée
                If Val(Variables.TabRéfPara(j, 21)) <> 0 Then
                    NombreIndex = NombreIndex + 1
                End If
                ' ==================================
                ' Incrémentation de l'indicateur du nombre de Paramètres pour la Famille sélectionnée
                NombreParamètre = NombreParamètre + 1
            Next
            ' ==================================
            ' Balayage des Paramètres pour le transfert des informations du paramètre sélectionné de la famille sélectionnée dans la Feuille des Paramètres
            For k = 1 To NombreIndex
                ' ==================================
                ' Balayage des index pour le transfert des informations du paramètre sélectionné de la famille sélectionnée dans la Feuille des Paramètres
                For j = PremierParam To PremierParam + NombreParamètre - 1
                    ' ==================================
                    ' Recherche du Paramètre dont l'index correspond à l'index recherché
                    If Variables.TabRéfPara(j, 21) = k Then
                        ' ==================================
                        ' Incrémentation du pointeur de ligne
                        NuméroLigne = NuméroLigne + 1
                        ' ==================================
                        ' Numéro Ligne 1
                        NomColonne = "ColNumLigne" : LigneParamètre = NuméroLigne + 1 & ";" '   Chr(9)
                        ' ==================================
                        ' Code Sandre Lieu de Surveillance 2
                        NomColonne = "ColLieuSurv" : LigneParamètre = LigneParamètre & CodeSandreLieuSurveillance & ";" '   Chr(9)
                        ' ==================================
                        ' Code Sandre Réseau 3
                        NomColonne = "ColRéseau" : LigneParamètre = LigneParamètre & CodeSandreRéseau & ";" '   Chr(9)
                        ' ==================================
                        ' Code Sandre Saisisseur 4
                        NomColonne = "ColSaissi" : LigneParamètre = LigneParamètre & CodeSandreSaisisseur & ";" '   Chr(9)
                        ' ==================================
                        ' Zone Destination Dragage 5
                        NomColonne = "ColZoneDestDrag" : LigneParamètre = LigneParamètre & Passage.TextBox5.Text & ";" '   Chr(9)
                        ' ==================================
                        ' Campagne 6
                        NomColonne = "ColCampagne" : LigneParamètre = LigneParamètre & Passage.TextBox7.Text & ";" '   Chr(9)
                        ' ==================================
                        ' Sortie 7
                        NomColonne = "ColSortie" : LigneParamètre = LigneParamètre & Passage.TextBox9.Text & ";" '   Chr(9)
                        ' ==================================
                        ' Date Passage 8
                        NomColonne = "ColDatePassage" : LigneParamètre = LigneParamètre & Passage.TextBox11.Text & ";" '   Chr(9)
                        ' ==================================
                        ' Heure Passage 9
                        NomColonne = "ColHeurePassage" : LigneParamètre = LigneParamètre & Passage.TextBox12.Text & ";" '   Chr(9)
                        ' ==================================
                        ' Sonde 10
                        NomColonne = "ColSonde" : LigneParamètre = LigneParamètre & Passage.TextBox14.Text & ";" '   Chr(9)
                        ' ==================================
                        ' Code Sandre Unité Sonde 11
                        NomColonne = "ColUnitéSonde" : LigneParamètre = LigneParamètre & CodeSandreUnitéSonde & ";" '   Chr(9)
                        ' ==================================
                        ' Mnémonique Passage 12
                        NomColonne = "ColMnémoPassage" : LigneParamètre = LigneParamètre & Passage.TextBox17.Text & ";" '   Chr(9)
                        ' ==================================
                        ' Commentaires Passage 13
                        NomColonne = "ColCommentPassage" : LigneParamètre = LigneParamètre & Passage.TextBox18.Text & ";" '   Chr(9)
                        ' ==================================
                        ' Latitude Passage 14
                        NomColonne = "ColLatitudePass" : LigneParamètre = LigneParamètre & Passage.TextBox1.Text & ";" '   Chr(9)
                        ' ==================================
                        ' Longitude Passage 15
                        NomColonne = "ColLongitudePass" : LigneParamètre = LigneParamètre & Passage.TextBox2.Text & ";" '   Chr(9)
                        ' ==================================
                        ' Code Sandre Positionnement Passage 16
                        NomColonne = "ColPosition" : LigneParamètre = LigneParamètre & CodeSandrePositionnementPassage & ";" '   Chr(9)
                        ' ==================================
                        ' Nombre Individu Passage 17
                        NomColonne = "ColNombrePass" : LigneParamètre = LigneParamètre & Passage.TextBox20.Text & ";" '   Chr(9)
                        ' ==================================
                        ' Code Sandre Engin Prélèvement 18
                        ' ==================================
                        ' Recherche du Code Sandre correspondant
                        Information = ""
                        For Sandre = EnginPrélèvement To 5000
                            ' ==================================
                            ' Sortie de la boucle si fin de tableau
                            If Variables.TabRéfInfo(Sandre, 0) = "" Then Exit For
                            ' ==================================
                            If Variables.TabRéfInfo(Sandre, 0) = Variables.TabRéfPara(j, 10) Then
                                ' ==================================
                                ' Récupération du Code Sandre
                                Information = Variables.TabRéfInfo(Sandre, 1)
                                Exit For
                            End If
                        Next
                        NomColonne = "ColEnginPrlvmt" : LigneParamètre = LigneParamètre & Information & ";" '   Chr(9)
                        ' ==================================
                        ' Code Sandre Niveau Prélèvement 19
                        NomColonne = "ColNiveauPrlvmt" : LigneParamètre = LigneParamètre & CodeSandreNiveauPrélèvement & ";" '   Chr(9)
                        ' ==================================
                        ' Code Sandre Préleveur 20
                        NomColonne = "ColPréleveur" : LigneParamètre = LigneParamètre & CodeSandrePréleveur & ";" '   Chr(9)
                        ' ==================================
                        ' Mnémonique Prélèvement 21 
                        NomColonne = "ColMnémoPrlvmt" : LigneParamètre = LigneParamètre & Prélèvement.TextBox2.Text & ";" '   Chr(9)
                        ' ==================================
                        ' Immersion Prélèvement 22
                        NomColonne = "ColImmPrlvmt" : LigneParamètre = LigneParamètre & Prélèvement.TextBox3.Text & ";" '   Chr(9)
                        ' ==================================
                        ' Immersion Max Prélèvement 23
                        NomColonne = "ColImmMaxPrlvmt" : LigneParamètre = LigneParamètre & Prélèvement.TextBox6.Text & ";" '   Chr(9)
                        ' ==================================
                        ' Immersion Min Prélèvement 24
                        NomColonne = "ColImmMinPrlvmt" : LigneParamètre = LigneParamètre & Prélèvement.TextBox5.Text & ";" '   Chr(9)
                        ' ==================================
                        ' Code Sandre Unité Immersion 25
                        NomColonne = "ColUnitéImm" : LigneParamètre = LigneParamètre & CodeSandreUnitéImmersion & ";" '   Chr(9)
                        ' ==================================
                        ' Taille Prélèvement 26
                        NomColonne = "ColTaillePrlvmt" : LigneParamètre = LigneParamètre & Prélèvement.TextBox9.Text & ";" '   Chr(9)
                        ' ==================================
                        ' Code Sandre Unité Taille Prélèvement 27
                        NomColonne = "ColUnitéPrlvmt" : LigneParamètre = LigneParamètre & CodeSandreUnitéTaillePrélèvement & ";" '   Chr(9)
                        ' ==================================
                        ' Heure Prélèvement 28
                        NomColonne = "ColHeurePrlvmt" : LigneParamètre = LigneParamètre & Prélèvement.TextBox11.Text & ";" '   Chr(9)
                        ' ==================================
                        ' Commentaires Prélèvement 29
                        NomColonne = "ColCommPrlvmt" : LigneParamètre = LigneParamètre & Prélèvement.TextBox12.Text & ";" '   Chr(9)
                        ' ==================================
                        ' Latitude Prélèvement 30
                        NomColonne = "ColLatitudePrlvmt" : LigneParamètre = LigneParamètre & Prélèvement.TextBox14.Text & ";" '   Chr(9)
                        ' ==================================
                        ' Longitude Prélèvement 31
                        NomColonne = "ColLongitudePrlvmt" : LigneParamètre = LigneParamètre & Prélèvement.TextBox15.Text & ";" '   Chr(9)
                        ' ==================================
                        ' Code Sandre Positionnement Prélèvement 32
                        NomColonne = "ColPosPrlvmt" : LigneParamètre = LigneParamètre & CodeSandrePositionnementPrélèvement & ";" '   Chr(9)
                        ' ==================================
                        ' Nombre Individu Prélèvement 33
                        NomColonne = "ColNbrIndPré" : LigneParamètre = LigneParamètre & Prélèvement.TextBox17.Text & ";" '   Chr(9)
                        ' ==================================
                        ' Lot Aquacole 34
                        NomColonne = "ColLotAquacole" : LigneParamètre = LigneParamètre & Échantillon.TextBox1.Text & ";" '   Chr(9)
                        ' ==================================
                        ' Code Sandre Support Échantillon 35
                        NomColonne = "ColSupportÉchant" : LigneParamètre = LigneParamètre & CodeSandreSupportÉchantillon & ";" '   Chr(9)
                        ' ==================================
                        ' Taxon Support Échantillon 36
                        NomColonne = "ColTaxonÉchant" : LigneParamètre = LigneParamètre & Échantillon.TextBox3.Text & ";" '   Chr(9)
                        ' ==================================
                        ' Code Sandre Groupe Taxon Support Échantillon 37
                        NomColonne = "ColGrpTaxÉchant" : LigneParamètre = LigneParamètre & CodeSandreGroupeTaxon & ";" '   Chr(9)
                        ' ==================================
                        ' Numéro Échantillon 38
                        NomColonne = "ColNumÉchant" : LigneParamètre = LigneParamètre & Échantillon.TextBox5.Text & ";" '   Chr(9)
                        ' ==================================
                        ' Taille Échantillon 39
                        NomColonne = "ColTailleÉchant" : LigneParamètre = LigneParamètre & Échantillon.TextBox6.Text & ";" '   Chr(9)
                        ' ==================================
                        ' Code Sandre Unité Taille Échantillon 40
                        NomColonne = "ColUnitéÉchant" : LigneParamètre = LigneParamètre & CodeSandreUnitéTailleÉchantillon & ";" '   Chr(9)
                        ' ==================================
                        ' Commentaire Échantillon 41
                        NomColonne = "ColCommÉchant" : LigneParamètre = LigneParamètre & Échantillon.TextBox9.Text & ";" '   Chr(9)
                        ' ==================================
                        ' Nombre Individu Échantillon 42 
                        NomColonne = "ColNombreÉchant" : LigneParamètre = LigneParamètre & Échantillon.TextBox8.Text & ";" '   Chr(9)
                        ' ==================================
                        ' Niveau Saisie Résultat 43
                        ' ==================================
                        ' Recherche du Code Sandre correspondant
                        Information = ""
                        For Sandre = NiveauSaisie To 5000
                            ' ==================================
                            ' Sortie de la boucle si fin de tableau
                            If Variables.TabRéfInfo(Sandre, 0) = "" Then Exit For
                            ' ==================================
                            If Variables.TabRéfInfo(Sandre, 0) = Variables.TabRéfPara(j, 13) Then
                                ' ==================================
                                ' Récupération du Code Sandre
                                Information = Variables.TabRéfInfo(Sandre, 1)
                                Exit For
                            End If
                        Next
                        NomColonne = "ColNivRés" : LigneParamètre = LigneParamètre & Information & ";" '   Chr(9)
                        ' ==================================
                        ' Code Sandre Paramètre 44
                        NomColonne = "ColCodeParam" : LigneParamètre = LigneParamètre & Variables.TabRéfPara(j, 3) & ";" '   Chr(9)
                        ' ==================================
                        ' Libellé Paramètre 45
                        NomColonne = "ColLibParam" : LigneParamètre = LigneParamètre & Variables.TabRéfPara(j, 1) & ";" '   Chr(9)
                        ' ==================================
                        ' Code Sandre Support 46
                        ' ==================================
                        ' Recherche du Code Sandre correspondant
                        Information = ""
                        For Sandre = EntréeSupport To 5000
                            ' ==================================
                            ' Sortie de la boucle si fin de tableau
                            If Variables.TabRéfInfo(Sandre, 0) = "" Then Exit For
                            ' ==================================
                            If Variables.TabRéfInfo(Sandre, 0) = Variables.TabRéfPara(j, 9) Then
                                ' ==================================
                                ' Récupération du Code Sandre
                                Information = Variables.TabRéfInfo(Sandre, 1)
                                Exit For
                            End If
                        Next
                        NomColonne = "ColCodeSupport" : LigneParamètre = LigneParamètre & Information & ";" '   Chr(9)
                        ' ==================================
                        ' Code Sandre Fraction 47
                        ' ==================================
                        ' Recherche du Code Sandre correspondant
                        Information = ""
                        For Sandre = Fraction To 5000
                            ' ==================================
                            ' Sortie de la boucle si fin de tableau
                            If Variables.TabRéfInfo(Sandre, 0) = "" Then Exit For
                            ' ==================================
                            If Variables.TabRéfInfo(Sandre, 0) = Variables.TabRéfPara(j, 8) Then
                                ' ==================================
                                ' Récupération du Code Sandre
                                Information = Variables.TabRéfInfo(Sandre, 1)
                                Exit For
                            End If
                        Next
                        NomColonne = "ColCodeFraction" : LigneParamètre = LigneParamètre & Information & ";" '   Chr(9)
                        ' ==================================
                        ' Code Sandre Méthode 48
                        ' ==================================
                        ' Recherche du Code Sandre correspondant
                        Information = ""
                        For Sandre = Méthode To 5000
                            ' ==================================
                            ' Sortie de la boucle si fin de tableau
                            If Variables.TabRéfInfo(Sandre, 0) = "" Then Exit For
                            ' ==================================
                            If Variables.TabRéfInfo(Sandre, 0) = Variables.TabRéfPara(j, 6) Then
                                ' ==================================
                                ' Récupération du Code Sandre
                                Information = Variables.TabRéfInfo(Sandre, 1)
                                Exit For
                            End If
                        Next
                        NomColonne = "ColCodeMéthode" : LigneParamètre = LigneParamètre & Information & ";" '   Chr(9)
                        ' ==================================
                        ' Numéro individu 49
                        NomColonne = "ColNumIndividu" : LigneParamètre = LigneParamètre & Variables.TabRéfPara(j, 16) & ";" '   Chr(9)

                        ' Modifications Version 1.6 Ifremer

                        '                       ' ==================================
                        '                       ' Résultat 50
                        '                       NomColonne = "ColRésultat" : LigneParamètre = LigneParamètre & Variables.TabRéfPara(j, 17) & ";" '   Chr(9)
                        '                       ' ==================================
                        '                       ' Résultat Qualitatif 51
                        '                       NomColonne = "ColRésQualitatif" : LigneParamètre = LigneParamètre & Variables.TabRéfPara(j, 15) & ";" '   Chr(9)



                        ' ==================================
                        ' Code Sandre Taxon Résultat
                        NomColonne = "ColCodeTaxRes" : LigneParamètre = LigneParamètre & Variables.TabRéfPara(j, 25) & ";" '   Chr(9)

                        ' ==================================
                        ' Code Sandre Groupe Taxon Résultat
                        NomColonne = "ColCodeGroTaxRes" : LigneParamètre = LigneParamètre & Variables.TabRéfPara(j, 24) & ";" '   Chr(9)

                        ' ==================================
                        ' Résultat Numérique 50
                        NomColonne = "ColRésultat" : LigneParamètre = LigneParamètre & Variables.TabRéfPara(j, 17) & ";" '   Chr(9)

                        ' ==================================
                        ' Code Sandre Résultat Qualitatif
                        NomColonne = "ColCodeResQua" : LigneParamètre = LigneParamètre & Variables.TabRéfPara(j, 23) & ";" '   Chr(9)

                        ' ==================================
                        ' Libellé Résultat Qualitatif 51
                        NomColonne = "ColRésQualitatif" : LigneParamètre = LigneParamètre & Variables.TabRéfPara(j, 15) & ";" '   Chr(9)




                        ' ==================================
                        ' Code Sandre Unité 52
                        ' ==================================
                        ' Recherche du Code Sandre correspondant
                        Information = ""
                        For Sandre = Unité To 5000
                            ' ==================================
                            ' Sortie de la boucle si fin de tableau
                            If Variables.TabRéfInfo(Sandre, 0) = "" Then Exit For
                            ' ==================================
                            If Variables.TabRéfInfo(Sandre, 0) = Variables.TabRéfPara(j, 18) Then
                                ' ==================================
                                ' Récupération du Code Sandre
                                Information = Variables.TabRéfInfo(Sandre, 1)
                                Exit For
                            End If
                        Next
                        NomColonne = "ColCodeUnité" : LigneParamètre = LigneParamètre & Information & ";" '   Chr(9)
                        ' ==================================
                        ' Code Sandre Analyste 53
                        ' ==================================
                        ' Recherche du Code Sandre correspondant
                        Information = ""
                        For Sandre = Analyste To 5000
                            ' ==================================
                            ' Sortie de la boucle si fin de tableau
                            If Variables.TabRéfInfo(Sandre, 0) = "" Then Exit For
                            ' ==================================
                            If Variables.TabRéfInfo(Sandre, 0) = Variables.TabRéfPara(j, 4) Then
                                ' ==================================
                                ' Récupération du Code Sandre
                                Information = Variables.TabRéfInfo(Sandre, 2)
                                Exit For
                            End If
                        Next
                        NomColonne = "ColAnalyste" : LigneParamètre = LigneParamètre & Information & ";" '   Chr(9)
                        ' ==================================
                        ' Code Sandre Engin Analyse 54
                        ' ==================================
                        ' Recherche du Code Sandre correspondant
                        Information = ""
                        For Sandre = EnginAnalyse To 5000
                            ' ==================================
                            ' Sortie de la boucle si fin de tableau
                            If Variables.TabRéfInfo(Sandre, 0) = "" Then Exit For
                            ' ==================================
                            If Variables.TabRéfInfo(Sandre, 0) = Variables.TabRéfPara(j, 7) Then
                                ' ==================================
                                ' Récupération du Code Sandre
                                Information = Variables.TabRéfInfo(Sandre, 1)
                                Exit For
                            End If
                        Next
                        NomColonne = "ColCodeEngin" : LigneParamètre = LigneParamètre & Information & ";" '   Chr(9)
                        ' ==================================
                        ' Code Remarque 55
                        ' ==================================
                        ' Recherche du Code Sandre correspondant
                        Information = ""
                        For Sandre = CodeRemarque To 5000
                            ' ==================================
                            ' Sortie de la boucle si fin de tableau
                            If Variables.TabRéfInfo(Sandre, 0) = "" Then Exit For
                            ' ==================================
                            If Variables.TabRéfInfo(Sandre, 0) = Variables.TabRéfPara(j, 20) Then
                                ' ==================================
                                ' Récupération du Code Sandre
                                Information = Variables.TabRéfInfo(Sandre, 1)
                                Exit For
                            End If
                        Next
                        NomColonne = "ColCodeRemarque" : LigneParamètre = LigneParamètre & Information & ";" '   Chr(9)
                        ' ==================================
                        ' Code Sandre Précision 56
                        ' ==================================
                        ' Recherche du Code Sandre correspondant
                        '                       Information = ""
                        '                       For Sandre = Précision To 5000
                        ' ==================================
                        ' Sortie de la boucle si fin de tableau
                        '                       If Variables.TabRéfInfo(Sandre, 0) = "" Then Exit For
                        ' ==================================
                        '                       If Variables.TabRéfInfo(Sandre, 0) = Variables.TabRéfPara(j, 11) Then
                        ' ==================================
                        ' Récupération du Code Sandre
                        '                       Information = Variables.TabRéfInfo(Sandre, 1)
                        '                       Exit For
                        '                   End If
                        '               Next
                        NomColonne = "ColCodePrécision" : LigneParamètre = LigneParamètre & Variables.TabRéfPara(j, 11) & ";" '   Chr(9)
                        ' ==================================
                        ' Code Sandre Type Précision 57
                        ' ==================================
                        ' Recherche du Code Sandre correspondant
                        Information = ""
                        For Sandre = TypePrécision To 5000
                            ' ==================================
                            ' Sortie de la boucle si fin de tableau
                            If Variables.TabRéfInfo(Sandre, 0) = "" Then Exit For
                            ' ==================================
                            If Variables.TabRéfInfo(Sandre, 0) = Variables.TabRéfPara(j, 12) Then
                                ' ==================================
                                ' Récupération du Code Sandre
                                Information = Variables.TabRéfInfo(Sandre, 1)
                                Exit For
                            End If
                        Next
                        NomColonne = "ColCodeTypePrécision" : LigneParamètre = LigneParamètre & Information & ";" '   Chr(9)
                        ' ==================================
                        ' Commentaire Mesure 58
                        NomColonne = "ColCommentaire" : LigneParamètre = LigneParamètre & Variables.TabRéfPara(j, 19) ' & ";" '   Chr(9)

                        ' ==================================
                        ' Sauvegarde des informations collationnées
                        NomFichier.WriteLine(LigneParamètre)

                    End If
                Next
            Next
        Next
        ' ==================================
        ' Fermeture du fichier
        NomFichier.Close()
        ' ==================================
        ' Fermeture de l'application
        Fermeture()

        Exit Sub
        '=========================================================================================================
TraitErreur1:
        If Err.Number <> 0 Then
            MsgBox(Err.Number & "    " & Err.Description, MsgBoxStyle.Exclamation, " QUADRISPEL - Erreur de sauvegarde")
        End If
    End Sub

    ' ==============================================================================================================================================
    ' Procédure de datage du texte
    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        '==============================================
        ' Changement visuel de sélection de bouton
        Button3.Select()
        '==============================================
        ' Traitement de la date du jour
        '        TextBox1.Text = Strings.Right(Today, 2) & "-" & Strings.Mid(Today, 4, 2) & "-" & Strings.Left(Today, 2) & " " & TextBox1.Text

        ' ==================================
        ' Vérification si une date de Passage a été inscrite
        If Passage.TextBox11.Text = "" Then
            MsgBox("Vous n'avez pas défini de Date de Passage !", MsgBoxStyle.Exclamation, " QUADRISPEL - Date de Passage")
            Exit Sub
        End If
        '==============================================
        ' Suppression de la date si elle existe
        If Strings.Mid(TextBox1.Text, 5, 1) = "-" And Strings.Mid(TextBox1.Text, 8, 1) = "-" Then
            If Strings.Mid(TextBox1.Text, 11, 1) = " " Then
                TextBox1.Text = Strings.Right(TextBox1.Text, Len(TextBox1.Text) - 11)
            Else
                TextBox1.Text = Strings.Right(TextBox1.Text, Len(TextBox1.Text) - 10)
            End If
        End If
        '==============================================
        ' Traitement de la date du Passage
        TextBox1.Text = Strings.Right(Passage.TextBox11.Text, 4) & "-" & Strings.Mid(Passage.TextBox11.Text, 4, 2) & "-" & Strings.Left(Passage.TextBox11.Text, 2) & " " & TextBox1.Text
    End Sub

    ' ==============================================================================================================================================
    ' Procédure d'effacement du texte
    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        '==============================================
        ' Changement visuel de sélection de bouton
        Button3.Select()
        '==============================================
        TextBox1.Text = ""
    End Sub

    ' ==============================================================================================================================================
    ' Procédure de fermeture
    Private Sub Fermeture()
        ' ============================================
        ' Essai pour éviter qu'une application ne passe devant la feuille ayant demandé celle-ci
        Form1.Activate()
        ' ============================================
        Me.Visible = False
    End Sub

    ' ==============================================================================================================================================
    ' Procédure d'ouverture par Fondu
    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        Me.Opacity = Me.Opacity + 0.06
        If Me.Opacity >= 0.95 Then
            Timer1.Enabled = False
            Me.Opacity = 1
        End If
    End Sub

    '=============================================================================================================================================
    ' Traitement du repositionnement automatique de la feuille quand on l'a déplacée
    Private Sub Sauve_Ifremer_MouseEnter(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.MouseEnter
        Me.CenterToScreen()
    End Sub

    '=============================================================================================================================================
    ' Traitement de réduction des fenêtres
    Private Sub Sauve_Ifremer_Move(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Move
        ' ============================================
        ' Passage de la feuille principale transparente quand on réduit cette feuille
        If Me.WindowState = 1 Then
            Form1.Opacity = 0
        End If
        ' ============================================
        ' Passage de la feuille principale opaque quand on rétablit cette feuille
        If Me.WindowState = 0 Then
            Form1.Opacity = 1
        End If
    End Sub

End Class